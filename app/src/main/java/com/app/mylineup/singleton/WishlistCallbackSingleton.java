package com.app.mylineup.singleton;

import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.CallbackWishList;

import java.util.HashMap;
import java.util.Map;

public class WishlistCallbackSingleton {

    public static Map<String, CallbackWishList> callbackList;

    public synchronized static void addListener(String tag, CallbackWishList callbackLittleOne) {
        if (callbackList == null)
            callbackList = new HashMap<>();

        callbackList.put(tag, callbackLittleOne);
    }

    public synchronized static CallbackWishList getListener(String tag, boolean removeAfterUse) {
        CallbackWishList callbackLittleOne = null;
        if (callbackList != null && callbackList.size() != 0) {
            if (callbackList.containsKey(tag)) {
                callbackLittleOne = callbackList.get(tag);
                if (removeAfterUse) {
                    callbackList.remove(tag);
                }
            }
        }
        return callbackLittleOne;
    }

    public synchronized static void notifyAllListeners() {
        if (callbackList == null || callbackList.size() == 0)
            return;
        for (Map.Entry<String, CallbackWishList> entry : callbackList.entrySet()) {
            entry.getValue().changeLittleOne();
        }
    }

    public synchronized static void removeListener(String tag) {
        if (callbackList == null || callbackList.size() == 0)
            return;
        if (callbackList.containsKey(tag))
            callbackList.remove(tag);
    }
}
