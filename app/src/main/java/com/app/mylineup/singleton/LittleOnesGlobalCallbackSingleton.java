package com.app.mylineup.singleton;

import com.app.mylineup.interfaces.CallbackLittleOne;

import java.util.HashMap;
import java.util.Map;

public class LittleOnesGlobalCallbackSingleton {
    public static Map<String, CallbackLittleOne> callbackList;

    public synchronized static void addListener(String tag, CallbackLittleOne callbackLittleOne) {
        if (callbackList == null)
            callbackList = new HashMap<>();

        callbackList.put(tag, callbackLittleOne);
    }

    public synchronized static CallbackLittleOne getListener(String tag, boolean removeAfterUse) {
        CallbackLittleOne callbackLittleOne = null;
        if (callbackList != null && callbackList.size() != 0) {
            if (callbackList.containsKey(tag)) {
                callbackLittleOne = callbackList.get(tag);
                if (removeAfterUse) {
                    callbackList.remove(tag);
                }
            }
        }
        return callbackLittleOne;
    }

    public synchronized static void notifyAllListeners() {
        for (Map.Entry<String, CallbackLittleOne> entry : callbackList.entrySet()) {
            entry.getValue().deleteLittleOne();
        }
    }

    public synchronized static void removeListener(String tag) {
        if (callbackList == null || callbackList.size() == 0)
            return;
        if (callbackList.containsKey(tag))
            callbackList.remove(tag);
    }
}
