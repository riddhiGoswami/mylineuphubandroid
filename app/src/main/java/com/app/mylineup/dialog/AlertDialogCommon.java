package com.app.mylineup.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.app.mylineup.R;

public class AlertDialogCommon extends AlertDialog.Builder
{
    private Context mContext;
    private CallBackClickListener callBackClickListener;
    private AlertDialog dialog;

    public AlertDialogCommon(Context context) {
        super(context);
        this.mContext=context;
    }

    public AlertDialogCommon(Context context, int themeResId) {
        super(context, themeResId);
        this.mContext=context;
    }

    public void showDialog(String title,String msg,String positiveText,String negativeText,boolean isPositiveDisplay,boolean isNegativeDisplay,CallBackClickListener callBackClickListener)
    {
        this.callBackClickListener = callBackClickListener;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.CustomDialogTheme);
        builder.setCancelable(false);

        builder.setTitle(title);
        builder.setMessage(msg);

        if(isPositiveDisplay)
        {
            builder.setPositiveButton(positiveText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (callBackClickListener!=null)
                            {
                                callBackClickListener.OnDialogPositiveBtn();
                            }
                        }
                    });

        }

        if(isNegativeDisplay)
        {
            builder.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            if (callBackClickListener!=null)
                            {
                                callBackClickListener.OnDialogNegativeBtn();
                            }
                        }
                    });
        }

        dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {

                //dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(mContext.getResources().getColor(R.color.colorBlack));
                //dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mContext.getResources().getColor(R.color.colorBlack));

            }
        });

        dialog.show();
    }

    public void showDialog(String msg,CallBackClickListener callBackClickListener)
    {
        this.callBackClickListener = callBackClickListener;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.CustomDialogTheme);
        builder.setCancelable(false);

        builder.setTitle(mContext.getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (callBackClickListener!=null)
                {
                    callBackClickListener.OnDialogPositiveBtn();
                }
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    public void showDialogWithContext(Context context, String msg,CallBackClickListener callBackClickListener)
    {
        this.callBackClickListener = callBackClickListener;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.CustomDialogTheme);
        builder.setCancelable(false);

        builder.setTitle(mContext.getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
                if (callBackClickListener!=null)
                {
                    callBackClickListener.OnDialogPositiveBtn();
                }
            }
        });

        dialog = builder.create();
        dialog.show();
    }


    ///for small errors
    public void showDialog(String msg)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.CustomDialogTheme);
        builder.setCancelable(false);

        builder.setTitle(mContext.getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.ok),null);

        dialog = builder.create();
        dialog.show();
    }

    public void hideDialog()
    {
        if(dialog != null && dialog.isShowing())
        {
            dialog.dismiss();
        }
    }

    public interface CallBackClickListener
    {
        void OnDialogPositiveBtn();
        void OnDialogNegativeBtn();
    }
}
