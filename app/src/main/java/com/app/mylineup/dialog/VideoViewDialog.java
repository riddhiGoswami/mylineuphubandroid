package com.app.mylineup.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateWishListActivity;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import java.io.Serializable;

import de.hdodenhof.circleimageview.CircleImageView;

public class VideoViewDialog extends DialogFragment/*YouTubeBaseActivity*/ implements YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_REQUEST = 1;
    private AppCompatImageView tv_video_skip;
    private int videoType;
    private View view;
    private YouTubePlayerSupportFragment youtubePlayerFragment;
    private OnSkipClickListener onSkipClickListener;
    private YouTubePlayer localYouTubePlayer;
    private boolean isSkipDisplay = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_video_view, container, false);

        prepareYoutubePlayer(view);

        return view;
    }

    public VideoViewDialog() {

    }

    public VideoViewDialog(int videoType, OnSkipClickListener onSkipClickListener, boolean isSkipDisplay) {
        this.videoType = videoType;
        this.onSkipClickListener = onSkipClickListener;
        this.isSkipDisplay = isSkipDisplay;
    }

    private void prepareYoutubePlayer(View view) {
        setCancelable(false);

        tv_video_skip = view.findViewById(R.id.tv_video_skip);

        /*if (isSkipDisplay) {
            tv_video_skip.setText(getString(R.string.str_video_skip));
        } else {
            tv_video_skip.setText(getString(R.string.str_close));
        }*/

        youtubePlayerFragment = (YouTubePlayerSupportFragment)
                getActivity().getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);
        if (youtubePlayerFragment == null) {
            youtubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
            getChildFragmentManager().beginTransaction().add(R.id.youtube_fragment, youtubePlayerFragment).commit();
        }
        youtubePlayerFragment.initialize(getString(R.string.google_api_key), this);

        tv_video_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isSkipDisplay) {
                    onSkipClickListener.onSkipClick(videoType);
                } else {
                    onSkipClickListener.onSkipClick(6);
                }
                localYouTubePlayer.release();
                localYouTubePlayer = null;
                dismiss();

            }
        });

    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        localYouTubePlayer = youTubePlayer;


        youTubePlayer.setFullscreenControlFlags(0);
        if (!wasRestored) {
            if (videoType == 1) {
                //Add wishlist
                youTubePlayer.loadVideo("gO-ruM8Ndwk"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
            } else if (videoType == 2) {
                //Add connection
                youTubePlayer.loadVideo("DWtslLvOcO8");
            } else if (videoType == 3) {
                //Create little one
                youTubePlayer.loadVideo("cym9cG1U0as");
            } else if (videoType == 4) {
                //Complete your profile
                youTubePlayer.loadVideo("hGDCCx1ZaKo");
            } else if (videoType == 5) {
                //Mark wishlist as purchased
                youTubePlayer.loadVideo("mWuWw5U1UWo");
            }
            //  youTubePlayer.setFullscreen(true);
            youTubePlayer.setShowFullscreenButton(false);
            youTubePlayer.play();

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }


/*        Window window = getDialog().getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);*/


    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(getActivity(), RECOVERY_REQUEST).show();
        } else {
            String error = String.format("Player error", errorReason.toString());
            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }

    public YouTubePlayer hideFullScreen() {
        if (localYouTubePlayer != null) {
            localYouTubePlayer.setFullscreen(false);
        }
        return localYouTubePlayer;
    }

    public interface OnSkipClickListener {
        public void onSkipClick(int videoType);
    }
}
