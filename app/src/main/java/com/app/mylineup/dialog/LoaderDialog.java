package com.app.mylineup.dialog;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import com.airbnb.lottie.LottieAnimationView;
import com.app.mylineup.R;

public class LoaderDialog
{
    private Dialog dialog;
    private Context context;
    private LottieAnimationView lottie;

    private final int interval = 2500; // 1 Second
    private Handler handler = new Handler();
    Runnable runnable;

    View view;

    public LoaderDialog(Context context)
    {
        this.context = context;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_loader);

        lottie = dialog.findViewById(R.id.lottie);
        view = dialog.findViewById(R.id.view);
    }

    public void show()
    {
        dialog.show();
        lottie.playAnimation();

        int colorFrom = context.getResources().getColor(R.color.colorAccent);
        int colorTo = context.getResources().getColor(R.color.colorAccent);
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimation.setDuration(2500); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator)
            {
                view.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();

        runnable = new Runnable(){
            public void run() {
                int colorFrom = context.getResources().getColor(R.color.colorAccent);
                int colorTo = context.getResources().getColor(R.color.colorAccent);
                ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
                colorAnimation.setDuration(2500); // milliseconds
                colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                    @Override
                    public void onAnimationUpdate(ValueAnimator animator)
                    {
                        view.setBackgroundColor((int) animator.getAnimatedValue());
                    }

                });
                handler.postDelayed(runnable, interval);
                colorAnimation.start();
            }
        };
        handler.postDelayed(runnable, interval);
    }

    public void dismiss()
    {
        if(dialog != null && dialog.isShowing())
        {
            try {
                dialog.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }
            lottie.cancelAnimation();

        }
    }

}
