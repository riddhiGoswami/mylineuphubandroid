package com.app.mylineup.dialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.browser.trusted.sharing.ShareTarget;
import androidx.core.content.ContextCompat;

import com.app.mylineup.R;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.CustomPlayerUiController;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerUtils;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBar;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBarListener;
/*import com.google.android.youtube.player.YouTubePlayerView;*/

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;


public class CustomVideoDialog extends AppCompatActivity/*YouTubeBaseActivity*/ implements YouTubePlayer.OnInitializedListener, CustomPlayerUiController.OnSkipClickListener {

    private static final int RECOVERY_REQUEST = 1;
    private AppCompatImageView tv_video_skip;
    private int videoType;
    //private YouTubePlayerSupportFragment youtubePlayerFragment;
    private OnSkipClickListener onSkipClickListener;
    private YouTubePlayer localYouTubePlayer;
    private boolean isSkipDisplay = false;
    private YouTubePlayerView youTubePlayerView;
    private Intent passIntent;
    //private YouTubePlayerSeekBar youtube_player_seekbar;
    private boolean isAllowBackPress = false;
    private int requestCode = -1;
    private ProgressBar progressbar;
    private CustomPlayerUiController customPlayerUiController;
    private boolean isAfterSkip = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_video_main_layout);

        setTheme(R.style.AppDialogTheme);
        isAfterSkip = false;
        videoType = getIntent().getIntExtra("videoType", -1);
        isSkipDisplay = getIntent().getBooleanExtra("isSkipDisplay", false);
        //onSkipClickListener = getIntent().getParcelableExtra("onSkipListener");
        if (getIntent().hasExtra("passIntent")) {
            passIntent = getIntent().getParcelableExtra("passIntent");
        }
        if (getIntent().hasExtra("requestCode")) {
            requestCode = getIntent().getIntExtra("requestCode", -1);
        }
        youTubePlayerView = findViewById(R.id.youtube_view);
        progressbar = findViewById(R.id.progressbar);
        //youtube_player_seekbar = findViewById(R.id.youtube_player_seekbar);

        // youTubePlayerView.initialize(getString(R.string.google_api_key), this);

       /* tv_video_skip = findViewById(R.id.tv_video_skip);
        tv_video_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSkipDisplay) {
                    //isAllowBackPress = true;
                    finish();
                } else {
                    if (requestCode != -1) {
                        startActivityForResult(passIntent, requestCode);
                    }else{
                        startActivity(passIntent);
                    }
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
                //onBackPressed();
            }
        });*/

        getLifecycle().addObserver(youTubePlayerView);

        View customPlayerUi = youTubePlayerView.inflateCustomPlayerUi(R.layout.custom_video_dialog);

        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer) {
                //progressbar.setVisibility(View.GONE);
                customPlayerUiController = new CustomPlayerUiController(CustomVideoDialog.this,
                        customPlayerUi,
                        youTubePlayer,
                        youTubePlayerView,
                        videoType,
                        CustomVideoDialog.this::onSkipClick);
                youTubePlayer.addListener(customPlayerUiController);
                youTubePlayerView.addFullScreenListener(customPlayerUiController);
             /*  youTubePlayer.addListener(youtube_player_seekbar);

                youtube_player_seekbar.setYoutubePlayerSeekBarListener(new YouTubePlayerSeekBarListener() {
                    @Override
                    public void seekTo(float time) {
                        youTubePlayer.seekTo(time);
                    }
                });*/


                String videoId = "";
                if (videoType == 1) {
                    //Add wishlist
                    videoId = "gO-ruM8Ndwk";
                    //youTubePlayer.loadVideo("gO-ruM8Ndwk"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
                } else if (videoType == 2) {
                    //Add connection
                    videoId = "DWtslLvOcO8";
                    //youTubePlayer.loadVideo("DWtslLvOcO8");
                } else if (videoType == 3) {
                    //Create little one
                    videoId = "cym9cG1U0as";
                    // youTubePlayer.loadVideo("cym9cG1U0as");
                } else if (videoType == 4) {
                    //Complete your profile
                    videoId = "hGDCCx1ZaKo";
                    //  youTubePlayer.loadVideo("hGDCCx1ZaKo");
                } else if (videoType == 5) {
                    //Mark wishlist as purchased
                    videoId = "mWuWw5U1UWo";
                    //youTubePlayer.loadVideo("mWuWw5U1UWo");
                }
                //  youTubePlayer.setFullscreen(true);
                //     youTubePlayer.setShowFullscreenButton(false);


                YouTubePlayerUtils.loadOrCueVideo(
                        youTubePlayer, getLifecycle(),
                        videoId, 0f
                );

                youTubePlayer.play();

            }

            @Override
            public void onStateChange(@NotNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, @NotNull PlayerConstants.PlayerState state) {
                Log.w("state", "state => " + state);
                if (state == PlayerConstants.PlayerState.PLAYING || state == PlayerConstants.PlayerState.PAUSED || state == PlayerConstants.PlayerState.VIDEO_CUED) {
                    if (state == PlayerConstants.PlayerState.PLAYING) {
                        customPlayerUiController.playPauseButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pause_video));
                        customPlayerUiController.playPauseButton.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorBlack), android.graphics.PorterDuff.Mode.MULTIPLY);

                    } else if (state == PlayerConstants.PlayerState.PAUSED) {
                        customPlayerUiController.playPauseButton.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_play_video));
                        customPlayerUiController.playPauseButton.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorBlack), android.graphics.PorterDuff.Mode.MULTIPLY);
                    }
                    progressbar.setVisibility(View.GONE);
                } else if (state == PlayerConstants.PlayerState.BUFFERING) {
                    progressbar.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static void start(Context context, ShareTarget.Params params) {
        Intent starter = new Intent(context, CustomVideoDialog.class);
        starter.putExtra(ShareTarget.Params.class.getCanonicalName(), (Parcelable) params);
        context.startActivity(starter);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        localYouTubePlayer = youTubePlayer;


        youTubePlayer.setFullscreenControlFlags(0);
        if (!wasRestored) {
            if (videoType == 1) {
                //Add wishlist
                youTubePlayer.loadVideo("gO-ruM8Ndwk"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
            } else if (videoType == 2) {
                //Add connection
                youTubePlayer.loadVideo("DWtslLvOcO8");
            } else if (videoType == 3) {
                //Create little one
                youTubePlayer.loadVideo("cym9cG1U0as");
            } else if (videoType == 4) {
                //Complete your profile
                youTubePlayer.loadVideo("hGDCCx1ZaKo");
            } else if (videoType == 5) {
                //Mark wishlist as purchased
                youTubePlayer.loadVideo("mWuWw5U1UWo");
            }
            //  youTubePlayer.setFullscreen(true);
            youTubePlayer.setShowFullscreenButton(false);
            youTubePlayer.play();
        }

    }

    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format("Player error", errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    public YouTubePlayer hideFullScreen() {
        if (localYouTubePlayer != null) {
            localYouTubePlayer.setFullscreen(false);
        }
        return localYouTubePlayer;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("hereonActivityResult","111..."+requestCode);
        if (requestCode == 10 && resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();

        }
    }

    @Override
    public void onSkipClick(int videoType) {
        Log.w("isSkipDisplay", "" + isSkipDisplay);
        if (!isSkipDisplay) {
            //isAllowBackPress = true;
            finish();
            overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

        } else {
            isAfterSkip = true;
            Log.w("requestCode", "" + requestCode);
            if (requestCode != -1) {
                startActivityForResult(passIntent, requestCode);

            } else {
                startActivity(passIntent);
                finish();

            }

            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
    }

    public interface OnSkipClickListener {
        public void onSkipClick(int videoType);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w("isAfterSkip",""+isAfterSkip);
        if(isAfterSkip){
            this.finish();
        }
    }
}
