package com.app.mylineup.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;

import com.app.mylineup.R;
import com.app.mylineup.interfaces.CallbackClickListner;


public class ImageChooserDialog
{
    Context context;
    private CallbackClickListner callbackClickListner;

    public ImageChooserDialog(Context context, CallbackClickListner callbackClickListner, boolean isRemoveImage, boolean isRemoveOptionDisplay)
    {
        this.context = context;
        this.callbackClickListner = callbackClickListner;
        openDialog(isRemoveImage, isRemoveOptionDisplay);
    }

    private void openDialog(boolean isRemoveImage, boolean isRemoveOptionDisplay)
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        //builderSingle.setIcon(R.drawable.ic_logo_vector);
        builderSingle.setTitle("Choose Photo");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item);
        arrayAdapter.add("Camera");
        arrayAdapter.add("Gallery");
        /*arrayAdapter.add("No Image");
        */
        if(isRemoveOptionDisplay) {
            if (isRemoveImage) {
                arrayAdapter.add("Remove Photo");
            } else {
                arrayAdapter.add("No Image");
            }
        }

        builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String strName = arrayAdapter.getItem(which);
                if (strName.equalsIgnoreCase("Camera"))
                {
                    callbackClickListner.onClickListener(null, 0);
                    dialog.dismiss();
                } else if (strName.equalsIgnoreCase("Gallery"))
                {
                    callbackClickListner.onClickListener(null, 1);
                    dialog.dismiss();
                } else if (strName.equalsIgnoreCase("Remove Photo"))
                {
                    callbackClickListner.onClickListener(null, 2);
                    dialog.dismiss();
                } else if (strName.equalsIgnoreCase("No Image"))
                {
                    callbackClickListner.onClickListener(null, 3);
                    dialog.dismiss();
                }
            }
        });
        builderSingle.show();
    }
}
