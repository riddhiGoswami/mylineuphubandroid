package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowUpcomingOccasionBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.giftFeeds.UpcomingOccasion;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class UpComingOccasionAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<UpcomingOccasion> list;
    private String myId;

    public UpComingOccasionAdapter(Context context, List<UpcomingOccasion> occasionList, String myId) {
        list= occasionList;
        this.context = context;
        this.myId = myId;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowUpcomingOccasionBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.row_upcoming_occasion,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof MyViewHolder){
            MyViewHolder holder1 = (MyViewHolder)holder;
            if(!list.get(position).getImage().equalsIgnoreCase("")){
                Picasso.get().load(list.get(position).getImage()).placeholder(R.drawable.app_icon).into(holder1.binding.ivOccasionImg);
            }else {
                holder1.binding.ivOccasionImg.setImageResource(R.drawable.app_icon);
            }

            if(list.get(position).getName() != null && !list.get(position).getName().equalsIgnoreCase("")){
                holder1.binding.tvOccasionName.setText(list.get(position).getName());
            }
            if(list.get(position).getDate() != null && !list.get(position).getDate().equalsIgnoreCase("")){
                holder1.binding.tvDate.setText(list.get(position).getDate());
            }

            if(list.get(position).getType() != null && !list.get(position).getType().equalsIgnoreCase("")){
                if(list.get(position).getType().equalsIgnoreCase("special_day") || list.get(position).getType().equalsIgnoreCase("wishlist")){
                    holder1.binding.ivSmallIcon.setImageResource(R.drawable.ic_special_day);
                }
                else if(list.get(position).getType().equalsIgnoreCase("birthday") || list.get(position).getType().equalsIgnoreCase("little_one_birthday")){
                    holder1.binding.ivSmallIcon.setImageResource(R.drawable.ic_birthday);
                }
                else if(list.get(position).getType().equalsIgnoreCase("anniversary")){
                    holder1.binding.ivSmallIcon.setImageResource(R.drawable.ic_anniversary);
                }
                else {
                    holder1.binding.cardSmallIcon.setVisibility(View.GONE);
                }
            }else {
                holder1.binding.cardSmallIcon.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowUpcomingOccasionBinding binding;
        public MyViewHolder( RowUpcomingOccasionBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(view -> {

                Log.e("Upcoming Occassions", list.get(getAdapterPosition()).getType()
                        +"ID: "+list.get(getAdapterPosition()).getUserId());

                if(list.get(getAdapterPosition()).getType().equalsIgnoreCase("special_day")){
                    return;
                } // enable if need to filter again.
                else if (list.get(getAdapterPosition()).getType().equalsIgnoreCase("wishlist")) {
                    Intent intent = new Intent(context, WishlistActivity.class);

                    if(!list.get(getAdapterPosition()).getUserId().equalsIgnoreCase(myId))
                    {
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                        intent.putExtra(Constant.USER_ID, list.get(getAdapterPosition()).getUserId());
                    }
                    intent.putExtra(Constant.WISHLIST_ID,list.get(getAdapterPosition()).getWishlistId());
                    intent.putExtra(Constant.WISHLIST_NAME,list.get(getAdapterPosition()).getName());
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                }
                else /*if (list.get(getAdapterPosition()).getType().equalsIgnoreCase("birthday"))*/ {
                    if(list.get(getAdapterPosition()).getLittleOneId() != null){
                        Intent intent = new Intent(context, LittleOneProfileActivity.class);

                        intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                        intent.putExtra(Constant.LITTLE_ONE_ID, list.get(getAdapterPosition()).getLittleOneId());
                        intent.putExtra(Constant.USER_NAME, "");
                        //intent.putExtra(Constant.PARENT_ID, list.get(getAdapterPosition()).getUserId());

                        ((Activity) context).startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }else {
                        Intent intent = new Intent(context, UserProfileActivity.class);
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                        intent.putExtra(Constant.USER_ID, list.get(getAdapterPosition()).getUserId());
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                }
            });
        }
    }
}
