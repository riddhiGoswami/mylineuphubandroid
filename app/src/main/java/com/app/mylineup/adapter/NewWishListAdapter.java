package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.mylineup.R;
import com.app.mylineup.activity.RegisterActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowNewWishlistBinding;
import com.app.mylineup.databinding.RowProgressBinding;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.notifications.Wishlist;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NewWishListAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<Wishlist> wishlistList;

    private OnLoadmoreListener onLoadmoreListener;
    int VIEW_PROGRESS=0,VIEW_ITEM=1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    public NewWishListAdapter(Context context,List<Wishlist> wishlistList,RecyclerView recyclerView,OnLoadmoreListener onLoadmoreListener)
    {
        this.context = context;
        this.onLoadmoreListener = onLoadmoreListener;
        this.wishlistList = wishlistList;

        if(recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager)
        {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if(!loading && totalItemCount <= (lastVisibleItem + visibleThreshold))
                    {
                        if (onLoadmoreListener != null)
                        {
                            onLoadmoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if(viewType == VIEW_ITEM){
            RowNewWishlistBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_new_wishlist,parent,false);
            return new MyViewHolder(binding);
        }else {
            RowProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.row_progress,parent,false);
            return new ProgressViewHolder(binding);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder) {
            MyViewHolder holder1 = (MyViewHolder) holder;

            if(!wishlistList.get(position).getProfilePicture().equalsIgnoreCase("")){
                Picasso.get().load(wishlistList.get(position).getProfilePicture()).placeholder(R.drawable.app_icon).into(holder1.binding.ivImage);
            }else {
                holder1.binding.ivImage.setImageResource(R.drawable.app_icon);
            }

            if(wishlistList.get(position).getHeader() != null && !wishlistList.get(position).getHeader().equalsIgnoreCase("")){
                if (wishlistList.get(position).getHeader().contains("has created")) {
                    int index = wishlistList.get(position).getHeader().indexOf("has created");
                    SpannableString ss = new SpannableString(wishlistList.get(position).getHeader());
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss);
                }
                else {
                    holder1.binding.tvMsg.setText(wishlistList.get(position).getHeader());
                }
            }

            if(wishlistList.get(position).getWishlistName() != null && !wishlistList.get(position).getWishlistName().equalsIgnoreCase("")){
                holder1.binding.tvWishlistName.setText(wishlistList.get(position).getWishlistName());
            }

            if(wishlistList.get(position).getTotalItems() != null && !wishlistList.get(position).getTotalItems().equalsIgnoreCase("")){
                holder1.binding.tvItemCount.setText(wishlistList.get(position).getTotalItems()+" "+context.getString(R.string.lbl_items));
            }
        }
        else if(holder instanceof ProgressViewHolder){
            ProgressViewHolder progressViewHolder = (ProgressViewHolder)holder;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            progressViewHolder.progressBinding.progressBar.setLayoutParams(params);

        }
    }

    @Override
    public int getItemCount() {
        return wishlistList.size();
    }

    public void setLoaded(boolean loding) {
        loading = loding;
    }

    @Override
    public int getItemViewType(int position) {
        return wishlistList.get(position)==null?VIEW_PROGRESS:VIEW_ITEM;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        RowNewWishlistBinding binding;

        public MyViewHolder(RowNewWishlistBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, WishlistActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                intent.putExtra(Constant.USER_ID, wishlistList.get(getAdapterPosition()).getUserId());
                intent.putExtra(Constant.WISHLIST_ID,wishlistList.get(getAdapterPosition()).getWishlistId());
                intent.putExtra(Constant.WISHLIST_NAME,wishlistList.get(getAdapterPosition()).getWishlistName());
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder
    {
        RowProgressBinding progressBinding;
        public ProgressViewHolder(RowProgressBinding binding) {
            super(binding.getRoot());
            progressBinding = binding;
        }
    }
}
