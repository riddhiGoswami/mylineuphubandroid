package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.app.mylineup.R;
import com.app.mylineup.activity.BaseActivity;
import com.app.mylineup.activity.LoginActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.IntroBeen;

import java.util.List;

public class IntroSliderAdapter extends PagerAdapter {

    private Context context;
    private List<IntroBeen> list;
    private ViewPagerListener listener;

    public IntroSliderAdapter(Context activity, List<IntroBeen> list,ViewPagerListener listener) {
        context = activity;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        IntroBeen customPagerEnum = list.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_intro_slider, collection, false);
        ImageView imageView = layout.findViewById(R.id.ivIntro);
        MyTextView tvTitle = layout.findViewById(R.id.tvTitle);
        MyTextView tvDes = layout.findViewById(R.id.tvDes);
        MyTextView tvSkip = layout.findViewById(R.id.tvSkip);
        MyTextView tvNext = layout.findViewById(R.id.tvNext);
        ImageView ivArrow = layout.findViewById(R.id.ivArrow);
        LinearLayout llNext = layout.findViewById(R.id.llNext);
        imageView.setImageResource(customPagerEnum.getImg());
        tvTitle.setText(customPagerEnum.getTitle());
        tvDes.setText(customPagerEnum.getDes());

        if(position == (list.size()-1)) {
            tvNext.setText(context.getString(R.string.lbl_get_stated));
            //tvNext.setCompoundDrawables(null,null,null,null);
            ivArrow.setVisibility(View.GONE);
            tvSkip.setVisibility(View.GONE);
        }

        llNext.setOnClickListener(v -> {
            if(listener != null) {
                listener.onNext(position);
            }
        });

        tvSkip.setOnClickListener(v -> {
            Intent intent = new Intent(context, LoginActivity.class);
            intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
            ((Activity)context).startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            ((Activity)context).finish();
        });

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((RelativeLayout) view);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    public interface ViewPagerListener{
        void onNext(int pos);
    }
}
