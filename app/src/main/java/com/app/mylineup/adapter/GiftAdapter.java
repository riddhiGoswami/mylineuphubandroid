package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowGiftfeedsBinding;
import com.app.mylineup.databinding.RowProgressBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.interfaces.CallbackWishListItem;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.interfaces.WishListItemSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.DateUtil;
import com.app.mylineup.pojo.giftFeeds.Feed;
import com.app.mylineup.pojo.giftFeeds.Item;
import com.app.mylineup.pojo.userProfile.CheckConnected;
import com.app.mylineup.view.DepthPageTransformer;
import com.app.mylineup.web_services.PrettyPrinter;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.mylineup.other.DateUtil.DATE_FORMAT_Y_M_D;
import static com.app.mylineup.other.DateUtil.isDateInThePast;

public class GiftAdapter extends RecyclerView.Adapter {

    private Context context;
    private Long startClickTime;
    private List<Feed> list;
    private long MAX_CLICK_DURATION = 100;
    private AlertDialogCommon adc;

    private OnLoadmoreListener onLoadmoreListener;
    int VIEW_PROGRESS = 0, VIEW_ITEM = 1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnItemClickGiftListener onItemClickGiftListener;


    public GiftAdapter(Context context, List<Feed> feedList, OnLoadmoreListener onLoadmoreListener, RecyclerView recyclerView, OnItemClickGiftListener onItemClickGiftListener) {
        this.context = context;
        list = feedList;
        this.adc = new AlertDialogCommon((Activity) context);
        this.onItemClickGiftListener = onItemClickGiftListener;
        this.onLoadmoreListener = onLoadmoreListener;

        if (recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadmoreListener != null) {
                            onLoadmoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            RowGiftfeedsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_giftfeeds, parent, false);
            return new MyViewHolder(binding);
        } else {
            RowProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_progress, parent, false);
            return new ProgressViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            MyViewHolder holder1 = (MyViewHolder) holder;

            if (!list.get(position).getProfilePicture().equalsIgnoreCase("")) {
                Picasso.get().load(list.get(position).getProfilePicture()).placeholder(R.drawable.app_icon).into(holder1.binding.ivUsrImg);
            } else {
                holder1.binding.ivUsrImg.setImageResource(R.drawable.app_icon);
            }

            if (list.get(position).getName() != null && !list.get(position).getName().equalsIgnoreCase("")) {
                holder1.binding.tvUserName.setText(list.get(position).getName());
            }
            if (list.get(position).getPostedAt() != null && !list.get(position).getPostedAt().equalsIgnoreCase("")) {
                holder1.binding.tvPostedTime.setText(list.get(position).getPostedAt());
            }

            if (list.get(position).getFeedText() != null && !list.get(position).getFeedText().equalsIgnoreCase("")) {
                holder1.binding.tvFeedText.setText(list.get(position).getFeedText());
                if (list.get(position).getFeedText().contains("wishlist on")) {
                    int index = list.get(position).getFeedText().indexOf("wishlist on");
                    SpannableString ss = new SpannableString(list.get(position).getFeedText());
                    ss.setSpan(new StyleSpan(Typeface.BOLD), index + 11, list.get(position).getFeedText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvFeedText.setText(ss);
                } else {
                    holder1.binding.tvFeedText.setText(list.get(position).getFeedText());
                }
            }

            ImageAdapter imageAdapter = new ImageAdapter(context, list.get(position).getItems(), list.get(position).getLittleOneId(), list.get(position).getLittleOneName());
            holder1.binding.viewpager.setAdapter(imageAdapter);
            //holder1.binding.circleIndicator.setViewPager(holder1.binding.viewpager);
            holder1.binding.circleIndicator.attachTo(holder1.binding.viewpager);
            holder1.binding.viewpager.setPageTransformer(true, new DepthPageTransformer());
        } else if (holder instanceof ProgressViewHolder) {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            progressViewHolder.progressBinding.progressBar.setLayoutParams(params);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setLoaded(boolean loding) {
        loading = loding;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_PROGRESS : VIEW_ITEM;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        private RowGiftfeedsBinding binding;

        public MyViewHolder(RowGiftfeedsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.llGiftfeedWishlist.setOnClickListener(v -> {

                WishListItemSingleton.get(context, new CallbackWishListItem() {

                    @Override
                    public void purchaseWishListItem(boolean isPurchased, String itemId) {
                        if (isPurchased) {

                            List<Item> list1 = list.get(getAdapterPosition()).getItems();
                            for(int i = 0;i<list1.size();i++){
                                if(list1.get(i).getItemId().equalsIgnoreCase(itemId)){
                                    list1.get(i).setStatus("1");
                                    break;
                                }
                            }

                            ImageAdapter imageAdapter = new ImageAdapter(context, list1, list.get(getAdapterPosition()).getLittleOneId(), list.get(getAdapterPosition()).getLittleOneName());
                            binding.viewpager.setAdapter(imageAdapter);
                            //holder1.binding.circleIndicator.setViewPager(holder1.binding.viewpager);
                            binding.circleIndicator.attachTo(binding.viewpager);
                            binding.viewpager.setPageTransformer(true, new DepthPageTransformer());
                        }
                    }
                });

//                Intent intent = new Intent(context, UserProfileActivity.class);
//                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
//                intent.putExtra(Constant.USER_ID,list.get(getAdapterPosition()).getUserId());
//                ((Activity)context).startActivity(intent);
//                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);


               /* if (list.get(getAdapterPosition()).getEventDate()!=null
                        && !list.get(getAdapterPosition()).getEventDate().isEmpty()
                        && !isDateInThePast(list.get(getAdapterPosition()).getEventDate(), DATE_FORMAT_Y_M_D)) {*/
                Intent intent = new Intent(context, WishlistActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                intent.putExtra(Constant.USER_ID, list.get(getAdapterPosition()).getUserId());
                intent.putExtra(Constant.WISHLIST_ID, list.get(getAdapterPosition()).getWishListId());
                intent.putExtra(Constant.WISHLIST_NAME, list.get(getAdapterPosition()).getWishListName());
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                /*}
                else {
                    adc.showDialog("This event has expired.");
                }*/
            });
            binding.llGiftfeedProfile.setOnClickListener(v -> {
                onItemClickGiftListener.onClick(list.get(getAdapterPosition()).getUserId());
            });

           /* binding.llGiftfeedProfile.setOnClickListener(v -> {
                Intent intent = new Intent(context, UserProfileActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                intent.putExtra(Constant.USER_ID, list.get(getAdapterPosition()).getUserId());
                ((Activity) context).startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });*/

            /*binding.viewpager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            startClickTime = Calendar.getInstance().getTimeInMillis();
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            if(clickDuration < MAX_CLICK_DURATION) {
                                //click event has occurred
                                Intent intent = new Intent(context, UserProfileActivity.class);
                                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                                ((Activity)context).startActivity(intent);
                                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                            }
                        }
                    }
                    return false;
                }
            });*/
        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        RowProgressBinding progressBinding;

        public ProgressViewHolder(RowProgressBinding binding) {
            super(binding.getRoot());
            progressBinding = binding;
        }
    }

}
