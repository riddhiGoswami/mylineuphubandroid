package com.app.mylineup.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.mylineup.pojo.littleOnes.LittleOne;

import java.util.List;

public class SpinnerItemAdapter extends ArrayAdapter<String> {

    private List<String> list;
    private Context context;

    public SpinnerItemAdapter(@NonNull Context context, int textViewResourceId, @NonNull List<String> list) {
        super(context, textViewResourceId, list);

        this.list = list;
        this.context = context;
    }


    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "font/Ubuntu-Regular.ttf");
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(face);
        /*
        View v =super.getDropDownView(position, convertView, parent);

        Typeface externalFont=Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTCom-Lt.ttf");
        ((TextView) v).setTypeface(externalFont);
        v.setBackgroundColor(Color.GREEN);

        return getCustomView(position, convertView, parent);*/
        return view;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View row;
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "font/Ubuntu-Regular.ttf");
        if(position == (list.size()-1))
        {
            row=inflater.inflate(/*R.layout.item_spinner_dropdown_last*/android.R.layout.simple_spinner_dropdown_item, parent, false);
            CheckedTextView tvName=row.findViewById(android.R.id.text1);
            tvName.setTypeface(face);
            tvName.setText(list.get(position).toString());
        }
        else
        {
            row=inflater.inflate(/*R.layout.item_spinner_dropdown*/android.R.layout.simple_spinner_dropdown_item, parent, false);
            /*MyTextView*/
            CheckedTextView tvName=row.findViewById(android.R.id.text1);
            tvName.setTypeface(face);
            tvName.setText(list.get(position).toString());
        }

        return row;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View row;
        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "font/Ubuntu-Regular.ttf");
        if (position == (list.size() - 1)) {
            row = inflater.inflate(/*R.layout.item_spinner_dropdown_last*/android.R.layout.simple_spinner_dropdown_item, parent, false);
            CheckedTextView tvName = row.findViewById(android.R.id.text1);
            tvName.setTypeface(face);
            tvName.setText(list.get(position).toString());
        } else {
            row = inflater.inflate(/*R.layout.item_spinner_dropdown*/android.R.layout.simple_spinner_dropdown_item, parent, false);
            /*MyTextView*/
            CheckedTextView tvName = row.findViewById(android.R.id.text1);
            tvName.setTypeface(face);
            tvName.setText(list.get(position).toString());
        }

        return row;
    }
}
