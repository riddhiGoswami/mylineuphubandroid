package com.app.mylineup.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.databinding.RowLittleonesItemBinding;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.userProfile.MyLittleOne;
import com.app.mylineup.view.DepthPageTransformer;
import com.bumptech.glide.Glide;

import java.util.List;

public class LittleOnesAdapter extends RecyclerView.Adapter<LittleOnesAdapter.LittleOnesViewHolder>
{
    private List<MyLittleOne> myLittleOnes;
    private Context context;
    private String userName;
    private CallbackLittleOne callbackLittleOne;

    public LittleOnesAdapter(List<MyLittleOne> myLittleOnes, Context context, String userName)
    {
        this.myLittleOnes = myLittleOnes;
        this.context = context;
        this.userName = userName;
    }

    public LittleOnesAdapter(List<MyLittleOne> myLittleOnes, Context context, String userName, CallbackLittleOne callbackLittleOne)
    {
        this.myLittleOnes = myLittleOnes;
        this.context = context;
        this.userName = userName;
        this.callbackLittleOne = callbackLittleOne;
    }

    @NonNull
    @Override
    public LittleOnesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowLittleonesItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.row_littleones_item,
                parent, false
        );
        return new LittleOnesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LittleOnesViewHolder holder, int position)
    {
        String profilePicture = myLittleOnes.get(position).getProfilePicture();
        String age = myLittleOnes.get(position).getAge();
        String name = myLittleOnes.get(position).getName();

        if (!TextUtils.isEmpty(profilePicture))
        {
            Glide.with(context).load(profilePicture).into(holder.binding.civPhoto);
        }

        if (!TextUtils.isEmpty(age))
        {
            holder.binding.tvAge.setText(age);
        }

        if (!TextUtils.isEmpty(name))
        {
            holder.binding.tvName.setText(name);
        }


    }

    @Override
    public int getItemCount()
    {
        return myLittleOnes.size();
    }

    class LittleOnesViewHolder extends RecyclerView.ViewHolder
    {

        private RowLittleonesItemBinding binding;

        public LittleOnesViewHolder(@NonNull RowLittleonesItemBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v ->
            {
                LittleOneSingleton.get(context, new CallbackLittleOne() {
                    @Override
                    public void editLittleOne(String firstName, String lastName) {
                        binding.tvName.setText(firstName + " " + lastName);
                    }

                    @Override
                    public void deleteLittleOne() {
                    }

                    @Override
                    public void hideLittleOne() {
                    }

                    @Override
                    public void addLittleOne(LittleOne littleOne) {

                    }
                });

                Intent intent = new Intent(context, LittleOneProfileActivity.class);

                intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                intent.putExtra(Constant.LITTLE_ONE_ID, myLittleOnes.get(getAdapterPosition()).getId());
                intent.putExtra(Constant.USER_NAME, "");
                intent.putExtra(Constant.LITTLE_ONE_CALLBACK, callbackLittleOne);

                /*intent.putExtra(Constant.WISHLIST_ID, list.get(getAdapterPosition()).getWishlistId());
                intent.putExtra(Constant.WISHLIST_NAME, list.get(getAdapterPosition()).getWishlistName());
                */
                context.startActivity(intent);
                ((android.app.Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });

        }


    }
}
