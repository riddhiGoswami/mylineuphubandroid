package com.app.mylineup.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowActivityItemBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.userProfile.Activity;
import com.app.mylineup.view.DepthPageTransformer;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;


public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.MyViewHolder>
{

    private String TAG = "PetAdapter";
    //private List<WishlistBeen> wishlistBeenList;
    private Context mContext;
    private List<Activity> list;
    public List<com.app.mylineup.pojo.littleOnes.mylittleone.Activity> activityList;
    private boolean isMyAcc;

    public ActivityAdapter(Context context, List<Activity> list, boolean isMyAcc)
    {
        this.mContext = context;
        this.list = list;
        this.isMyAcc = isMyAcc;
    }

    public ActivityAdapter(List<com.app.mylineup.pojo.littleOnes.mylittleone.Activity> activityList, Context mContext, boolean isMyAcc)
    {
        this.mContext = mContext;
        this.activityList = activityList;
        this.isMyAcc = isMyAcc;
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowActivityItemBinding binding;

        public MyViewHolder(RowActivityItemBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v ->
            {
                Intent intent = new Intent(mContext, WishlistActivity.class);
                if (list != null && list.size()>0) {
                    if (!isMyAcc) {
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                        intent.putExtra(Constant.USER_ID, list.get(getAdapterPosition()).getUserId());
                    }
                    intent.putExtra(Constant.WISHLIST_ID, list.get(getAdapterPosition()).getWishlistId());
                    intent.putExtra(Constant.WISHLIST_NAME, list.get(getAdapterPosition()).getWishlistName());
                    mContext.startActivity(intent);
                    ((android.app.Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }else if (activityList != null && activityList.size()>0) {
                    if (!isMyAcc) {
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                        intent.putExtra(Constant.USER_ID, activityList.get(getAdapterPosition()).getUserId());
                    }
                    intent.putExtra(Constant.WISHLIST_ID, activityList.get(getAdapterPosition()).getWishlistId());
                    intent.putExtra(Constant.WISHLIST_NAME, activityList.get(getAdapterPosition()).getWishlistName());
                    mContext.startActivity(intent);
                    ((android.app.Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RowActivityItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_activity_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        ActivityWishlistImageAdapter wishlistImageAdapter = null;
        if (isMyAcc)
        {
            if (list != null)
            {
                Activity activity = list.get(position);

                if (activity.getProfilePicture() != null && !activity.getProfilePicture().equalsIgnoreCase(""))
                {
                    Picasso.get().load(activity.getProfilePicture()).into(holder.binding.ivUser);
                }

                if (activity.getName() != null && !activity.getName().equalsIgnoreCase(""))
                {
                    holder.binding.tvName.setText(activity.getName());
                }
                if (activity.getPostedAt() != null && !activity.getPostedAt().equalsIgnoreCase(""))
                {
                    holder.binding.tvPostedTime.setText(activity.getPostedAt());
                }
                if (activity.getFeedText() != null && !activity.getFeedText().equalsIgnoreCase(""))
                {
                    holder.binding.tvFeedText.setText(activity.getFeedText());
                }
                wishlistImageAdapter = new ActivityWishlistImageAdapter(mContext, list.get(position).getItems(), isMyAcc);
            }
        } else
        {
            if (list != null)
            {
                Activity activity = list.get(position);

                if (activity.getProfilePicture() != null && !activity.getProfilePicture().equalsIgnoreCase(""))
                {
                    Picasso.get().load(activity.getProfilePicture()).into(holder.binding.ivUser);
                }

                if (activity.getName() != null && !activity.getName().equalsIgnoreCase(""))
                {
                    holder.binding.tvName.setText(activity.getName());
                }
                if (activity.getPostedAt() != null && !activity.getPostedAt().equalsIgnoreCase(""))
                {
                    holder.binding.tvPostedTime.setText(activity.getPostedAt());
                }
                if (activity.getFeedText() != null && !activity.getFeedText().equalsIgnoreCase(""))
                {
                    holder.binding.tvFeedText.setText(activity.getFeedText());
                }
                wishlistImageAdapter = new ActivityWishlistImageAdapter(mContext, list.get(position).getItems(), isMyAcc);
            }

            if (activityList != null)
            {
                com.app.mylineup.pojo.littleOnes.mylittleone.Activity activity = activityList.get(position);

                if (activity.getProfilePicture() != null && !activity.getProfilePicture().equalsIgnoreCase(""))
                {
                    Picasso.get().load(activity.getProfilePicture()).into(holder.binding.ivUser);
                }

                if (activity.getName() != null && !activity.getName().equalsIgnoreCase(""))
                {
                    holder.binding.tvName.setText(activity.getName());
                }
                if (activity.getPostedAt() != null && !activity.getPostedAt().equalsIgnoreCase(""))
                {
                    holder.binding.tvPostedTime.setText(activity.getPostedAt());
                }
                if (activity.getFeedText() != null && !activity.getFeedText().equalsIgnoreCase(""))
                {
                    holder.binding.tvFeedText.setText(activity.getFeedText());
                }
                wishlistImageAdapter = new ActivityWishlistImageAdapter(activityList.get(position).getItems(),
                        mContext,
                        isMyAcc
                );
            }
        }

        if (wishlistImageAdapter != null)
        {
            holder.binding.viewPager.setAdapter(wishlistImageAdapter);
        }
        //holder.binding.circleIndicator.setViewPager(holder.binding.viewPager);
        holder.binding.circleIndicator.attachTo(holder.binding.viewPager);
        holder.binding.viewPager.setPageTransformer(true, new DepthPageTransformer());
    }

    @Override
    public int getItemCount()
    {
        if (isMyAcc)
        {
            if (list != null)
            {
                return list.size();
            }
        } else
        {
            if (list != null)
            {
                return list.size();
            }

            if (activityList != null)
            {
                return activityList.size();
            }
        }
        return 0;
    }
}