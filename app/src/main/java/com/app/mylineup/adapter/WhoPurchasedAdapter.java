package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.databinding.RowProgressBinding;
import com.app.mylineup.databinding.RowWhoPurchasedBinding;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.notifications.PurchasedList;
import com.squareup.picasso.Picasso;

import java.util.List;

public class WhoPurchasedAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<PurchasedList> purchasedLists;

    private OnLoadmoreListener onLoadmoreListener;
    int VIEW_PROGRESS=0,VIEW_ITEM=1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    public WhoPurchasedAdapter(Context context, List<PurchasedList> purchasedLists,RecyclerView recyclerView, OnLoadmoreListener onLoadmoreListener){
        this.context = context;
        this.onLoadmoreListener = onLoadmoreListener;
        this.purchasedLists = purchasedLists;

        if(recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager)
        {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if(!loading && totalItemCount <= (lastVisibleItem + visibleThreshold))
                    {
                        if (onLoadmoreListener != null)
                        {
                            onLoadmoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == VIEW_ITEM){
            RowWhoPurchasedBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_who_purchased,parent,false);
            return new MyViewHolder(binding);
        }else {
            RowProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.row_progress,parent,false);
            return new ProgressViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof MyViewHolder){
            MyViewHolder holder1 = (MyViewHolder) holder;

            if(!purchasedLists.get(position).getProfilePicture().equalsIgnoreCase("")){
                Picasso.get().load(purchasedLists.get(position).getProfilePicture()).placeholder(R.drawable.app_icon).into(holder1.binding.ivImage);
            }else {
                holder1.binding.ivImage.setImageResource(R.drawable.app_icon);
            }

            if(purchasedLists.get(position).getMessage() != null && !purchasedLists.get(position).getMessage().equalsIgnoreCase("")){
//                holder1.binding.tvMsg.setText(purchasedLists.get(position).getMessage());
                if (purchasedLists.get(position).getMessage().contains("purchased")) {
                    int index = purchasedLists.get(position).getMessage().indexOf("purchased");
                    SpannableString ss = new SpannableString(purchasedLists.get(position).getMessage());
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss);
                }
                else {
                    holder1.binding.tvMsg.setText(purchasedLists.get(position).getMessage());
                }
            }
        }
        else if(holder instanceof ProgressViewHolder){
            ProgressViewHolder progressViewHolder = (ProgressViewHolder)holder;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            progressViewHolder.progressBinding.progressBar.setLayoutParams(params);

        }
    }

    @Override
    public int getItemCount() {
        return purchasedLists.size();
    }

    public void setLoaded(boolean loding) {
        loading = loding;
    }

    @Override
    public int getItemViewType(int position) {
        return purchasedLists.get(position)==null?VIEW_PROGRESS:VIEW_ITEM;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        private RowWhoPurchasedBinding binding;
        public MyViewHolder(RowWhoPurchasedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, ItemDetailActivity.class);
                intent.putExtra(Constant.ITEM_ID,purchasedLists.get(getAdapterPosition()).getItemId());
                ((Activity)context).startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder
    {
        RowProgressBinding progressBinding;
        public ProgressViewHolder(RowProgressBinding binding) {
            super(binding.getRoot());
            progressBinding = binding;
        }
    }
}
