package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.application.Application;
import com.app.mylineup.databinding.RowProgressBinding;
import com.app.mylineup.databinding.RowWishlistBinding;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.wishlistItems.Item;
import com.app.mylineup.singleton.SelectedLittleOneId;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class WishListAdapter extends RecyclerView.Adapter {
    private OnLoadmoreListener onLoadmoreListener;
    int VIEW_PROGRESS = 0, VIEW_ITEM = 1, VIEW_HEADER = 2;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    private Context context;
    private String type;
    private List<Item> list;
    private boolean isMyAcc;
    private String littleOneId;

    public WishListAdapter(Context context, List<Item> itemList, String type, boolean isMyAcc, RecyclerView recyclerView, OnLoadmoreListener onLoadmoreListener) {
        list = itemList;
        this.context = context;
        this.type = type;
        this.onLoadmoreListener = onLoadmoreListener;
        this.isMyAcc = isMyAcc;

        if (recyclerView != null && recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadmoreListener != null) {
                            onLoadmoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            RowWishlistBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_wishlist, parent, false);
            return new MyViewHolder(binding);
        } else/* if (viewType == VIEW_HEADER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_wishlist_item, parent, false);
            return new AddWishlistHeader(itemView);
        }
        else*/ {
            RowProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_progress, parent, false);
            return new ProgressViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {
            MyViewHolder holder1 = (MyViewHolder) holder;

            if (list == null || list.size() == 0) return;


            if (type.equalsIgnoreCase(Constant.FROM_WISHLIST_DETAIL)) {
                if (!list.get(position).getBrand().equalsIgnoreCase("")) {
                    holder1.itemRowBinding.tvWishlistName.setText(list.get(position).getBrand());
                }
            } else {
                if (list.get(position).getWishlist().size() != 0 && !list.get(position).getWishlist().get(0).getName().equalsIgnoreCase("")) {
                    holder1.itemRowBinding.tvWishlistName.setText(
                            list.get(position).getWishlist().get(0).getName());// + " On "
                    //+ DateUtil.toDateString(list.get(position).getWishlist().get(0).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y)
                    //);
                }
            }


            if (!list.get(position).getImage().equalsIgnoreCase("")) {
                Picasso.get().load(list.get(position).getImage()).placeholder(R.drawable.app_icon).into(holder1.itemRowBinding.ivItemImg);
            } else {
                Glide.with(context).load(Application.getInstance().getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).into(holder1.itemRowBinding.ivItemImg);
//                holder1.itemRowBinding.ivItemImg.setImageResource(R.drawable.app_icon);
            }

            if (!list.get(position).getName().equalsIgnoreCase("")) {
                holder1.itemRowBinding.tvItemName.setText(list.get(position).getName());
            }

            Log.w("isMyAcc", "" + isMyAcc);

            if (!isMyAcc) {
                if (list.get(position).getStatus() != null && !list.get(position).getStatus().isEmpty() && list.get(position).getStatus().equalsIgnoreCase("1")) {
                    /*if (!list.get(position).getPurchaseTag()) {*/
                    holder1.itemRowBinding.ivPurchased.setVisibility(View.VISIBLE);
                } else {
                    holder1.itemRowBinding.ivPurchased.setVisibility(View.GONE);
                    //}
                }
            } else {
                if (!list.get(position).getPurchaseTag()) {
                    holder1.itemRowBinding.ivPurchased.setVisibility(View.GONE);
                } else {
                    holder1.itemRowBinding.ivPurchased.setVisibility(View.VISIBLE);
                    //}
                }
            }
            /*if (list.get(position).getStatus() != null && !list.get(position).getStatus().isEmpty() && list.get(position).getStatus().equalsIgnoreCase("1")) {
             *//*if (!list.get(position).getPurchaseTag()) {*//*
                holder1.itemRowBinding.ivPurchased.setVisibility(View.VISIBLE);
            } else {
                holder1.itemRowBinding.ivPurchased.setVisibility(View.GONE);
                //}
            }*/

            if (!list.get(position).getPrice().equalsIgnoreCase("")) {
                String tempStr = list.get(position).getPrice().replace(",", "");
                //  String tempStrNew = tempStr.replace(".", "");
                try {
                    holder1.itemRowBinding.tvPrice.setText(context.getString(R.string.currency) + Utility.getFormattedAmount(Double.parseDouble(tempStr)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder1.itemRowBinding.tvPrice.setText(context.getString(R.string.currency) + "0");
            }


           /* if(!list.get(position).getPrice().equalsIgnoreCase("")){
//                holder1.itemRowBinding.tvPrice.setText(context.getString(R.string.currency)+list.get(position).getPrice());
                if (list.get(position).getPrice().contains(",")) {
                    holder1.itemRowBinding.tvPrice.setText(context.getString(R.string.currency)+list.get(position).getPrice());

                    binding.tvPrice.setText(getString(R.string.currency) + Utility.getFormattedAmount(Double.parseDouble(tempStr)));
                }
                else {
                    holder1.itemRowBinding.tvPrice.setText(
                            context.getString(R.string.currency)
                                    + Utility.getFormattedAmount(Double.parseDouble(list.get(position).getPrice())));
                }
            }else {
                holder1.itemRowBinding.tvPrice.setText(context.getString(R.string.currency)+"0");
            }*/

        }
    }

    @Override
    public int getItemCount() {
        return list.size(); //+1;
    }

    @Override
    public int getItemViewType(int position) {

//        if (position == 0) {
//            return VIEW_HEADER;
//        }
//        else if (position>0) {
//            return VIEW_ITEM;
//        }
//        else {
//            return VIEW_PROGRESS;
//        }
        return list.get(position) == null ? VIEW_PROGRESS : VIEW_ITEM;
    }

    public void setLoaded(boolean loding) {
        loading = loding;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        public RowWishlistBinding itemRowBinding;

        public MyViewHolder(RowWishlistBinding binding) {
            super(binding.getRoot());
            itemRowBinding = binding;

            itemRowBinding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, ItemDetailActivity.class);
                if (!isMyAcc) {
                    intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                }
                intent.putExtra(Constant.ITEM_ID, list.get(getAdapterPosition()).getId());
                intent.putExtra(Constant.LITTLE_ONE_ID, littleOneId);
                ((Activity) context).startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });
        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressViewHolder(RowProgressBinding binding) {
            super(binding.getRoot());
        }
    }

//    private class AddWishlistHeader extends RecyclerView.ViewHolder {
//
//        private LinearLayout llNewWishList;
//
//        public AddWishlistHeader(@NonNull View itemView) {
//            super(itemView);
//            llNewWishList = itemView.findViewById(R.id.llNewWishList);
//
//            llNewWishList.setOnClickListener(v -> {
//                Intent intent = new Intent(context, CreateWishListActivity.class);
//                context.startActivity(intent);
//                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//            });
//        }
//    }

    public void setList(List<Item> itemList) {

        list = itemList;
        notifyDataSetChanged();
        loading = false;
    }
}
