package com.app.mylineup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.RowLikeInterestBinding;
import com.app.mylineup.pojo.AboutMeBeen;
import com.app.mylineup.pojo.userProfile.LikesInterest;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LikeInterestAdapter extends RecyclerView.Adapter
{
    private Context context;
    //private List<AboutMeBeen> list;
    private List<LikesInterest> list;
    private List<com.app.mylineup.pojo.littleOnes.mylittleone.LikesInterest> likesInterestList;

    public LikeInterestAdapter(Context context, List<LikesInterest> interestList)
    {
        this.context = context;
        list = interestList;
    }

    public LikeInterestAdapter(List<com.app.mylineup.pojo.littleOnes.mylittleone.LikesInterest> likesInterestList, Context context)
    {
        this.context = context;
        this.likesInterestList = likesInterestList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowLikeInterestBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_like_interest, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
            if (list != null)
            {
                if (list.get(position).getImage() != null && !list.get(position).getImage().equalsIgnoreCase(""))
                {
                    Picasso.get().load(list.get(position).getImage()).into(holder1.binding.ivAbout);
                }
                //holder1.binding.ivAbout.setImageResource(list.get(position).getImage());

                if (list.get(position).getVariantItemValue() != null && !list.get(position).getVariantItemValue().equalsIgnoreCase(""))
                {
                    holder1.binding.tvSize.setText(list.get(position).getVariantItemValue());
                }
                if (list.get(position).getName() != null && !list.get(position).getName().equalsIgnoreCase(""))
                {
                    holder1.binding.tvName.setText(list.get(position).getName());
                }
            }

            if (likesInterestList != null)
            {
                if (likesInterestList.get(position).getImage() != null && !likesInterestList.get(position).getImage().equalsIgnoreCase(""))
                {
                    Picasso.get().load(likesInterestList.get(position).getImage()).into(holder1.binding.ivAbout);
                }
                //holder1.binding.ivAbout.setImageResource(list.get(position).getImage());

                if (likesInterestList.get(position).getVariantItemValue() != null && !likesInterestList.get(position).getVariantItemValue().equalsIgnoreCase(""))
                {
                    holder1.binding.tvSize.setText(likesInterestList.get(position).getVariantItemValue());
                }
                if (likesInterestList.get(position).getName() != null && !likesInterestList.get(position).getName().equalsIgnoreCase(""))
                {
                    holder1.binding.tvName.setText(likesInterestList.get(position).getName());
                }
            }
        }
    }

    @Override
    public int getItemCount()
    {
        if (list != null)
        {
            return list.size();
        } else if (likesInterestList != null)
        {
            return likesInterestList.size();
        }
        return 0;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowLikeInterestBinding binding;

        public MyViewHolder(RowLikeInterestBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
