package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.databinding.RowNewsBinding;
import com.app.mylineup.databinding.RowProgressBinding;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.notifications.Notification;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Notification> list;

    private OnLoadmoreListener onLoadmoreListener;
    int VIEW_PROGRESS = 0, VIEW_ITEM = 1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnItemClickGiftListener onItemClickGiftListener;
    //private OnNewsItemClickListener onNewsItemClickListener;

    public NewsAdapter(Context context, List<Notification> newsList, RecyclerView recyclerView, OnLoadmoreListener onLoadmoreListener, OnItemClickGiftListener onItemClickGiftListener/*,OnNewsItemClickListener onNewsItemClickListener*/) {
        this.context = context;
        this.onLoadmoreListener = onLoadmoreListener;
        this.list = newsList;
        this.onItemClickGiftListener = onItemClickGiftListener;
        //  this.onNewsItemClickListener = onNewsItemClickListener;

        if (recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadmoreListener != null) {
                            onLoadmoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            RowNewsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_news, parent, false);
            return new MyViewHolder(binding);
        } else if (viewType == VIEW_PROGRESS) {
            RowProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_progress, parent, false);
            return new ProgressViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            MyViewHolder holder1 = (MyViewHolder) holder;
            if (!list.get(position).getProfilePicture().equalsIgnoreCase("")) {
                Picasso.get().load(list.get(position).getProfilePicture()).placeholder(R.drawable.app_icon).into(holder1.binding.ivImage);
            } else {
                holder1.binding.ivImage.setImageResource(R.drawable.app_icon);
            }

            Log.w("getAdapterPosition())OnBind", "" + list.get(position).getType());
            if (list.get(position).getMessage() != null && !list.get(position).getMessage().equalsIgnoreCase("")) {
//                holder1.binding.tvMsg.setText(list.get(position).getMessage());
                String strMsg = list.get(position).getMessage();
                if (strMsg.contains("has accepted")) {
                    int index = strMsg.indexOf("has accepted");
                    SpannableString ss = new SpannableString(strMsg);
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss);
                }else if (strMsg.contains("accepted")) {
                    int index = strMsg.indexOf("accepted");
                    SpannableString ss = new SpannableString(strMsg);
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss);
                } else if (list.get(position).getType().equalsIgnoreCase("birthday")) {
                    Log.w("getMessage()Test", "" + strMsg);
                    int index = -1;
                    if (strMsg.contains("'s")) {
                        index = strMsg.indexOf("'s");

                    } else if (strMsg.contains("has")) {
                        index = strMsg.indexOf("has");
                    } else if (strMsg.contains("is")) {
                        index = strMsg.indexOf("is");
                    }
                    SpannableString ss = new SpannableString(strMsg);

                    if (index > 0) {
                        ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    holder1.binding.tvMsg.setText(ss);
                } else if (list.get(position).getType().equalsIgnoreCase("new_request")) {
                    Log.w("getMessage()Test", "" + strMsg);
                    int index = 0;
                    if (strMsg.contains("sent")) {
                        index = strMsg.indexOf("sent");
                    } else if (strMsg.contains("has")) {
                        index = strMsg.indexOf("has");
                    }

                    SpannableString ss = new SpannableString(strMsg);
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss);
                }else if (list.get(position).getType().equalsIgnoreCase("little_one_birthday")) {

                    Log.w("getMessage()TestLitleOnebday", "" + strMsg);

                    int index = strMsg.indexOf("\'s birthday");
                    SpannableString ss = new SpannableString(strMsg);
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss); //remove space between s s*/
                }else if (list.get(position).getType().equalsIgnoreCase("little_one_birthday_invite")) {

                    Log.w("getMessage()TestLitleOnebday", "" + strMsg);

                    int index = strMsg.indexOf("\'s birthday");
                    SpannableString ss = new SpannableString(strMsg);
                    ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder1.binding.tvMsg.setText(ss); //remove space between s s*/
                } else {
                    holder1.binding.tvMsg.setText(strMsg);
                }
            } else if (list.get(position).getType().equalsIgnoreCase("little_one_birthday")) {
                String strMsg = list.get(position).getMessage();
                Log.w("getMessage()TestLitleOnebday", "" + strMsg);

                int index = strMsg.indexOf("\'s birthday");
                SpannableString ss = new SpannableString(strMsg);
                ss.setSpan(new StyleSpan(Typeface.BOLD), 0, index, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder1.binding.tvMsg.setText(ss); //remove space between s s*/
            }

            if (list.get(position).getCreatedAt() != null && !list.get(position).getCreatedAt().equalsIgnoreCase("")) {
                holder1.binding.tvPostedTime.setText(list.get(position).getCreatedAt());
            }
        } else if (holder instanceof ProgressViewHolder) {
            ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            progressViewHolder.progressBinding.progressBar.setLayoutParams(params);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setLoaded(boolean loding) {
        loading = loding;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_PROGRESS : VIEW_ITEM;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {

        private RowNewsBinding binding;

        public MyViewHolder(RowNewsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                if (list.get(getAdapterPosition()).getType() != null && (list.get(getAdapterPosition()).getType().equalsIgnoreCase("request_accept") || list.get(getAdapterPosition()).getType().equalsIgnoreCase("birthday") || list.get(getAdapterPosition()).getType().equalsIgnoreCase("new_request"))) {

                    onItemClickGiftListener.onClick(list.get(getAdapterPosition()).getFriendId());
                    /*Intent intent = new Intent(context, UserProfileActivity.class);
                    intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                    intent.putExtra(Constant.USER_ID,list.get(getAdapterPosition()).getFriendId());
                    ((Activity)context).startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/
                } else if (list.get(getAdapterPosition()).getType().equalsIgnoreCase("little_one_birthday") || list.get(getAdapterPosition()).getType().equalsIgnoreCase("little_one_birthday_invite")) {
                    Intent intent = new Intent(context, LittleOneProfileActivity.class);

                    intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                    intent.putExtra(Constant.LITTLE_ONE_ID, list.get(getAdapterPosition()).getFriendId());
                    intent.putExtra(Constant.USER_NAME, "");

                    ((Activity) context).startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                    // onNewsItemClickListener.onClick(list.get(getAdapterPosition()).getFriendId(), list.get(getAdapterPosition()).getType());
                }
            });
        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder {
        RowProgressBinding progressBinding;

        public ProgressViewHolder(RowProgressBinding binding) {
            super(binding.getRoot());
            progressBinding = binding;
        }
    }
}
