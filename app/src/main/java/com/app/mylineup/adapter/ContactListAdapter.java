package com.app.mylineup.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ItemContactHeaderBinding;
import com.app.mylineup.databinding.ItemContactSectionBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.interfaces.Contact;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Contact> list;
    private OnActionListener actionListener;
    private AlertDialogCommon alertDialogCommon;

    int SECTION = 1;
    int HEADER = 2;

    //
    private int ACTION_PERFORM = Integer.MAX_VALUE; //0-Invite,1-Connect,2-Friend,3-Accept request,4-Reject request,5-Request sent

    public ContactListAdapter(Context context, List<Contact> contactList, OnActionListener actionListener, AlertDialogCommon alertDialogCommon) {
        list = contactList;
        this.context = context;
        this.actionListener = actionListener;
        this.alertDialogCommon = alertDialogCommon;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == SECTION) {
            ItemContactSectionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_contact_section,parent,false);
            return new MySection(binding);
        }
        else {
            ItemContactHeaderBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_contact_header,parent,false);
            return new MyHeader(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MyHeader) {
            MyHeader header = (MyHeader) holder;
            header.binding.tvHeader.setText(list.get(position).getHeader());
        }
        else if(holder instanceof MySection) {
            MySection section = (MySection) holder;
            section.binding.tvContactName.setText(list.get(position).getDisplayName());
            if(list.get(position).getImage() != null && !list.get(position).getImage().equalsIgnoreCase("")) {
                //section.binding.ivImgContact.setImageURI(Uri.parse(list.get(position).getImage()));
                Picasso.get().load(list.get(position).getImage()).into(section.binding.ivImgContact);
            }else {
                section.binding.ivImgContact.setImageResource(R.drawable.ic_user_icon);
            }

            //0 - Invite (Invite Button)
            //1 - Connect (Send Request Button)
            //2 - Friend (Show profile redirect - both are already friends)
            //3 - show Accept / Reject button (Request pending)
            //4 - Request Sent (Display popup - show text 'Request already send to this user')
            if(list.get(position).getType() != null && (list.get(position).getType().equalsIgnoreCase("0")
            || list.get(position).getType().equalsIgnoreCase("0.0"))){
                section.binding.tvAction.setText("Invite");
                section.binding.ivAcceptReq.setVisibility(View.GONE);
                section.binding.ivRejectReq.setVisibility(View.GONE);
                section.binding.tvAction.setVisibility(View.VISIBLE);
            } else if(list.get(position).getType() != null && (list.get(position).getType().equalsIgnoreCase("1")
                    || list.get(position).getType().equalsIgnoreCase("1.0"))){
                section.binding.tvAction.setText("Connect");
                section.binding.ivAcceptReq.setVisibility(View.GONE);
                section.binding.ivRejectReq.setVisibility(View.GONE);
                section.binding.tvAction.setVisibility(View.VISIBLE);
            }else if(list.get(position).getType() != null && (list.get(position).getType().equalsIgnoreCase("2")
            || list.get(position).getType().equalsIgnoreCase("2.0"))){
                section.binding.tvAction.setText("Friend");
                section.binding.ivAcceptReq.setVisibility(View.GONE);
                section.binding.ivRejectReq.setVisibility(View.GONE);
                section.binding.tvAction.setVisibility(View.VISIBLE);
            }else if(list.get(position).getType() != null && (list.get(position).getType().equalsIgnoreCase("3")
                    || list.get(position).getType().equalsIgnoreCase("3.0"))){
                section.binding.ivAcceptReq.setVisibility(View.VISIBLE);
                section.binding.ivRejectReq.setVisibility(View.VISIBLE);
                section.binding.tvAction.setVisibility(View.GONE);
            }else if(list.get(position).getType() != null && (list.get(position).getType().equalsIgnoreCase("4")
                    || list.get(position).getType().equalsIgnoreCase("4.0"))){
                section.binding.tvAction.setText("Request Sent");
                section.binding.ivAcceptReq.setVisibility(View.GONE);
                section.binding.ivRejectReq.setVisibility(View.GONE);
                section.binding.tvAction.setVisibility(View.VISIBLE);
            }

            section.binding.tvAction.setOnClickListener(v -> {
                if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Invite"))
                    ACTION_PERFORM = 0;
                else if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Connect"))
                    ACTION_PERFORM = 1;
                else if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Friend"))
                    ACTION_PERFORM = 2;
                else if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Request Sent")){
                    ACTION_PERFORM = 5;
                    alertDialogCommon.showDialog(context.getString(R.string.request_already_sent));
                    return;
                }
                actionListener.onPerformAction(ACTION_PERFORM,position,list.get(position));
            });

            section.binding.llContainer.setOnClickListener(v -> {
                if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Invite"))
                    return;
                else if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Connect"))
                    ACTION_PERFORM = 2;
                else if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Friend"))
                    ACTION_PERFORM = 2;
                else if(section.binding.tvAction.getText().toString().equalsIgnoreCase("Request Sent"))
                    ACTION_PERFORM = 5;
                actionListener.onPerformAction(ACTION_PERFORM,position,list.get(position));
            });

            section.binding.ivAcceptReq.setOnClickListener(v -> {ACTION_PERFORM = 3;actionListener.onPerformAction(ACTION_PERFORM,position,list.get(position));});
            section.binding.ivRejectReq.setOnClickListener(v -> {ACTION_PERFORM = 4;actionListener.onPerformAction(ACTION_PERFORM,position,list.get(position));});
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).isSection()?SECTION:HEADER;
    }

    private class MyHeader extends RecyclerView.ViewHolder {

        private ItemContactHeaderBinding binding;

        public MyHeader(ItemContactHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


    private class MySection extends RecyclerView.ViewHolder {

        private ItemContactSectionBinding binding;
        public MySection(ItemContactSectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnActionListener{
        void onPerformAction(int type,int position,Contact contact);
    }
}
