package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateLittleOneActivity;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.databinding.RowLittleoneBinding;
import com.app.mylineup.databinding.RowLittleoneProfileBinding;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.bumptech.glide.Glide;

import java.util.List;

public class LittleOneAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<LittleOne> list;
    private String from;
    private int selectedPos = Integer.MAX_VALUE;

    public static String FROM_EDIT_PROFILE = "from_edit_profile";
    public static String FROM_ACCOUNT_SETTING = "from_account_setting";

    public LittleOneAdapter(Context activity, List<LittleOne> littleOne, String whichScreen)
    {
        from = whichScreen;
        context = activity;
        list = littleOne;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (from.equalsIgnoreCase(FROM_ACCOUNT_SETTING))
        {
            RowLittleoneBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.row_littleone,
                    parent,
                    false
            );
            return new MyViewHolderEdit(binding);
        } else
        {
            RowLittleoneProfileBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_littleone_profile, parent, false);
            return new MyViewHolder(binding);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof MyViewHolderEdit)
        {
            MyViewHolderEdit holder1 = (MyViewHolderEdit) holder;
            if (list.get(position).getFirstName() != null)
            {
                holder1.binding.tvName.setText(list.get(position).getFirstName() + " " + list.get(position).getLastName());
            }

            String profilePicture = list.get(position).getProfilePicture();
            if (!TextUtils.isEmpty(profilePicture))
            {
                Glide.with(context).load(profilePicture).into(holder1.binding.ivAbout);
            }

            holder1.binding.ivEditLittleOneProfile.setOnClickListener(view ->
            {
                selectedPos = position;
                LittleOneSingleton.get(context, new CallbackLittleOne()
                {
                    @Override
                    public void editLittleOne(String firstName, String lastName)
                    {
                        list.get(selectedPos).setFirstName(firstName);
                        list.get(selectedPos).setLastName(lastName);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void deleteLittleOne()
                    {
                        list.remove(selectedPos);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void hideLittleOne()
                    {

                    }

                    @Override
                    public void addLittleOne(LittleOne littleOne)
                    {

                    }
                });

                Intent intent = new Intent(context, CreateLittleOneActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_EDIT_LITTLE_ONE);
                intent.putExtra(Constant.LITTLE_ONE_ID, list.get(position).getId());
                context.startActivity(intent);

                /*Intent videoIntent = new Intent(context, VideoViewDialog.class);
                videoIntent.putExtra("videoType",3);
                videoIntent.putExtra("intentToPass",intent);
                context.startActivity(videoIntent);*/

                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });

            holder1.binding.rootView.setOnClickListener(view ->
            {

                LittleOneSingleton.get(context, new CallbackLittleOne()
                {
                    @Override
                    public void editLittleOne(String firstName, String lastName)
                    {
                    }

                    @Override
                    public void deleteLittleOne()
                    {
                        list.remove(selectedPos);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void hideLittleOne()
                    {

                    }

                    @Override
                    public void addLittleOne(LittleOne littleOne)
                    {

                    }
                });

                selectedPos = position;

                Intent intent = new Intent(context, LittleOneProfileActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                intent.putExtra(Constant.LITTLE_ONE_ID, list.get(position).getId());
                intent.putExtra(Constant.USER_NAME, "");
                context.startActivity(intent);

                /*Intent videoIntent = new Intent(context, VideoViewDialog.class);
                videoIntent.putExtra("videoType",3);
                videoIntent.putExtra("intentToPass",intent);
                context.startActivity(videoIntent);*/

                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });

           /* holder1.binding.ivAbout.setOnClickListener(view ->
            {
                selectedPos = position;

                Intent intent = new Intent(context, LittleOneProfileActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                intent.putExtra(Constant.LITTLE_ONE_ID, list.get(position).getId());
                intent.putExtra(Constant.USER_NAME, "");
                context.startActivity(intent);

                *//*Intent videoIntent = new Intent(context, VideoViewDialog.class);
                videoIntent.putExtra("videoType",3);
                videoIntent.putExtra("intentToPass",intent);
                context.startActivity(videoIntent);*//*

                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });*/


        } else if (holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
            holder1.binding.tvLittleOneName.setText(list.get(position).getFirstName() + " " + list.get(position).getLastName());

            holder1.binding.ivEditLittleOne.setOnClickListener(v ->
            {
                selectedPos = position;
                LittleOneSingleton.get(context, new CallbackLittleOne()
                {
                    @Override
                    public void editLittleOne(String firstName, String lastName)
                    {
                        list.get(selectedPos).setFirstName(firstName);
                        list.get(selectedPos).setLastName(lastName);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void deleteLittleOne()
                    {
                        list.remove(selectedPos);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void hideLittleOne()
                    {

                    }

                    @Override
                    public void addLittleOne(LittleOne littleOne)
                    {

                    }
                });

                Intent intent = new Intent(context, CreateLittleOneActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_EDIT_LITTLE_ONE);
                intent.putExtra(Constant.LITTLE_ONE_ID, list.get(position).getId());
                context.startActivity(intent);

                /*Intent videoIntent = new Intent(context, VideoViewDialog.class);
                videoIntent.putExtra("videoType",3);
                videoIntent.putExtra("intentToPass",intent);
                context.startActivity(videoIntent);*/

                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });

            holder1.binding.rootView.setOnClickListener(view ->
            {
                selectedPos = position;


                Intent intent = new Intent(context, LittleOneProfileActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                intent.putExtra(Constant.LITTLE_ONE_ID, list.get(position).getId());
                intent.putExtra(Constant.USER_NAME, "");
                context.startActivity(intent);

                /*Intent videoIntent = new Intent(context, VideoViewDialog.class);
                videoIntent.putExtra("videoType",3);
                videoIntent.putExtra("intentToPass",intent);
                context.startActivity(videoIntent);*/

                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            });


        }
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    private class MyViewHolderEdit extends RecyclerView.ViewHolder
    {
        private RowLittleoneBinding binding;

        public MyViewHolderEdit(RowLittleoneBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {

        private RowLittleoneProfileBinding binding;

        public MyViewHolder(RowLittleoneProfileBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
