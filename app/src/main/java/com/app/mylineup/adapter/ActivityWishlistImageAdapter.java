package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import com.app.mylineup.R;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.littleOnes.mylittleone.Item_;
import com.app.mylineup.pojo.wishlists.Item;
import com.app.mylineup.pojo.wishlists.Wishlist;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ActivityWishlistImageAdapter extends PagerAdapter
{

    public String TAG = "WishlistImageAdapter";
    private Context context;
    //private List<WishlistImageBeen> wishlistImageBeens;
    private List<Item> list;
    private List<Item_> littleOneItemList;
    private Wishlist wishlist;
    private boolean isMyAcc;

    public ActivityWishlistImageAdapter(Context context, List<Item> items, boolean isMyAcc)
    {
        this.context = context;
        this.list = items;
        this.isMyAcc = isMyAcc;
        this.wishlist = wishlist;
    }

    public ActivityWishlistImageAdapter(List<Item_> littleOneItemList, Context context, boolean isMyAcc)
    {
        this.context = context;
        this.littleOneItemList = littleOneItemList;
        this.isMyAcc = isMyAcc;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.row_wishlist_image_item, null);
        ImageView imageView = view.findViewById(R.id.ivWishlist);
        TextView ivPurchased = view.findViewById(R.id.ivPurchased);
        LinearLayout llLittleOne = view.findViewById(R.id.llLittleOne);
        TextView tvLittleOneName = view.findViewById(R.id.tvLittleOneName);
        CardView card = view.findViewById(R.id.card);

       // Log.e(TAG, "instantiateItem() Position:- " + position);
        if (isMyAcc)
        {
            if (list != null)
            {
               // Log.e(TAG, "instantiateItem() Image:- " + list.get(position).getImage());
                Item it = list.get(position);

                if (list.get(position).getImage() != null && !list.get(position).getImage().equalsIgnoreCase("") &&
                        !list.get(position).getImage().contains("default.png"))
                {
                    Picasso.get().load(list.get(position).getImage()).into(imageView);
                } else
                {
                    imageView.setImageResource(R.drawable.app_icon);

                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(400, 400);
                    params.gravity = Gravity.CENTER;
                    imageView.setLayoutParams(params);
                }

                if (it.getStatus() != null && !it.getStatus().isEmpty())
                {
                    if (!list.get(position).getPurchaseTag())
                    {
                        ivPurchased.setVisibility(View.GONE);
                    } else
                    {
                        ivPurchased.setVisibility(View.VISIBLE);
                    }
                }

//        if (wishlist!=null
//                && wishlist.getLittleOneId()!=null
//                && !wishlist.getLittleOneId().isEmpty()
//                && !wishlist.getLittleOneId().equalsIgnoreCase("0")) {
//            llLittleOne.setVisibility(View.VISIBLE);
//            tvLittleOneName.setText(wishlist.getLittleOneName());
//        }
//        else {
//            llLittleOne.setVisibility(View.GONE);
//        }

                card.setOnClickListener(v ->
                {
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    if (!isMyAcc)
                    {
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                    }
                    intent.putExtra(Constant.ITEM_ID, list.get(position).getItemId());

                    ((Activity) context).startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                });
            }
        } else
        {

            if (list != null)
            {
               // Log.e(TAG, "instantiateItem() Image:- " + list.get(position).getImage());
                Item it = list.get(position);

                if (list.get(position).getImage() != null && !list.get(position).getImage().equalsIgnoreCase("") &&
                        !list.get(position).getImage().contains("default.png"))
                {
                    Picasso.get().load(list.get(position).getImage()).into(imageView);
                } else
                {
                    imageView.setImageResource(R.drawable.app_icon);

                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(400, 400);
                    params.gravity = Gravity.CENTER;
                    imageView.setLayoutParams(params);
                }

                if (it.getStatus() != null && !it.getStatus().isEmpty())
                {
                    if (!list.get(position).getPurchaseTag())
                    {
                        ivPurchased.setVisibility(View.GONE);
                    } else
                    {
                        ivPurchased.setVisibility(View.VISIBLE);
                    }
                }

//        if (wishlist!=null
//                && wishlist.getLittleOneId()!=null
//                && !wishlist.getLittleOneId().isEmpty()
//                && !wishlist.getLittleOneId().equalsIgnoreCase("0")) {
//            llLittleOne.setVisibility(View.VISIBLE);
//            tvLittleOneName.setText(wishlist.getLittleOneName());
//        }
//        else {
//            llLittleOne.setVisibility(View.GONE);
//        }

                card.setOnClickListener(v ->
                {
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    if (!isMyAcc)
                    {
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                    }
                    intent.putExtra(Constant.ITEM_ID, list.get(position).getItemId());

                    ((Activity) context).startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                });
            }

            if (littleOneItemList != null)
            {
               // Log.e(TAG, "instantiateItem() Image:- " + littleOneItemList.get(position).getImage());
                Item_ it = littleOneItemList.get(position);

                if (littleOneItemList.get(position).getImage() != null && !littleOneItemList
                        .get(position).getImage().equalsIgnoreCase("") &&
                        !littleOneItemList.get(position).getImage().contains("default.png"))
                {
                    Picasso.get().load(littleOneItemList.get(position).getImage()).into(imageView);
                } else
                {
                    imageView.setImageResource(R.drawable.app_icon);

                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(400, 400);
                    params.gravity = Gravity.CENTER;
                    imageView.setLayoutParams(params);
                }

                if (it.getStatus() != null && !it.getStatus().isEmpty())
                {
                    if (!littleOneItemList.get(position).getPurchaseTag())
                    {
                        ivPurchased.setVisibility(View.GONE);
                    } else
                    {
                        ivPurchased.setVisibility(View.VISIBLE);
                    }
                }

//        if (wishlist!=null
//                && wishlist.getLittleOneId()!=null
//                && !wishlist.getLittleOneId().isEmpty()
//                && !wishlist.getLittleOneId().equalsIgnoreCase("0")) {
//            llLittleOne.setVisibility(View.VISIBLE);
//            tvLittleOneName.setText(wishlist.getLittleOneName());
//        }
//        else {
//            llLittleOne.setVisibility(View.GONE);
//        }

                card.setOnClickListener(v ->
                {
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    if (!isMyAcc)
                    {
                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                    }
                    intent.putExtra(Constant.ITEM_ID, littleOneItemList.get(position).getItemId());

                    ((Activity) context).startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                });
            }
        }

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view)
    {
        container.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object)
    {
        return view == object;
    }

    @Override
    public int getCount()
    {
        if (isMyAcc)
        {
            if (list != null)
            {
                return list.size();
            }
        }else {
            if (list != null)
            {
                return list.size();
            }
            if (littleOneItemList != null) {
                return littleOneItemList.size();
            }
        }
        return 0;
    }
}