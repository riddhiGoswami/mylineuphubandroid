package com.app.mylineup.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ItemFaqBinding;
import com.app.mylineup.pojo.faq.Faq;

import java.util.List;

public class FaqAdapter extends RecyclerView.Adapter
{
    private Context context;
    private int selectedPos = Integer.MAX_VALUE;
    private List<Faq> list;

    public FaqAdapter(Context context, List<Faq> list) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        ItemFaqBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_faq,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder)holder;
            holder1.binding.ivExpandCollapse.setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

            if(list.get(position).getQuestion() != null && !list.get(position).getQuestion().equalsIgnoreCase("")){
                holder1.binding.tvQue.setText(list.get(position).getQuestion());
            }
            if(list.get(position).getAnswer() != null && !list.get(position).getAnswer().equalsIgnoreCase("")){
                holder1.binding.tvAns.setText(list.get(position).getAnswer());
            }

            if(selectedPos == position)
            {
                holder1.binding.ivExpandCollapse.setColorFilter(context.getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                holder1.binding.ivExpandCollapse.setRotation(45);

                //holder1.binding.tvAns.setVisibility(View.VISIBLE);
                holder1.binding.tvAns.setAlpha(0.0f);
                holder1.binding.tvAns.animate()
                        .translationY(holder1.binding.tvAns.getHeight())
                        .setDuration(300)
                        .alpha(1.0f)
                        .setListener(null);

                holder1.binding.tvAns.setVisibility(View.VISIBLE);
            }
            else
            {
                holder1.binding.ivExpandCollapse.setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
                holder1.binding.ivExpandCollapse.setRotation(90);
                holder1.binding.tvAns.setVisibility(View.GONE);
            }

            holder1.binding.getRoot().setOnClickListener(v -> {
                if(position == selectedPos)
                { selectedPos = Integer.MAX_VALUE; }
                else
                {selectedPos = position;}
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private ItemFaqBinding binding;

        public MyViewHolder(ItemFaqBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
