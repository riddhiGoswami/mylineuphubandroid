package com.app.mylineup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.databinding.RowAboutItemBinding;
import com.app.mylineup.pojo.userProfile.ClothingFootware;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


public class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.MyViewHolder> {

    private String TAG = "AboutAdapter";
    //private List<AboutMeBeen> aboutMeBeenList;
    private Context mContext;
    private List<ClothingFootware> list;
    private List<com.app.mylineup.pojo.littleOnes.mylittleone.ClothingFootware> littleOneClothingFootwareList;

    public AboutAdapter(Context context, List<ClothingFootware> aboutMeBeenList)
    {
        this.mContext = context;
        this.list = aboutMeBeenList;
    }

    public AboutAdapter( List<com.app.mylineup.pojo.littleOnes.mylittleone.ClothingFootware> littleOneClothingFootwareList, Context mContext)
    {
        this.mContext = mContext;
        this.littleOneClothingFootwareList = littleOneClothingFootwareList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        RowAboutItemBinding binding;

        public MyViewHolder(RowAboutItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RowAboutItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_about_item,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        Log.e(TAG,"onBindViewHolder()");

        if (list != null)
        {
            if(list.get(position).getImage() != null && !list.get(position).getImage().equalsIgnoreCase("")) {
                Picasso.get().load(list.get(position).getImage()).into(holder.binding.ivAbout);
            }
            //holder.binding.ivAbout.setImageResource(aboutMeBeenList.get(position).getImage());
            if(list.get(position).getName() != null && !list.get(position).getName().equalsIgnoreCase("")){
                holder.binding.tvName.setText(list.get(position).getName());
            }
            if(list.get(position).getVariantItemValue() != null && !list.get(position).getVariantItemValue().equalsIgnoreCase("")){
                holder.binding.tvSize.setText(list.get(position).getVariantItemValue());
            }

            if(position % 2 != 0 || ((list.size()-1) == position  && (list.size() % 2 != 0) ))
            {holder.binding.view.setVisibility(View.GONE);}
            else
            {holder.binding.view.setVisibility(View.VISIBLE);}
        }

        if (littleOneClothingFootwareList != null)
        {
            if(littleOneClothingFootwareList.get(position).getImage() != null && !littleOneClothingFootwareList.get(position).getImage().equalsIgnoreCase("")) {
                Picasso.get().load(littleOneClothingFootwareList.get(position).getImage()).into(holder.binding.ivAbout);
            }
            //holder.binding.ivAbout.setImageResource(aboutMeBeenlittleOneClothingFootwareList.get(position).getImage());
            if(littleOneClothingFootwareList.get(position).getName() != null && !littleOneClothingFootwareList.get(position).getName().equalsIgnoreCase("")){
                holder.binding.tvName.setText(littleOneClothingFootwareList.get(position).getName());
            }
            if(littleOneClothingFootwareList.get(position).getVariantItemValue() != null && !littleOneClothingFootwareList.get(position).getVariantItemValue().equalsIgnoreCase("")){
                holder.binding.tvSize.setText(littleOneClothingFootwareList.get(position).getVariantItemValue());
            }

            if(position % 2 != 0 || ((littleOneClothingFootwareList.size()-1) == position  && (littleOneClothingFootwareList.size() % 2 != 0) ))
            {holder.binding.view.setVisibility(View.GONE);}
            else
            {holder.binding.view.setVisibility(View.VISIBLE);}
        }


    }

    @Override
    public int getItemCount()
    {
        if (list != null) {
            return list.size();
        } else if (littleOneClothingFootwareList != null){
            return littleOneClothingFootwareList.size();
        }
        return 0;
    }
}