package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.L;
import com.app.mylineup.R;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowWishlistItemBinding;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.CallbackWishList;
import com.app.mylineup.interfaces.CallbackWishListItem;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.interfaces.WishListItemSingleton;
import com.app.mylineup.interfaces.WishListSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneWishlist;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.pojo.wishlists.Item;
import com.app.mylineup.pojo.wishlists.Wishlist;
import com.app.mylineup.singleton.SelectedLittleOneId;
import com.app.mylineup.view.DepthPageTransformer;
import com.chahinem.pageindicator.PageIndicator;
import com.squareup.picasso.Picasso;

import java.net.URLDecoder;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import me.relex.circleindicator.CircleIndicator;


public class UserWishlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String TAG = "UserWishlistAdapter";
    private Context mContext;
    private List<Wishlist> userProfileWishlist;
    private List<LittleOneWishlist> littleOneWishlist;

    private Long startClickTime;
    private long MAX_CLICK_DURATION = 100;
    private String userName;
    private String defaultImage;
    boolean isMyAcc;
    private String userId;
    private ListChangeCallback listChangeCallback;

    public UserWishlistAdapter(List<LittleOneWishlist> littleOneWishlist,
                               Context mContext,
                               boolean isMyAcc, String name, String defaultImage, String userId, ListChangeCallback listChangeCallback) {

        this.mContext = mContext;
        this.littleOneWishlist = littleOneWishlist;
        this.isMyAcc = isMyAcc;
        this.userName = name;
        this.defaultImage = defaultImage;
        this.userId = userId;
        this.listChangeCallback = listChangeCallback;
    }

    public UserWishlistAdapter(Context context,
                               List<Wishlist> list,
                               boolean isMyAcc, String name, String defaultImage, String userId, ListChangeCallback listChangeCallback) {

        this.mContext = context;
        this.userProfileWishlist = list;
        this.isMyAcc = isMyAcc;
        this.userName = name;
        this.defaultImage = defaultImage;
        this.userId = userId;
        this.listChangeCallback = listChangeCallback;
    }


    //    class AddWishlistHeader extends RecyclerView.ViewHolder {
//
//        private LinearLayout llNewWishList;
//
//        public AddWishlistHeader(@NonNull View itemView) {
//            super(itemView);
//            llNewWishList = itemView.findViewById(R.id.llNewWishList);
//
//            llNewWishList.setOnClickListener(v -> {
//                Intent intent = new Intent(mContext, CreateWishListActivity.class);
//                mContext.startActivity(intent);
//                ((Activity)mContext).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//            });
//        }
//    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private ViewPager viewPager;
        private CardView ivNoItem;
        private ImageView ivNoImageView;
        // private CircleIndicator circleIndicator;
        private PageIndicator circleIndicator;
        private LinearLayout llOpenWishList;
        private TextView tvWishlistName;
        private TextView tvItemCount;
        private LinearLayout llLittleOne;
        private TextView tvLittleOneName;


        private RowWishlistItemBinding binding;

        public MyViewHolder(View view) {
            super(view);
            viewPager = view.findViewById(R.id.view_pager);
            ivNoItem = view.findViewById(R.id.ivNoItem);
            ivNoImageView = view.findViewById(R.id.ivNoImageView);
            circleIndicator = view.findViewById(R.id.circleIndicator);
            llOpenWishList = view.findViewById(R.id.llOpenWishList);
            tvWishlistName = view.findViewById(R.id.tvWishlistName);
            tvItemCount = view.findViewById(R.id.tvItemCount);
            llLittleOne = view.findViewById(R.id.llLittleOne);
            tvLittleOneName = view.findViewById(R.id.tvLittleOneName);

            llOpenWishList.setOnClickListener(v ->
            {

                Intent intent = new Intent(mContext, WishlistActivity.class);
                Log.w("isMyAcc", "" + isMyAcc);
                Log.w("isMyAcc", "" + userId);
                //userProfileWishlist.get(getAdapterPosition()).getUserId().equalsIgnoreCase(logi)
                if (userProfileWishlist != null && userProfileWishlist.size() > 0) {
                    Log.w("isMyAcc", "userProfileWishlist => " + userProfileWishlist.get(getAdapterPosition()).getUserId());
                    if( userProfileWishlist.get(getAdapterPosition()).getLittleOneId() != null){
                        SelectedLittleOneId.littleOneId = userProfileWishlist.get(getAdapterPosition()).getLittleOneId();
                    }

                   /* WishListSingleton.get(mContext, new CallbackWishList() {
                        @Override
                        public void editWishlist(String name, String madeForID) {
                            try {
                                Log.w("madeForID", "userProfileWishlist madeForID => " + madeForID + "  userId => " + userProfileWishlist.get(getAdapterPosition()).getUserId());
                                if (madeForID.equalsIgnoreCase(userProfileWishlist.get(getAdapterPosition()).getUserId())) {
                                    tvWishlistName.setText(
                                            URLDecoder.decode(name, "UTF-8"));
                                } else {
                                    userProfileWishlist.remove(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                    listChangeCallback.onListChange(madeForID);
                                }
                                // + " On "
                                //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                tvWishlistName.setText(
                                        name);// + " On "
                                //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                            }
                        }

                        @Override
                        public void createItem(String wishlistId) {

                        }

                        @Override
                        public void deleteWishlist() {
                            userProfileWishlist.remove(getAdapterPosition());
                            notifyItemRemoved(getAdapterPosition());
                        }

                        @Override
                        public void changeLittleOne() {

                        }

                    });*/

                    if (!userId.equalsIgnoreCase(userProfileWishlist.get(getAdapterPosition()).getUserId()) && !isMyAcc) {
                        isMyAcc = false;
                        WishListItemSingleton.get(mContext, new CallbackWishListItem() {

                            @Override
                            public void purchaseWishListItem(boolean isPurchased, String itemId) {
                                if (isPurchased) {

                                    List<Item> list1 = userProfileWishlist.get(getAdapterPosition()).getItems();
                                    for (int i = 0; i < list1.size(); i++) {
                                        if (list1.get(i).getItemId().equalsIgnoreCase(itemId)) {
                                            list1.get(i).setStatus("1");
                                            break;
                                        }
                                    }

                                    WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(mContext, list1, isMyAcc, userProfileWishlist.get(getAdapterPosition()));
                                    viewPager.setAdapter(wishlistImageAdapter);
                                    //viewHolder.circleIndicator.setViewPager(viewHolder.viewPager);
                                    circleIndicator.attachTo(viewPager);
                                    viewPager.setPageTransformer(true, new DepthPageTransformer());
                                }
                            }
                        });


                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                        intent.putExtra(Constant.USER_ID, userProfileWishlist.get(getAdapterPosition()).getUserId());
                        intent.putExtra(Constant.USER_NAME, userName);
                    }
                    intent.putExtra(Constant.WISHLIST_ID, userProfileWishlist.get(getAdapterPosition()).getId());
                    intent.putExtra(Constant.LITTLE_ONE_ID, userProfileWishlist.get(getAdapterPosition()).getLittleOneId());
                    intent.putExtra(Constant.WISHLIST_NAME, userProfileWishlist.get(getAdapterPosition()).getName());
                    intent.putExtra(Constant.EXP_DATE, userProfileWishlist.get(getAdapterPosition()).getEventExpired());
                    mContext.startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else if (littleOneWishlist != null && littleOneWishlist.size() > 0) {
                    if( littleOneWishlist.get(getAdapterPosition()).getLittleOneId() != null){
                        SelectedLittleOneId.littleOneId = littleOneWishlist.get(getAdapterPosition()).getLittleOneId();
                    }

                    Log.w("isMyAcc", "littleOneWishlist => " + littleOneWishlist.get(getAdapterPosition()).getUserId());
                    WishListSingleton.get(mContext, new CallbackWishList() {
                        @Override
                        public void editWishlist(String name, String madeForID) {
                            try {
                                Log.w("madeForID", "littleOneWishlist madeForID => " + madeForID + "  userId => " + littleOneWishlist.get(getAdapterPosition()).getLittleOneId());
                                if (madeForID.equalsIgnoreCase(littleOneWishlist.get(getAdapterPosition()).getLittleOneId())) {
                                    tvWishlistName.setText(
                                            URLDecoder.decode(name, "UTF-8"));
                                } else {
                                    littleOneWishlist.remove(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                    listChangeCallback.onListChange(madeForID);
                                }
                                // + " On "
                                //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                tvWishlistName.setText(
                                        name);// + " On "
                                //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                            }
                        }

                        @Override
                        public void createItem(String wishlistId) {

                        }

                        @Override
                        public void deleteWishlist() {
                            littleOneWishlist.remove(getAdapterPosition());
                            notifyItemRemoved(getAdapterPosition());
                        }

                        @Override
                        public void changeLittleOne() {

                        }

                    });
                    if (!isMyAcc && !userId.equalsIgnoreCase(littleOneWishlist.get(getAdapterPosition()).getUserId())) {
                        isMyAcc = false;
                        WishListItemSingleton.get(mContext, new CallbackWishListItem() {

                            @Override
                            public void purchaseWishListItem(boolean isPurchased, String itemId) {
                                if (isPurchased) {

                                    List<com.app.mylineup.pojo.littleOnes.mylittleone.Item> list1 = littleOneWishlist.get(getAdapterPosition()).getItems();
                                    for (int i = 0; i < list1.size(); i++) {
                                        if (list1.get(i).getItemId().equalsIgnoreCase(itemId)) {
                                            list1.get(i).setStatus("1");
                                            break;
                                        }
                                    }

                                    WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(
                                            mContext,
                                            isMyAcc,
                                            littleOneWishlist.get(getAdapterPosition()),
                                            list1
                                    );
                                    viewPager.setAdapter(wishlistImageAdapter);
                                    circleIndicator.attachTo(viewPager);
                                    //   viewHolder.circleIndicator.setViewPager(viewHolder.viewPager);
                                    viewPager.setPageTransformer(true, new DepthPageTransformer());
                                }
                            }
                        });

                        intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                        intent.putExtra(Constant.USER_ID, littleOneWishlist.get(getAdapterPosition()).getUserId());
                        intent.putExtra(Constant.USER_NAME, userName);
                    }
                    intent.putExtra(Constant.WISHLIST_ID, littleOneWishlist.get(getAdapterPosition()).getId());
                    intent.putExtra(Constant.WISHLIST_NAME, littleOneWishlist.get(getAdapterPosition()).getName());
                    intent.putExtra(Constant.EXP_DATE, littleOneWishlist.get(getAdapterPosition()).getEventDate());
                    mContext.startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });

//            binding.getRoot().setOnClickListener(v -> {
                /*Intent intent = new Intent(mContext, ItemDetailActivity.class);
                ((Activity)mContext).startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/
//            });

            /*binding.viewPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            startClickTime = Calendar.getInstance().getTimeInMillis();
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            if(clickDuration < MAX_CLICK_DURATION) {
                                //click event has occurred
                                Intent intent = new Intent(mContext, ItemDetailActivity.class);
                                ((Activity)mContext).startActivity(intent);
                                ((Activity)mContext).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                            }
                        }
                    }
                    return false;
                }
            });*/
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (viewType == 0) {
//            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_new_wishlist_item, parent, false);
//            return new AddWishlistHeader(itemView);
//
////            AddNewWishlistItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.add_new_wishlist_item, parent, false);
////            return new MyViewHolder(binding)
//        }
//        else {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wishlist_item, parent, false);
        return new MyViewHolder(itemView);

//            RowWishlistItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_wishlist_item, parent, false);
//            return new MyViewHolder(binding);
//        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
//        if (holder instanceof AddWishlistHeader) {
//            AddWishlistHeader header = (AddWishlistHeader) holder;
//        }
//        else {
        MyViewHolder viewHolder = (MyViewHolder) holder;

        if (viewHolder != null) {
            if (isMyAcc) {
                if (userProfileWishlist != null && userProfileWishlist.size() > 0) {
                    Log.w("getUserId", "userProfileWishlist => " + userProfileWishlist.get(position).getUserId());
                    if (userProfileWishlist.get(position).getItems() != null) {
                        if (userProfileWishlist.get(position).getItems().size() < 2) {
                            viewHolder.tvItemCount.setText(userProfileWishlist.get(position).getItems().size() + " Item");
                        } else {
                            viewHolder.tvItemCount.setText(userProfileWishlist.get(position).getItems().size() + " Items");
                        }
                        try {
                            viewHolder.tvWishlistName.setText(
                                    URLDecoder.decode(userProfileWishlist.get(position).getName(), "UTF-8")); // + " On "
                            //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            viewHolder.tvWishlistName.setText(
                                    userProfileWishlist.get(position).getName());// + " On "
                            //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                        }

                        if (userProfileWishlist.get(position).getItems().size() > 0) {
                            viewHolder.viewPager.setVisibility(View.VISIBLE);
                            viewHolder.circleIndicator.setVisibility(View.VISIBLE);
                            viewHolder.ivNoItem.setVisibility(View.GONE);

                            WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(mContext, userProfileWishlist.get(position).getItems(), isMyAcc, userProfileWishlist.get(position));
                            viewHolder.viewPager.setAdapter(wishlistImageAdapter);
                            //viewHolder.circleIndicator.setViewPager(viewHolder.viewPager);
                            viewHolder.circleIndicator.attachTo(viewHolder.viewPager);
                            viewHolder.viewPager.setPageTransformer(true, new DepthPageTransformer());
                        } else {
                            viewHolder.viewPager.setVisibility(View.GONE);
                            viewHolder.ivNoItem.setVisibility(View.VISIBLE);
                            Picasso.get().load(defaultImage).fit().into(viewHolder.ivNoImageView);
                            viewHolder.circleIndicator.setVisibility(View.INVISIBLE);
                            Log.w("hasLittleOne", "" + userProfileWishlist.get(position).getLittleOneName() + "     " + userProfileWishlist.get(position).getLittleOneId());
                            // viewHolder.llLittleOne.setVisibility(View.VISIBLE);

                            if (userProfileWishlist.get(position).getLittleOneName() != null && !userProfileWishlist.get(position).getLittleOneName().isEmpty() && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("0")) {
                                viewHolder.llLittleOne.setVisibility(View.VISIBLE);
                                viewHolder.tvLittleOneName.setText(userProfileWishlist.get(position).getLittleOneName());
                            } else {
                                viewHolder.llLittleOne.setVisibility(View.GONE);
                            }

//                            if (userProfileWishlist.get(position).getLittleOneId() != null && !userProfileWishlist.get(position).getLittleOneId().isEmpty() && !userProfileWishlist.get(position).getLittleOneId().equalsIgnoreCase("0") /*&& !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("")*/) {
//                                viewHolder.llLittleOne.setVisibility(View.VISIBLE);
//                                if (userProfileWishlist.get(position).getLittleOneName() != null && !userProfileWishlist.get(position).getLittleOneName().isEmpty() && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("0")) {
//                                    viewHolder.tvLittleOneName.setText(userProfileWishlist.get(position).getLittleOneName());
//                                }
//                            } else {
//                                viewHolder.llLittleOne.setVisibility(View.GONE);
//                            }
                        }
                    } else {
                        viewHolder.tvItemCount.setText("0 Item");
                    }

                    viewHolder.llLittleOne.setOnClickListener(v -> {
                        if (!userProfileWishlist.get(position).getLittleOneId().equalsIgnoreCase("0") && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("")) {
                            Intent intent = new Intent(mContext, LittleOneProfileActivity.class);
                            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                            intent.putExtra(Constant.LITTLE_ONE_ID, userProfileWishlist.get(position).getLittleOneId());
                            intent.putExtra(Constant.USER_NAME, "");
                            mContext.startActivity(intent);
                            ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    });
                }
            } else {
                if (userProfileWishlist != null && userProfileWishlist.size() > 0) {
                    if (userProfileWishlist.get(position).getItems() != null) {
                        if (userProfileWishlist.get(position).getItems().size() < 2) {
                            viewHolder.tvItemCount.setText(userProfileWishlist.get(position).getItems().size() + " Item");
                        } else {
                            viewHolder.tvItemCount.setText(userProfileWishlist.get(position).getItems().size() + " Items");
                        }
                        try {
                            viewHolder.tvWishlistName.setText(
                                    URLDecoder.decode(userProfileWishlist.get(position).getName(), "UTF-8")); // + " On "
                            //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            viewHolder.tvWishlistName.setText(
                                    userProfileWishlist.get(position).getName());// + " On "
                            //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                        }

                        if (userProfileWishlist.get(position).getItems().size() > 0) {
                            viewHolder.viewPager.setVisibility(View.VISIBLE);
                            viewHolder.circleIndicator.setVisibility(View.VISIBLE);
                            viewHolder.ivNoItem.setVisibility(View.GONE);

                            WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(mContext, userProfileWishlist.get(position).getItems(), isMyAcc, userProfileWishlist.get(position));
                            viewHolder.viewPager.setAdapter(wishlistImageAdapter);
                            //  viewHolder.circleIndicator.setViewPager(viewHolder.viewPager);
                            viewHolder.circleIndicator.attachTo(viewHolder.viewPager);
                            viewHolder.viewPager.setPageTransformer(true, new DepthPageTransformer());
                        } else {
                            Log.w("defaultImage", "" + defaultImage);
                            viewHolder.viewPager.setVisibility(View.GONE);
                            viewHolder.ivNoItem.setVisibility(View.VISIBLE);
                            Picasso.get().load(defaultImage).fit().into(viewHolder.ivNoImageView);
                            viewHolder.circleIndicator.setVisibility(View.INVISIBLE);

                            if (userProfileWishlist.get(position).getLittleOneName() != null && !userProfileWishlist.get(position).getLittleOneName().isEmpty() && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("0")) {
                                viewHolder.llLittleOne.setVisibility(View.VISIBLE);
                                viewHolder.tvLittleOneName.setText(userProfileWishlist.get(position).getLittleOneName());
                            } else {
                                viewHolder.llLittleOne.setVisibility(View.GONE);
                            }

//                            if (!userProfileWishlist.get(position).getLittleOneId().equalsIgnoreCase("0") && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("")) {
//                                viewHolder.llLittleOne.setVisibility(View.VISIBLE);
//                                viewHolder.tvLittleOneName.setText(userProfileWishlist.get(position).getLittleOneName());
//                            } else {
//                                viewHolder.llLittleOne.setVisibility(View.GONE);
//                            }
                        }
                    } else {
                        viewHolder.tvItemCount.setText("0 Item");
                    }

                    viewHolder.llLittleOne.setOnClickListener(v -> {
                        if (!userProfileWishlist.get(position).getLittleOneId().equalsIgnoreCase("0") && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("")) {
                            Intent intent = new Intent(mContext, LittleOneProfileActivity.class);
                            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                            intent.putExtra(Constant.LITTLE_ONE_ID, userProfileWishlist.get(position).getLittleOneId());
                            intent.putExtra(Constant.USER_NAME, "");
                            mContext.startActivity(intent);
                            ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    });
                }

                if (littleOneWishlist != null && littleOneWishlist.size() > 0) {
                    if (littleOneWishlist.get(position).getItems() != null) {
                        if (littleOneWishlist.get(position).getItems().size() < 2) {
                            viewHolder.tvItemCount.setText(littleOneWishlist.get(position).getItems().size() + " Item");
                        } else {
                            viewHolder.tvItemCount.setText(littleOneWishlist.get(position).getItems().size() + " Items");
                        }

                        try {
                            viewHolder.tvWishlistName.setText(
                                    URLDecoder.decode(littleOneWishlist.get(position).getName(), "UTF-8")); // + " On "
                            //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            viewHolder.tvWishlistName.setText(
                                    littleOneWishlist.get(position).getName());// + " On "
                            //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
                        }

                        if (littleOneWishlist.get(position).getItems().size() > 0) {
                            viewHolder.viewPager.setVisibility(View.VISIBLE);
                            viewHolder.circleIndicator.setVisibility(View.VISIBLE);
                            viewHolder.ivNoItem.setVisibility(View.GONE);

                            WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(
                                    mContext,
                                    isMyAcc,
                                    littleOneWishlist.get(position),
                                    littleOneWishlist.get(position).getItems()
                            );
                            viewHolder.viewPager.setAdapter(wishlistImageAdapter);
                            viewHolder.circleIndicator.attachTo(viewHolder.viewPager);
                            //   viewHolder.circleIndicator.setViewPager(viewHolder.viewPager);
                            viewHolder.viewPager.setPageTransformer(true, new DepthPageTransformer());
                        } else {
                            viewHolder.viewPager.setVisibility(View.GONE);
                            viewHolder.ivNoItem.setVisibility(View.VISIBLE);
                            viewHolder.circleIndicator.setVisibility(View.INVISIBLE);
                            Picasso.get().load(defaultImage).fit().into(viewHolder.ivNoImageView);
                           /* if (!littleOneWishlist.get(position).getLittleOneId().equalsIgnoreCase("0") && !littleOneWishlist.get(position).getName().equalsIgnoreCase("")) {
                                viewHolder.llLittleOne.setVisibility(View.VISIBLE);
                                viewHolder.tvLittleOneName.setText(littleOneWishlist.get(position).getName());
                            } else {
                                viewHolder.llLittleOne.setVisibility(View.GONE);
                            }*/
                        }

                    } else {
                        viewHolder.tvItemCount.setText("0 Item");
                    }

                    viewHolder.llLittleOne.setOnClickListener(v -> {
                        if (!userProfileWishlist.get(position).getLittleOneId().equalsIgnoreCase("0") && !userProfileWishlist.get(position).getLittleOneName().equalsIgnoreCase("")) {
                            Intent intent = new Intent(mContext, LittleOneProfileActivity.class);
                            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
                            intent.putExtra(Constant.LITTLE_ONE_ID, littleOneWishlist.get(position).getLittleOneId());
                            intent.putExtra(Constant.USER_NAME, "");
                            mContext.startActivity(intent);
                            ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    });
                }
            }
        }
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position == 0) {
//            return 0;
//        }
//        else {
//            return 1;
//        }
//    }

    @Override
    public int getItemCount() {
        if (isMyAcc) {
            if (userProfileWishlist != null) {
                return userProfileWishlist.size();
            }
        } else {
            if (userProfileWishlist != null) {
                return userProfileWishlist.size();
            } else if (littleOneWishlist != null) {
                return littleOneWishlist.size();
            }
        }
        return 0;
    }

    public interface ListChangeCallback {
        public void onListChange(String madeForID);
    }


}