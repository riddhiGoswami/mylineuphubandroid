package com.app.mylineup.adapter;

import android.view.ViewGroup;


import com.app.mylineup.view.WrappingViewPager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class TabAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    private int mCurrentPosition = -1;
    private boolean isAdjust = true;

    public TabAdapter(FragmentManager fm)
    {
        super(fm);
    }

    public TabAdapter(FragmentManager supportFragmentManager, boolean b) {
        super(supportFragmentManager);
        isAdjust = b;
    }

    @Override
    public Fragment getItem(int position)
    {

        return mFragmentList.get(position);
    }

    public void addFragment(Fragment fragment, String title)
    {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        return mFragmentTitleList.get(position);
    }

    @Override
    public int getCount()
    {
        return mFragmentList.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (position != mCurrentPosition) {
            Fragment fragment = (Fragment) object;
            WrappingViewPager pager = (WrappingViewPager) container;
            if (fragment != null && fragment.getView() != null) {
                mCurrentPosition = position;
                if(isAdjust)
                {pager.measureCurrentView(fragment.getView());}
            }
        }
    }

}
