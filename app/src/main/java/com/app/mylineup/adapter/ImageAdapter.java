package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.interfaces.CallbackWishListItem;
import com.app.mylineup.interfaces.WishListItemSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.giftFeeds.Item;
import com.app.mylineup.view.DepthPageTransformer;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

public class ImageAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Item> list;
    private String littleOneId, littleOneName;

    public ImageAdapter(Context context, List<Item> items, String little_one_id, String little_one_name) {
        mContext = context;
        list = items;
        littleOneId = little_one_id;
        littleOneName = little_one_name;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((CardView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.row_images, container, false);
        container.addView(itemView);

        ImageView imageView = itemView.findViewById(R.id.ivItem);
        TextView ivPurchased = itemView.findViewById(R.id.ivPurchased);
        LinearLayout llLittleOne = itemView.findViewById(R.id.llLittleOne);
        TextView tvLittleOneName = itemView.findViewById(R.id.tvLittleOneName);

        CardView card = itemView.findViewById(R.id.card);
        if (list.get(position).getImage() != null && !list.get(position).getImage().equalsIgnoreCase("") &&
                !list.get(position).getImage().contains("default.png")) {
            Picasso.get().load(list.get(position).getImage()).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.app_icon);

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(400, 400);
            params.gravity = Gravity.CENTER;
            imageView.setLayoutParams(params);
        }

        if (list.get(position).getStatus().equalsIgnoreCase("1")) {
            ivPurchased.setVisibility(View.VISIBLE);
        } else {
            ivPurchased.setVisibility(View.INVISIBLE);
        }

        if (!littleOneId.equalsIgnoreCase("0") && !littleOneName.equalsIgnoreCase("")) {
            llLittleOne.setVisibility(View.VISIBLE);
            tvLittleOneName.setText(littleOneName);
        } else {
            llLittleOne.setVisibility(View.GONE);
        }

        imageView.setOnClickListener(v -> {

            WishListItemSingleton.get(mContext, new CallbackWishListItem() {

                @Override
                public void purchaseWishListItem(boolean isPurchased, String itemId) {
                    if (isPurchased) {

                        list.get(position).setStatus("1");
                        ivPurchased.setVisibility(View.VISIBLE);


                    }
                }
            });


            Intent intent = new Intent(mContext, ItemDetailActivity.class);
            intent.putExtra(Constant.ITEM_ID, list.get(position).getItemId());
            intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
            ((Activity) mContext).startActivity(intent);
            ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        ivPurchased.setOnClickListener(v -> {

            WishListItemSingleton.get(mContext, new CallbackWishListItem() {

                @Override
                public void purchaseWishListItem(boolean isPurchased, String itemId) {
                    if (isPurchased) {

                        list.get(position).setStatus("1");
                        ivPurchased.setVisibility(View.VISIBLE);


                    }
                }
            });


            Intent intent = new Intent(mContext, ItemDetailActivity.class);
            intent.putExtra(Constant.ITEM_ID, list.get(position).getItemId());
            intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
            ((Activity) mContext).startActivity(intent);
            ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        llLittleOne.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, LittleOneProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
            intent.putExtra(Constant.LITTLE_ONE_ID, littleOneId);
            intent.putExtra(Constant.USER_NAME, "");
            mContext.startActivity(intent);
            ((Activity) mContext).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((CardView) object);
    }
}
