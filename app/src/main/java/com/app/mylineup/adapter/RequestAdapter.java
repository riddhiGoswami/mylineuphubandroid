package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.mylineup.R;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.databinding.RowProgressBinding;
import com.app.mylineup.databinding.RowRequestBinding;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.notifications.FriendRequest;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RequestAdapter extends RecyclerView.Adapter {

    private Context context;
    private FriendRequstAction friendRequstAction;
    private List<FriendRequest> friendRequestList;

    private OnLoadmoreListener onLoadmoreListener;
    int VIEW_PROGRESS=0,VIEW_ITEM=1;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private  OnItemClickGiftListener onItemClickGiftListener;

    public RequestAdapter(Context context, List<FriendRequest> friendRequestList, FriendRequstAction friendRequstAction, RecyclerView recyclerView, OnLoadmoreListener onLoadmoreListener, OnItemClickGiftListener onItemClickGiftListener)
    {
        this.context = context;
        this.friendRequstAction = friendRequstAction;
        this.onLoadmoreListener = onLoadmoreListener;
        this.friendRequestList = friendRequestList;
        this.onItemClickGiftListener = onItemClickGiftListener;

        if(recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager)
        {
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
                {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = layoutManager.getItemCount();
                    lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                    if(!loading && totalItemCount <= (lastVisibleItem + visibleThreshold))
                    {
                        if (onLoadmoreListener != null)
                        {
                            onLoadmoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if(viewType == VIEW_ITEM){
            RowRequestBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_request,parent,false);
            return new MyViewHolder(binding);
        }else{
            RowProgressBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.row_progress,parent,false);
            return new ProgressViewHolder(binding);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof  MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;

            if(friendRequestList.get(position).getActionType().equalsIgnoreCase("1")){
                holder1.binding.llAcceptReject.setVisibility(View.GONE);
                holder1.binding.tvAcceptReject.setVisibility(View.VISIBLE);
                holder1.binding.tvAcceptReject.setText(context.getString(R.string.accepted));
            }
            else if(friendRequestList.get(position).getActionType().equalsIgnoreCase("2")){
                holder1.binding.llAcceptReject.setVisibility(View.GONE);
                holder1.binding.tvAcceptReject.setVisibility(View.VISIBLE);
                holder1.binding.tvAcceptReject.setText(context.getString(R.string.rejected));
            }
            else {
                holder1.binding.llAcceptReject.setVisibility(View.VISIBLE);
                holder1.binding.tvAcceptReject.setVisibility(View.GONE);
            }

            if(!friendRequestList.get(position).getProfilePicture().equalsIgnoreCase("")){
                Picasso.get().load(friendRequestList.get(position).getProfilePicture()).placeholder(R.drawable.app_icon).into(holder1.binding.ivImage);
            }else {
                holder1.binding.ivImage.setImageResource(R.drawable.app_icon);
            }

            if(friendRequestList.get(position).getUsername() != null && !friendRequestList.get(position).getUsername().equalsIgnoreCase("")){
                holder1.binding.tvName.setText(friendRequestList.get(position).getUsername());
            }

            if(friendRequestList.get(position).getMatualConnection() != null && !friendRequestList.get(position).getMatualConnection().equalsIgnoreCase("")){
                String x = "";
                if(friendRequestList.get(position).getMatualConnection().equalsIgnoreCase("0") ||
                        friendRequestList.get(position).getMatualConnection().equalsIgnoreCase("1")){
                    x = " mutual connection";
                }else {
                    x = " mutual connections";
                }
                holder1.binding.tvMutualCount.setText(friendRequestList.get(position).getMatualConnection()+x);
            }

            holder1.binding.acceptRequest.setOnClickListener(v -> {
                if(friendRequstAction != null)
                    friendRequstAction.perFormActionOnRequest("1",position);
            });

            holder1.binding.declineRequest.setOnClickListener(v -> {
                if(friendRequstAction != null)
                    friendRequstAction.perFormActionOnRequest("2",position);
            });

            holder1.binding.llUser.setOnClickListener(v -> {
                Log.w("onProfileClick","11...");

                onItemClickGiftListener.onClick(friendRequestList.get(position).getUserId());
                /*Intent intent = new Intent(context, UserProfileActivity.class);
                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                intent.putExtra(Constant.USER_ID,friendRequestList.get(position).getUserId());
                ((Activity)context).startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/
            });
        }
        else if(holder instanceof ProgressViewHolder){
            ProgressViewHolder progressViewHolder = (ProgressViewHolder)holder;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER;
            progressViewHolder.progressBinding.progressBar.setLayoutParams(params);

        }

    }

    @Override
    public int getItemCount() {
        return friendRequestList.size();
    }

    public void setLoaded(boolean loding) {
        loading = loding;
    }

    @Override
    public int getItemViewType(int position) {
        return friendRequestList.get(position)==null?VIEW_PROGRESS:VIEW_ITEM;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowRequestBinding binding;
        public MyViewHolder(RowRequestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private class ProgressViewHolder extends RecyclerView.ViewHolder
    {
        RowProgressBinding progressBinding;
        public ProgressViewHolder(RowProgressBinding binding) {
            super(binding.getRoot());
            progressBinding = binding;
        }
    }

    public interface FriendRequstAction
    {
        void perFormActionOnRequest(String requestType,int position);
    }
}
