package com.app.mylineup.custom_view;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;

import android.util.AttributeSet;


import com.app.mylineup.R;

import java.util.HashMap;

import androidx.appcompat.widget.AppCompatEditText;


public class MyEditText extends AppCompatEditText
{

    HashMap<String,Typeface> typefaceMap;
    Context context;

    public MyEditText(Context context) {
        super(context);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(context,attrs);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace(context,attrs);
    }

    private void setTypeFace(Context context, AttributeSet attrs)
    {
        if(typefaceMap == null)
        {
            typefaceMap = new HashMap<String, Typeface>();
        }
        this.context = context;
        if(this.isInEditMode())
        {
            return;
        }
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyTextView);
        if(typedArray != null)
        {
            String typefacepath = typedArray.getString(R.styleable.MyTextView_customTypeFace);

            if(typefacepath != null)
            {
                Typeface typeface = null;
                if(typefaceMap.containsKey(typefacepath))
                {
                    typeface = typefaceMap.get(typefacepath);
                }
                else
                {
                    AssetManager asset = context.getAssets();
                    typeface = Typeface.createFromAsset(asset,typefacepath);
                    typefaceMap.put(typefacepath,typeface);
                }

                setTypeface(typeface);
            }
            else
            {
                Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),context.getResources().getString(R.string.font_regular));
                this.setTypeface(typeface);
            }

            typedArray.recycle();
        }
    }

    public void setMediumTextView(MyEditText textView)
    {
        Typeface font = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.font_medium));
        textView.setTypeface(font);
        textView.invalidate();
    }
}
