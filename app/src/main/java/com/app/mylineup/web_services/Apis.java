package com.app.mylineup.web_services;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface Apis
{
    @GET("init/{version}/android/{user_id}")
    Call<Object> init(@Path("version") String version,@Path("user_id") String id);

    @FormUrlEncoded
    @POST("user_register")
    Call<Object> registerUser(@Field("full_name") String full_name,
                              @Field("phone") String phone,
                              @Field("email") String email,
                              @Field("password") String password,
                              @Field("device_token") String device_token,
                              @Field("device_type") String device_type,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longitude,
                              @Field("device_name") String device_name,
                              @Field("zip_code") String zipCode,
                              @Field("birthday_date") String birthdayDate);

    @FormUrlEncoded
    @POST("user_login")
    Call<Object> loginUser(@Field("email") String email,
                              @Field("password") String password,
                              @Field("device_token") String device_token,
                              @Field("device_type") String device_type,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("resend_activation_mail")
    Call<Object> resendEmailForVerification(@Field("email") String email);

    @FormUrlEncoded
    @POST("change_password")
    Call<Object> changePassword(@Field("current_password") String current_password,
                           @Field("new_password") String new_password,
                           @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("logout")
    Call<Object> logout(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("forgot_password")
    Call<Object> forgotPassword(@Field("email") String email);


    @FormUrlEncoded
    @POST("notification_setting_update")
    Call<Object> updateNotificationSetting(@Field("user_id") String user_id,
                                           @Field("setting_name") String setting_name, //Setting Name = new_connections/weekly_updates/item_purchased/app_notification
                                           @Field("status") String status);  //status (0/1)

    ///get and update myprofile
    //@FormUrlEncoded
    @Multipart
    @POST("myprofile")
    Call<Object> getUserProfile(@Part("user_id") RequestBody user_id,
                                @Part("full_name") RequestBody full_name,//optional
                                @Part("birthday_date") RequestBody birthday_date,//optional
                                @Part("anniversary_date") RequestBody anniversary_date,//optional
                                @Part("phone") RequestBody phone,//optional
                                @Part MultipartBody.Part profile_picture,//optional
                                @Part("zip_code") RequestBody zip_code,//optional
                                @Part("user_info") RequestBody user_info); //optional

    //add and edit little one
    @Multipart
    @POST("little_one_add_edit")
    Call<Object> addLittleOne(@Part("user_id") RequestBody user_id,
                              @Part("first_name") RequestBody first_name,
                              @Part("last_name") RequestBody last_name,
                              @Part("birthday_date") RequestBody birthday_date,
                              @Part("little_one_id") RequestBody little_one_id,
                              @Part("top") RequestBody top,
                              @Part("bottom") RequestBody bottom,
                              @Part("shoes") RequestBody shoes,
                              @Part MultipartBody.Part profile_picture,//optional
                              @Part("user_info") RequestBody user_info);//Optional

    //"Date Format=YYYY-MM-DD (Ex. 2020-01-30)
    //phone format= (XXX) XXX-XXXX
    //profile_picture format: jpg|jpeg|png , Not more than 1 mb"

    @GET("little_one_data/{id}")
    Call<Object> getLittleOneData(@Path("id") String id);

    //add and edit little one
//    @FormUrlEncoded
//    @POST("little_one_add_edit")
//    Call<Object> addLittleOne(@Field("user_id") String user_id,
//                           @Field("first_name") String first_name,
//                           @Field("last_name") String last_name,
//                           @Field("birthday_date") String birthday_date,
//                           @Field("little_one_id") String little_one_id,
//                           @Field("top") String top,
//                           @Field("bottom") String bottom,
//                           @Field("shoes") String shoes,
//                           @Field("user_info") String user_info);//Optional


    @FormUrlEncoded
    @POST("little_one_list")
    Call<Object> getLittleOne(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("little_one_delete")
    Call<Object> deleteLittleOne(@Field("little_one_id") String little_one_id);


    @FormUrlEncoded
    @POST("my_connections")
    Call<Object> myConnections(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("remove_profile_picture")
    Call<Object> removeprofilePic(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("remove_profile_picture")
    Call<Object> removeprofilePicLittleOne(@Field("user_id") String user_id,
                                           @Field("user_type") String user_type);


    @FormUrlEncoded
    @POST("little_one_list")
    Call<Object> littleOneList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("wishlist_add_edit")
    Call<Object> addEditWishlist(@Field("user_id") String user_id,
                                 @Field("name") String name,
                                 @Field("wish_for_user_type") String wish_for_user_type,
                                 @Field("event_date") String event_date,
                                 @Field("type") String type,
                                 @Field("wishlist_id") String wishlist_id, //optional
                                 @Field("invites") String invites);//optional

    //Friend request send/ Accept/ Reject
    @FormUrlEncoded
    @POST("friend_status_update")
    Call<Object> friendRequest(@Field("user_id") String user_id,
                                 @Field("friend_id") String friend_id,
                                 @Field("status") String status);

    //"status: 0 / 1 / 2
    //0 - Send request, 1 - accept request, 2 -reject requst, 3 - Remove connection"

    //contact list
    @FormUrlEncoded
    @POST("user_list")
    Call<Object> findFriends(@Field("user_id") String user_id,
                               @Field("contacts") String contacts);


    //item_add
    @Multipart
    @POST("item_add")
    Call<Object> addItem(@Part("user_id") RequestBody user_id,
                             @Part("name") RequestBody name,
                             @Part("brand") RequestBody brand,
                             @Part("price") RequestBody price,
                             @Part("url") RequestBody url,
                             @Part("size") RequestBody size,
                             @Part("wishlist_id") RequestBody wishlist_id,
                             @Part("where_to_buy") RequestBody where_to_buy,
                             @Part("description") RequestBody description,
                             @Part MultipartBody.Part image);

    //item_list
    @FormUrlEncoded
    @POST("item_list")
    Call<Object> getItemList(@Field("user_id") String user_id,
                             @Field("wishlist_id") String wishlist_id,
                             @Field("page_number") String page_number);

    //item detail
    @FormUrlEncoded
    @POST("item")
    Call<Object> getItemDetail(@Field("item_id") String item_id, @Field("user_id") String user_id);

    //wishlist
    @FormUrlEncoded
    @POST("wishlist")
    Call<Object> getWishlists(@Field("user_id") String user_id);

    //item_remove_image
    @FormUrlEncoded
    @POST("item_remove_image")
    Call<Object> removeItemProfile(@Field("item_id") String item_id);

    //item_edit
    @Multipart
    @POST("item_edit")
    Call<Object> editItem(@Part("item_id") RequestBody item_id,
                         @Part("name") RequestBody name,
                         @Part("brand") RequestBody brand,
                         @Part("price") RequestBody price,
                         @Part("url") RequestBody url,
                         @Part("size") RequestBody size,
                         @Part("wishlist_id") RequestBody wishlist_id,
                         @Part("where_to_buy") RequestBody where_to_buy,
                         @Part("description") RequestBody description,
                         @Part MultipartBody.Part image);

    //gift_feed
    @FormUrlEncoded
    @POST("gift_feed")
    Call<Object> getGiftFeeds(@Field("user_id") String user_id,
                              @Field("page_number") String page_number);

    //mark_purchased
    @FormUrlEncoded
    @POST("mark_purchased")
    Call<Object> markAsPurchased(@Field("user_id") String user_id,
                              @Field("item_id") String item_id,
                              @Field("purchase_date") String purchase_date);


    //notifications
    @FormUrlEncoded
    @POST("notifications")
    Call<Object> notification(@Field("user_id") String user_id);

    //notifications_list type wise
    @FormUrlEncoded
    @POST("notifications_list")
    Call<Object> notificationListTypeWise(@Field("user_id") String user_id,
                              @Field("page_number") String page_number,
                              @Field("list_type") String list_type);
    //list_type:notifications/friend_requests/wish_list/purchased_list"

    //user_profile
    @FormUrlEncoded
    @POST("user_profile")
    Call<Object> getUserProfile(@Field("user_id") String user_id);

    //little_one_profile
    @FormUrlEncoded
    @POST("little_one_profile")
    Call<Object> getLittleOneProfile(@Field("little_one_id") String little_one_id);

    //faq
    @GET("faq")
    Call<Object> getFaq();

    //social_check_exists
    @FormUrlEncoded
    @POST("social_check_exists")
    Call<Object> checkSocialLoginExist(@Field("email") String email,
                                       @Field("device_token") String device_token,
                                       @Field("device_type") String device_type);

    //social_login
    @FormUrlEncoded
    @POST("social_login")
    Call<Object> submitSocialLogin(@Field("email") String email,
                                       @Field("device_token") String device_token,
                                       @Field("device_type") String device_type,
                                   @Field("full_name") String full_name,
                                   @Field("phone") String phone,
                                   @Field("social_id") String social_id,
                                   @Field("social_type") String social_type,
                                   @Field("zip_code") String zip_code,
                                   @Field("birthday_date") String birthday_date);

    //Ex: social_type : 1 - facebook , 2-google"

    //item_delete
    @FormUrlEncoded
    @POST("item_delete")
    Call<Object> deleteItem(@Field("item_id") String item_id);

    //wishlist_delete
    @FormUrlEncoded
    @POST("wishlist_delete")
    Call<Object> deleteWishlist(@Field("wishlist_id") String wishlist_id);

    //wishlist_delete
    @FormUrlEncoded
    @POST("connection_remove")
    Call<Object> removeConnection(@Field("user_id") String userId, @Field("friend_id") String friendId);


    //wishlist_delete
    @FormUrlEncoded
    @POST("check_connected")
    Call<Object> checkConnected(@Field("profile_user_id") String profileUserId, @Field("logged_in_user_id") String loggedInUserId);

    //get amazon item data
    @FormUrlEncoded
    @POST("amazon_item_data")
    Call<Object> getAmazonItemData(@Field("item_url") String itemUrl);
}