package com.app.mylineup.web_services;

import android.content.Context;

import com.alphabetik.BuildConfig;
import com.app.mylineup.R;
import com.app.mylineup.other.SharePref;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static Retrofit getClient(Context context, SharePref sharePref) {

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .addNetworkInterceptor(new NetworkInterceptor(context, sharePref))
                //.addInterceptor(new NetworkInterceptor(context, sharePref))
                .connectTimeout(60, TimeUnit.MINUTES)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        /*HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);*/

        //  OkHttpClient okHttpClient = builder.build();

        retrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.api_base_url))
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        return retrofit;
    }

}
