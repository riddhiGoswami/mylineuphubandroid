package com.app.mylineup.web_services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.app.mylineup.R;
import com.app.mylineup.activity.LoginActivity;
import com.app.mylineup.application.Application;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okio.Buffer;
import retrofit2.Call;

public class PrettyPrinter {
    private static final Gson gson = new GsonBuilder()
            .setPrettyPrinting().create();

    public static String print(Object o) {
        return gson.toJson(o);
    }

    public static String print(Call<Object> call) {
        Buffer buffer = new Buffer();
        try {
            call.request().body().writeTo(buffer);
            //Log.e("TAG","buffer.readUtf8() = "+buffer.readUtf8());
            return buffer.readUtf8();
        } catch (IOException e) {
            Log.e("TAG", "e = " + e.getMessage());
            return e.getLocalizedMessage();
        }
    }

    public static void checkStatusCode(int code, AlertDialogCommon dialogCommon, Context context, Application application) {
        if (code == 403) {
            dialogCommon.showDialog(context.getString(R.string.app_name), context.getString(R.string.session_expire), context.getString(R.string.ok), "", true, false, new AlertDialogCommon.CallBackClickListener() {
                @Override
                public void OnDialogPositiveBtn() {
                    application.getSharedPref().saveSession(Constant.IS_LOGIN, "");
                    application.getSharedPref().clearAll();
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }

                @Override
                public void OnDialogNegativeBtn() {

                }
            });
        }
    }

}
