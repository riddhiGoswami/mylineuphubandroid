package com.app.mylineup.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.adapter.UserWishlistAdapter;
import com.app.mylineup.databinding.FragmentUserWishlistBinding;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneWishlist;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.pojo.wishlists.Wishlist;
import com.app.mylineup.pojo.wishlists.Wishlists;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.youtube.player.YouTubePlayer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class UserWishlistFragment<videoViewDialog> extends BaseFragment /*implements VideoViewDialog.OnSkipClickListener*/ implements UserWishlistAdapter.ListChangeCallback {

    private FragmentUserWishlistBinding binding;
    public static String CALLBACK_TAG = UserWishlistFragment.class.getSimpleName();
    private UserWishlistAdapter wishlistAdapter;
    private boolean isMyAcc;

    private List<Wishlist> list = new ArrayList<>();
    private List<LittleOneWishlist> littleOneWishlist = new ArrayList<>();

    private TabLayout tabLayout;
    private UserProfile userProfile;
    private LittleOneProfile littleOneProfile;
    private static VideoViewDialog videoViewDialog;
    private ChangeProfileCallback changeProfileCallback;

    public UserWishlistFragment() {
    }

    @SuppressLint("ValidFragment")
    public UserWishlistFragment(boolean isMyAcc, TabLayout tabLayout, UserProfile userProfile, ChangeProfileCallback changeProfileCallback) {
        this.isMyAcc = isMyAcc;
        this.tabLayout = tabLayout;
        this.userProfile = userProfile;
        this.changeProfileCallback = changeProfileCallback;
    }

    public UserWishlistFragment(boolean isMyAcc, TabLayout tabLayout, LittleOneProfile littleOneProfile, ChangeProfileCallback changeProfileCallback) {
        this.isMyAcc = isMyAcc;
        this.tabLayout = tabLayout;
        this.littleOneProfile = littleOneProfile;
        this.changeProfileCallback = changeProfileCallback;
    }

    public static YouTubePlayer handleVideoDialog() {
        if (videoViewDialog != null) {
            return videoViewDialog.hideFullScreen();
        } else {
            return null;
        }
    }

    @Override
    protected String getCallbackTag() {
        return CALLBACK_TAG;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_wishlist, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvUserWishlist.setLayoutManager(layoutManager);
        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
        String userId = loginData.getUser().getId();
        Log.w("isSameUser", "userId => " + userId);


        if (userProfile != null) {
            wishlistAdapter = new UserWishlistAdapter(getActivity(), list, isMyAcc, userProfile.getProfile().getName(), application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE), userId, this);
        } else if (littleOneProfile != null) {
            wishlistAdapter = new UserWishlistAdapter(littleOneWishlist, getActivity(), isMyAcc, littleOneProfile.getProfile().getName(), application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE), userId, this);
        }
        binding.rvUserWishlist.setAdapter(wishlistAdapter);
        binding.rvUserWishlist.setNestedScrollingEnabled(false);

        if (isMyAcc) {
            binding.llNewWishList.setVisibility(View.VISIBLE);
            binding.llNewWishList.setOnClickListener(view ->
            {
                if (!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {

                    // if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
                    application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
                    // }

                    Intent intentI = new Intent(getActivity(), CreateWishListActivity.class);
                    intentI.putExtra(Constant.FROM, UserWishlistFragment.class.getName());
                    /*startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

                    Intent intent = new Intent(getActivity(), CustomVideoDialog.class);
                    intent.putExtra("videoType", 1);
                    intent.putExtra("isSkipDisplay", true);
                    intent.putExtra("passIntent", intentI);
                    //  intent.putExtra("onSkipListener", (Parcelable) this);
                    startActivity(intent);

                    /*videoViewDialog = new VideoViewDialog(1, this, true);
                    videoViewDialog.show(getChildFragmentManager(),  "Share Download Image Bottom Sheet");*/

                } else {
                    Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
                    intent.putExtra(Constant.FROM, UserWishlistFragment.class.getName());
                    startActivity(intent);

                    getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }


            });
        }

        if (userProfile != null) {
            if (userProfile.getWishlist() != null && userProfile.getWishlist().size() > 0) {
                list.addAll(userProfile.getWishlist());
                wishlistAdapter.notifyDataSetChanged();

                tabLayout.getTabAt(0).setText("Wishlists (" + list.size() + ")");
            } else {
                binding.ivNoData.setVisibility(View.VISIBLE);
            }
        }

        if (littleOneProfile != null) {
            Log.w("littleOneProfile.getLittleOneWishlist()", "" + littleOneProfile.getLittleOneWishlist().size());
            if (littleOneProfile.getLittleOneWishlist() != null && littleOneProfile.getLittleOneWishlist().size() > 0) {
                littleOneWishlist.addAll(littleOneProfile.getLittleOneWishlist());
                wishlistAdapter.notifyDataSetChanged();
                Log.w("littleOneWishlist.size()", "" + littleOneWishlist.size());
                tabLayout.getTabAt(0).setText("Wishlists (" + littleOneWishlist.size() + ")");
            } else {
                binding.ivNoData.setVisibility(View.VISIBLE);
            }
        }

        //getWishlistApiCall();
    }

    public void hideShowNoData(boolean isShow) {
        if (isShow) {
            binding.ivNoData.setVisibility(View.VISIBLE);
        } else {
            binding.ivNoData.setVisibility(View.GONE);
        }
    }

    public void setCount() {
        Log.w("insideCountSet", "...........");

        if (isMyAcc) {
            Log.w("onListChange", "User Profile..." + list.size());
            if (list != null && list.size() > 0) {
                tabLayout.getTabAt(0).setText("Wishlists (" + list.size() + ")");
            } else {
                tabLayout.getTabAt(0).setText("Wishlists (0)");
            }
        } else {
            if (userProfile != null) {
                Log.w("onListChange", "User Profile..." + list.size());
                if (list != null && list.size() > 0) {
                    tabLayout.getTabAt(0).setText("Wishlists (" + list.size() + ")");
                } else {
                    tabLayout.getTabAt(0).setText("Wishlists (0)");
                }
            }
            if (littleOneProfile != null) {
                Log.w("onListChange", "little one Profile..." + littleOneWishlist.size());
                if (littleOneWishlist != null && littleOneWishlist.size() > 0) {
                    tabLayout.getTabAt(0).setText("Wishlists (" + littleOneWishlist.size() + ")");
                } else {
                    tabLayout.getTabAt(0).setText("Wishlists (0)");
                }
            }
        }
    }

    private void getWishlistApiCall() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getWishlists(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, getActivity(), application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Wishlists wishlists = gson.fromJson(x, Wishlists.class);
                        if (wishlists != null && wishlists.getWishlist() != null) {
                            list.addAll(wishlists.getWishlist());
                            wishlistAdapter.notifyDataSetChanged();

                            tabLayout.getTabAt(0).setText("Wishlists (" + list.size() + ")");

                            if (binding.ivNoData.getVisibility() == View.VISIBLE) {
                                binding.ivNoData.setVisibility(View.GONE);
                            }
                        } else {
                            binding.ivNoData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                        binding.ivNoData.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Log.e("TAG", "Error = " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onListChange(String madeForID) {
        Log.w("onListChange", "User Profile...");
        changeProfileCallback.callProfile(madeForID);
        if (userProfile != null) {
            Log.w("onListChange", "User Profile..." + list.size());
            if (list.isEmpty()) {
                binding.ivNoData.setVisibility(View.VISIBLE);
            }
        } else if (littleOneProfile != null) {
            Log.w("onListChange", "LittleOne Profile..." + littleOneWishlist.size());
            if (littleOneWishlist.isEmpty()) {
                binding.ivNoData.setVisibility(View.VISIBLE);
            }
        }
    }

    public interface ChangeProfileCallback {
        public void callProfile(String madeForID);
    }


   /* @Override
    public void onSkipClick(int videoType) {

        if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
        }

        Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
        intent.putExtra(Constant.FROM, UserWishlistFragment.class.getName());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }*/
}
