package com.app.mylineup.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.mylineup.R;
import com.app.mylineup.activity.HomeActivity;
import com.app.mylineup.adapter.WishListAdapter;
import com.app.mylineup.databinding.FragmentItemListBinding;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.wishlistItems.Item;
import com.app.mylineup.pojo.wishlistItems.WishlistItems;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistItemsFragment extends BaseFragment implements OnLoadmoreListener
{
    public static String CALLBACK_TAG = WishlistItemsFragment.class.getSimpleName();
    private FragmentItemListBinding binding;
    private WishListAdapter wishListAdapter;
    private WishlistItems wishlistItems;
    private List<Item> itemList = new ArrayList<>();

    private MyWishlistOtItemChange myWishlistOtItemChange;

    private int page = 1;
    private boolean isMyAcc = true;

    @Override
    protected String getCallbackTag() {
        return CALLBACK_TAG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.llTopNewWishList.setVisibility(View.GONE);

        binding.llNewWishList.setVisibility(View.GONE);

        binding.rvWishList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        wishListAdapter = new WishListAdapter(context, itemList,Constant.FROM_WISHLIST,isMyAcc,binding.rvWishList,this);
        binding.rvWishList.setAdapter(wishListAdapter);

        page = 1;
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
        getAllItemsApiCall();

        registerReceiver1();


        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshListener();
            }
        });

        binding.tvTapToAdd.setOnClickListener(v -> {
            if (getActivity()!=null && !getActivity().isFinishing() && getActivity() instanceof HomeActivity) {
                ((HomeActivity) getActivity()).callMethod();
            }
        });

    }

    private void onRefreshListener() {
        binding.swipeRefresh.setRefreshing(true);

        wishListAdapter.setLoaded(true);
        itemList.clear();
        wishListAdapter.notifyDataSetChanged();

        page = 1;
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
        getAllItemsApiCall();
    }

    private void registerReceiver1() {

        myWishlistOtItemChange = new MyWishlistOtItemChange();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.addChangeInData");
        getActivity().registerReceiver(myWishlistOtItemChange, filter);
    }

    @Override
    public void onResume() {
        super.onResume();
//        onRefreshListener();
    }

    private void getAllItemsApiCall() {

        if(!binding.swipeRefresh.isRefreshing()) {
            loaderDialog.show();
        }
        Call<Object> call = application.getApis().getItemList(loginData.getUser().getId(),null,page+"");
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));
        //itemList.clear();

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","getAllItemsApiCall response = "+x);
                loaderDialog.dismiss();
                if(binding.swipeRefresh.isRefreshing()) {
                    binding.swipeRefresh.setRefreshing(false);
                }
                itemList.clear();
                wishListAdapter.notifyDataSetChanged();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,getActivity(),application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        wishlistItems = gson.fromJson(x, WishlistItems.class);
                        if(wishlistItems != null && wishlistItems.getItems() != null){
//                            Item i = new Item();
//                            itemList.add(i);
                            itemList.addAll(wishlistItems.getItems());
                            //wishListAdapter.notifyDataSetChanged();
                            //wishListAdapter.setList(itemList);
                            wishListAdapter = new WishListAdapter(context, itemList,Constant.FROM_WISHLIST,isMyAcc,binding.rvWishList,WishlistItemsFragment.this);
                            binding.rvWishList.setAdapter(wishListAdapter);

                            if (!itemList.isEmpty()) {
                                binding.rvWishList.setVisibility(View.VISIBLE);
                                binding.llNoData.setVisibility(View.GONE);
                            }
                            else {
//                                binding.ivNoData.setImageResource(R.drawable.ic_no_wishlist);
                                binding.rvWishList.setVisibility(View.GONE);
                                binding.llNoData.setVisibility(View.VISIBLE);
                            }
                        }else {
//                            binding.ivNoData.setImageResource(R.drawable.ic_no_item);
                            binding.rvWishList.setVisibility(View.GONE);
                            binding.llNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        //dialogCommon.showDialog(jsonObject.getString("message"));
//                        binding.ivNoData.setImageResource(R.drawable.ic_no_item);
                        binding.rvWishList.setVisibility(View.GONE);
                        binding.llNoData.setVisibility(View.VISIBLE);
                    }
                }
                catch (Exception e)
                {
                    Log.e("TAG","error = "+e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onLoadMore() {

        itemList.add(null);
        wishListAdapter.notifyItemInserted(itemList.size() - 1);
        page = page+1;

        Call<Object> call = application.getApis().getItemList(loginData.getUser().getId(),null,page+"");
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","here API  onLoadMore response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,getActivity(),application);
                try {
                    itemList.remove(itemList.size()-1);
                    wishListAdapter.notifyItemRemoved(itemList.size());

                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status")) {

                        WishlistItems wishlistItems = gson.fromJson(x, WishlistItems.class);
                        if(wishlistItems.getStatus() && wishlistItems.getItems().size()>0)
                        {
                            for(int i = 0;i<wishlistItems.getItems().size();i++) {
                                itemList.add(wishlistItems.getItems().get(i));
                                wishListAdapter.notifyItemInserted(itemList.size());
                            }
                            wishListAdapter.setLoaded(false);
                        }
                        else {
                            wishListAdapter.setLoaded(true);
                        }
                    }
                    else
                    {
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private class MyWishlistOtItemChange extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("TAG","Receive");
            onRefreshListener();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(myWishlistOtItemChange);
    }
}
