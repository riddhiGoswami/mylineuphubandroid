package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.application.Application;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.LoaderDialog;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.loginData.LoginData;
import com.app.mylineup.pojo.userProfile.CheckConnected;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseFragment extends Fragment
{
    protected Context context;

    protected AlertDialogCommon dialogCommon;
    protected AlertListDialog listDialog;
    protected LoaderDialog loaderDialog;

    protected Application application;

    protected Gson gson;
    protected LoginData loginData;
    abstract protected String getCallbackTag();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LittleOnesGlobalCallbackSingleton.removeListener(getCallbackTag());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        gson = new Gson();
        dialogCommon = new AlertDialogCommon(context);
        loaderDialog = new LoaderDialog(context);
        listDialog = new AlertListDialog(context);

        application = Application.getInstance();
        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
    }

    protected void getCheckConnectedApi(String userId) {

        loaderDialog.show();

        Call<Object> call = application.getApis().checkConnected(userId,loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                loaderDialog.dismiss();
                String x = gson.toJson(response.body());
                Log.e("TAG","response here test = "+x);
                try {
                    if(x != null){
                        JSONObject jsonObject = new JSONObject(x);
                        if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            CheckConnected checkConnected = gson.fromJson(x, CheckConnected.class);
                            if(checkConnected.getStatus()){
                                Intent intent = new Intent(context, UserProfileActivity.class);
                                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                                intent.putExtra(Constant.USER_ID,checkConnected.getProfile_user_id());
                                ((Activity) context).startActivity(intent);
                                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        }
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }
}
