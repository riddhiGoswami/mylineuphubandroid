package com.app.mylineup.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.adapter.UserWishlistAdapter;
import com.app.mylineup.databinding.FragmentItemListBinding;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.pojo.WishlistImageBeen;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.wishlistItems.WishlistItems;
import com.app.mylineup.pojo.wishlists.Wishlist;
import com.app.mylineup.pojo.wishlists.Wishlists;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.youtube.player.YouTubePlayer;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistListFragment extends BaseFragment /*implements VideoViewDialog.OnSkipClickListener*/ implements UserWishlistAdapter.ListChangeCallback, UserWishlistFragment.ChangeProfileCallback {
    public static String CALLBACK_TAG = WishlistListFragment.class.getSimpleName();
    private FragmentItemListBinding binding;
    private UserWishlistAdapter wishlistAdapter;

    private MyWishlistOtItemChange myWishlistOtItemChange;
    private List<Wishlist> list = new ArrayList<>();
    private Wishlists wishlists;
    private static VideoViewDialog videoViewDialog;

    @Override
    protected String getCallbackTag() {
        return CALLBACK_TAG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        binding.llTopNewWishList.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvWishList.setLayoutManager(layoutManager);
        wishlistAdapter = new UserWishlistAdapter(getActivity(), list, true, loginData.getUser().getFirstName() + " " + loginData.getUser().getLastName(), application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE), loginData.getUser().getId(), this);
        //binding.rvWishList.setNestedScrollingEnabled(false);

        binding.rvWishList.setPadding(0, 0,
                0, 0);

        registerReceiver1();

        binding.rvWishList.setAdapter(wishlistAdapter);
        binding.llNewWishList.setVisibility(View.VISIBLE);

        binding.llNewWishList.setOnClickListener(view -> {

            Log.w("isWishlistSeen", "" + application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN));
            if (!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {

                //  if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
                //  }
                Intent intentI = new Intent(getActivity(), CreateWishListActivity.class);
                /*startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

                Intent intent = new Intent(getActivity(), CustomVideoDialog.class);
                intent.putExtra("videoType", 1);
                intent.putExtra("isSkipDisplay", true);
                intent.putExtra("passIntent", intentI);

                //  intent.putExtra("onSkipListener", (Parcelable) this);
                startActivity(intent);


                /*videoViewDialog = new VideoViewDialog(1, this, true);
                videoViewDialog.show(getChildFragmentManager(), "Share Download Image Bottom Sheet");*/
            } else {
                Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

        });

        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshListener();
            }
        });

        binding.tvTapToAdd.setOnClickListener(v -> {

            Log.w("isWishlistSeen", "" + application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN));
            if (!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {

                //  if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
                //  }
                Intent intentI = new Intent(getActivity(), CreateWishListActivity.class);
                /*startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

                Intent intent = new Intent(getActivity(), CustomVideoDialog.class);
                intent.putExtra("videoType", 1);
                intent.putExtra("isSkipDisplay", true);
                intent.putExtra("passIntent", intentI);

                //  intent.putExtra("onSkipListener", (Parcelable) this);
                startActivity(intent);


                /*videoViewDialog = new VideoViewDialog(1, this, true);
                videoViewDialog.show(getChildFragmentManager(), "Share Download Image Bottom Sheet");*/
            } else {
                Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
                startActivity(intent);

                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }

//            Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
//            //startActivity(intent);
//
//            Intent videoIntent = new Intent(getActivity(), VideoViewDialog.class);
//            videoIntent.putExtra("videoType", 1);
//            videoIntent.putExtra("intentToPass", intent);
//            startActivity(videoIntent);
//
//            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        getWishlistApiCall();

        LittleOnesGlobalCallbackSingleton.addListener(CALLBACK_TAG, new CallbackLittleOne() {
            @Override
            public void editLittleOne(String firstName, String lastName) {

            }

            @Override
            public void deleteLittleOne() {
                getWishlistApiCall();
            }

            @Override
            public void hideLittleOne() {

            }

            @Override
            public void addLittleOne(LittleOne littleOne) {

            }
        });

    }

    public static YouTubePlayer handleVideoDialog() {
        if (videoViewDialog != null) {
            return videoViewDialog.hideFullScreen();
        } else {
            return null;
        }

    }


    private void onRefreshListener() {
        binding.swipeRefresh.setRefreshing(true);
        list.clear();
        wishlistAdapter.notifyDataSetChanged();

        getWishlistApiCall();
    }

    private void getWishlistApiCall() {

        if (!binding.swipeRefresh.isRefreshing()) {
            loaderDialog.show();
        }
        list.clear();
        Call<Object> call = application.getApis().getWishlists(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                if (binding.swipeRefresh.isRefreshing()) {
                    binding.swipeRefresh.setRefreshing(false);
                }
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, getActivity(), application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        wishlists = gson.fromJson(x, Wishlists.class);
                        if (wishlists != null && wishlists.getWishlist() != null) {
//                            Wishlist wl = new Wishlist();
//                            wl.setType(0);
//                            list.add(wl);
                            list.addAll(wishlists.getWishlist());
                            wishlistAdapter.notifyDataSetChanged();
                            if (!list.isEmpty()) {
                                binding.rvWishList.setVisibility(View.VISIBLE);
                                binding.llNoData.setVisibility(View.GONE);
                            } else {
                                binding.ivNoData.setImageResource(R.drawable.nt_wishlist);
                                binding.tvNoData.setText(getString(R.string.new_wishlist));
                                binding.rvWishList.setVisibility(View.GONE);
                                binding.llNoData.setVisibility(View.VISIBLE);
                            }
                        } else {
                            binding.ivNoData.setImageResource(R.drawable.nt_wishlist);
                            binding.tvNoData.setText(getString(R.string.new_wishlist));
                            binding.llNoData.setVisibility(View.VISIBLE);
                            binding.rvWishList.setVisibility(View.GONE);
                        }
                    } else {
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                        binding.ivNoData.setImageResource(R.drawable.nt_wishlist);
                        binding.tvNoData.setText(getString(R.string.new_wishlist));
                        binding.llNoData.setVisibility(View.VISIBLE);
                        binding.rvWishList.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void registerReceiver1() {

        myWishlistOtItemChange = new MyWishlistOtItemChange();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.addChangeInData");
        getActivity().registerReceiver(myWishlistOtItemChange, filter);
    }

    @Override
    public void onListChange(String madeForID) {

    }

    @Override
    public void callProfile(String madeForID) {

    }

   /* @Override
    public void onSkipClick(int videoType) {

        if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
        }
        Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
        startActivity(intent);

        getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }*/

    private class MyWishlistOtItemChange extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            onRefreshListener();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(myWishlistOtItemChange);
    }
}
