package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.adapter.GiftAdapter;
import com.app.mylineup.adapter.UpComingOccasionAdapter;
import com.app.mylineup.databinding.FragmentGiftfeedBinding;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.giftFeeds.Feed;
import com.app.mylineup.pojo.giftFeeds.GiftFeeds;
import com.app.mylineup.pojo.giftFeeds.UpcomingOccasion;
import com.app.mylineup.pojo.userProfile.CheckConnected;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.web_services.PrettyPrinter;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GiftFeedFragment extends BaseFragment implements  OnItemClickGiftListener, OnLoadmoreListener
{
    private FragmentGiftfeedBinding binding;
    private UpComingOccasionAdapter upComingOccasionAdapter;
    private GiftAdapter giftAdapter;
    private GiftFeeds giftFeeds;
    private List<Feed> feedList = new ArrayList<>();
    private List<UpcomingOccasion> occasionList= new ArrayList<>();

    private int page = 1;
    private int lastVisibleItem, totalItemCount;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_giftfeed,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.rvUpcomingOccasion.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        upComingOccasionAdapter = new UpComingOccasionAdapter(context,occasionList, loginData.getUser().getId());
        binding.rvUpcomingOccasion.setAdapter(upComingOccasionAdapter);
        binding.rvUpcomingOccasion.setNestedScrollingEnabled(false);

        binding.rvGiftFeed.setLayoutManager(new LinearLayoutManager(context));
        giftAdapter = new GiftAdapter(context,feedList,this,binding.rvGiftFeed, this);
        binding.rvGiftFeed.setAdapter(giftAdapter);
        binding.rvGiftFeed.setNestedScrollingEnabled(false);

        page = 1;
        Log.w("loadFragments","giftfeed");
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else {
            getUserProfile();
            Log.w("calledFrom","Line => 91");
            getGiftFeedApi();
        }

        binding.ivAccSetting.setOnClickListener(v -> {
//            Intent intent = new Intent(context, UserProfileActivity.class);
//            intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
//            startActivity(intent);
//            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

            Intent intent = new Intent(context, AccountSettingActivity.class);
            startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshCall();
            }
        });
    }

    private void onRefreshCall() {

        feedList.clear();
        giftAdapter.notifyDataSetChanged();

        occasionList.clear();
        upComingOccasionAdapter.notifyDataSetChanged();

        binding.swipeRefresh.setRefreshing(true);
        page = 1;
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else {
            Log.w("calledFrom","Line => 127");
            getGiftFeedApi();
        }
    }

    private void getGiftFeedApi() {

        if(!binding.swipeRefresh.isRefreshing()) {
            loaderDialog.show();
        }

        Call<Object> call = application.getApis().getGiftFeeds(loginData.getUser().getId(),page+"");
        Log.e("TAG","initial url = "+call.request().url());
        Log.e("TAG","initial param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
               /* occasionList.clear();
                feedList.clear();*/
                String x = gson.toJson(response.body());
                Log.e("TAG","getGiftFeedApi response = "+x);
                loaderDialog.dismiss();
                if(binding.swipeRefresh.isRefreshing()) {
                    binding.swipeRefresh.setRefreshing(false);
                }

                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,getActivity(),application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        giftFeeds = gson.fromJson(x, GiftFeeds.class);
                        if(giftFeeds != null && giftFeeds.getUpcomingOccasions() != null && giftFeeds.getUpcomingOccasions().size()>0){
                            occasionList.addAll(giftFeeds.getUpcomingOccasions());
                            upComingOccasionAdapter.notifyDataSetChanged();
                            if(binding.llUpcomingEvent.getVisibility() == View.GONE){
                                binding.llUpcomingEvent.setVisibility(View.VISIBLE);
                            }
                        }else {
                            binding.llUpcomingEvent.setVisibility(View.GONE);
                        }

                        if(giftFeeds != null && giftFeeds.getFeed() != null && giftFeeds.getFeed().size()>0){
                            feedList.addAll(giftFeeds.getFeed());
                            giftAdapter.notifyDataSetChanged();
                            binding.llGiftfeedList.setVisibility(View.VISIBLE);
                            if(binding.llNoData.getVisibility() == View.VISIBLE){
                                binding.llNoData.setVisibility(View.GONE);
                            }
                        }else {
                            binding.llNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                        binding.llNoData.setVisibility(View.VISIBLE);
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onLoadMore() {

        page = page+1;
        feedList.add(null);
        giftAdapter.notifyItemInserted(feedList.size() - 1);

        Call<Object> call = application.getApis().getGiftFeeds(loginData.getUser().getId(),page+"");
        Log.e("TAG","onLoadMore url = "+call.request().url());
        Log.e("TAG","onLoadMore param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "onLoadMore response = " + x);
                loaderDialog.dismiss();

                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, getActivity(), application);
                try {
                    feedList.remove(feedList.size() - 1);
                    giftAdapter.notifyItemRemoved(feedList.size());

                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        GiftFeeds giftFeedsTemp = gson.fromJson(x, GiftFeeds.class);

                        if (giftFeedsTemp != null && giftFeedsTemp.getFeed() != null && giftFeedsTemp.getFeed().size()>0) {
                            for (int i = 0; i < giftFeedsTemp.getFeed().size(); i++) {
                                feedList.add(giftFeedsTemp.getFeed().get(i));
                                giftAdapter.notifyItemInserted(feedList.size());
                            }
                            giftAdapter.setLoaded(false);
                        }else {
                            giftAdapter.setLoaded(true);
                        }
                    } else {
                        giftAdapter.setLoaded(true);
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.e("TAG","hidden = "+hidden);
        if(!hidden){
            onRefreshCall();
        }
    }

    private void getUserProfile() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getUserProfile(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","getUserProfile response = "+x);
                try {
                    if(x != null){
                        JSONObject jsonObject = new JSONObject(x);
                        if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            UserProfile userProfile = gson.fromJson(x,UserProfile.class);
                            if(Global.isValidString(userProfile.getProfile().getProfilePicture()))
                            { Picasso.get().load(userProfile.getProfile().getProfilePicture()).into(binding.ivAccSetting);}
                        }
//                        else
//                        {
//                            dialogCommon.showDialog(jsonObject.getString("message"));
//                        }
                    }
                    //{"profile":{"profile_picture":"https://www.mylineuphub.com/assets/images/default.png","name":"SAURAVKUMAR THAKKAR"},"status":false,"message":"No data found"}
                }
                catch (Exception e)
                {}
                finally {
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(),dialogCommon,getActivity(),application);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    public void getCheckConnectedApi(String userId) {

        loaderDialog.show();

        Call<Object> call = application.getApis().checkConnected(userId,loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                loaderDialog.dismiss();
                String x = gson.toJson(response.body());
                Log.e("TAG","getCheckConnectedApi response = "+x);
                try {
                    if(x != null){
                        JSONObject jsonObject = new JSONObject(x);
                        if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            CheckConnected checkConnected = gson.fromJson(x, CheckConnected.class);
                                if(checkConnected.getStatus()){
                                    Intent intent = new Intent(context, UserProfileActivity.class);
                                    intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                                    intent.putExtra(Constant.USER_ID,checkConnected.getProfile_user_id());
                                    ((Activity) context).startActivity(intent);
                                    ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                        }
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }


    public void setValue() {
        loginData =  application.getSharedPref().getUserSession(Constant.LOGIN_DATA);

        Log.w("getProfilePicture()",""+loginData.getUser().getProfilePicture());
        if (Global.isValidString(loginData.getUser().getProfilePicture())) {
            Picasso.get().load(loginData.getUser().getProfilePicture()).into(binding.ivAccSetting);
        }
    }

    @Override
    public void onClick(String profileId) {
        getCheckConnectedApi(profileId);
    }
}
