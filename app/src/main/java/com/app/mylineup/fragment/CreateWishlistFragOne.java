package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.airbnb.lottie.L;
import com.app.mylineup.R;
import com.app.mylineup.activity.CreateLittleOneActivity;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.adapter.CustomSpinner;
import com.app.mylineup.databinding.FragmentWishlistOneBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.CallbackWishList;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.interfaces.WishListSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.connections.Connection;
import com.app.mylineup.pojo.littleOnes.LittleOne;
import com.app.mylineup.pojo.littleOnes.LittleOnes;
import com.app.mylineup.pojo.loginData.Wishlist;
import com.app.mylineup.pojo.wishlistItems.Wishlist_;
import com.app.mylineup.singleton.SelectedLittleOneId;
import com.app.mylineup.singleton.WishlistCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.youtube.player.YouTubePlayer;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class CreateWishlistFragOne extends BaseFragment /*implements VideoViewDialog.OnSkipClickListener*/ {
    private FragmentWishlistOneBinding binding;
    private String wishlistType = "public";
    private String madeForID = "";
    private List<LittleOne> littleOnes = new ArrayList<>();
    private CustomSpinner customSpinner;
    private String eventDate = "";
    private String wishlistId = null;
    private List<Connection> connectionList;
    private boolean isFinistActivity = false, isBroadCast = false, isEditWishList = false;
    private Wishlist_ wishlist;
    private static VideoViewDialog videoViewDialog;
    private boolean isAddLittleOne = false;
    private CallbackWishList callbackWishList;
    private String littleOneAddedId = "";
    private boolean isChangesLittleOne = false;

    public CreateWishlistFragOne(boolean isFinistActivity, boolean isBroadCast, boolean isEditWishList, Wishlist_ wishlist) {
        this.isFinistActivity = isFinistActivity;
        this.isBroadCast = isBroadCast;
        this.isEditWishList = isEditWishList;
        this.wishlist = wishlist;
    }

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wishlist_one, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        isAddLittleOne = false;
        littleOneAddedId = "";
        isChangesLittleOne = false;
        customSpinner = new CustomSpinner(getActivity(), android.R.layout.simple_spinner_dropdown_item, littleOnes, binding.spinnerMadeFor);
        binding.spinnerMadeFor.setAdapter(customSpinner);
        binding.spinnerMadeFor.setOnItemSelectedListener(listener);

        callbackWishList = WishListSingleton.getCallBack();


        binding.etWishListName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.tvTextCount.setText(s.length() + "/120");
            }
        });

        binding.tvDate.setOnClickListener(v ->
        {
            Log.w("onClickHere", "" + binding.tvDate.getText().toString().trim());
            String str = "";
            if (!binding.tvDate.getText().toString().trim().equalsIgnoreCase("")) {
                str = binding.tvDate.getText().toString().trim();
            }
            Global.openDatePicker(getActivity(), str, false, true, true, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    eventDate = sdf.format(myCalendar.getTime());
                    Log.e("TAG", "eventDate = " + eventDate);

                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    //int position = p.indexOf(',');
                    //String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    binding.tvDate.setText(p);
                    binding.ivDeleteDate.setVisibility(View.VISIBLE);
                }
            });
        });


        binding.llPublic.setOnClickListener(v ->
        {

            wishlistType = "public";

            binding.tvPublic.setTextColor(getResources().getColor(R.color.lightPink));
            binding.ivPublic.setImageDrawable(getResources().getDrawable(R.drawable.ic_public_selected));
            binding.llPublic.setBackgroundResource(R.drawable.bg_public_private_selected);

            binding.tvPrivate.setTextColor(getResources().getColor(R.color.textColor));
            binding.ivPrivate.setImageDrawable(getResources().getDrawable(R.drawable.ic_private_icon));
            binding.llPrivate.setBackgroundResource(0);

            LinearLayout tabStrip = ((LinearLayout) CreateWishListActivity.binding.tabLayout.getChildAt(0));
            for (int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
            }
        });

        binding.llPrivate.setOnClickListener(v ->
        {

            wishlistType = "private";

            binding.tvPrivate.setTextColor(getResources().getColor(R.color.lightPink));
            binding.ivPrivate.setImageDrawable(getResources().getDrawable(R.drawable.ic_private_icon_selected));
            binding.llPrivate.setBackgroundResource(R.drawable.bg_public_private_selected);

            binding.tvPublic.setTextColor(getResources().getColor(R.color.textColor));
            binding.ivPublic.setImageDrawable(getResources().getDrawable(R.drawable.ic_public));
            binding.llPublic.setBackgroundResource(0);

            LinearLayout tabStrip = ((LinearLayout) CreateWishListActivity.binding.tabLayout.getChildAt(0));
            for (int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return false;
                    }
                });
            }

        });

        binding.tvCreateWishlist.setOnClickListener(view ->
        {
            if (!Global.isValidString(binding.etWishListName.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_wishlist_name));
            } else if (madeForID.equalsIgnoreCase("")) {
                dialogCommon.showDialog(getString(R.string.please_select_wishlist_made_for));
            }
//            else if(!Global.isValidString(binding.tvDate.getText().toString()))
//            {dialogCommon.showDialog(getString(R.string.please_select_occasion_date));}
            else {
                String status = NetworkUtil.getConnectivityStatusString(getActivity());
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    callCreateWishlistApi();
            }

        });

        binding.ivDeleteDate.setOnClickListener(view -> {
            binding.tvDate.setText("");
            eventDate = "";
            binding.ivDeleteDate.setVisibility(View.GONE);
        });

        if (isEditWishList) {
            binding.tvCreateWishlist.setText(getString(R.string.save_wishlist));
            binding.etWishListName.setText(wishlist.getName());
            //binding.tvWishlistTitle.setText(getString(R.string.lbl_edit_your_wishlist));
            binding.llCreateWishlistDesc.setVisibility(View.GONE);
            wishlistId = wishlist.getId();
            eventDate = wishlist.getEventDate();

            String inputPattern = "yyyy-MM-dd";
            String outputPattern = "MMMM dd, yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            if (wishlist.getEventDate() != null) {
                binding.ivDeleteDate.setVisibility(View.VISIBLE);
                try {
                    Log.w("wishlist.getEventDate()", "" + wishlist.getEventDate());
                    date = inputFormat.parse(wishlist.getEventDate());
                    str = outputFormat.format(date);
                    binding.tvDate.setText(str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                binding.ivDeleteDate.setVisibility(View.GONE);
            }
        }
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            callLittleOneApi();
    }

    public void callCreateWishlistApi() {
        loaderDialog.show();

        String privateListId = null;
        if (wishlistType.equalsIgnoreCase("private") && connectionList != null) {
            for (int i = 0; i < connectionList.size(); i++) {
                if (connectionList.get(i).isSelected()) {
                    if (privateListId == null) {
                        privateListId = connectionList.get(i).getId();
                    } else {
                        privateListId = privateListId + "," + connectionList.get(i).getId();
                    }

                }
            }
        }
        Log.e("TAG", "privateListId = " + privateListId);
        /*if(privateListId == null)
        {privateListId = loginData.getUser().getId();}*/
        String wihlistname = binding.etWishListName.getText().toString().trim();

        Call<Object> call = application.getApis().addEditWishlist(madeForID,
                binding.etWishListName.getText().toString().trim(),
                loginData.getUser().getId().equalsIgnoreCase(madeForID) ? "1" : "2",
                eventDate,
                wishlistType.equalsIgnoreCase("private") ? "1" : "0",
                wishlistId,
                privateListId);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, context, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Intent intent = new Intent("android.addChangeInData");
                        getActivity().sendBroadcast(intent);

                        if (isBroadCast) {
                            Intent intent1 = new Intent("android.addChangeInWishList");
                            getActivity().sendBroadcast(intent1);
                        }

                        String wishlistName = binding.etWishListName.getText().toString().trim();
                        String eventDate1 = eventDate;
                        binding.etWishListName.setText("");
                        eventDate = "";
                        binding.tvDate.setText("");
                        String wishlistId = (int) Double.parseDouble(jsonObject.getString("wishlist_id")) + "";

                        Log.e("isEditWishList", "" + isEditWishList);
                        if (!isEditWishList) {
                            Wishlist wishlist = new Wishlist();
                            wishlist.setId(wishlistId);
                            wishlist.setName(wishlistName);
                            loginData.getWishlist().add(wishlist);
                            application.getSharedPref().saveSession(Constant.LOGIN_DATA, gson.toJson(loginData));
                            if(isChangesLittleOne){
                                WishlistCallbackSingleton.notifyAllListeners();
                            }
                        } else {
                            WishlistCallbackSingleton.notifyAllListeners();
                            if (callbackWishList != null) {
                                callbackWishList.editWishlist(wihlistname, madeForID);
                            }

                            if (loginData.getWishlist() != null) {
                                for (int i = 0; i < loginData.getWishlist().size(); i++) {
                                    if (loginData.getWishlist().get(i).getId().equalsIgnoreCase(wishlistId)) {
                                        loginData.getWishlist().get(i).setName(wishlistName);
                                        break;
                                    }
                                }
                                application.getSharedPref().saveSession(Constant.LOGIN_DATA, gson.toJson(loginData));
                            }
                        }
                        Log.w("wishlistName", "" + wishlistName);

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                Log.w("isFinistActivity", "" + isFinistActivity);
                                if (isFinistActivity) {
                                    if (isEditWishList) {
                                        WishlistActivity.isFromEditWishlist = true;
                                        WishlistActivity.afterEditName = wishlistName;
                                    }

                                    Intent intent = new Intent();
                                    intent.putExtra("NAME", wishlistName);
                                    intent.putExtra("DATE", eventDate1);
                                    intent.putExtra("wish_for", loginData.getUser().getId().equalsIgnoreCase(madeForID) ? "1" : "2");
                                    intent.putExtra("little_one_id", madeForID);
                                    getActivity().setResult(RESULT_OK, intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                                } else {

                                    Intent intent = new Intent(getActivity(), WishlistActivity.class);
                                    intent.putExtra(Constant.WISHLIST_ID, wishlistId);
                                    intent.putExtra(Constant.WISHLIST_NAME, wishlistName);
                                    startActivity(intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.w("onResumeCalled", "11111....");
        callLittleOneApi();
    }

    private void callLittleOneApi() {
        loaderDialog.show();
        Call<Object> call = application.getApis().littleOneList(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                littleOnes.clear();
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, context, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        LittleOnes littleOne = gson.fromJson(x, LittleOnes.class);

                        LittleOne littleOne1 = new LittleOne();
                        littleOne1.setFirstName(loginData.getUser().getFirstName());
                        littleOne1.setLastName(loginData.getUser().getLastName() + " (Me)");
                        littleOne1.setId(loginData.getUser().getId());
                        littleOnes.add(littleOne1);

                        littleOnes.addAll(littleOne.getLittleOne());

                        LittleOne littleOne2 = new LittleOne();
                        littleOne2.setFirstName("Create Little One");
                        littleOne2.setLastName("");
                        littleOne2.setId("");
                        littleOnes.add(littleOne2);

                        customSpinner.notifyDataSetChanged();
                    } else {
                        LittleOne littleOne1 = new LittleOne();
                        littleOne1.setFirstName(loginData.getUser().getFirstName());
                        littleOne1.setLastName(loginData.getUser().getLastName() + " (Me)");
                        littleOne1.setId(loginData.getUser().getId());
                        littleOnes.add(littleOne1);

                        LittleOne littleOne2 = new LittleOne();
                        littleOne2.setFirstName("Create Little One");
                        littleOne2.setLastName("");
                        littleOne2.setId("");
                        littleOnes.add(littleOne2);

                        customSpinner.notifyDataSetChanged();
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                    }

                    Log.w("getLittleOneId","here => "+SelectedLittleOneId.littleOneId + " isEditWishList=> "+isEditWishList + "   isAddLittleOne=> "+isAddLittleOne);
                    for (int i = 0; i < littleOnes.size(); i++) {
                        if (SelectedLittleOneId.littleOneId != null  && !SelectedLittleOneId.littleOneId.equalsIgnoreCase("") && littleOnes.get(i).getId().equalsIgnoreCase(SelectedLittleOneId.littleOneId)) {
                            Log.w("setSelection","Line => 461");
                            binding.spinnerMadeFor.setSelection(i);
                            break;
                        }
                    }

                    if (isEditWishList) {
                        if (wishlist != null) {
                            if (wishlist.getWishFor().equalsIgnoreCase("2")) {
                                for (int i = 0; i < littleOnes.size(); i++) {
                                    if (littleOnes.get(i).getId().equalsIgnoreCase(wishlist.getLittleOneId())) {
                                        Log.w("setSelection","Line => 472");
                                        binding.spinnerMadeFor.setSelection(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }


                    Log.w("isAddLittleOne", "" + isAddLittleOne);
                    if (isAddLittleOne) {
                        //   binding.spinnerMadeFor.setSelection(binding.spinnerMadeFor.getCount() - 2);
                        for (int i = 0; i < littleOnes.size(); i++) {
                            if (littleOnes.get(i).getId().equalsIgnoreCase(littleOneAddedId)) {
                                Log.w("setSelection","Line => 487");
                                binding.spinnerMadeFor.setSelection(i);
                                isChangesLittleOne = true;
                            }
                        }
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            Log.e("TAG", "little one = " + littleOnes.get(position) + "   position => "+position);
            madeForID = littleOnes.get(position).getId();

            if ((littleOnes.size() - 1) == position) {
                binding.spinnerMadeFor.setSelection(0);
                /*Intent intent = new Intent(context, CreateLittleOneActivity.class);
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

                callDialog();

                /*Intent videoIntent = new Intent(context, VideoViewDialog.class);
                videoIntent.putExtra("videoType",3);
                videoIntent.putExtra("intentToPass",intent);
                context.startActivity(videoIntent);*/


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public static YouTubePlayer handleVideoDialog() {
        if (videoViewDialog != null) {
            return videoViewDialog.hideFullScreen();
        } else {
            return null;
        }
    }

    private void callDialog() {

        LittleOneSingleton.get(context, new CallbackLittleOne() {
            @Override
            public void editLittleOne(String firstName, String lastName) {
            }

            @Override
            public void deleteLittleOne() {
            }

            @Override
            public void hideLittleOne() {
            }

            @Override
            public void addLittleOne(com.app.mylineup.pojo.myProfile.LittleOne littleOne) {
                isAddLittleOne = true;
                littleOneAddedId = littleOne.getId();

            }
        });

        if (!application.getSharedPref().getVideoData(Constant.LITTLEONE_SEEN)) {
            isAddLittleOne = false;
            littleOneAddedId = "";
            isChangesLittleOne = false;
            //   if(!application.getSharedPref().getVideoData(Constant.LITTLEONE_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.LITTLEONE_SEEN, true);
            //  }
            Log.w("onCallCreateLilOne","Line => 569");
            Intent intentI = new Intent(context, CreateLittleOneActivity.class);
            /*context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/


            Intent intent = new Intent(context, CustomVideoDialog.class);
            intent.putExtra("videoType", 3);
            intent.putExtra("isSkipDisplay", true);
            intent.putExtra("passIntent", intentI);
            // intent.putExtra("requestCode",10);
            //  intent.putExtra("onSkipListener", (Parcelable) this);
            startActivity(intent);


            /*videoViewDialog = new VideoViewDialog(3, this, true);
            videoViewDialog.show(getChildFragmentManager(),  "Share Download Image Bottom Sheet");*/
        } else {
            isAddLittleOne = false;
            littleOneAddedId = "";
            isChangesLittleOne = false;
            Log.w("onCallCreateLilOne","Line => 590");
            Intent intent = new Intent(context, CreateLittleOneActivity.class);
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }


    }

    public void setList(List<Connection> connectionList) {
        this.connectionList = connectionList;
    }

   /* @Override
    public void onSkipClick(int videoType) {

        if(!application.getSharedPref().getVideoData(Constant.LITTLEONE_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.LITTLEONE_SEEN, true);
        }
        Intent intent = new Intent(context, CreateLittleOneActivity.class);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }*/

}
