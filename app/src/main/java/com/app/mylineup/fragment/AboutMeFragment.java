package com.app.mylineup.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.adapter.AboutAdapter;
import com.app.mylineup.adapter.LikeInterestAdapter;
import com.app.mylineup.databinding.FragmentAboutMeBinding;
import com.app.mylineup.other.DateUtil;
import com.app.mylineup.pojo.AboutMeBeen;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.littleOnes.mylittleone.Profile;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.google.android.flexbox.AlignItems;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;

public class AboutMeFragment extends BaseFragment
{

    private FragmentAboutMeBinding binding;
    private String TAG = "Tab3Fragment";
    private AboutAdapter aboutAdapter;
    private List<AboutMeBeen> aboutMeBeenList = new ArrayList<>();

    private LikeInterestAdapter interestAdapter;
    private List<AboutMeBeen> interestList = new ArrayList<>();

    ChipCloudConfig config;
    private UserProfile userProfile;
    private LittleOneProfile littleOneProfile;
    private boolean isLittleOne = false;

    public AboutMeFragment(){}

    public AboutMeFragment(UserProfile userProfile)
    {
        this.userProfile = userProfile;
        isLittleOne = false;
    }

    public AboutMeFragment(LittleOneProfile littleOneProfile)
    {
        this.littleOneProfile = littleOneProfile;
        isLittleOne = true;
    }

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about_me, container, false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        if (userProfile != null)
        {
            if (userProfile.getProfile().getBirthdayDate() == null
                    || userProfile.getProfile().getBirthdayDate().isEmpty())
            {
                binding.llBirthday.setVisibility(View.GONE);
            } else
            {
                binding.tvBirthdayValue.setText(DateUtil.toDateString(userProfile.getProfile().getBirthdayDate(),
                        DateUtil.DATE_FORMAT_Y_M_D, DateUtil.DATE_FORMAT_M_D));
            }

            if (userProfile.getProfile().getAnniversaryDate() == null
                    || userProfile.getProfile().getAnniversaryDate().isEmpty())
            {
                binding.llAnniversary.setVisibility(View.GONE);
            } else
            {
                binding.tvAnniversaryValue.setText(DateUtil.toDateString(userProfile.getProfile().getAnniversaryDate(),
                        DateUtil.DATE_FORMAT_Y_M_D, DateUtil.DATE_FORMAT_M_D));
            }

            if ((userProfile.getProfile().getBirthdayDate() == null || userProfile.getProfile().getBirthdayDate().isEmpty()) &&
                    (userProfile.getProfile().getAnniversaryDate() == null || userProfile.getProfile().getAnniversaryDate().isEmpty()) &&
                    (userProfile.getFavouriteStore() == null || userProfile.getFavouriteStore().size() == 0) &&
                    (userProfile.getClothingFootware() == null || userProfile.getClothingFootware().size() == 0) &&
                    (userProfile.getTopBrands() == null || userProfile.getTopBrands().size() == 0) &&
                    (userProfile.getLikesInterest() == null || userProfile.getLikesInterest().size() == 0))
            {
                binding.llContainer.setVisibility(View.GONE);
                binding.ivNoData.setVisibility(View.VISIBLE);
                return;
            }

            if (userProfile.getClothingFootware() != null && userProfile.getClothingFootware().size() != 0)
            {
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                binding.rvPreference.setLayoutManager(layoutManager);
                aboutAdapter = new AboutAdapter(getActivity(), userProfile.getClothingFootware()/*aboutMeBeenList*/);
                binding.rvPreference.setAdapter(aboutAdapter);
                binding.rvPreference.setNestedScrollingEnabled(false);
            } else
            {

                binding.llClothingFootwear.setVisibility(View.GONE);
                binding.viewClothing.setVisibility(View.GONE);
            }

            if (userProfile.getLikesInterest() != null && userProfile.getLikesInterest().size() != 0)
            {
                interestAdapter = new LikeInterestAdapter(getActivity(), userProfile.getLikesInterest()/*interestList*/);
                binding.rvLikeInterest.setLayoutManager(new LinearLayoutManager(getActivity()));
                binding.rvLikeInterest.setNestedScrollingEnabled(false);
                binding.rvLikeInterest.setAdapter(interestAdapter);
            } else
            {
                binding.llLineInterest.setVisibility(View.GONE);
            }
        }

        if (littleOneProfile != null)
        {
            if (littleOneProfile.getProfile().getBirthdayDate() == null
                    || littleOneProfile.getProfile().getBirthdayDate().isEmpty())
            {
                binding.llBirthday.setVisibility(View.GONE);
            } else
            {
                binding.tvBirthdayValue.setText(DateUtil.toDateString(littleOneProfile.getProfile().getBirthdayDate(),
                        DateUtil.DATE_FORMAT_Y_M_D, DateUtil.DATE_FORMAT_M_D));
            }

            binding.llAnniversary.setVisibility(View.GONE);
           /* if (littleOneProfile.getProfile().getAnniversaryDate() == null
                    || littleOneProfile.getProfile().getAnniversaryDate().isEmpty())
            {
                binding.llAnniversary.setVisibility(View.GONE);
            } else
            {
                binding.tvAnniversaryValue.setText(DateUtil.toDateString(littleOneProfile.getProfile().getAnniversaryDate(),
                        DateUtil.DATE_FORMAT_Y_M_D, DateUtil.DATE_FORMAT_M_D));
            }*/

            if ((littleOneProfile.getProfile().getBirthdayDate() == null || littleOneProfile.getProfile().getBirthdayDate().isEmpty()) &&
                    (littleOneProfile.getFavouriteStore() == null || littleOneProfile.getFavouriteStore().size() == 0) &&
                    (littleOneProfile.getClothingFootware() == null || littleOneProfile.getClothingFootware().size() == 0) &&
                    (littleOneProfile.getTopBrands() == null || littleOneProfile.getTopBrands().size() == 0) &&
                    (littleOneProfile.getLikesInterest() == null || littleOneProfile.getLikesInterest().size() == 0))
            {
                binding.llContainer.setVisibility(View.GONE);
                binding.ivNoData.setVisibility(View.VISIBLE);
                return;
            }

            if (littleOneProfile.getClothingFootware() != null && littleOneProfile.getClothingFootware().size() != 0)
            {
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
                binding.rvPreference.setLayoutManager(layoutManager);
                aboutAdapter = new AboutAdapter(littleOneProfile.getClothingFootware(), getActivity());
                binding.rvPreference.setAdapter(aboutAdapter);
                binding.rvPreference.setNestedScrollingEnabled(false);
            } else
            {

                binding.llClothingFootwear.setVisibility(View.GONE);
                binding.viewClothing.setVisibility(View.GONE);
            }

            if (littleOneProfile.getLikesInterest() != null && littleOneProfile.getLikesInterest().size() != 0)
            {
                interestAdapter = new LikeInterestAdapter( littleOneProfile.getLikesInterest(),getActivity()/*interestList*/);
                binding.rvLikeInterest.setLayoutManager(new LinearLayoutManager(getActivity()));
                binding.rvLikeInterest.setNestedScrollingEnabled(false);
                binding.rvLikeInterest.setAdapter(interestAdapter);
            } else
            {
                binding.llLineInterest.setVisibility(View.GONE);
            }
        }

        config = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedTextColor(getResources().getColor(R.color.colorBlack))
                .uncheckedTextColor(getResources().getColor(R.color.colorBlack))
                .chipTextSize(16f)
                .chipPadding(20, 10, 20, 10);
        //callAboutMeApi();
        setTopBrandFlex();
        setFavoriteStoreFlex();
    }

    private void setFavoriteStoreFlex()
    {
        binding.flexBoxStore.setAlignItems(AlignItems.CENTER);

        final ChipCloud chipCloud = new ChipCloud(getActivity(), binding.flexBoxStore, config);

        if (userProfile != null)
        {
            if (userProfile.getFavouriteStore() != null && userProfile.getFavouriteStore().size() > 0)
            {
                for (int i = 0; i < userProfile.getFavouriteStore().size(); i++)
                {
                    chipCloud.addChip(userProfile.getFavouriteStore().get(i), null);
                }
            } else
            {
                binding.llFavouriteStore.setVisibility(View.GONE);
                binding.viewFavourite.setVisibility(View.GONE);
            }
        }

        if (littleOneProfile != null)
        {
            if (littleOneProfile.getFavouriteStore() != null && littleOneProfile.getFavouriteStore().size() > 0)
            {
                for (int i = 0; i < littleOneProfile.getFavouriteStore().size(); i++)
                {
                    chipCloud.addChip(littleOneProfile.getFavouriteStore().get(i), null);
                }
            } else
            {
                binding.llFavouriteStore.setVisibility(View.GONE);
                binding.viewFavourite.setVisibility(View.GONE);
            }
        }
    }

    private void setTopBrandFlex()
    {
        binding.flexBoxBrand.setAlignItems(AlignItems.CENTER);

        final ChipCloud chipCloud = new ChipCloud(getActivity(), binding.flexBoxBrand, config);
        if (userProfile != null)
        {
            if (userProfile.getTopBrands() != null && userProfile.getTopBrands().size() > 0)
            {

                for (int i = 0; i < userProfile.getTopBrands().size(); i++)
                {
                    chipCloud.addChip(userProfile.getTopBrands().get(i), null);
                }
            } else
            {
                binding.llTopBrand.setVisibility(View.GONE);
                binding.viewTopBrand.setVisibility(View.GONE);
            }
        }

        if (littleOneProfile != null)
        {
            if (littleOneProfile.getTopBrands() != null && littleOneProfile.getTopBrands().size() > 0)
            {

                for (int i = 0; i < littleOneProfile.getTopBrands().size(); i++)
                {
                    chipCloud.addChip(littleOneProfile.getTopBrands().get(i), null);
                }
            } else
            {
                binding.llTopBrand.setVisibility(View.GONE);
                binding.viewTopBrand.setVisibility(View.GONE);
            }
        }
    }

    private void callAboutMeApi()
    {
        Log.e(TAG, "callAboutMe()");

        aboutMeBeenList.clear();

        if (userProfile.getClothingFootware() != null && userProfile.getClothingFootware().size() > 0)
        {

            aboutMeBeenList.add(new AboutMeBeen("", "SHOES", "12.5", R.drawable.ic_shoes_icon));
            aboutMeBeenList.add(new AboutMeBeen("", "SIZING PREFERENCE", "Men's", R.drawable.ic_sizing_icon));
            aboutMeBeenList.add(new AboutMeBeen("", "SHIRTS", "Medium", R.drawable.ic_shirts_icon));
            aboutMeBeenList.add(new AboutMeBeen("", "INSEAM/WAIST", "32/34", R.drawable.ic_inseam_waist_icon));
            aboutMeBeenList.add(new AboutMeBeen("", "COLLAR/SLEEVE", "16/34.5", R.drawable.ic_collar_sleeve_icon));

            aboutAdapter.notifyDataSetChanged();
        }

        if (userProfile.getLikesInterest() != null && userProfile.getLikesInterest().size() > 0)
        {

            interestList.add(new AboutMeBeen("", "BEVERAGE", "Coke, WhiteClaw", R.drawable.ic_beverages_icon));
            interestList.add(new AboutMeBeen("", "BOOK GENRES", "Non-Fiction", R.drawable.ic_book_icon));
            interestList.add(new AboutMeBeen("", "FOOD TYPES", "Mexican, italian", R.drawable.ic_food_icon));
            interestList.add(new AboutMeBeen("", "HOBBIES/ACTIVITIES", "Hiking, Travel, Concerts", R.drawable.ic_hobbies_icon));
            interestList.add(new AboutMeBeen("", "IDOLS", "Patrick Mahomes, Keanu Reeves", R.drawable.ic_idols_icon));
            interestList.add(new AboutMeBeen("", "MUSIC GENRES", "Country, Hip Hop, Alternative", R.drawable.ic_music_icon));
            interestList.add(new AboutMeBeen("", "RESTAURANTS", "Q39, Chuy’s, Garozzo’s, Joe’s Kansas City", R.drawable.ic_restaurants_icon));
            interestList.add(new AboutMeBeen("", "SPORTS TEAM", "Q39, Chuy’s, Garozzo’s, Joe’s Kansas City", R.drawable.ic_sports_icon));

            interestAdapter.notifyDataSetChanged();
        }
    }
}