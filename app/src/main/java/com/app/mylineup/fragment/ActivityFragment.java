package com.app.mylineup.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.adapter.ActivityAdapter;
import com.app.mylineup.databinding.FragmentActivityBinding;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.pojo.WishlistImageBeen;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.littleOnes.mylittleone.Profile;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.pojo.wishlists.Wishlist;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActivityFragment extends BaseFragment {

    private String TAG = "Tab2Fragment";
    private FragmentActivityBinding binding;

    private ActivityAdapter wishlistAdapter;
    private UserProfile userProfile;
    private Profile profile;
    private LittleOneProfile littleOneProfile;
    private boolean isMyAcc;

    public ActivityFragment() {
    }

    public ActivityFragment(boolean isMyAcc, UserProfile userProfile) {
        this.userProfile = userProfile;
        this.isMyAcc = isMyAcc;
    }

    public ActivityFragment(boolean isMyAcc, Profile profile) {
        this.profile = profile;
        this.isMyAcc = isMyAcc;
    }

    public ActivityFragment(boolean isMyAcc, LittleOneProfile littleOneProfile) {
        this.littleOneProfile = littleOneProfile;
        this.isMyAcc = isMyAcc;
    }

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_activity, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {


        if (userProfile != null) {
            if (userProfile.getActivity() == null || userProfile.getActivity().size() == 0) {
                binding.ivNoData.setVisibility(View.VISIBLE);
                return;
            }
        }

        if (littleOneProfile != null) {
            if (littleOneProfile.getActivity() == null || littleOneProfile.getActivity().size() == 0) {
                binding.ivNoData.setVisibility(View.VISIBLE);
                return;
            }
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvActivity.setLayoutManager(layoutManager);

        if (isMyAcc) {
            if (userProfile != null) {
                wishlistAdapter = new ActivityAdapter(getActivity(), userProfile.getActivity(), isMyAcc);
            }

        } else {
            if (userProfile != null) {
                wishlistAdapter = new ActivityAdapter(getActivity(), userProfile.getActivity(), isMyAcc);
            } else if (littleOneProfile != null) {
                wishlistAdapter = new ActivityAdapter(littleOneProfile.getActivity(), getActivity(), isMyAcc);
            }
        }

        binding.rvActivity.setNestedScrollingEnabled(false);
        binding.rvActivity.setAdapter(wishlistAdapter);
    }
}
