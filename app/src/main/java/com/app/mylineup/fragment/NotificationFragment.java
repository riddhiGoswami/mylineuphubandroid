package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.activity.NotificationListingActivity;
import com.app.mylineup.adapter.NewWishListAdapter;
import com.app.mylineup.adapter.NewsAdapter;
import com.app.mylineup.adapter.RequestAdapter;
import com.app.mylineup.adapter.WhoPurchasedAdapter;
import com.app.mylineup.databinding.FragmentNotificationBinding;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.notifications.FriendRequest;
import com.app.mylineup.pojo.notifications.Notification;
import com.app.mylineup.pojo.notifications.Notifications;
import com.app.mylineup.pojo.notifications.PurchasedList;
import com.app.mylineup.pojo.notifications.Wishlist;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.web_services.PrettyPrinter;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends BaseFragment implements RequestAdapter.FriendRequstAction, /*OnNewsItemClickListener,*/ OnItemClickGiftListener {
    private FragmentNotificationBinding binding;
    private RequestAdapter requestAdapter;
    private NewWishListAdapter newWishListAdapter;
    private NewsAdapter newsAdapter;
    private WhoPurchasedAdapter whoPurchasedAdapter;

    private List<FriendRequest> friendRequestList = new ArrayList<>();
    private List<Notification> newsList = new ArrayList<>();
    private List<PurchasedList> purchasedLists = new ArrayList<>();
    private List<Wishlist> wishlistList = new ArrayList<>();
    // private OnNewsItemClickListener onItemClickGiftListener;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        //news
        binding.rvNews.setLayoutManager(new LinearLayoutManager(context));
        newsAdapter = new NewsAdapter(context, newsList, binding.rvNews, null, this/*, this*/);
        binding.rvNews.setAdapter(newsAdapter);

        //who purchased
        binding.rvWhoPurchased.setLayoutManager(new LinearLayoutManager(context));
        whoPurchasedAdapter = new WhoPurchasedAdapter(context, purchasedLists, binding.rvWhoPurchased, null);
        binding.rvWhoPurchased.setAdapter(whoPurchasedAdapter);

        //connection request
        binding.rvRequest.setLayoutManager(new LinearLayoutManager(context));
        requestAdapter = new RequestAdapter(context, friendRequestList, this, binding.rvRequest, null, this);
        binding.rvRequest.setAdapter(requestAdapter);

        //wishlist
        binding.rvNewWishList.setLayoutManager(new LinearLayoutManager(context));
        newWishListAdapter = new NewWishListAdapter(context, wishlistList, binding.rvNewWishList, null);
        binding.rvNewWishList.setAdapter(newWishListAdapter);

        Log.w("loadFragments", "notification");

        String nstatus = NetworkUtil.getConnectivityStatusString(getActivity());
        if (nstatus.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getUserProfile();

        binding.ivAccSetting.setOnClickListener(v -> {
//            Intent intent = new Intent(context, UserProfileActivity.class);
//            intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
//            startActivity(intent);
//            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

            Intent intent = new Intent(context, AccountSettingActivity.class);
            startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

//        binding.ivSetting.setOnClickListener(v -> {
//            Intent intent = new Intent(context, AccountSettingActivity.class);
//            startActivity(intent);
//            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//        });

        binding.rippleSeeAllConnectionRequest.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), NotificationListingActivity.class);
            intent.putExtra(Constant.NOTIFICATION_TYPE, "2");
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        });
        binding.rippleSeeAllNews.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), NotificationListingActivity.class);
            intent.putExtra(Constant.NOTIFICATION_TYPE, "1");
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        });
        binding.rippleSeeAllNewWishList.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), NotificationListingActivity.class);
            intent.putExtra(Constant.NOTIFICATION_TYPE, "3");
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        });
        binding.rippleSeeAllWhoPurchased.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), NotificationListingActivity.class);
            intent.putExtra(Constant.NOTIFICATION_TYPE, "4");
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        });

        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getNotifications();

        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshCall();
            }
        });
    }

    private void onRefreshCall() {

        binding.swipeRefresh.setRefreshing(true);

        binding.llContainer.setVisibility(View.GONE);

        friendRequestList.clear();
        requestAdapter.notifyDataSetChanged();

        newsList.clear();
        newsAdapter.notifyDataSetChanged();

        purchasedLists.clear();
        whoPurchasedAdapter.notifyDataSetChanged();

        wishlistList.clear();
        newWishListAdapter.notifyDataSetChanged();

        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getNotifications();
    }

    private void getNotifications() {

        if (!binding.swipeRefresh.isRefreshing()) {
            loaderDialog.show();
        }
        Call<Object> call = application.getApis().notification(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                newsList.clear();
                purchasedLists.clear();
                wishlistList.clear();
                friendRequestList.clear();
                if (binding.swipeRefresh.isRefreshing()) {
                    binding.swipeRefresh.setRefreshing(false);
                }
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, getActivity(), application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Notifications notifications = gson.fromJson(x, Notifications.class);
                        if (notifications != null) {

                            if ((notifications.getNotifications() == null || notifications.getNotifications().size() == 0) &&
                                    (notifications.getFriendRequests() == null || notifications.getFriendRequests().size() == 0) &&
                                    (notifications.getWishlist() == null || notifications.getWishlist().size() == 0) &&
                                    (notifications.getPurchasedList() == null || notifications.getPurchasedList().size() == 0)) {
                                binding.llContainer.setVisibility(View.GONE);
                                binding.llNoData.setVisibility(View.VISIBLE);
                            }else{
                                binding.llContainer.setVisibility(View.VISIBLE);
                                binding.llNoData.setVisibility(View.GONE);
                            }
                            binding.llContainer.setVisibility(View.VISIBLE);

                            if (notifications.getNotifications() != null && notifications.getNotifications().size() > 0) {
                                newsList.addAll(notifications.getNotifications());
                                newsAdapter.notifyDataSetChanged();
                                if (notifications.getNotificationsSeeAll()) {
                                    binding.rippleSeeAllNews.setVisibility(View.VISIBLE);
                                }
                                binding.ivNoDataNews.setVisibility(View.GONE);
                                binding.llNotification.setVisibility(View.VISIBLE);
                                binding.viewNotification.setVisibility(View.VISIBLE);
                            } else {
                                binding.ivNoDataNews.setVisibility(View.VISIBLE);
                                binding.llNotification.setVisibility(View.GONE);
                                binding.viewNotification.setVisibility(View.GONE);
                            }

                            if (notifications.getFriendRequests() != null && notifications.getFriendRequests().size() > 0) {
                                friendRequestList.addAll(notifications.getFriendRequests());
                                requestAdapter.notifyDataSetChanged();
                                if (notifications.getFriendRequestsSeeAll()) {
                                    binding.rippleSeeAllConnectionRequest.setVisibility(View.VISIBLE);
                                }
                                binding.ivNoDataRequest.setVisibility(View.GONE);
                                binding.llNewConnection.setVisibility(View.VISIBLE);
                                binding.viewConnection.setVisibility(View.VISIBLE);
                            } else {
                                binding.ivNoDataRequest.setVisibility(View.VISIBLE);
                                binding.llNewConnection.setVisibility(View.GONE);
                                binding.viewConnection.setVisibility(View.GONE);
                            }

                            if (notifications.getWishlist() != null && notifications.getWishlist().size() > 0) {
                                wishlistList.addAll(notifications.getWishlist());
                                newWishListAdapter.notifyDataSetChanged();
                                if (notifications.getWishlistSeeAll()) {
                                    binding.rippleSeeAllNewWishList.setVisibility(View.VISIBLE);
                                }
                                binding.ivNoDataWishlist.setVisibility(View.GONE);
                                binding.llWishlist.setVisibility(View.VISIBLE);
                                binding.vieWishlist.setVisibility(View.VISIBLE);
                            } else {
                                binding.ivNoDataWishlist.setVisibility(View.VISIBLE);
                                binding.llWishlist.setVisibility(View.GONE);
                                binding.vieWishlist.setVisibility(View.GONE);
                            }

                            if (notifications.getPurchasedList() != null && notifications.getPurchasedList().size() > 0) {
                                purchasedLists.addAll(notifications.getPurchasedList());
                                whoPurchasedAdapter.notifyDataSetChanged();
                                if (notifications.getPurchasedListSeeAll()) {
                                    binding.rippleSeeAllWhoPurchased.setVisibility(View.VISIBLE);
                                }
                                binding.ivNoDataPurchased.setVisibility(View.GONE);
                                binding.llPurchased.setVisibility(View.VISIBLE);
                            } else {
                                binding.ivNoDataPurchased.setVisibility(View.VISIBLE);
                                binding.llPurchased.setVisibility(View.GONE);
                            }
                        }

                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void perFormActionOnRequest(String requestType, int position) {
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else {
            loaderDialog.show();
            Call<Object> call = application.getApis().friendRequest(loginData.getUser().getId(), friendRequestList.get(position).getUserId(), requestType);
            Log.e("TAG", "url = " + call.request().url());
            Log.e("TAG", "param = " + PrettyPrinter.print(call));

            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    String x = gson.toJson(response.body());
                    Log.e("TAG", "response = " + x);
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(), dialogCommon, context, application);
                    try {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            if (requestType.equalsIgnoreCase("1")) {
                                friendRequestList.get(position).setActionType("1");
                            } else {
                                friendRequestList.get(position).setActionType("2");
                            }
                            requestAdapter.notifyDataSetChanged();

                        /*friendRequestList.remove(position);
                        requestAdapter.notifyDataSetChanged();

                        if(friendRequestList.size() == 0){
                            binding.rippleSeeAllConnectionRequest.setVisibility(View.GONE);
                        }*/
                        } else {
                            dialogCommon.showDialog(jsonObject.getString("message"));
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    loaderDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.e("TAG", "hidden = " + hidden);
        if (!hidden) {
            onRefreshCall();
        }
    }

    private void getUserProfile() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getUserProfile(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                try {
                    if (x != null) {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            UserProfile userProfile = gson.fromJson(x, UserProfile.class);
                            if (Global.isValidString(userProfile.getProfile().getProfilePicture())) {
                                Picasso.get().load(userProfile.getProfile().getProfilePicture()).into(binding.ivAccSetting);
                            }
                        }
//                        else
//                        {
//                            dialogCommon.showDialog(jsonObject.getString("message"));
//                        }
                    }
                    //{"profile":{"profile_picture":"https://www.mylineuphub.com/assets/images/default.png","name":"SAURAVKUMAR THAKKAR"},"status":false,"message":"No data found"}
                } catch (Exception e) {
                } finally {
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(), dialogCommon, getActivity(), application);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    public void getCheckConnectedApi(String userId, String type) {


        if (type.equalsIgnoreCase("little_one_birthday")) {
            Intent intent = new Intent(context, LittleOneProfileActivity.class);

            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
            intent.putExtra(Constant.LITTLE_ONE_ID, userId);
            intent.putExtra(Constant.USER_NAME, "");

            ((Activity) context).startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        }

    }

    public void setValue() {
        loginData =  application.getSharedPref().getUserSession(Constant.LOGIN_DATA);

        Log.w("getProfilePicture()",""+loginData.getUser().getProfilePicture());
        if (Global.isValidString(loginData.getUser().getProfilePicture())) {
            Picasso.get().load(loginData.getUser().getProfilePicture()).into(binding.ivAccSetting);
        }
    }

    @Override
    public void onClick(String profileId) {
        getCheckConnectedApi(profileId);
    }
}
