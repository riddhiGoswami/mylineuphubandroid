package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabetik.Alphabetik;
import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.ContactListActivity;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.adapter.ConnectionAdapter;
import com.app.mylineup.databinding.FragmentConnectionBinding;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.connections.Connection;
import com.app.mylineup.pojo.connections.Connections;
import com.app.mylineup.pojo.userProfile.CheckConnected;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.youtube.player.YouTubePlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectionFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, OnItemClickGiftListener/*, VideoViewDialog.OnSkipClickListener*/
{
    private FragmentConnectionBinding binding;
    private ConnectionAdapter adapter;

    private Connections connections;
    private List<Connection> connectionList = new ArrayList<>();
    private List<Connection> tempList = new ArrayList<>();
    private OnItemClickGiftListener onItemClickGiftListener;
    private static VideoViewDialog videoViewDialog;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_connection,container,false);

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init()
    {
        String[] customAlphabet = getResources().getStringArray(R.array.my_string_abcd);
        binding.alphSectionIndex.setAlphabet(customAlphabet);

        binding.alphSectionIndex.onSectionIndexClickListener(new Alphabetik.SectionIndexClickListener() {
            @Override
            public void onItemClick(View view, int position, String character) {
                String info = " Position = " + position + " Char = " + character;
                Log.e("View: ", view + "," + info);
                binding.rvConnection.smoothScrollToPosition(getPositionFromData(character));
            }
        });

        binding.swipeRefresh.setOnRefreshListener(this);

        binding.rvConnection.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ConnectionAdapter(context,tempList, this);
        binding.rvConnection.setAdapter(adapter);

        binding.ivFindFriend.setOnClickListener(v -> {
            addConnections();
        });

        binding.tvTapToAdd.setOnClickListener(v -> { addConnections(); });

        Log.w("loadFragments","connection");

        /*String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else*/
        callMyConnectionApi();

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                findSearchResult(s.toString());
            }
        });
    }

    private void addConnections() {

        if(!application.getSharedPref().getVideoData(Constant.CONNECTION_SEEN)) {

           // if(!application.getSharedPref().getVideoData(Constant.CONNECTION_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.CONNECTION_SEEN, true);
          //  }
            Intent intentI = new Intent(context, ContactListActivity.class);
            /*startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/

            Intent intent = new Intent(getActivity(), CustomVideoDialog.class);
            intent.putExtra("videoType", 2);
            intent.putExtra("isSkipDisplay", true);
            intent.putExtra("passIntent",intentI);

            //  intent.putExtra("onSkipListener", (Parcelable) this);
            startActivity(intent);

            /*videoViewDialog = new VideoViewDialog(2, this, true);
            videoViewDialog.show(getChildFragmentManager(),  "Share Download Image Bottom Sheet");*/
        }else{
            Intent intent = new Intent(context, ContactListActivity.class);
            startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        }


    }

    public static YouTubePlayer handleVideoDialog() {
        if(videoViewDialog !=null) {
            return videoViewDialog.hideFullScreen();
        }else{
            return null;
        }
    }

    private void findSearchResult(String search)
    {
        if(adapter == null || connectionList == null || connectionList.size() == 0){
            return;
        }
        if(!search.equalsIgnoreCase(""))
        {
            tempList.clear();
            adapter.notifyDataSetChanged();

            for(int i = 0;i<connectionList.size();i++)
            {
                if((connectionList.get(i).getFirstName()+" "+connectionList.get(i).getLastName()).toLowerCase().contains(search.toLowerCase()))
                {
                    tempList.add(connectionList.get(i));
                }
            }

            adapter.notifyDataSetChanged();
        }
        else
        {
            tempList.clear();
            tempList.addAll(connectionList);
            adapter.notifyDataSetChanged();
        }
    }

    private void callMyConnectionApi()
    {
        connectionList.clear();
        tempList.clear();
        if(!binding.swipeRefresh.isRefreshing()){
            loaderDialog.show();
        }

        Call<Object> call = application.getApis().myConnections(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                connectionList.clear();
                tempList.clear();
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                if(binding.swipeRefresh.isRefreshing())
                {binding.swipeRefresh.setRefreshing(false);}
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,getActivity(),application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        connections = gson.fromJson(x,Connections.class);
                        if(connections.getConnections() != null && connections.getConnections().size()>0) {
                            connectionList.addAll(connections.getConnections());
                            tempList.addAll(connections.getConnections());
                            adapter.notifyDataSetChanged();

                            if (connectionList.size()>0) {
                                binding.llNoData.setVisibility(View.GONE);
                            }
                            else {
                                binding.llNoData.setVisibility(View.VISIBLE);
                            }
//                            binding.llNoData.setVisibility(View.VISIBLE);
//                            if(binding.llNoData.getVisibility()==View.VISIBLE){
//                                binding.llNoData.setVisibility(View.GONE);
//                            }
                            /*if(binding.alphSectionIndex.getVisibility() == View.GONE){
                                binding.alphSectionIndex.setVisibility(View.VISIBLE);
                            }*/
                        }else {
                            binding.llNoData.setVisibility(View.VISIBLE);
                            //binding.alphSectionIndex.setVisibility(View.GONE);
                        }
                    }else {
                        binding.llNoData.setVisibility(View.VISIBLE);
                        //binding.alphSectionIndex.setVisibility(View.GONE);
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private int getPositionFromData(String character) {
        for(int i = 0;i<connectionList.size();i++)
        {
            if((connectionList.get(i).getFirstName().charAt(0)+"").equalsIgnoreCase("" + character))
            {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onRefresh()
    {
        binding.swipeRefresh.setRefreshing(true);

        connectionList.clear();
        tempList.clear();
        adapter.notifyDataSetChanged();
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
        callMyConnectionApi();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.e("TAG","hidden = "+hidden);
        if(!hidden){

            if(!binding.etSearch.getText().toString().equalsIgnoreCase("")){
                binding.etSearch.setText("");
            }
            binding.swipeRefresh.setRefreshing(true);

            connectionList.clear();
            tempList.clear();

            adapter.notifyDataSetChanged();
            String status = NetworkUtil.getConnectivityStatusString(getActivity());
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            callMyConnectionApi();
        }
    }
    public void getCheckConnectedApi(String userId) {

        loaderDialog.show();

        Call<Object> call = application.getApis().checkConnected(userId,loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                loaderDialog.dismiss();
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                try {
                    if(x != null){
                        JSONObject jsonObject = new JSONObject(x);
                        if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            CheckConnected checkConnected = gson.fromJson(x, CheckConnected.class);
                            if(checkConnected.getStatus()){
                                Intent intent = new Intent(context, UserProfileActivity.class);
                                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                                intent.putExtra(Constant.USER_ID,checkConnected.getProfile_user_id());
                                ((Activity) context).startActivity(intent);
                                ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        }
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }


    @Override
    public void onClick(String profileId) {
        getCheckConnectedApi(profileId);
    }

    /*@Override
    public void onSkipClick(int videoType) {

        if(!application.getSharedPref().getVideoData(Constant.CONNECTION_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.CONNECTION_SEEN, true);
        }
        Intent intent = new Intent(context, ContactListActivity.class);
        startActivity(intent);
        ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }*/
}
