package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.LoginActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.adapter.WishListAdapter;
import com.app.mylineup.databinding.FragmentWishlistBinding;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.web_services.PrettyPrinter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class WishlistFragment extends BaseFragment {
    private FragmentWishlistBinding binding;

    private WishlistListFragment wishlistListFragment;
    private WishlistItemsFragment itemsFragment;
//    private FragmentTransaction transaction;
    private MyPagerAdapter myPagerAdapter;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wishlist, container, false);
        init();
        return binding.getRoot();
    }

    private void init() {
        wishlistListFragment = new WishlistListFragment();
        itemsFragment = new WishlistItemsFragment();

        myPagerAdapter = new MyPagerAdapter(getChildFragmentManager());
        binding.viewPager.setOffscreenPageLimit(2);
        binding.viewPager.setAdapter(myPagerAdapter);

//        loadListFragment();
        Log.w("loadFragments", "wishlist");
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getUserProfile();

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),
                "font/Ubuntu-Bold.ttf");
        binding.tvLists.setTypeface(typeface);
        binding.tvItems.setTypeface(typeface);

        binding.ivAccSetting.setOnClickListener(v -> {
//            Intent intent = new Intent(context, UserProfileActivity.class);
//            intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
//            startActivity(intent);
//            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

            Intent intent = new Intent(context, AccountSettingActivity.class);
            startActivity(intent);
            ((Activity) context).overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.tvLists.setOnClickListener(v -> {
            binding.tvLists.setTextColor(context.getResources().getColor(R.color.colorAccent));
            binding.tvItems.setTextColor(context.getResources().getColor(R.color.textColor));

            binding.tvLists.setBackground(context.getResources().getDrawable(R.drawable.bg_wishlist_selected));
            binding.tvItems.setBackground(null);

            binding.viewPager.setCurrentItem(0);

//            loadListFragment();
        });

        binding.tvItems.setOnClickListener(v -> {
            binding.tvLists.setTextColor(context.getResources().getColor(R.color.textColor));
            binding.tvItems.setTextColor(context.getResources().getColor(R.color.colorAccent));

            binding.tvLists.setBackground(null);
            binding.tvItems.setBackground(context.getResources().getDrawable(R.drawable.bg_wishlist_selected));

            binding.viewPager.setCurrentItem(1);

//            loadItemsFragment();
        });

        binding.tvOccasion.setOnClickListener(v -> {

            ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
            aboutMeBeen.add(new OnlyString("All Occasions"));
            aboutMeBeen.add(new OnlyString("Christmas"));
            aboutMeBeen.add(new OnlyString("Birthday"));

            listDialog.showDialog("", true, getString(R.string.label_cancel), aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                @Override
                public void OnItemSelected(CommonPojo commonPojo, int position) {
                    if (commonPojo instanceof OnlyString) {
                        OnlyString been = (OnlyString) commonPojo;
                        Log.e("TAG", "item = " + been.getTitle());
                        binding.tvOccasion.setText(been.getTitle());
                    }
                }
            });

        });
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            fm.addOnBackStackChangedListener(() -> {
            });
        }

        @Override
        public Fragment getItem(int pos) {
            if (pos == 1) {
                return itemsFragment;
            }
            return wishlistListFragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


//    private void loadItemsFragment() {
//        transaction = getChildFragmentManager().beginTransaction();
////        if (wishlistListFragment.isAdded()) {
////            transaction.remove(wishlistListFragment);
////        }
//        if (!itemsFragment.isAdded()) {
//            //transaction.show(itemsFragment);
//            transaction.add(R.id.frameLayout, itemsFragment);
//        } else {
//            transaction.replace(R.id.frameLayout, itemsFragment);
//        }
//
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.commitAllowingStateLoss();
//    }

//    private void loadListFragment() {
//        transaction = getChildFragmentManager().beginTransaction();
//        if (!wishlistListFragment.isAdded()) {
//            //transaction.show(wishlistListFragment);
//            transaction.add(R.id.frameLayout, wishlistListFragment);
//        } else {
//            transaction.replace(R.id.frameLayout, wishlistListFragment);
//        }
//        /*else {
//            transaction.add(R.id.frameLayout, wishlistListFragment);
//        }*/
////        if (itemsFragment.isAdded()) {
////            transaction.remove(itemsFragment);
////        }
//
//        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//        transaction.commitAllowingStateLoss();
//    }

    private void getUserProfile() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getUserProfile(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                try {
                    if (x != null) {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            UserProfile userProfile = gson.fromJson(x, UserProfile.class);
                            if (Global.isValidString(userProfile.getProfile().getProfilePicture())) {
                                Picasso.get().load(userProfile.getProfile().getProfilePicture()).placeholder(R.drawable.ic_user_icon).into(binding.ivAccSetting);
                            }
                        }
//                        else
//                        {
//                            dialogCommon.showDialog(jsonObject.getString("message"));
//                        }
                    }
                    //{"profile":{"profile_picture":"https://www.mylineuphub.com/assets/images/default.png","name":"SAURAVKUMAR THAKKAR"},"status":false,"message":"No data found"}
                } catch (Exception e) {
                } finally {
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(), dialogCommon, getActivity(), application);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    public void setValue() {
        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);

        Log.w("getProfilePicture()", "" + loginData.getUser().getProfilePicture());
        if (Global.isValidString(loginData.getUser().getProfilePicture())) {
            Picasso.get().load(loginData.getUser().getProfilePicture()).placeholder(R.drawable.ic_user_icon).into(binding.ivAccSetting);
        }
    }


    /*  @Override
    public void onResume() {
        super.onResume();
        setValue();
    }*/
}
