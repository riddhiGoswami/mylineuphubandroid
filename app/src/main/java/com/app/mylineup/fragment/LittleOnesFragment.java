package com.app.mylineup.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.adapter.ActivityAdapter;
import com.app.mylineup.adapter.LittleOnesAdapter;
import com.app.mylineup.adapter.MarginItemDecoration;
import com.app.mylineup.databinding.FragmentLittleOnesBinding;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.userProfile.MyLittleOne;
import com.app.mylineup.pojo.userProfile.Profile;
import com.app.mylineup.pojo.userProfile.UserProfile;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LittleOnesFragment extends Fragment
{

    private FragmentLittleOnesBinding binding;
    private String TAG = "LittleOnesFragment";
    private UserProfile userProfile;
    private LittleOnesAdapter littleOnesAdapter;

    public LittleOnesFragment()
    {
        // Required empty public constructor
    }

    public LittleOnesFragment(UserProfile userProfile)
    {
        this.userProfile = userProfile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_little_ones, container, false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        String firstName = userProfile.getProfile().getName() + "'S LITTLE ONES";
        binding.tvTabTitle.setText(firstName);
        setRecyclerAdapter();
    }

    private void setRecyclerAdapter()
    {
        Profile profile = userProfile.getProfile();

        List<MyLittleOne> myLittleOnes = userProfile.getMyLittleOnes();
        Log.w("myLittleOnes",""+myLittleOnes.size());

        Log.w("hereTest",""+(profile == null || myLittleOnes == null || myLittleOnes.isEmpty()));
        if (profile == null || myLittleOnes == null || myLittleOnes.isEmpty())
        {
            binding.scrollLittleOne.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
            binding.ivNoData.setVisibility(View.VISIBLE);
            return;
        }
        binding.ivNoData.setVisibility(View.GONE);
        binding.rvLittleOnes.setVisibility(View.VISIBLE);
        binding.scrollLittleOne.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.whitesmoke));
        String userName = profile.getName();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvLittleOnes.setLayoutManager(layoutManager);
        binding.rvLittleOnes.addItemDecoration(new MarginItemDecoration(getResources()
                .getDimension(R.dimen.recycler_view_margin)));

        littleOnesAdapter = new LittleOnesAdapter(myLittleOnes, getContext(),userName);
        binding.rvLittleOnes.setNestedScrollingEnabled(false);
        binding.rvLittleOnes.setAdapter(littleOnesAdapter);
    }
}