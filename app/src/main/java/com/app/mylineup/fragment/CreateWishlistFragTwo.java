package com.app.mylineup.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.InviteAdapter;
import com.app.mylineup.databinding.FragmentWishlistTwoBinding;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.connections.Connection;
import com.app.mylineup.pojo.connections.Connections;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateWishlistFragTwo extends BaseFragment
{
    private FragmentWishlistTwoBinding binding;
    private InviteAdapter inviteAdapter;
    private Connections connections;
    private List<Connection> connectionList = new ArrayList<>();
    private List<Connection> selectedConnectionList = new ArrayList<>();

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wishlist_two,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.rvInvite.setLayoutManager(new LinearLayoutManager(getActivity()));
        inviteAdapter = new InviteAdapter(getActivity(),connectionList);
        binding.rvInvite.setAdapter(inviteAdapter);


        binding.tvSaveChange.setOnClickListener(v -> {

            for(int i = 0;i<connectionList.size();i++)
            {
                if(connectionList.get(i).isSelected())
                {
                    selectedConnectionList.add(connectionList.get(i));
                }
            }

        });
        String status = NetworkUtil.getConnectivityStatusString(getActivity());
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
        callMyConnectionApi();
    }

    private void callMyConnectionApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().myConnections(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,getActivity(),application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        connections = gson.fromJson(x, Connections.class);
                        if(connections.getConnections() != null)
                        {
                            connectionList.clear();
                            connectionList.addAll(connections.getConnections());
                            inviteAdapter.notifyDataSetChanged();
                        }
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    public List<Connection> getList()
    {
        return selectedConnectionList;
    }
}
