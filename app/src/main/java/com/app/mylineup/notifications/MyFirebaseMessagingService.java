package com.app.mylineup.notifications;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.app.mylineup.R;
import com.app.mylineup.activity.BaseActivity;
import com.app.mylineup.activity.HomeActivity;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.activity.LittleOneProfileActivity;
import com.app.mylineup.activity.LoginActivity;
import com.app.mylineup.activity.NotificationListingActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.application.Application;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;
import java.util.Timer;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    Application application = Application.getInstance();
    String msg = "", type = "", title = "", body = "";
    String userId = "", itemId = "", little_one_id = "";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        application.getSharedPref().saveSession(Constant.NOTIFICATION_TOKEN, s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("TAG", "remoteMessage = " + remoteMessage.getData().toString());

        //types:request_accept,birthday,purchased
        //{body={"user_id":"1","message":"Dhrumi Soni has birthday today. Wish all the best!"}, type=birthday, title=Birthday, message=Dhrumi Soni has birthday today. Wish all the best!}
        //{body={"user_id":"1","profile_picture":"https:\/\/www.mylineuphub.com\/assets\/images\/default.png","message":"Dhrumi  accepted your request"}, type=request_accept, title=Friend Request Accepted, message=Dhrumi  accepted your request}
        //{body={"user_id":"21","item_id":"64","message":"Saurav Thakkar has purchased My awesome item 1 for you."}, type=purchased, title=Your gift is ready, message=Saurav Thakkar has purchased My awesome item 1 for you.}
        //{body={"user_id":"88","message":"Patric has birthday today. Wish all the best!"}, type=birthday, title=Birthday}
        type = remoteMessage.getData().get("type");
        title = remoteMessage.getData().get("title");
        msg = remoteMessage.getData().get("message");
        body = remoteMessage.getData().get("body");

        try {
            if (body != null && !body.equalsIgnoreCase("")) {
                userId = new JSONObject(body).optString("user_id");
                itemId = new JSONObject(body).optString("item_id");

            }
        } catch (Exception e) {
        }

        //if(isAppIsInBackground(this)){ }
        sendNotification();
    }

    private void sendNotification() {

        application.getSharedPref().saveSession(Constant.SILENT_NOTIF_MSG, "");
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent;

        if (type != null && type.equalsIgnoreCase("request_accept")) {
            intent = new Intent(getApplicationContext(), UserProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
            intent.putExtra(Constant.USER_ID, userId);
        } else if (type != null && type.equalsIgnoreCase("purchased")) {
            intent = new Intent(getApplicationContext(), ItemDetailActivity.class);
            intent.putExtra(Constant.ITEM_ID, itemId);
            if (application != null) {
                application.getSharedPref().saveSession("FROM", "Notification");
            }
        } else if (type != null && type.equalsIgnoreCase("birthday")) {
            intent = new Intent(getApplicationContext(), UserProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
            intent.putExtra(Constant.USER_ID, userId);
        } else if (type != null && type.equalsIgnoreCase("your_birthday")) {
            intent = new Intent(getApplicationContext(), UserProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
            intent.putExtra(Constant.USER_ID, userId);
        } else if (type != null && type.equalsIgnoreCase("new_request")) {
            intent = new Intent(getApplicationContext(), NotificationListingActivity.class);
            intent.putExtra(Constant.NOTIFICATION_TYPE, "2");
            if (application != null) {
                application.getSharedPref().saveSession("FROM", "Notification");
            }
        } else if (type != null && type.equalsIgnoreCase("weekly")) {
            intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.putExtra(Constant.NOTIFICATION_TYPE, "2");
            if (application != null) {
                application.getSharedPref().saveSession("FROM", "Notification_Weekly");
            }
        } else if (type != null && type.equalsIgnoreCase("little_one_birthday")) {
            try {
                little_one_id = new JSONObject(body).optString("little_one_id");
            } catch (Exception e) {
            }
            intent = new Intent(getApplicationContext(), LittleOneProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
            intent.putExtra(Constant.LITTLE_ONE_ID, little_one_id);
            intent.putExtra(Constant.USER_NAME, "");

        } else if (type != null && type.equalsIgnoreCase("little_one_birthday_invite")) {
            try {
                little_one_id = new JSONObject(body).optString("little_one_id");
            } catch (Exception e) {
            }
            intent = new Intent(getApplicationContext(), LittleOneProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_LITTLE_ONE);
            intent.putExtra(Constant.LITTLE_ONE_ID, little_one_id);
            intent.putExtra(Constant.USER_NAME, "");

        } else if (type != null && type.equalsIgnoreCase("silent")/* && title != null && title.equalsIgnoreCase("logout")*/) {
            Log.w("BaseActivity.isActivityVisible", "" + BaseActivity.activity);


            if (BaseActivity.activity != null) {
                BaseActivity.activity.logoutUser( title);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                String NOTIFICATION_CHANNEL_ID = "101";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

                    //Configure Notification Channel
                    notificationChannel.setDescription("Game Notifications");
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                    notificationChannel.enableVibration(true);

                    notificationManager.createNotificationChannel(notificationChannel);
                }

                //https://www.mylineuphub.com/dashboard/assets/files/item_pictures/item_1603365071.jpg
//Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image-url")); //obtain the image
                // .setLargeIcon(bitmap)  //set it in the notification
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notification_icon)
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setSound(defaultSound)
                        //.setContentText(msg)
                        //.setStyle(style)
                        //.setLargeIcon()
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0,  new Intent() , PendingIntent.FLAG_CANCEL_CURRENT))
                        .setPriority(Notification.PRIORITY_MAX);


                notificationManager.notify(1, notificationBuilder.build());

            } else {
                Log.w("hereInsideElse", ".....");
                application.getSharedPref().saveSession(Constant.IS_LOGIN, "");
                application.getSharedPref().clearAll();

                application.getSharedPref().saveSession(Constant.SILENT_NOTIF_MSG, title);

            }

            return;

        } else {
            intent = new Intent(getApplicationContext(), HomeActivity.class);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(intent);

        //PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,PendingIntent.FLAG_ONE_SHOT);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "101";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

            //Configure Notification Channel
            notificationChannel.setDescription("Game Notifications");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        //https://www.mylineuphub.com/dashboard/assets/files/item_pictures/item_1603365071.jpg
//Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image-url")); //obtain the image
        // .setLargeIcon(bitmap)  //set it in the notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(title)
                .setAutoCancel(true)
                .setSound(defaultSound)
                .setContentText(msg)
                .setContentIntent(pendingIntent)
                //.setStyle(style)
                //.setLargeIcon()
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_MAX);


        notificationManager.notify(1, notificationBuilder.build());

        //Simple method for image downloading
        /*public Bitmap getBitmapfromUrl(String imageUrl) {
            try {
                URL url = new URL(imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }*/


    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + this.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(this, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
