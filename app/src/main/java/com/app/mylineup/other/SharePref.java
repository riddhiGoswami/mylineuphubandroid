package com.app.mylineup.other;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.app.mylineup.pojo.loginData.LoginData;
import com.google.gson.Gson;

public class SharePref
{
    private static SharePref sharePref = new SharePref();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences videoSharedPreferences;
    private static SharedPreferences imageSharedPreferences;
    private static SharedPreferences.Editor editor;
    private static SharedPreferences.Editor defaultImageEditor;
    private static SharedPreferences.Editor videoEditor;

    private SharePref() {} //prevent creating multiple instances by making the constructor private

    //The context passed into the getInstance should be Application level context.
    public static SharePref getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constant.USER_PREF, Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        if(videoSharedPreferences == null){
            videoSharedPreferences = context.getSharedPreferences(Constant.VIDEO_PREF, Activity.MODE_PRIVATE);
            videoEditor = videoSharedPreferences.edit();
        }
        if(imageSharedPreferences == null){
            imageSharedPreferences = context.getSharedPreferences(Constant.IMAGE_PREF, Activity.MODE_PRIVATE);
            defaultImageEditor = imageSharedPreferences.edit();
        }
        return sharePref;
    }

    public void saveSession(String key,String placeObjStr) {
        editor.putString(key, placeObjStr);
        editor.commit();
    }

    public void saveImageSession(String key,String placeObjStr) {
        defaultImageEditor.putString(key, placeObjStr);
        defaultImageEditor.commit();
    }

    public void saveVideoData(String key, boolean isSeen){
        videoEditor.putBoolean(key, isSeen);
        videoEditor.commit();
    }

    public boolean isContains(String key){
        return sharedPreferences.contains(key);
    }

    public boolean isContainsVideo(String key){
        return videoSharedPreferences.contains(key);
    }


    public String getSession(String key) {
        return sharedPreferences.getString(key, "");
    }

    public boolean getVideoData(String key) {
        return videoSharedPreferences.getBoolean(key, false);
    }

    public String getDefaultImageData(String key){
        return imageSharedPreferences.getString(key, "");
    }

    public LoginData getUserSession(String key){
        return new Gson().fromJson(sharedPreferences.getString(key, ""),LoginData.class);
    }

    public void removeSession(String key) {
        editor.remove(key);
        editor.commit();
    }

    public void clearAll() {
        editor.clear();
        editor.commit();
    }
}
