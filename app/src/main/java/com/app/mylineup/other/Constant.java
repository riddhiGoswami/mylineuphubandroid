package com.app.mylineup.other;

public class Constant {
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    public static final int REQUEST_CAMERA_PERMISSION = 103;
    public static final int REQUEST_READ_CONTACT_PERMISSION = 107;

    public static final int OPEN_GALLERY = 106;
    public static final int CAMERA_RESULT = 104;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 105;
    public static final int REQUEST_ID_STORAGE_READ_WRITE_PERMISSIONS = 108;
    public static final int REQUEST_SELECT_PICTURE_FOR_FRAGMENT = 0x02;
    //    public static final String PRIVACY_POLICY_URL = "https://www.mylineuphub.com/privacy-policy";
    public static final String COMMON_PDF_URL = "https://docs.google.com/viewer?embedded=true&url=";
    public static final String PRIVACY_POLICY_URL = "https://mylineuphub.com/privacy-policy/";//"https://www.mylineuphub.com/assets/files/privacy-policy.pdf";
    //    public static final String TERMS_OF_SERVICE_URL = "https://www.mylineuphub.com/terms-conditions";
    public static final String TERMS_OF_SERVICE_URL = "https://mylineuphub.com/terms-of-service";//"https://www.mylineuphub.com/assets/files/terms-conditions.pdf";
    //public static final String FAQ_URL=/*"https://mylineuphub.com/faq_app/";*
    public static final String FAQ_URL=    "https://mylineuphub.com/faq/";
    public static final String AFFILIATE_DISCLOUSER_POLICY=    "https://mylineuphub.com/amazon-associate-disclosure-policy/";
    public static final String SUPPORT_MAIL_URL="support@mylineuphub.com";
    public static final String  SUGGESTION_MAIL_URL="suggestions@mylineuphub.com";

    public static String newgpsLatitude = "";
    public static String newgpsLongitude = "";

    public static String SPINNER = "dropdown";
    public static String TEXTVIEW = "textbox";

    public static final String FROM_WISHLIST_DETAIL = "wishlist_detail";
    public static final String FROM_WISHLIST = "wishlist";
    public static final String WISHLIST_DATA = "wishlist_data";
    public static final String FROM_USER_PROFILE = "user_profile";//another user
    public static final String FROM_LITTLE_ONE = "from_little_one";//another user
    public static final String FROM_MYPROFILE = "myprofile";
    public static final String FROM_GIFTFEED = "giftfeed";
    public static final String FROM_EDIT_LITTLE_ONE = "edit_little_one";
    public static final String FROM = "from";
    public static final String LITTLE_ONE_ID = "little_one_id";
    public static final String PARENT_ID = "parent_id";
    public static final String USER_NAME = "user_name";
    public static final String WISHLIST_ID = "wishlist_id";
    public static final String ITEM_ID = "item_id";
    public static final String WISHLIST_NAME = "wishlist_name";
    public static final String ITEM_DETAIL = "item_detail";
    public static final String USER_ID = "user_id";
    public static final String SOCIAL_LOGIN_DATA = "social_login_data";
    public static final String NOTIFICATION_TYPE = "notification_type";//1-news,2-connection,3-wishlist,4-who purchased
    public static final String FROM_SHARE = "from_share";
    public static final String EXP_DATE = "exp_date";

    //Session
    public static final String USER_PREF = "user_pref";
    public static final String VIDEO_PREF = "video_pref";
    public static final String IMAGE_PREF = "image_pref";
    public static final String HEADER_KEY = "header_ket";
    public static final String DEFAULT_ITEM_IMAGE = "default_item_image";
    public static final String HEADER_VALUE = "header_value";
    public static final String NOTIFICATION_TOKEN = "notification_token";
    public static final String IS_LOGIN = "login";

    public static final String LOGIN_DATA = "login_data";

    public static final String WISHLIST_SEEN = "wishlist_seen";
    public static final String CONNECTION_SEEN = "connection_seen";
    public static final String LITTLEONE_SEEN = "littleone_seen";
    public static final String PROFILE_SEEN = "profile_seen";
    public static final String REVIEW_SEEN = "review_seen";

    public static final String SILENT_NOTIF_MSG="silent_notif_msg";

    //Callback
    public static final String LITTLE_ONE_CALLBACK = "little_one_callback";
    public static final String CALLBACK_TAG = "callback_tag";
}
