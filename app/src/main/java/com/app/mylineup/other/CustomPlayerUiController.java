package com.app.mylineup.other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.app.mylineup.R;
import com.app.mylineup.pojo.userProfile.Activity;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBar;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.views.YouTubePlayerSeekBarListener;

public class CustomPlayerUiController extends AbstractYouTubePlayerListener implements YouTubePlayerFullScreenListener {

    private final View playerUi;

    private Context context;
    private YouTubePlayer youTubePlayer;
    private YouTubePlayerView youTubePlayerView;

    // panel is used to intercept clicks on the WebView, I don't want the user to be able to click the WebView directly.
    private View panel;
    private ProgressBar progressbar;
    private TextView videoCurrentTimeTextView;
    private TextView videoDurationTextView;

    private final YouTubePlayerTracker playerTracker;
    private boolean fullscreen = false;
    private AppCompatImageView tv_video_skip;
    private int videoType;
    public YouTubePlayerSeekBar youtube_player_seekbar;
    private  OnSkipClickListener onSkipClickListener;
    public ImageView playPauseButton;

    public CustomPlayerUiController(Context context, View customPlayerUi, YouTubePlayer youTubePlayer, YouTubePlayerView youTubePlayerView, int videoType,OnSkipClickListener onSkipClickListener) {
        this.playerUi = customPlayerUi;
        this.context = context;
        this.youTubePlayer = youTubePlayer;
        this.youTubePlayerView = youTubePlayerView;
        this.videoType = videoType;

        this.onSkipClickListener = onSkipClickListener;


        playerTracker = new YouTubePlayerTracker();
        youTubePlayer.addListener(playerTracker);

        initViews(customPlayerUi);
    }

    private void initViews(View playerUi) {
        tv_video_skip = playerUi.findViewById(R.id.tv_video_skip);
        panel = playerUi.findViewById(R.id.panel);
        youtube_player_seekbar = playerUi.findViewById(R.id.youtube_player_seekbar);
        progressbar = playerUi.findViewById(R.id.progress_bar);

        youTubePlayer.addListener(youtube_player_seekbar);

        youTubePlayer.play();

        tv_video_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context,"Cancel Click...",Toast.LENGTH_LONG).show();
                onSkipClickListener.onSkipClick(videoType);
            }
        });



        youtube_player_seekbar.setYoutubePlayerSeekBarListener(new YouTubePlayerSeekBarListener() {
            @Override
            public void seekTo(float time) {
                youTubePlayer.seekTo(time);
            }
        });

        //videoCurrentTimeTextView = playerUi.findViewById(R.id.video_current_time);
       // videoDurationTextView = playerUi.findViewById(R.id.video_duration);
        playPauseButton = playerUi.findViewById(R.id.play_pause_button);
      //  Button enterExitFullscreenButton = playerUi.findViewById(R.id.enter_exit_fullscreen_button);

        playPauseButton.setOnClickListener((view) -> {
            if (playerTracker.getState() == PlayerConstants.PlayerState.PLAYING) {
                playPauseButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_play_video));
                youTubePlayer.pause();

            }
            else {
                playPauseButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_pause_video));
                youTubePlayer.play();
            }
        });




     /*   enterExitFullscreenButton.setOnClickListener((view) -> {
            if (fullscreen) youTubePlayerView.exitFullScreen();
            else youTubePlayerView.enterFullScreen();

            fullscreen = !fullscreen;
        });*/
    }


    @Override
    public void onReady(@NonNull YouTubePlayer youTubePlayer) {
        //Log.w("insideOnReady","111...");
      //  progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState state) {
        //Log.w("onStateChange","11...");
        if (state == PlayerConstants.PlayerState.PLAYING || state == PlayerConstants.PlayerState.PAUSED || state == PlayerConstants.PlayerState.VIDEO_CUED) {
           // onReady(youTubePlayer);


          /*  new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.w("ecexutedHere","111");
                    if(progressbar != null) {
                        progressbar.setVisibility(View.GONE);
                    }
                }
            }, 5000);*/


            panel.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
        }
        else if (state == PlayerConstants.PlayerState.BUFFERING) {
            //progressbar.setVisibility(View.VISIBLE);
            panel.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onCurrentSecond(@NonNull YouTubePlayer youTubePlayer, float second) {
       // videoCurrentTimeTextView.setText(second + "");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onVideoDuration(@NonNull YouTubePlayer youTubePlayer, float duration) {
       // videoDurationTextView.setText(duration + "");
    }

    @Override
    public void onYouTubePlayerEnterFullScreen() {
        ViewGroup.LayoutParams viewParams = playerUi.getLayoutParams();
        viewParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
        viewParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        playerUi.setLayoutParams(viewParams);
    }

    @Override
    public void onYouTubePlayerExitFullScreen() {
        ViewGroup.LayoutParams viewParams = playerUi.getLayoutParams();
        viewParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        viewParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        playerUi.setLayoutParams(viewParams);
    }

    public interface OnSkipClickListener {
        public void onSkipClick(int videoType);
    }
}