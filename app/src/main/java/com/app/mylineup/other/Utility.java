package com.app.mylineup.other;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.app.mylineup.custom_view.MyEditText;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static androidx.core.util.Preconditions.checkArgument;

public class Utility
{
    public static final String DATE_FORMAT_SHORT = "yyyy/MM/dd";
    public static final String DATE_TIME_FORMAT_SHORT = "yyyy-MM-dd HH:mm";
    public static final String DATE_TIME_SERVER_1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_TIME_SERVER_2 = "yyyy/MM/dd HH/mm";
    public static final String DATE_FORMAT_COMPARE = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_ = "yyyy-MM-dd, EEE";
    public static final String datePattern = "yyyy-MM-dd";
    public static final String datePattern1 = "MM.dd.yyyy";
    public static final String datePattern_us = "MM/dd/yyyy";
    public static final String datePattern_af = "dd/MM/yyyy";

    public static final String wishlistDate = "MMMM dd,yyyy";
    public static final String dayDate = "dd";


    static int phoneNumberPreviousLength=0;
    static EditText editText;


    public static String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }

    public static String getFormattedAmount(Double amount){
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        String formatted = formatter.format(amount); //NumberFormat.getNumberInstance(Locale.US).format(amount);
        return formatted;
    }

    public static String getDateInFormat(String date)
    {
        //YYYY-MM-DD
        try {
            SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
            Date newDate=spf.parse(date);
            spf= new SimpleDateFormat("dd MMM yyyy");
            date = spf.format(newDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDate);
            SimpleDateFormat sdf = new SimpleDateFormat(Utility.dayDate);
            int day = Integer.parseInt(sdf.format(calendar.getTime()));
            sdf = new SimpleDateFormat("MMMM dd,yyyy");
            String p = sdf.format(calendar.getTime());
            //int position = p.indexOf(',');
            //date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
            return p;
        }
        catch (Exception e)
        {return "";}

    }
 public static String getDateInFormatTemp(String date)
    {
        //YYYY-MM-DD
        try {
            SimpleDateFormat spf=new SimpleDateFormat("MMMM dd,yyyy");
            Date newDate=spf.parse(date);
            spf= new SimpleDateFormat("dd MMM yyyy");
            date = spf.format(newDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDate);
            SimpleDateFormat sdf = new SimpleDateFormat(Utility.dayDate);
            int day = Integer.parseInt(sdf.format(calendar.getTime()));
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            String p = sdf.format(calendar.getTime());
            //int position = p.indexOf(',');
            //date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
            return p;
        }
        catch (Exception e)
        {return "";}

    }

    public static class PhoneNumberChecker implements TextWatcher
    {
        private boolean backspacingFlag = false;
        //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
        private boolean editedFlag = false;
        //we need to mark the cursor position and restore it after the edition
        private int cursorComplement;


        public PhoneNumberChecker(MyEditText etPhone)
        {
            editText = etPhone;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            cursorComplement = charSequence.length()-editText.getSelectionStart();
            //we check if the user ir inputing or erasing a character
            if (i1 > i2) {
                backspacingFlag = true;
            } else {
                backspacingFlag = false;
            }
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            String string = s.toString();
            //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
            String phone = string.replaceAll("[^\\d]", "");

            //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
            //if the flag is false, this is a original user-typed entry. so we go on and do some magic
            if (!editedFlag) {

                //we start verifying the worst case, many characters mask need to be added
                //example: 999999999 <- 6+ digits already typed
                // masked: (999) 999-999
                if (phone.length() >= 6 && !backspacingFlag) {
                    //we will edit. next call on this textWatcher will be ignored
                    editedFlag = true;
                    //here is the core. we substring the raw digits and add the mask as convenient
                    String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3,6) + "-" + phone.substring(6);
                    editText.setText(ans);
                    //we deliver the cursor to its original position relative to the end of the string
                    editText.setSelection(editText.getText().length()-cursorComplement);

                    //we end at the most simple case, when just one character mask is needed
                    //example: 99999 <- 3+ digits already typed
                    // masked: (999) 99
                } else if (phone.length() >= 3 && !backspacingFlag) {
                    editedFlag = true;
                    String ans = "(" +phone.substring(0, 3) + ") " + phone.substring(3);
                    editText.setText(ans);
                    editText.setSelection(editText.getText().length()-cursorComplement);
                }
                // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
            } else {
                editedFlag = false;
            }


            /*---------------------------------------------------------------*/

            /*if(editText.getText().length() < phoneNumberPreviousLength)
            {
                phoneNumberPreviousLength = editText.getText().length();
                return;
            }

            String text = editText.getText().toString();
            phoneNumberPreviousLength = editText.getText().length();


            if(phoneNumberPreviousLength == 1)
            {
                editText.setText(new StringBuilder(text).insert(text.length()-1, "(").toString());
                editText.setSelection(editText.getText().length());
            }
            else if(phoneNumberPreviousLength == 5)
            {
                editText.setText(new StringBuilder(text).insert(text.length()-1, ") ").toString());
                editText.setSelection(editText.getText().length());
            }
            else if(phoneNumberPreviousLength == 10)
            {
                editText.setText(new StringBuilder(text).insert(text.length()-1, "-").toString());
                editText.setSelection(editText.getText().length());
            }
            phoneNumberPreviousLength = s.length();*/
        }
    }

    public static String getOriginalString(String name)
    {
        String x = name;
        if(x.contains("\\"))
        {
            x = x.replace("\\","");
        }
        return x;
    }

    public static boolean isValidUrl(String url)
    {
        try {
            new URL(url).toURI();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

}
