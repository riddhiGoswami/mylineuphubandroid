package com.app.mylineup.other;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.util.Log;
import android.widget.DatePicker;

import com.app.mylineup.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import androidx.core.app.ActivityCompat;

public class Global {
    private static Calendar myCalendar = Calendar.getInstance();
    private static Calendar currentDay = Calendar.getInstance();
    private static CallbackDateSelect callbackDateSelect;


    public static void openDatePicker(Context mContext, String str, boolean isFromWishlist, boolean isMinDate, boolean canSelectFutureDate, CallbackDateSelect callbackDateSelect1) {

        Log.w("strDate", "" + str);
        if (!str.equalsIgnoreCase("")) {


            if (isFromWishlist) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = format.parse(str);
                    myCalendar.setTime(date);
                    System.out.println(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else {

                SimpleDateFormat format = new SimpleDateFormat("MMMM dd,yyyy");
                try {
                    Date date = format.parse(str);
                    myCalendar.setTime(date);
                    System.out.println(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            callbackDateSelect = callbackDateSelect1;
            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.datepicker, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            if (isMinDate) {
                datePickerDialog.getDatePicker().setMinDate(currentDay.getTime().getTime());
            }
            if (!canSelectFutureDate) {
                datePickerDialog.getDatePicker().setMaxDate(currentDay.getTime().getTime());
            }

            datePickerDialog.show();

        }else{
            myCalendar = Calendar.getInstance();
            callbackDateSelect = callbackDateSelect1;
            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.datepicker, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            if (isMinDate) {
                datePickerDialog.getDatePicker().setMinDate(currentDay.getTime().getTime());
            }
            if (!canSelectFutureDate) {
                datePickerDialog.getDatePicker().setMaxDate(currentDay.getTime().getTime());
            }

            datePickerDialog.show();
        }




    }


    public static void openDatePickerCustomMinimumDate(Context mContext, String str, boolean isFromWishlist, String minDate, boolean canSelectFutureDate, CallbackDateSelect callbackDateSelect1) {

        Log.w("strDate", "" + str);
        if (!str.equalsIgnoreCase("")) {


            if (isFromWishlist) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = format.parse(str);
                    myCalendar.setTime(date);
                    System.out.println(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else {

                SimpleDateFormat format = new SimpleDateFormat("MMMM dd,yyyy");
                try {
                    Date date = format.parse(str);
                    myCalendar.setTime(date);
                    System.out.println(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            callbackDateSelect = callbackDateSelect1;
            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.datepicker, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date mDate = sdf.parse(minDate);
                long timeInMilliseconds = mDate.getTime();
                datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (!canSelectFutureDate) {
                datePickerDialog.getDatePicker().setMaxDate(currentDay.getTime().getTime());
            }

            datePickerDialog.show();

        }else{
            myCalendar = Calendar.getInstance();
            callbackDateSelect = callbackDateSelect1;
            DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, R.style.datepicker, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date mDate = sdf.parse(minDate);
                long timeInMilliseconds = mDate.getTime();
                datePickerDialog.getDatePicker().setMinDate(timeInMilliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (!canSelectFutureDate) {
                datePickerDialog.getDatePicker().setMaxDate(currentDay.getTime().getTime());
            }

            datePickerDialog.show();
        }




    }


    static DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern, Locale.US);

            if (callbackDateSelect != null) {
                callbackDateSelect.OnDateSelect(myCalendar);
            }
        }

    };

    public interface CallbackDateSelect {
        void OnDateSelect(Calendar myCalendar);
    }

    public static boolean isEmailValidat(String email) {
        if (email == null || email.trim().equalsIgnoreCase("")) {
            return false;
        }
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static boolean isValidString(String string) {
        if (string != null && !string.trim().equalsIgnoreCase("")) {
            return true;//valid
        }
        return false;
    }
}
