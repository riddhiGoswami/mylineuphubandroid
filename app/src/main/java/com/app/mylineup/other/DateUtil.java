package com.app.mylineup.other;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public final class DateUtil{
    public static final String DATE_FORMAT_Y_M_D = "yyyy-MM-dd";
    public static final String DATE_FORMAT_D_M_Y = "dd-MM-yyyy";
    public static final String DATE_FORMAT_M_D_Y = "MMM dd, yyyy";
    public static final String DATE_FORMAT_M_D = "MMM dd";
    public static final String DATE_TIME_FORMAT = "dd-MM-yyyy hh:mm a";
    public static final String TIME_FORMAT = "hh:mm a";
    public static final String TIME_FORMAT_WITHOUT_MERIDIAN = "hh:mm";
    public static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static final String REVERSE_DATE_FORMAT = "yyyy-MM-dd hh:mm";
    public static final String DATE_FORMAT_REVERSED = "hh:mm:ss yyyy-MM-dd";
    public static final String TAG = "DateUtil";

    public static String hourOFDayString(String dateString){
        return String.valueOf(hourOfDay(dateString));
    }

    public static Calendar calendar(String dateString){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(toDate(dateString));
        long offset = timeZoneOffset();
        calendar.setTimeInMillis(calendar.getTimeInMillis() + offset);
        return calendar;
    }

    public static int hourOfDay(String dateString){
        return calendar(dateString).get(Calendar.HOUR_OF_DAY);
    }

    public static long timeInMillis(String dateString){
        return calendar(dateString).getTimeInMillis();
    }

    public static Date toDate(String dateString){
        return toDate(dateString, DATE_FORMAT);
    }

    public static String toDate(Date date, String format){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Boolean isDateInThePast(String dateString, String format){
        try {

            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            Log.w("sdf.parse(dateString)","Item date => "+sdf.parse(dateString));
            Log.w("sdf.parse(dateString)","Today date => "+new Date(System.currentTimeMillis()));
            return sdf.parse(dateString).before(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date toDate(String dateString, String format){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toDate(Date date, String format, TimeZone tz){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            sdf.setTimeZone(tz);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toDate(Date date){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_D_M_Y, Locale.US);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toTime(Date date){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_WITHOUT_MERIDIAN, Locale.US);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toDateString(String dateString, String fromFormat, String toFormat){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(fromFormat, Locale.US);
            SimpleDateFormat sdf2 = new SimpleDateFormat(toFormat, Locale.US);
            return sdf2.format(sdf.parse(dateString));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toTimeString(String timeString) {
        if (timeString==null || timeString.isEmpty()) {
            return "";
        }
        else if (timeString.equalsIgnoreCase("AM") || timeString.equalsIgnoreCase("PM")) {
            return timeString;
        }
        Log.e(TAG, "toTimeString: "+timeString);
        String[] timeArray = timeString.split(":");
        int h = Integer.parseInt(timeArray[0]);
        if (timeString.contains("PM")) {
            return (h+12)+":"+(timeArray[1].split(" ")[0]);
        }
        else if (timeString.contains("AM")) {
            return (h-12)+":"+(timeArray[1].split(" ")[0]);
        }
        else {
            if (h>12) {
                return (h-12)+":"+timeArray[1]+" PM";
            }
            else {
                return h+":"+timeArray[1]+" AM";
            }
        }
    }

    public static String manipulateTime(String timeString, int count, boolean toAdd) {
        if (timeString == null || timeString.isEmpty()) {
            return "";
        } else if (timeString.equalsIgnoreCase("AM") || timeString.equalsIgnoreCase("PM")) {
            return timeString;
        }
        Log.e(TAG,"toTimeString: " + timeString);
        String[] timeArray = timeString.split(":");
        int h = Integer.parseInt(timeArray[0]);
        if (toAdd) {
            return (h+count)+":"+timeArray[1];
        }
        else {
            return (h-count)+":"+timeArray[1];
        }
    }

    public static String toDateTime(String dateString){
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.parseLong(dateString));
            return new SimpleDateFormat(DATE_TIME_FORMAT, Locale.US).format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toDateTimeReversed(String dateString){
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.parseLong(dateString));
            return new SimpleDateFormat(DATE_FORMAT_REVERSED, Locale.US).format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int timeZoneOffset(){
        TimeZone timezone = TimeZone.getDefault();
        return timezone.getOffset(Calendar.ZONE_OFFSET);
    }

    public static String toDateDmy(String dateString){
        try {
            // To get the required format, we must get the time from calendar
            Calendar cal = calendar(dateString);
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_D_M_Y, Locale.US);
            sdf.setTimeZone(Calendar.getInstance().getTimeZone());
            return sdf.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String hourAs12H(int hourOfDay) {
        int  hour = hourOfDay;

        String timeSet = "AM";

        if (hour > 12) {
            hour -= 12;
            timeSet = "PM";
        }
        else if (hour == 0) {
            hour = 12;
            timeSet = "AM";
        }
        else if (hour == 12)
            timeSet = "PM";

        return new StringBuilder()
                .append(hour)
                .append(" ")
                .append(timeSet)
                .append(" ")
                .toString();
    }

    public static String currentUtcTime(){
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(utcTimeZone());
        return df.format(new Date());
    }

    public static long epochTime(){
        return System.currentTimeMillis();
    }

    private static TimeZone utcTimeZone(){
        return TimeZone.getTimeZone("UTC");
    }
}
