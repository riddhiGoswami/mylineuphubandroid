package com.app.mylineup.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.LoginFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.L;
import com.app.mylineup.R;
import com.app.mylineup.adapter.CustomSpinner;
import com.app.mylineup.custom_view.MyEditText;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.databinding.ActivityCreateItemBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackImage;
import com.app.mylineup.interfaces.CallbackWishList;
import com.app.mylineup.interfaces.WishListSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.other.PictureOrientation;
import com.app.mylineup.other.RealPathUtil;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.AmazonItemData;
import com.app.mylineup.pojo.myProfile.MyProfile;
import com.app.mylineup.pojo.wishlistItems.Item;
import com.app.mylineup.web_services.PrettyPrinter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.youtube.player.YouTubePlayer;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.util.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.DataBindingUtil;
import androidx.loader.content.CursorLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.provider.Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION;

public class CreateItemActivity extends ImageActivity /*implements VideoViewDialog.OnSkipClickListener*/ {
    private static final String TAG = "CreateItemActivity";
    public static CreateItemActivity activity;
    private ActivityCreateItemBinding binding;
    private boolean isFirstSize = true;
    private boolean isFirstCategory = true;
    private boolean isFirstWishlist = true;

    private ArrayAdapter aa2;//for wishlist spinner
    String[] wishlist;
    private String imgPath = null;
    private String selectedWishlistId = "", wishlistName = "";
    private Item item = null;//for edit
    private boolean isImageRemoveOnly = false;

    public boolean isFromShare = false;
    private MyProfile userProfile;
    private AmazonItemData amazonItemData;
    private ArrayAdapter<String> adapter;
    private LinearLayout llSpinnerWishList;
    private Uri someUri;
    boolean isShareIntent = false;
    private String urlDataIntent = "";
    private static VideoViewDialog videoViewDialog;
    private boolean isAddWishlistClick = false;
    private boolean hasImage = false;
    private static final int REQUEST_APP_SETTINGS = 168;
    public boolean isGoToSetting = false;
    public boolean isDialogAlreadyDisplay = false;
    private CallbackWishList callbackWishList;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_item);
        activity = CreateItemActivity.this;
        isGoToSetting = false;
        isDialogAlreadyDisplay = false;

        callbackWishList = WishListSingleton.getCallBack();

        Log.w("hereOnCreateItem", ".....");
        if (loginData == null) {
            Intent intent = new Intent(activity, LoginActivity.class);

            //  checkIntentFirst(getIntent());
           /* String receivedAction = getIntent().getAction();
            Intent tempIntent = new Intent();
            if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {

                Log.w("someUri", "" + someUri);
                if (someUri != null) {
                    tempIntent.putExtra(Intent.EXTRA_STREAM, someUri);
                    tempIntent.setAction(Intent.ACTION_SEND);
                    tempIntent.setType("image/jpeg");

                    getIntent().addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    intent.putExtra("shareIntent", tempIntent);
                }
            }else{
                getIntent().addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra("shareIntent", getIntent());
            }


            Log.w("getIntent()", "" + getIntent());
*/

            getIntent().addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra("shareIntent", getIntent());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            finish();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            return;
        }

        init();
        Log.w("onSharedIntent()", "onCreate");

        /*if (getIntent().getAction() != null || getIntent().getData() != null) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                String[] PERMISSIONS = {
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                };

                Log.e("requestPermissionIntent", ".........");
                ActivityCompat.requestPermissions(this, PERMISSIONS, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
                return;
            }
        }*/


        // onSharedIntent();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getSetIntentData(intent);
    }


    private void getSetIntentData(Intent intent) {


        String receivedAction = intent.getAction();
        String receivedType = intent.getType();
        String receivedData = intent.getDataString();


        Log.w("receivedDataShare", "Action => " + receivedAction);
        Log.w("receivedDataShare", "Type => " + receivedType);
        Log.w("receivedDataShare", "Data => " + receivedData);
        Log.w("receivedDataShare", "EXTRA_TEXT => " + intent.getStringExtra(Intent.EXTRA_TEXT));

        urlDataIntent = intent.getStringExtra(Intent.EXTRA_TEXT);
        //new MyTask().execute();
       /* if(!urlDataIntent.equalsIgnoreCase("")) {
            callGetAmazonDataApi(urlDataIntent);
        }*/


        if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {
            isFromShare = true;
            isShareIntent = true;
           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (!Environment.isExternalStorageManager()) {
                    Intent i = new Intent();
                    i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    startActivityForResult(i, 20);
                    // startActivity(i);
                } else {
                    openGalleryPermission();
                    return;
                }
            } else {
                openGalleryPermission();
                return;
            }*/


            // check mime type
            if (receivedType != null && receivedType.startsWith("text/")) {

                String receivedText = intent
                        .getStringExtra(Intent.EXTRA_TEXT);
                if (receivedText != null) {
                    //do your stuff
                    if (receivedText.contains("http")) {
                        Log.e("indexOfHttp", "" + receivedText.indexOf("http"));
                        if (receivedText.indexOf("http") == 0) {
                            binding.etUrl.setText(receivedText);
                        } else {
                            String[] urlAry = receivedText.split("http");
                            binding.etUrl.setText("http" + urlAry[1]);
                            String[] titleAry = urlAry[0].split("\\|");
                            for (int i = 0; i < titleAry.length; i++) {
                                Log.e("titleAry", "" + titleAry[i]);
                            }
                            binding.etItemName.setText(titleAry[0]);
                        }

                    } else {
                        binding.etItemName.setText(receivedText);
                    }
                }
            } else if (receivedType != null && receivedType.startsWith("image/")) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    String[] PERMISSIONS = {
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                    };

                    Log.e("requestPermissionIntent", ".........");
                    ActivityCompat.requestPermissions(this, PERMISSIONS, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
                    return;
                }
                Uri receiveUri = (Uri) intent
                        .getParcelableExtra(Intent.EXTRA_STREAM);
                Log.w("receiveUri", "" + receiveUri);

                if (receiveUri != null) {
                    //do your stuff

                    Log.w("imgPathOnShareIntent", "" + imgPath);

//                    if (receiveUri.toString().contains("/MyLineup Hub")) {
//                        Log.e(TAG, "getSetIntentData: /MyLineup Hub found");
//                        imgPath = receiveUri.toString();
                    if (imgPath == null) {
                        try {

                            if (receiveUri.toString().contains("/storage/emulated/")) {
                                Uri tempUri = Uri.parse("file:/" + receiveUri);
                                Log.w("tempUri", "" + tempUri);
                                InputStream inputStream = getContentResolver().openInputStream(tempUri);
                                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                           /* Log.w("realpathUri",""+Build.VERSION.SDK_INT);
                            Log.w("realpathUri",getRealPathFromURI_API19(this, receiveUri));

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), receiveUri);*/
                                binding.ivItemImg.setImageBitmap(bitmap);
                            } else {
                                InputStream inputStream = getContentResolver().openInputStream(receiveUri);
                                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                saveImageBitmap(bitmap);

                           /* Log.w("realpathUri",""+Build.VERSION.SDK_INT);
                            Log.w("realpathUri",getRealPathFromURI_API19(this, receiveUri));

                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), receiveUri);*/
                                binding.ivItemImg.setImageBitmap(bitmap);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
//
//                    } else {
//                        Log.e(TAG, "getSetIntentData: /MyLineup Hub not found");
//                        if (imgPath == null) {
//                            try {
//                                InputStream inputStream = getContentResolver().openInputStream(receiveUri);
//                                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                                saveImageBitmap(bitmap);
//                            } catch (FileNotFoundException e) {
//                                e.printStackTrace();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            Glide.with(this).load(receiveUri).into(binding.ivItemImg);
//                        }
//                    }
                }
            }

        } else if (receivedAction != null && receivedAction.equals(Intent.ACTION_MAIN)) {

            Log.e("TAG", "onSharedIntent: nothing shared");
        }
        /*else{
            Glide.with(this).load(application.getSharedPref().getSession(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
            imgPath = null;
        }*/
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{id}, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }

        cursor.close();

        return filePath;
    }


    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }

        return result;
    }


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        Log.w("onResumeGetIntentData", "" + getIntent().getAction());
        Log.w("onResumeGetIntentData", "" + isGoToSetting);
        Log.w("onResumeGetIntentData", "" + isDialogAlreadyDisplay);
        Log.w("onResumeWishlist", "" + binding.spinnerWishList.getSelectedItemPosition() + " Total items" + binding.spinnerWishList.getCount());
/*
        if (binding.spinnerWishList.getSelectedItemPosition() == (binding.spinnerWishList.getCount()-1)){
            Log.w("onResumeWishlist", "" + binding.spinnerWishList.getSelectedItemPosition());
            binding.spinnerWishList.setSelection(binding.spinnerWishList.getCount()-1);
            binding.spinnerWishList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Log.w("onItemSelectedResume","..."+position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }*/


      /*  if(isGoToSetting){
            isGoToSetting = false;
            if(isDialogAlreadyDisplay){
                isDialogAlreadyDisplay = false;*/

        onSharedIntent();
            /*}
        }*/

       /* if(!isDialogAlreadyDisplay) {
            if (isGoToSetting) {
                isGoToSetting = false;
                Log.w("onSharedIntent()", "onResume");

                if(getIntent().getAction() != null){
                    onSharedIntent();
                }
            }
        }else{
             if (isGoToSetting) {
                isGoToSetting = false;
                Log.w("onSharedIntent()", "onResume");
                onSharedIntent();

            }
        }*/
    }

    private void onSharedIntent() {
        Log.w("onSharedIntent()", ".........");
        Intent receiverdIntent;
        if (getIntent().hasExtra("shareIntent")) {
            receiverdIntent = getIntent().getParcelableExtra("shareIntent");
        } else {
            receiverdIntent = getIntent();
        }

        getSetIntentData(receiverdIntent);
    }

    public void openGalleryLocalPermission() {

        Log.e("openGalleryPermission", "111...");
    /*    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            ActivityCompat.requestPermissions(this, new
                    String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,*//* Manifest.permission.MANAGE_EXTERNAL_STORAGE*//*}, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
        } else {*/
        ActivityCompat.requestPermissions(this, new
                String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
        //}
    }

    public void compressImage(Uri uri) {
        Bitmap bitmap = null;
        try {
            bitmap = getThumbnail(uri);
            saveImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public void saveImageBitmap(Bitmap finalBitmap) {
        Log.w("finalBitmap", "" + finalBitmap);

        File file = null;
        try {
            file = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgPath = file.getAbsolutePath().toString();
        someUri = Uri.parse(file.getAbsolutePath().toString());
//        Glide.with(this).load(someUri).into(binding.ivItemImg);
        Log.e("imgPath", "" + imgPath);
        Log.e("someUri", "" + someUri);

        if (loginData == null) {
            Intent intent = new Intent(activity, LoginActivity.class);

            String receivedAction = getIntent().getAction();
            Intent tempIntent = new Intent();
            if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {

                Log.w("someUri", "" + someUri);
                if (someUri != null) {
                    tempIntent.putExtra(Intent.EXTRA_STREAM, someUri);
                    tempIntent.setAction(Intent.ACTION_SEND);
                    tempIntent.setType("image/jpeg");

                    getIntent().addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    intent.putExtra("shareIntent", tempIntent);
                }
            } else {
                getIntent().addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                intent.putExtra("shareIntent", getIntent());
            }
        }

    }

    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = new File(Environment.getExternalStorageDirectory() +
                File.separator +
                getString(R.string.app_name)
        );

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        imageFilePath = image.getAbsolutePath();
        Log.w("imageFilePath", "" + imageFilePath);

        return image;
    }


    private void checkIntentFirst(Intent intent) {

        String receivedAction = intent.getAction();

        Log.w("receivedDataShare", "Action => " + receivedAction);
        Log.w("receivedDataShare", "EXTRA_TEXT => " + intent.getStringExtra(Intent.EXTRA_TEXT));

        if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                String[] PERMISSIONS = {
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                };

                Log.e("requestPermissionIntent", ".........");
                ActivityCompat.requestPermissions(this, PERMISSIONS, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
                return;
            }
            Uri receiveUri = (Uri) intent
                    .getParcelableExtra(Intent.EXTRA_STREAM);
            Log.w("receiveUri", "" + receiveUri);

            if (receiveUri != null) {

                try {
                    InputStream inputStream = getContentResolver().openInputStream(receiveUri);

                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    saveImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void init() {
        Log.w("createItem", "" + (getIntent().getExtras() != null && getIntent().hasExtra(Constant.WISHLIST_NAME)));
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.WISHLIST_NAME)) {
            wishlistName = getIntent().getStringExtra(Constant.WISHLIST_NAME);
            Log.w("createItem", "wishlistName => " + wishlistName);
        }

        String title = "";
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.ITEM_DETAIL)) {
            item = (Item) getIntent().getSerializableExtra(Constant.ITEM_DETAIL);
            title = getString(R.string.lbl_edit_item);
        } else {

            Uri resultUri = getIntent().getData();
            title = getString(R.string.lbl_create_new_item);
            Log.w("resultUri", "" + resultUri);
            if (resultUri != null) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    binding.ivItemImg.setImageBitmap(bitmap);
                    imgPath = RealPathUtil.getRealPath(activity, resultUri);
                    Log.w("imgPath", "here init => " + imgPath);
                    someUri = resultUri;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (getIntent() != null && getIntent().getAction() != null && getIntent().getAction().equals(Intent.ACTION_SEND)) {
                    if (getIntent().getType() != null && getIntent().getType().startsWith("text/")) {
                        Log.e("itsImagWeGot", "11...");
                        Picasso.get().load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).fit().into(binding.ivItemImg);
                        //binding.tvDelete.setVisibility(View.GONE);
                        binding.tvDelete.setVisibility(View.VISIBLE);
                        binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                        binding.tvDelete.setEnabled(false);
                        setTextViewDrawableColor(true);
                    }
                } else {
                    Picasso.get().load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).fit().into(binding.ivItemImg);
                    //binding.tvDelete.setVisibility(View.GONE);
                    binding.tvDelete.setVisibility(View.VISIBLE);
                    binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                    binding.tvDelete.setEnabled(false);
                    setTextViewDrawableColor(true);
                }
                //  Glide.with(this).load(application.getSharedPref().getSession(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                imgPath = null;
            }
        }

        setHeader(title, null, null);


        String[] country = {"Select...", "Footwear", "Food", "Other"};
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.item_spinner, country);
        aa.setDropDownViewResource(R.layout.item_spinner_dropdown);
        binding.spinnerCategory.setAdapter(aa);
        binding.spinnerCategory.setOnItemSelectedListener(listener);

        String[] size = {"Select...", "10.5 M(US)", "11.5 M(US)", "Other"};
        ArrayAdapter aa1 = new ArrayAdapter(this, R.layout.item_spinner, size);
        aa1.setDropDownViewResource(R.layout.item_spinner_dropdown);
        binding.spinnerSize.setAdapter(aa1);
        binding.spinnerSize.setOnItemSelectedListener(listener);


        if (loginData.getWishlist() == null) {
            wishlist = new String[2];
            wishlist[0] = "Select...";
            wishlist[1] = "Create Wishlist";
        } else {
            wishlist = new String[loginData.getWishlist().size() + 2];
            wishlist[0] = "Select...";
            for (int i = 0; i < loginData.getWishlist().size(); i++) {
                wishlist[i + 1] = loginData.getWishlist().get(i).getName();
            }
            wishlist[loginData.getWishlist().size() + 1] = "Create Wishlist";
        }

        aa2 = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, wishlist);
        //aa2.setDropDownViewResource(R.layout.item_spinner_dropdown);
        binding.spinnerWishList.setAdapter(aa2);


        llSpinnerWishList = findViewById(R.id.llSpinnerWishList);

        llSpinnerWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                binding.spinnerWishList.performClick();
            }
        });
        // binding.spinnerWishList.setOnItemSelectedListener(listener);

        binding.spinnerWishList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                Log.w("isFirstWishlist", "" + isFirstWishlist);
                Log.w("isFirstWishlistItem", "" + (wishlist[position].equalsIgnoreCase("Create Wishlist")));
                if (isFirstWishlist) {
                    selectedWishlistId = "";
                    wishlistName = "";
                    isFirstWishlist = !isFirstWishlist;
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                }
                if (wishlist[position].equalsIgnoreCase("Create Wishlist")) {
                    selectedWishlistId = "";
                    wishlistName = "";
                    isAddWishlistClick = true;
                    callDialog();

                    /*Intent videoIntent = new Intent(activity, VideoViewDialog.class);
                    videoIntent.putExtra("videoType",1);
                    videoIntent.putExtra("intentToPass",intent);
                    videoIntent.putExtra("hasReqCode",10);
                    startActivity(videoIntent);*/

                   /* Intent intent = new Intent(activity, CreateWishListActivity.class);
                    intent.putExtra(Constant.FROM, CreateItemActivity.class.getName());
                    startActivityForResult(intent, 10);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                    isFirstWishlist = !isFirstWishlist;
                    binding.spinnerWishList.setSelection(0);*/
                } else {
                    if (loginData.getWishlist() == null) return;
                    if (wishlist[position].equalsIgnoreCase("Select...")) {
                        selectedWishlistId = "";
                        wishlistName = "";
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    } else {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));
                    }

                    for (int i = 0; i < loginData.getWishlist().size(); i++) {
                        if (wishlist[position].equalsIgnoreCase(loginData.getWishlist().get(i).getName())) {
                            selectedWishlistId = loginData.getWishlist().get(i).getId();
                            wishlistName = loginData.getWishlist().get(i).getName();
                        }
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Log.w("loginData.getWishlist()", "" + loginData.getWishlist());

        if (!wishlistName.equalsIgnoreCase("")) {
            for (int i = 0; i < wishlist.length; i++) {
                if (wishlist[i].equalsIgnoreCase(wishlistName)) {
                    binding.spinnerWishList.setSelection(i);
                    break;
                }
            }
        }

        //open screen in edit mode so set value
        if (item != null) {
            setValue(item);
        }

        binding.etItemName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.tvTextCount.setText(s.length() + "/120");
            }
        });


        binding.etDes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.tvTextDesCount.setText(s.length() + "/120");
            }
        });

        binding.etDes.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (binding.etDes.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

        //binding.etPrice.addTextChangedListener(new CurrencyTextWatcher());
        //  binding.etPrice.addTextChangedListener(new NumberTextWatcher(binding.etPrice, "#,###.##"));
        binding.etPrice.addTextChangedListener(new NumberTextWatcherWithSeperator(binding.etPrice));

       /* binding.etPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               // binding.etPrice.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String str = String.valueOf(editable);
                    if (!str.equalsIgnoreCase("")) {
                        binding.etPrice.removeTextChangedListener(this);

                        String newStr = str.replace(",", "");
                        String convertedStr = doubleToStringNoDecimal(Double.parseDouble(newStr));
                        Log.w("doubleString", convertedStr);
                         binding.etPrice.setText(convertedStr);

                        binding.etPrice.addTextChangedListener(this);
                        binding.etPrice.setSelection(binding.etPrice.getText().length());

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });*/

        binding.tvRetake.setOnClickListener(v ->
        {
            /*callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                startCrop(imageUri);

            },true,false,true);*/
            boolean isRemoveImageOptionDisplay = false;
            if (imgPath != null) {
                isRemoveImageOptionDisplay = true;
            } else {
                isRemoveImageOptionDisplay = false;
            }

            callbackImage.openImageChooser(new CallbackImageListener() {
                @Override
                public void onImageSuccess(String imageFilePath, Uri imageUri, int cameraPhotoOrientation) {
                    Log.w("imageUri", "" + imageUri);
                    if (imageUri == null) {
                        //  Glide.with(this).load(application.getSharedPref().getSession(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                        Log.e("imgPath", "" + imgPath);
                        if (imgPath != null && imgPath.contains("http")) {
                            hasImage = true;
                            imgPath = null;
                            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.msg_delete_item_image), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    String status = NetworkUtil.getConnectivityStatusString(activity);
                                    if (status.equalsIgnoreCase("Not connected to Internet"))
                                        dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                                    else
                                        removeItemImgCallApi();
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });
                        } else {
                            if (hasImage) {
                                removeItemImgCallApi();
                            }
                            imgPath = null;
                            Glide.with(activity).load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                            //binding.tvDelete.setVisibility(View.GONE);
                            binding.tvDelete.setVisibility(View.VISIBLE);
                            binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                            binding.tvDelete.setEnabled(false);
                            setTextViewDrawableColor(true);
                        }

                    } else {
                        CreateItemActivity.this.imageUri = imageUri;
                        CreateItemActivity.this.startCrop(imageUri);
                    }

                }
            }, false, true, false, isRemoveImageOptionDisplay, CreateItemActivity.this);
        });

        binding.tvDelete.setOnClickListener(v ->
        {

            Log.w("itemImage", "" + item);
            if (item == null) {
                //binding.ivItemImg.setImageBitmap(null);
                Picasso.get().load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).fit().into(binding.ivItemImg);
                //  Glide.with(this).load(application.getSharedPref().getSession(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                imgPath = null;
                //binding.tvDelete.setVisibility(View.GONE);
                binding.tvDelete.setVisibility(View.VISIBLE);
                binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                binding.tvDelete.setEnabled(false);
                setTextViewDrawableColor(true);
            } else {
                if (imgPath != null) {
                    if (imgPath.contains("http") && !imgPath.contains("default")) {
                        dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.msg_delete_item_image), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                String status = NetworkUtil.getConnectivityStatusString(activity);
                                if (status.equalsIgnoreCase("Not connected to Internet"))
                                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                                else
                                    removeItemImgCallApi();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        Glide.with(activity).load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                        //binding.tvDelete.setVisibility(View.GONE);
                        binding.tvDelete.setVisibility(View.VISIBLE);
                        binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                        binding.tvDelete.setEnabled(false);
                        setTextViewDrawableColor(true);
                    }
                } else {
                    Glide.with(activity).load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                    //binding.tvDelete.setVisibility(View.GONE);
                    binding.tvDelete.setVisibility(View.VISIBLE);
                    binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                    binding.tvDelete.setEnabled(false);
                    setTextViewDrawableColor(true);
                }

            }
        });

        binding.tvSaveItem.setOnClickListener(v ->
        {
            boolean isValid = true;
            String message = "";
            if (binding.etItemName.getText().toString().equalsIgnoreCase("")) {
                isValid = false;
                dialogCommon.showDialog(getString(R.string.please_enter_item_name));
                binding.etItemName.requestFocus();
            } else if (binding.etBrandName.getText().toString().equalsIgnoreCase("")) {
                isValid = false;
                dialogCommon.showDialog(getString(R.string.please_enter_brand_name));
                binding.etBrandName.requestFocus();
            } else if (binding.etPrice.getText().toString().equalsIgnoreCase("")) {
                isValid = false;
                dialogCommon.showDialog(getString(R.string.please_enter_price));
                binding.etPrice.requestFocus();
            } else if (selectedWishlistId.equalsIgnoreCase("")) {
                isValid = false;
                dialogCommon.showDialog(getString(R.string.please_select_wishlist));
            }

            if (!binding.etUrl.getText().toString().trim().isEmpty()) {
                if (!Patterns.WEB_URL.matcher(binding.etUrl.getText().toString().trim()).matches()) {
                    isValid = false;
                    dialogCommon.showDialog(getString(R.string.not_a_valid_item_url));
                }
            }

            /*else if(binding.etUrl.getText().toString().equalsIgnoreCase("") || !URLUtil.isValidUrl(binding.etUrl.getText().toString().trim())){
                dialogCommon.showDialog(getString(R.string.please_enter_valid_url));
                binding.etUrl.requestFocus();
            }else if(binding.etSize.getText().toString().equalsIgnoreCase("")){
                dialogCommon.showDialog(getString(R.string.please_enter_size));
                binding.etSize.requestFocus();
            }*/ /*else if(binding.etWhereToBuy.getText().toString().equalsIgnoreCase("")){
                dialogCommon.showDialog(getString(R.string.please_enter_where_to_buy));
                binding.etWhereToBuy.requestFocus();
            }*/

            if (isValid) {
                if (item == null) {
                    String status = NetworkUtil.getConnectivityStatusString(activity);
                    if (status.equalsIgnoreCase("Not connected to Internet"))
                        dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                    else
                        addItemApiCall();
                } else {
                    String status = NetworkUtil.getConnectivityStatusString(activity);
                    if (status.equalsIgnoreCase("Not connected to Internet"))
                        dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                    else
                        editItemApi();
                }
            }
        });

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            callMyProfileApi();
    }

    public class NumberTextWatcherWithSeperator implements TextWatcher {

        private MyEditText editText;


        public NumberTextWatcherWithSeperator(MyEditText editText) {
            this.editText = editText;


        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            try {
                editText.removeTextChangedListener(this);
                String value = editText.getText().toString();

                if (!value.equals("")) {

                    if (value.startsWith(".")) {
                        editText.setText("0.");
                    }
                    if (value.startsWith("0") && !value.startsWith("0.")) {
                        editText.setText("");

                    }

                    String str = editText.getText().toString().replaceAll(",", "");
                    if (!value.equals(""))
                        editText.setText(getDecimalFormattedString(str));
                    editText.setSelection(editText.getText().toString().length());
                }
                editText.addTextChangedListener(this);
            } catch (Exception ex) {
                ex.printStackTrace();
                editText.addTextChangedListener(this);
            }
        }
    }

    private static String getDecimalFormattedString(String value) {
        StringTokenizer lst = new StringTokenizer(value, ".");
        String str1 = value;
        String str2 = "";
        if (lst.countTokens() > 1) {
            str1 = lst.nextToken();
            str2 = lst.nextToken();
        }
        String str3 = "";
        int i = 0;
        int j = -1 + str1.length();
        if (str1.charAt(-1 + str1.length()) == '.') {
            j--;
            str3 = ".";
        }
        for (int k = j; ; k--) {
            if (k < 0) {
                if (str2.length() > 0)
                    str3 = str3 + "." + str2;
                return str3;
            }
            if (i == 3) {
                str3 = "," + str3;
                i = 0;
            }
            str3 = str1.charAt(k) + str3;
            i++;
        }

    }


    public class NumberTextWatcher implements TextWatcher {

        private final DecimalFormat df;
        private final DecimalFormat dfnd;
        private final MyEditText et;
        private boolean hasFractionalPart;
        private int trailingZeroCount;

        public NumberTextWatcher(MyEditText editText, String pattern) {
            df = new DecimalFormat(pattern);
            df.setDecimalSeparatorAlwaysShown(true);
            dfnd = new DecimalFormat("#,###.00");
            this.et = editText;
            hasFractionalPart = false;
        }

        @Override
        public void afterTextChanged(Editable s) {
            et.removeTextChangedListener(this);

            if (s != null && !s.toString().isEmpty()) {
                try {
                    int inilen, endlen;
                    inilen = et.getText().length();
                    String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "").replace("$", "");
                    Number n = df.parse(v);
                    int cp = et.getSelectionStart();
                    if (hasFractionalPart) {
                        StringBuilder trailingZeros = new StringBuilder();
                        while (trailingZeroCount-- > 0)
                            trailingZeros.append('0');
                        et.setText(df.format(n) + trailingZeros.toString());
                    } else {
                        et.setText(dfnd.format(n));
                    }
                    et.setText("$".concat(et.getText().toString()));
                    endlen = et.getText().length();
                    int sel = (cp + (endlen - inilen));
                    if (sel > 0 && sel < et.getText().length()) {
                        et.setSelection(sel);
                    } else if (trailingZeroCount > -1) {
                        et.setSelection(et.getText().length() - 3);
                    } else {
                        et.setSelection(et.getText().length());
                    }
                } catch (NumberFormatException | ParseException e) {
                    e.printStackTrace();
                }
            }

            et.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int index = s.toString().indexOf(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()));
            trailingZeroCount = 0;
            if (index > -1) {
                for (index++; index < s.length(); index++) {
                    if (s.charAt(index) == '0')
                        trailingZeroCount++;
                    else {
                        trailingZeroCount = 0;
                    }
                }
                hasFractionalPart = true;
            } else {
                hasFractionalPart = false;
            }
        }
    }

    class CurrencyTextWatcher implements TextWatcher {

        boolean mEditing;

        public CurrencyTextWatcher() {
            mEditing = false;
        }

        public synchronized void afterTextChanged(Editable s) {
            if (!mEditing) {
                mEditing = true;

                String digits = s.toString().replaceAll("\\D", "");
                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                try {
                    formatter.applyPattern("#,###.##");
                    String formatted = formatter.format(Double.parseDouble(digits)/*/100*/);
                    s.replace(0, s.length(), formatted);
                    Log.w("formatted", "" + formatted);
                } catch (NumberFormatException nfe) {
                    s.clear();
                }

                mEditing = false;
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

    }


    public static String doubleToStringNoDecimal(double d) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        ;
        formatter.applyPattern("#,###.##");
        return formatter.format(d);
    }

    private void callDialog() {
        if (!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {

            // if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
            //  }

            Intent intentI = new Intent(activity, CreateWishListActivity.class);
            intentI.putExtra(Constant.FROM, CreateItemActivity.class.getName());
            /*startActivityForResult(intent, 10);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

            isFirstWishlist = !isFirstWishlist;
            binding.spinnerWishList.setSelection(0);

            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 1);
            intent.putExtra("isSkipDisplay", true);
            intent.putExtra("passIntent", intentI);
            intent.putExtra("requestCode", 10);
            //  intent.putExtra("onSkipListener", (Parcelable) this);
            startActivityForResult(intent, 10);

            /*videoViewDialog = new VideoViewDialog(1,  this, true);
            videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
        } else {
            Intent intent = new Intent(activity, CreateWishListActivity.class);
            intent.putExtra(Constant.FROM, CreateItemActivity.class.getName());
            startActivityForResult(intent, 10);

            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }


    }


    public static YouTubePlayer handleVideoDialog() {
        return videoViewDialog.hideFullScreen();
    }

    class MyTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected String doInBackground(Integer... params) {

            Log.w("urlDataIntent", "" + urlDataIntent);
            try {
                JSONObject response = getJSONObjectFromURL(urlDataIntent); // calls method to get JSON object

                Log.w("responseItemDetail", "" + response);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return "Task Completed.";
        }

        @Override
        protected void onPostExecute(String result) {
            //  progressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onPreExecute() {
            //   txt.setText("Task Starting...");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // txt.setText("Running..."+ values[0]);
            //progressBar.setProgress(values[0]);
        }
    }

    public static JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException {

        HttpURLConnection urlConnection = null;

        URL url = new URL(urlString);

        urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */);
        urlConnection.setConnectTimeout(15000 /* milliseconds */);

        urlConnection.setDoOutput(true);

        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        char[] buffer = new char[1024];

        String jsonString = new String();

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        jsonString = sb.toString();

        System.out.println("JSON: " + jsonString);
        urlConnection.disconnect();

        return new JSONObject(jsonString);
    }

    private void removeItemImgCallApi() {

        loaderDialog.show();
        Call<Object> call = application.getApis().removeItemProfile(item.getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        //binding.tvDelete.setVisibility(View.GONE);
                        binding.tvDelete.setVisibility(View.VISIBLE);
                        binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                        binding.tvDelete.setEnabled(false);
                        setTextViewDrawableColor(true);
                        //binding.ivItemImg.setImageBitmap(null);
                        Glide.with(activity).load(application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE)).into(binding.ivItemImg);
                        imgPath = null;

                        isImageRemoveOnly = true;
                        hasImage = false;
                        Intent intent = new Intent("android.addChangeInData");
                        sendBroadcast(intent);
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setValue(Item item) {

        if (item.getImage() != null && !item.getImage().equalsIgnoreCase("") &&
                !item.getImage().contains("default.png")) {
            Picasso.get().load(item.getImage()).placeholder(R.drawable.app_icon).into(binding.ivItemImg);

            if (item.getImage().equalsIgnoreCase("http://lineup.excellentwebworld.in/dashboard/assets/images/default_item.png")) {
                //binding.tvDelete.setVisibility(View.GONE);
                binding.tvDelete.setVisibility(View.VISIBLE);
                binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorBlack));
                binding.tvDelete.setEnabled(false);
                setTextViewDrawableColor(true);
            } else {
                binding.tvDelete.setVisibility(View.VISIBLE);
                binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorAccent));
                binding.tvDelete.setEnabled(true);
                setTextViewDrawableColor(false);
            }

            imgPath = item.getImage();
            hasImage = true;
            Log.w("imgPath", "here setValue => " + imgPath);
        }

        if (item.getName() != null && !item.getName().equalsIgnoreCase("")) {
            binding.etItemName.setText(item.getName());
            binding.tvTextCount.setText(item.getName().length() + "/120");
        }

        if (item.getBrand() != null && !item.getBrand().equalsIgnoreCase("")) {
            binding.etBrandName.setText(item.getBrand());
        }

        if (item.getPrice() != null && !item.getPrice().equalsIgnoreCase("")) {
            Log.e(TAG, "Price: " + item.getPrice());
            if (item.getPrice().contains(",")) {
                binding.etPrice.setText(item.getPrice());
            } else {
                binding.etPrice.setText(Utility.getFormattedAmount(Double.parseDouble(item.getPrice())));
                //binding.etPrice.setText(item.getPrice());
            }
        }

        if (item.getUrl() != null && !item.getUrl().equalsIgnoreCase("")) {
            binding.etUrl.setText(item.getUrl());
        }

        if (item.getSize() != null && !item.getSize().equalsIgnoreCase("")) {
            binding.etSize.setText(item.getSize());
        }

        if (item.getWhereToBuy() != null && !item.getWhereToBuy().equalsIgnoreCase("")) {
            binding.etWhereToBuy.setText(item.getWhereToBuy());
        }

        if (item.getWishlist() != null && !item.getWishlist().get(0).getName().equalsIgnoreCase("")) {
            for (int i = 0; i < wishlist.length; i++) {
                if (wishlist[i].equalsIgnoreCase(item.getWishlist().get(0).getName())) {
                    binding.spinnerWishList.setSelection(i);
                    break;
                }
            }
        }

        if (item.getDescription() != null && !item.getDescription().equalsIgnoreCase("")) {
            binding.etDes.setText(item.getDescription());
            binding.tvTextDesCount.setText(item.getDescription().length() + "/120");
        }
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = "SampleCropImage";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(activity);

    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        //uCrop = uCrop.withAspectRatio(1, 1);//radio_square
        return uCrop;
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(50);
        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("hereOnResult", "CreateItemActivity   1111");
        if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK) {
            binding.tvDelete.setVisibility(View.VISIBLE);
            binding.tvDelete.setTextColor(ContextCompat.getColor(CreateItemActivity.this, R.color.colorAccent));
            binding.tvDelete.setEnabled(true);
            setTextViewDrawableColor(false);
            final Uri resultUri = UCrop.getOutput(data);//retake image
            someUri = resultUri;
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                binding.ivItemImg.setImageBitmap(bitmap);
                Log.w("hereImgSelected", "" + bitmap);
                imgPath = RealPathUtil.getRealPath(activity, resultUri);
                Log.w("imgPath", "here onActivityResult => " + imgPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 10 && resultCode == RESULT_OK) {
            if (data.getStringExtra("NAME") != null) {
                Log.w("NAME", "" + data.getStringExtra("NAME"));
                loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
                int pos = -1;
                wishlist = new String[loginData.getWishlist().size() + 2];
                wishlist[0] = "Select...";
                for (int i = 0; i < loginData.getWishlist().size(); i++) {
                    wishlist[i + 1] = loginData.getWishlist().get(i).getName();
                    if (data.getStringExtra("NAME").equalsIgnoreCase(loginData.getWishlist().get(i).getName())) {
                        pos = i;
                    }
                }
                wishlist[loginData.getWishlist().size() + 1] = "Create Wishlist";

                aa2 = new ArrayAdapter(this,/* R.layout.item_spinner*/android.R.layout.simple_spinner_dropdown_item, wishlist);
                //aa2.setDropDownViewResource(R.layout.item_spinner_dropdown);
                binding.spinnerWishList.setAdapter(aa2);
                binding.spinnerWishList.setSelection(pos + 1);

                binding.spinnerWishList.setEnabled(true);
            } else {
                loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);

                int pos = -1;
                wishlist = new String[loginData.getWishlist().size() + 2];
                wishlist[0] = "Select...";
                for (int i = 0; i < loginData.getWishlist().size(); i++) {
                    wishlist[i + 1] = loginData.getWishlist().get(i).getName();
                }
                wishlist[loginData.getWishlist().size() + 1] = "Create Wishlist";

                aa2 = new ArrayAdapter(this,/* R.layout.item_spinner*/android.R.layout.simple_spinner_dropdown_item, wishlist);
                //aa2.setDropDownViewResource(R.layout.item_spinner_dropdown);
                binding.spinnerWishList.setAdapter(aa2);
                // binding.spinnerWishList.setSelection(wishlist.length-1);

                binding.spinnerWishList.setEnabled(true);
            }


        } else if (requestCode == 20 && resultCode == RESULT_OK) {
            if (!Environment.isExternalStorageManager()) {
                Intent i = new Intent();
                i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(i, 20);
            } else {
                openGalleryLocalPermission();
                return;
            }

        }/*else  if (requestCode == REQUEST_APP_SETTINGS) {
          openGalleryPermission();
        }*/
    }

    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            switch (parent.getId()) {
                case R.id.spinnerCategory:
                    if (isFirstCategory) {
                        isFirstCategory = !isFirstCategory;
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    } else {//((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));
                    }
                    break;

                case R.id.spinnerSize:
                    if (isFirstSize) {
                        isFirstSize = !isFirstSize;
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    } else {//((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));
                    }
                    break;

                case R.id.spinnerWishList:
                    Log.w("isFirstWishlist", "" + isFirstWishlist);
                    Log.w("isFirstWishlistItem", "" + (wishlist[position].equalsIgnoreCase("Create Wishlist")));
                    if (isFirstWishlist) {
                        selectedWishlistId = "";
                        wishlistName = "";
                        isFirstWishlist = !isFirstWishlist;
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    }
                    if (wishlist[position].equalsIgnoreCase("Create Wishlist")) {
                        selectedWishlistId = "";
                        wishlistName = "";

                       /* Intent intent = new Intent(activity, CreateWishListActivity.class);
                        intent.putExtra(Constant.FROM, CreateItemActivity.class.getName());
                        startActivityForResult(intent, 10);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        isFirstWishlist = !isFirstWishlist;
                        binding.spinnerWishList.setSelection(0);*/

                        callDialog();

                        /*Intent videoIntent = new Intent(activity, VideoViewDialog.class);
                        videoIntent.putExtra("videoType",1);
                        videoIntent.putExtra("intentToPass",intent);
                        videoIntent.putExtra("hasReqCode",10);
                        startActivity(videoIntent);*/


                    } else {
                        if (loginData.getWishlist() == null) return;
                        if (wishlist[position].equalsIgnoreCase("Select...")) {
                            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                        } else {
                            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));
                        }

                        for (int i = 0; i < loginData.getWishlist().size(); i++) {
                            if (wishlist[position].equalsIgnoreCase(loginData.getWishlist().get(i).getName())) {
                                selectedWishlistId = loginData.getWishlist().get(i).getId();
                                wishlistName = loginData.getWishlist().get(i).getName();
                            }
                        }

                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void addItemApiCall() {
        isShareIntent = false;

      /*  if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED*//* ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.MANAGE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED*//*) {
            String[] PERMISSIONS = {
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.MANAGE_EXTERNAL_STORAGE,
            };

            ActivityCompat.requestPermissions(this, PERMISSIONS, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return;
        }*/

        loaderDialog.show();
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), loginData.getUser().getId());
        RequestBody itemName = RequestBody.create(MediaType.parse("text/plain"), binding.etItemName.getText().toString().trim());
        RequestBody brandName = RequestBody.create(MediaType.parse("text/plain"), binding.etBrandName.getText().toString().trim());
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), binding.etPrice.getText().toString().trim());
        RequestBody url = RequestBody.create(MediaType.parse("text/plain"), binding.etUrl.getText().toString().trim());
        RequestBody size = RequestBody.create(MediaType.parse("text/plain"), binding.etSize.getText().toString().trim());
        RequestBody whereToBuy = RequestBody.create(MediaType.parse("text/plain"), binding.etWhereToBuy.getText().toString().trim());
        RequestBody des = RequestBody.create(MediaType.parse("text/plain"), binding.etDes.getText().toString().trim());
        RequestBody selectedWishlistId = RequestBody.create(MediaType.parse("text/plain"), this.selectedWishlistId);
        MultipartBody.Part imgFile;

        Log.w("imgPathFinal", "" + imgPath);
        if (imgPath != null) {
          /*  if (someUri != null) {

                File file = new File(FileUtils.getPath(activity, someUri));

                Uri uris = Uri.fromFile(file);
                Log.w("uris", "" + uris);

                Bitmap mSelectedImage;
                try {
                    //mSelectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse("file:" + uris));
                    mSelectedImage = PictureOrientation.bitmapFromUri(this, uris*//*Uri.parse("file:" + uris)*//*);
                    FileOutputStream os = new FileOutputStream(file);
                    mSelectedImage.compress(Bitmap.CompressFormat.JPEG, 100, os);
                    os.flush();
                    os.close();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                imgFile = MultipartBody.Part.createFormData("image", imgPath, RequestBody.create(MediaType.parse("image/*"), file*//* new File(imgPath)*//*));
            } else {*/
            imgFile = MultipartBody.Part.createFormData("image", imgPath, RequestBody.create(MediaType.parse("image/*"), new File(imgPath)));
            //}
        } else {
            imgFile = null;
        }


        Call<Object> call = application.getApis().addItem(id, itemName, brandName, price, url, size, selectedWishlistId, whereToBuy, des, imgFile);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Intent intent = new Intent("android.addChangeInData");
                        sendBroadcast(intent);

                        Intent intent1 = new Intent("android.addChangeInWishList");
                        sendBroadcast(intent1);

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {

                                if (jsonObject.has("item")) {
                                    try {
                                        JSONObject object = jsonObject.getJSONObject("item");
                                        if (object.has("wishlist_id")) {
                                            JSONArray jsonArray = object.getJSONArray("wishlist_id");

                                            Log.w("goowishListScreen", "" + isFromShare);
                                            Log.w("wishlistIDInDetail", "" + jsonArray.getString(0) + "  ==  Name => " + wishlistName);

                                            Intent intent = new Intent(activity, WishlistActivity.class);
                                            intent.putExtra(Constant.WISHLIST_ID, jsonArray.getString(0));
                                            intent.putExtra(Constant.WISHLIST_NAME, wishlistName);
                                            if (isFromShare) {
                                                intent.putExtra(Constant.FROM, Constant.FROM_SHARE);
                                            }
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                            finish();
                                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);


                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });

    }

    private void editItemApi() {

        loaderDialog.show();
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), item.getId());
        RequestBody itemName = RequestBody.create(MediaType.parse("text/plain"), binding.etItemName.getText().toString().trim());
        RequestBody brandName = RequestBody.create(MediaType.parse("text/plain"), binding.etBrandName.getText().toString().trim());
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), binding.etPrice.getText().toString().trim());
        RequestBody url = RequestBody.create(MediaType.parse("text/plain"), binding.etUrl.getText().toString().trim());
        RequestBody size = RequestBody.create(MediaType.parse("text/plain"), binding.etSize.getText().toString().trim());
        RequestBody whereToBuy = RequestBody.create(MediaType.parse("text/plain"), binding.etWhereToBuy.getText().toString().trim());
        RequestBody des = RequestBody.create(MediaType.parse("text/plain"), binding.etDes.getText().toString().trim());
        RequestBody selectedWishlist = RequestBody.create(MediaType.parse("text/plain"), this.selectedWishlistId);
        MultipartBody.Part imgFile;
        if (imgPath != null && !imgPath.contains("http")) {
            imgFile = MultipartBody.Part.createFormData("image", imgPath, RequestBody.create(MediaType.parse("image/*"), new File(imgPath)));
        } else {
            imgFile = null;
        }

        /*nhgv*/

        Call<Object> call = application.getApis().editItem(id, itemName, brandName, price, url, size, selectedWishlist, whereToBuy, des, imgFile);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Intent intent = new Intent("android.addChangeInData");
                        sendBroadcast(intent);

                        Intent intent1 = new Intent("android.addChangeInWishList");
                        sendBroadcast(intent1);

                        isImageRemoveOnly = false;

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                Log.w("onPositiveClick", "item     " + item + "     ==> jsonObject.has(item)     " + jsonObject.has("item"));

                                if (callbackWishList != null) {
                                    callbackWishList.createItem(selectedWishlistId);
                                }

                                if (item != null) {
                                    Intent intent1 = new Intent();
                                    intent1.putExtra(Constant.ITEM_ID, item.getId());
                                    setResult(RESULT_OK, intent1);
                                    finish();
                                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                                    return;
                                }

                                if (jsonObject.has("item")) {
                                    try {
                                        JSONObject object = jsonObject.getJSONObject("item");
                                        if (object.has("wishlist_id")) {
                                            JSONArray jsonArray = object.getJSONArray("wishlist_id");
                                            Intent intent = new Intent(activity, WishlistActivity.class);
                                            intent.putExtra(Constant.WISHLIST_ID, jsonArray.getString(0));
                                            intent.putExtra(Constant.WISHLIST_NAME, wishlistName);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                            finish();

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setTextViewDrawableColor(boolean isBlack) {

        if (isBlack)
            binding.tvDelete.setCompoundDrawableTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.colorBlack)));
        else
            binding.tvDelete.setCompoundDrawableTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));

    }

    @Override
    public void onBackPressed() {
        if (isImageRemoveOnly) {
            Intent intent1 = new Intent();
            intent1.putExtra(Constant.ITEM_ID, item.getId());
            setResult(RESULT_OK, intent1);
            finish();

            super.onBackPressed();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

        } else {
            if (videoViewDialog != null && handleVideoDialog() != null) {
                return;
            } else {
                // if(!isTaskRoot()){
                super.onBackPressed();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                /*}else{
                    Intent intent = new Intent(activity, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }*/
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPermissionAllow = true;
        Log.e("isPermissionCreateItem", ".....");
        // if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
        try {
            for (int grantResult : grantResults) {
                Log.e("isPermissionGranted", "" + (grantResult != PackageManager.PERMISSION_GRANTED));
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    isPermissionAllow = false;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // }

        if (requestCode == Constant.REQUEST_ID_MULTIPLE_PERMISSIONS) {
            /*if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED&&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.MANAGE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {*/
            Log.e("isPermissionAllowCreateItem", "" + isPermissionAllow);
            if (isPermissionAllow) {
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else if (isShareIntent) {
                    if (loginData != null)
                        onSharedIntent();
                    else {
                        Uri receiveUri = (Uri) getIntent()
                                .getParcelableExtra(Intent.EXTRA_STREAM);
                        Log.w("receiveUri", "" + receiveUri);

                        if (receiveUri != null) {

                            try {
                                InputStream inputStream = getContentResolver().openInputStream(receiveUri);

                                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                saveImageBitmap(bitmap);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    if (loginData != null)
                        addItemApiCall();
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                    isGoToSetting = true;
                    Log.e("isGoToSetting", "" + isGoToSetting);
                    Log.e("isDialogAlreadyDisplay", "" + isDialogAlreadyDisplay);
                    if (!isDialogAlreadyDisplay) {
                        isDialogAlreadyDisplay = true;
                        dialogCommon.showDialog("", "You need to allow permission to add item.", "Go to setting", getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                goToSettings();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    }

                }
            }
        }
    }

    private void goToSettings() {
       /* Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);*/
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void callMyProfileApi() {
        loaderDialog.show();
        RequestBody id = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), loginData.getUser().getId());

        Call<Object> call = application.getApis().getUserProfile(id/*Integer.parseInt(loginData.getUser().getId())*/, null, null, null, null, null, null, null);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        userProfile = gson.fromJson(x, MyProfile.class);
                        setUpBrandAutoComplete(userProfile.getTopBrands());
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    Log.e("TAG", "Error = " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void callGetAmazonDataApi(String itemUrl) {
        loaderDialog.show();

        Call<Object> call = application.getApis().getAmazonItemData(itemUrl);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        amazonItemData = gson.fromJson(x, AmazonItemData.class);

                        binding.etItemName.setText(amazonItemData.title);
                        binding.etPrice.setText(amazonItemData.price);
                        binding.etUrl.setText(amazonItemData.url);

                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    Log.e("TAG", "Error = " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }


    private void setUpBrandAutoComplete(List<String> topBrands) {
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, topBrands);
        binding.etBrandName.setAdapter(adapter);
//        binding.etBrandName.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//                if(event.getAction() == MotionEvent.ACTION_UP) {
//////                    if(event.getRawX() >=
////                            (binding.etBrandName.getRight() -
////                                    binding.etBrandName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
////                        // your action here
//
//                        Log.e("TAG","Click");
//                        if(!binding.BrandName.getText().toString().trim().equalsIgnoreCase(""))
//                        {//                        }
//                        return true;
////                    }
//                }
//                return false;
//            }
//        });
    }
}