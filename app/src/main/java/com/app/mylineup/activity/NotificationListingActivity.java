package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.NewWishListAdapter;
import com.app.mylineup.adapter.NewsAdapter;
import com.app.mylineup.adapter.RequestAdapter;
import com.app.mylineup.adapter.WhoPurchasedAdapter;
import com.app.mylineup.databinding.ActivityNotificationListingBinding;
import com.app.mylineup.interfaces.OnItemClickGiftListener;
import com.app.mylineup.interfaces.OnLoadmoreListener;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.notifications.FriendRequest;
import com.app.mylineup.pojo.notifications.Notification;
import com.app.mylineup.pojo.notifications.Notifications;
import com.app.mylineup.pojo.notifications.PurchasedList;
import com.app.mylineup.pojo.notifications.Wishlist;
import com.app.mylineup.pojo.userProfile.CheckConnected;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationListingActivity extends BaseActivity implements RequestAdapter.FriendRequstAction, OnLoadmoreListener, /*OnNewsItemClickListener,*/ OnItemClickGiftListener {

    private ActivityNotificationListingBinding binding;
    private String notificationType = "";

    private RequestAdapter requestAdapter;
    private NewWishListAdapter newWishListAdapter;
    private NewsAdapter newsAdapter;
    private WhoPurchasedAdapter whoPurchasedAdapter;

    private List<FriendRequest> friendRequestList = new ArrayList<>();
    private List<Notification> newsList = new ArrayList<>();
    private List<PurchasedList> purchasedLists = new ArrayList<>();
    private List<Wishlist> wishlistList = new ArrayList<>();

    private int page = 1;
    private String notification_type = "";
    private OnItemClickGiftListener onItemClickGiftListener;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_listing);
        init();
    }

    private void init() {

        notificationType = getIntent().getStringExtra(Constant.NOTIFICATION_TYPE);
        String title = "";

        if (notificationType.equalsIgnoreCase("1")) {
            //news
            binding.rvNotification.setLayoutManager(new LinearLayoutManager(activity));
            newsAdapter = new NewsAdapter(activity, newsList, binding.rvNotification, this, this);
            binding.rvNotification.setAdapter(newsAdapter);
            title = getString(R.string.notifications);
            notification_type = "notifications";
        } else if (notificationType.equalsIgnoreCase("2")) {
            //connection request
            binding.rvNotification.setLayoutManager(new LinearLayoutManager(activity));
            requestAdapter = new RequestAdapter(activity, friendRequestList, this, binding.rvNotification, null, this);
            binding.rvNotification.setAdapter(requestAdapter);

            title = getString(R.string.connection_request);
            notification_type = "friend_requests";
        } else if (notificationType.equalsIgnoreCase("3")) {
            //wishlist
            binding.rvNotification.setLayoutManager(new LinearLayoutManager(activity));
            newWishListAdapter = new NewWishListAdapter(activity, wishlistList, binding.rvNotification, null);
            binding.rvNotification.setAdapter(newWishListAdapter);

            title = getString(R.string.new_wishlists);
            notification_type = "wish_list";
        } else if (notificationType.equalsIgnoreCase("4")) {
            //who purchased
            binding.rvNotification.setLayoutManager(new LinearLayoutManager(activity));
            whoPurchasedAdapter = new WhoPurchasedAdapter(activity, purchasedLists, binding.rvNotification, this);
            binding.rvNotification.setAdapter(whoPurchasedAdapter);

            title = getString(R.string.lbl_who_purchased_gift);
            notification_type = "purchased_list";
        }

        setHeader(title, null, null);

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getNotifications();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

    }

    private void getNotifications() {

        loaderDialog.show();
        Call<Object> call = application.getApis().notificationListTypeWise(loginData.getUser().getId(), page + "", notification_type);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Notifications notifications = gson.fromJson(x, Notifications.class);
                        if (notifications != null) {

                            if (notification_type.equalsIgnoreCase("notifications")) {
                                if (notifications.getNotifications() != null && notifications.getNotifications().size() > 0) {
                                    newsList.addAll(notifications.getNotifications());
                                    newsAdapter.notifyDataSetChanged();
                                } else {
                                    binding.ivNoData.setImageResource(R.drawable.nt_no_notifications);
                                    binding.tvNoData.setText(getString(R.string.no_notification));
                                    binding.llNoData.setVisibility(View.VISIBLE);
                                }
                            } else if (notification_type.equalsIgnoreCase("friend_requests")) {
                                if (notifications.getFriendRequests() != null && notifications.getFriendRequests().size() > 0) {
                                    friendRequestList.addAll(notifications.getFriendRequests());
                                    requestAdapter.notifyDataSetChanged();
                                } else {
                                    binding.ivNoData.setImageResource(R.drawable.nt_connection_request);
                                    binding.tvNoData.setText(getString(R.string.no_connection_requests));
                                    binding.llNoData.setVisibility(View.VISIBLE);
                                }
                            } else if (notification_type.equalsIgnoreCase("wish_list")) {
                                if (notifications.getWishlist() != null && notifications.getWishlist().size() > 0) {
                                    wishlistList.addAll(notifications.getWishlist());
                                    newWishListAdapter.notifyDataSetChanged();
                                } else {
                                    binding.ivNoData.setImageResource(R.drawable.nt_wishlist);
                                    binding.tvNoData.setText(getString(R.string.no_wishlist_added));
                                    binding.llNoData.setVisibility(View.VISIBLE);
                                }
                            } else if (notification_type.equalsIgnoreCase("purchased_list")) {
                                if (notifications.getPurchasedList() != null && notifications.getPurchasedList().size() > 0) {
                                    purchasedLists.addAll(notifications.getPurchasedList());
                                    whoPurchasedAdapter.notifyDataSetChanged();
                                } else {
                                    binding.ivNoData.setImageResource(R.drawable.nt_who_purchased_gift);
                                    binding.tvNoData.setText(getString(R.string.who_purchased_gift));
                                    binding.llNoData.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void perFormActionOnRequest(String requestType, int pos) {
        Call<Object> call = application.getApis().friendRequest(loginData.getUser().getId(), friendRequestList.get(pos).getUserId(), requestType);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        if (requestType.equalsIgnoreCase("1")) {
                            friendRequestList.get(pos).setActionType("1");
                        } else {
                            friendRequestList.get(pos).setActionType("2");
                        }
                        requestAdapter.notifyDataSetChanged();
                        /*friendRequestList.remove(pos);
                        requestAdapter.notifyDataSetChanged();*/
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onLoadMore() {

        page = page + 1;

        if (notification_type.equalsIgnoreCase("notifications")) {
            newsList.add(null);
            newsAdapter.notifyItemInserted(newsList.size() - 1);
        } else if (notification_type.equalsIgnoreCase("friend_requests")) {
            friendRequestList.add(null);
            requestAdapter.notifyItemInserted(friendRequestList.size() - 1);
        } else if (notification_type.equalsIgnoreCase("wish_list")) {
            wishlistList.add(null);
            newWishListAdapter.notifyItemInserted(wishlistList.size() - 1);
        } else if (notification_type.equalsIgnoreCase("purchased_list")) {
            purchasedLists.add(null);
            whoPurchasedAdapter.notifyItemInserted(purchasedLists.size() - 1);
        }

        Call<Object> call = application.getApis().notificationListTypeWise(loginData.getUser().getId(), page + "", notification_type);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    if (notification_type.equalsIgnoreCase("notifications")) {
                        newsList.remove(newsList.size() - 1);
                        newsAdapter.notifyItemRemoved(newsList.size());
                    } else if (notification_type.equalsIgnoreCase("friend_requests")) {
                        friendRequestList.remove(friendRequestList.size() - 1);
                        requestAdapter.notifyItemRemoved(friendRequestList.size());
                    } else if (notification_type.equalsIgnoreCase("wish_list")) {
                        wishlistList.remove(wishlistList.size() - 1);
                        newWishListAdapter.notifyItemRemoved(wishlistList.size());
                    } else if (notification_type.equalsIgnoreCase("purchased_list")) {
                        purchasedLists.remove(purchasedLists.size() - 1);
                        whoPurchasedAdapter.notifyItemRemoved(purchasedLists.size());
                    }


                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        Notifications notifications = gson.fromJson(x, Notifications.class);
                        if (notifications != null) {

                            if (notification_type.equalsIgnoreCase("notifications")) {
                                if (notifications.getNotifications() != null && notifications.getNotifications().size() > 0) {
                                    for (int i = 0; i < notifications.getNotifications().size(); i++) {
                                        newsList.add(notifications.getNotifications().get(i));
                                        newsAdapter.notifyItemInserted(newsList.size());
                                    }
                                    newsAdapter.setLoaded(false);
                                } else {
                                    newsAdapter.setLoaded(true);
                                }
                            } else if (notification_type.equalsIgnoreCase("friend_requests")) {
                                if (notifications.getFriendRequests() != null && notifications.getFriendRequests().size() > 0) {
                                    for (int i = 0; i < notifications.getFriendRequests().size(); i++) {
                                        friendRequestList.add(notifications.getFriendRequests().get(i));
                                        requestAdapter.notifyItemInserted(friendRequestList.size());
                                    }
                                    requestAdapter.setLoaded(false);
                                } else {
                                    requestAdapter.setLoaded(true);
                                }
                            } else if (notification_type.equalsIgnoreCase("wish_list")) {
                                if (notifications.getWishlist() != null && notifications.getWishlist().size() > 0) {
                                    for (int i = 0; i < notifications.getWishlist().size(); i++) {
                                        wishlistList.add(notifications.getWishlist().get(i));
                                        newWishListAdapter.notifyItemInserted(wishlistList.size());
                                    }
                                    newWishListAdapter.setLoaded(false);
                                } else {
                                    newWishListAdapter.setLoaded(true);
                                }
                            } else if (notification_type.equalsIgnoreCase("purchased_list")) {
                                if (notifications.getPurchasedList() != null && notifications.getPurchasedList().size() > 0) {
                                    for (int i = 0; i < notifications.getPurchasedList().size(); i++) {
                                        purchasedLists.add(notifications.getPurchasedList().get(i));
                                        whoPurchasedAdapter.notifyItemInserted(purchasedLists.size());
                                    }
                                    whoPurchasedAdapter.setLoaded(false);
                                } else {
                                    whoPurchasedAdapter.setLoaded(true);
                                }
                            }
                        }

                    } else {
                        if (notification_type.equalsIgnoreCase("notifications")) {
                            newsAdapter.setLoaded(true);
                        } else if (notification_type.equalsIgnoreCase("friend_requests")) {
                            requestAdapter.setLoaded(true);
                        } else if (notification_type.equalsIgnoreCase("wish_list")) {
                            newWishListAdapter.setLoaded(true);
                        } else if (notification_type.equalsIgnoreCase("purchased_list")) {
                            whoPurchasedAdapter.setLoaded(true);
                        }
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });

    }

    public void getCheckConnectedApi(String userId) {

        loaderDialog.show();

        Call<Object> call = application.getApis().checkConnected(userId, loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                loaderDialog.dismiss();
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                try {
                    if (x != null) {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            CheckConnected checkConnected = gson.fromJson(x, CheckConnected.class);
                            if (checkConnected.getStatus()) {

                                Intent intent = new Intent(activity, UserProfileActivity.class);
                                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                                intent.putExtra(Constant.USER_ID, checkConnected.getProfile_user_id());
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        }
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(String profileId) {
        getCheckConnectedApi(profileId);
    }
}
