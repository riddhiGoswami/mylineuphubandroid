package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.mylineup.R;
import com.app.mylineup.adapter.IntroSliderAdapter;
import com.app.mylineup.databinding.ActivityIntroSliderBinding;
import com.app.mylineup.pojo.IntroBeen;

import java.util.ArrayList;
import java.util.List;

public class IntroSliderActivity extends BaseActivity
{
    public static IntroSliderActivity activity;
    private ActivityIntroSliderBinding binding;
    private List<IntroBeen> list = new ArrayList<>();
    private IntroSliderAdapter adapter;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_intro_slider);
        activity = IntroSliderActivity.this;
        init();
    }

    private void init()
    {
        list.add(new IntroBeen(R.drawable.intro1,"Start Your Wishlist","Birthday? Holiday? Or want to keep track of cool things you want? Name your list, add a photo and link of your item and quickly start building your shareable wishlists."));
        list.add(new IntroBeen(R.drawable.intro2,"Set Up Your Profile","Add your favorite stores, brands, and sizes. Have children, pets or someone close who's not very tech-savvy? Set up a \"little ones\" profile and manage and share their wishlists."));
        list.add(new IntroBeen(R.drawable.intro3,"Invite and Connect","Let your close friends and family know what you really want, and encourage them to start their own so you can give the perfect gift."));

        adapter = new IntroSliderAdapter(activity, list, new IntroSliderAdapter.ViewPagerListener() {
            @Override
            public void onNext(int pos) {
                if(pos==(list.size()-1)) {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                    finish();
                }
                else {
                    binding.viewPagerIntro.setCurrentItem(pos+1);
                }
            }
        });
        binding.viewPagerIntro.setAdapter(adapter);
    }
}
