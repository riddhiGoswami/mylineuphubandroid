package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.mylineup.R;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;

public class FaceBookLogin extends BaseActivity {

    private CallbackManager mCallbackManager;
    private static final String EMAIL = "email";
    private LoginButton mLoginButton;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fb);
        init();
    }

    private void init(){

        mCallbackManager = CallbackManager.Factory.create();

        mLoginButton = findViewById(R.id.login_button);
        mLoginButton.setPermissions(Arrays.asList(EMAIL));

        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserDetail(loginResult);
            }

            @Override
            public void onCancel() {
                Log.e("TAG","FB onCancel");
                Intent intent = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            }

            @Override
            public void onError(FacebookException e) {
                // Handle exception
                Log.e("TAG","FB onError = "+e.getLocalizedMessage());
                dialogCommon.showDialog(e.getLocalizedMessage(), new AlertDialogCommon.CallBackClickListener() {
                    @Override
                    public void OnDialogPositiveBtn() {
                        Intent intent = new Intent();
                        setResult(RESULT_CANCELED);
                        finish();
                    }

                    @Override
                    public void OnDialogNegativeBtn() {

                    }
                });
            }
        });

        //mLoginButton.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        LoginManager.getInstance().logOut();
        mLoginButton.performClick();
    }

    private void getUserDetail(LoginResult loginResult) {

        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject json_object, GraphResponse response)
                    {
                        JSONObject profile_pic_url,profile_pic_data;
                        try
                        {
                            JSONObject object = new JSONObject();
                            Log.e("name ","name = "+json_object.get("name").toString());
                            Log.e("name ","result = "+json_object.toString());
                            Log.e("email ","email = "+json_object.get("email").toString());
                            Log.e("picture ","picture = "+json_object.get("picture").toString());
                            Log.e("id ","id = "+json_object.get("id").toString());
                            profile_pic_data = new JSONObject(json_object.get("picture").toString());
                            profile_pic_url = new JSONObject(profile_pic_data.getString("data"));
                            Log.e("data ","data = "+profile_pic_data.get("data").toString());
                            Log.e("data ","data = "+profile_pic_url.getString("url"));
                            String profile_url = profile_pic_url.getString("url");


                            object.put("name",json_object.get("name").toString());
                            object.put("email",json_object.get("email").toString());
                            object.put("id",json_object.get("id").toString());
                            object.put("socialType","1");
                            Log.e("TAG","object = "+object.toString());

                            Intent intent = new Intent();
                            intent.putExtra(Constant.SOCIAL_LOGIN_DATA,object.toString());
                            setResult(RESULT_OK,intent);
                            finish();

                            Log.e("url ","url = "+profile_url);
                        }
                        catch (Exception e)
                        {
                            Log.e("TAG","Fb login error = "+e.getMessage());
                            dialogCommon.showDialog(e.getLocalizedMessage(), new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    Intent intent = new Intent();
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });
                        }
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
