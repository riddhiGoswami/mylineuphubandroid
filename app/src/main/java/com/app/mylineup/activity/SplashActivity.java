package com.app.mylineup.activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.databinding.DataBindingUtil;

import com.app.mylineup.BuildConfig;
import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivitySplashBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    private static final String TAG = "SplashActivity";
    private ActivitySplashBinding binding;
    private Animation animation;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        init();
    }

    private void init() {
        if (application != null) {
            application.getSharedPref().saveSession("FROM", "");

            if (!application.getSharedPref().isContainsVideo(Constant.WISHLIST_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, false);
            }
            if (!application.getSharedPref().isContainsVideo(Constant.CONNECTION_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.CONNECTION_SEEN, false);
            }
            if (!application.getSharedPref().isContainsVideo(Constant.LITTLEONE_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.LITTLEONE_SEEN, false);
            }
            if (!application.getSharedPref().isContainsVideo(Constant.PROFILE_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.PROFILE_SEEN, false);
            }
            if (!application.getSharedPref().isContainsVideo(Constant.REVIEW_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.REVIEW_SEEN, false);
            }
        }

        animation = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        binding.ivSplash.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet")) {
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        } else {
            callInitApi();
        }
    }

    private void callInitApi() {
        String versionName = BuildConfig.VERSION_NAME;

        String userId = "";
        if (loginData != null && loginData.getUser() != null && loginData.getUser().getId() != null) {
            userId = loginData.getUser().getId();
        } else {
            userId = "";
        }

        Call<Object> call = application.getApis().init(versionName, userId);
        Log.e("TAG", "url = " + call.request().url());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                Log.e("TAG", "code = " + response.code());
                Log.e("TAG", "msg = " + response.message());
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    //save default image for each item
                    if (jsonObject.has("default_image")) {
                        try {
                            application.getSharedPref().saveImageSession(Constant.DEFAULT_ITEM_IMAGE, jsonObject.getString("default_image"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (jsonObject.has("update") && jsonObject.getBoolean("update") && jsonObject.has("message") &&
                            !jsonObject.getString("message").equalsIgnoreCase("")) {
                        ///compulsory update
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                openPlayStore();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    }else if (jsonObject.has("blocked") && jsonObject.getBoolean("blocked") &&
                            jsonObject.has("message") && !jsonObject.getString("message").equalsIgnoreCase("")) {
                        //optional update
                        dialogCommon.showDialog(getString(R.string.app_name), jsonObject.getString("message"), getString(R.string.ok), getString(R.string.label_cancel), true, false, new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                application.getSharedPref().saveSession(Constant.IS_LOGIN, "");
                                application.getSharedPref().clearAll();
                             //   goInside();

                                Intent intent = new Intent(activity, LoginActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                                finish();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else if (jsonObject.has("status") && jsonObject.getBoolean("status") && jsonObject.has("update") && !jsonObject.getBoolean("update") &&
                            jsonObject.has("message") && !jsonObject.getString("message").equalsIgnoreCase("")) {
                        //optional update
                        dialogCommon.showDialog(getString(R.string.app_name), jsonObject.getString("message"), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                openPlayStore();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {
                                goInside();
                            }
                        });
                    }  else if (jsonObject.has("maintenance") && jsonObject.getBoolean("maintenance")) {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    } else if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        goInside();
                    }
                } catch (Exception e) {
                }
//{"status":true,"update":false,"maintenance":false,"message":""}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }

    private void goInside() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (application.getSharedPref().getSession(Constant.IS_LOGIN).equalsIgnoreCase("1")) {
                    Log.e(TAG, "x-api-key: " + application.getSharedPref().getSession(Constant.HEADER_VALUE));
                    Intent intent = new Intent(activity, HomeActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(activity, IntroSliderActivity.class);
                    //Intent intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                finish();
            }
        }, 200);
    }

    private void openPlayStore() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
