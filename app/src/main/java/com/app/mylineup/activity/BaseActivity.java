package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.app.mylineup.R;
import com.app.mylineup.application.Application;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.LoaderDialog;
import com.app.mylineup.interfaces.CallbackImage;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.loginData.LoginData;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.singleton.WishlistCallbackSingleton;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity
{
    public static BaseActivity activity;
    protected Gson gson;

    private ImageView ivBack;
    protected MyTextView tvHeader;
    private ImageView ivRight2,ivRight;
    private MaterialRippleLayout materialRight2,materialRight;
    private onHeaderAction onHeaderAction;

    protected CallbackImage callbackImage;
    protected AlertDialogCommon dialogCommon;
    protected AlertListDialog listDialog;

    protected LoaderDialog loaderDialog;
    protected Application application;
    protected LoginData loginData;

    public static boolean isActivityVisible;

    abstract protected String getCallbackTag();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        gson = new Gson();
        isActivityVisible = true;
        dialogCommon = new AlertDialogCommon(activity);
        loaderDialog = new LoaderDialog(activity);
        listDialog = new AlertListDialog(activity);

//        try {
//            callbackImage= (CallbackImage) activity;
//        }
//        catch (ClassCastException e){
//            e.printStackTrace();
//        }

        if(activity instanceof ImageActivity){
            Log.e("TAG1","ImageActivity instance");
            try {
                callbackImage= (CallbackImage) activity;
            }
            catch (ClassCastException e){
                e.printStackTrace();
            }
        }

        application = Application.getInstance();

        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
        init();
    }

    private void init()
    {

    }

    public LoginData getLoginData(){

        return application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
    }

    public void setHeader(String title,Object share,Object edit)
    {
        tvHeader = findViewById(R.id.tvHeader);
        ivBack = findViewById(R.id.ivBack);
        ivRight2 = findViewById(R.id.ivRight2);
        ivRight = findViewById(R.id.ivRight);

        materialRight2 = findViewById(R.id.materialRight2);
        materialRight = findViewById(R.id.materialRight);

        if(share != null)
        {
            ivRight2.setImageResource((int)share);
            ivRight2.setVisibility(View.VISIBLE);
        }
        else {materialRight2.setVisibility(View.INVISIBLE);}

        if(edit != null)
        {
            ivRight.setImageResource((int)edit);
            ivRight.setVisibility(View.VISIBLE);
        }else {materialRight.setVisibility(View.INVISIBLE);}

        ivRight.setOnClickListener(v -> {
            if(onHeaderAction == null)return;
            onHeaderAction.onClick(R.id.ivRight);
        });
        ivRight2.setOnClickListener(v -> {
            if(onHeaderAction == null)return;
            onHeaderAction.onClick(R.id.ivRight2);
        });

        ivBack.setOnClickListener(v -> {
            onBackPressed();
        });

        tvHeader.setText(title);
    }


    public void setInterFace(onHeaderAction onHeaderAction)
    {
        this.onHeaderAction = onHeaderAction;
    }

    public interface onHeaderAction
    {
        void onClick(int id);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void hideKeyboardFrom() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LittleOnesGlobalCallbackSingleton.removeListener(getCallbackTag());
        WishlistCallbackSingleton.removeListener(getCallbackTag());
        isActivityVisible = false;
    }

    public static String unescapeJava(String escaped) {

        if (escaped.indexOf("\\u") == -1)
            return escaped;

        String processed = "";

        int position = escaped.indexOf("\\u");
        while (position != -1) {
            if (position != 0)
                processed += escaped.substring(0, position);
            String token = escaped.substring(position + 2, position + 6);
            escaped = escaped.substring(position + 6);
            processed += (char) Integer.parseInt(token, 16);
            position = escaped.indexOf("\\u");
        }
        processed += escaped;

        return processed;
    }

    public void logoutUser(String title){
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                // Toast.makeText(getApplicationContext(), title, Toast.LENGTH_LONG).show();
           /*     application.getSharedPref().saveSession(Constant.IS_LOGIN, "");
                application.getSharedPref().clearAll();
                AlertDialogCommon dialogCommonT = new AlertDialogCommon(getApplicationContext());

                dialogCommonT.showDialogWithContext(getApplicationContext(), title, new AlertDialogCommon.CallBackClickListener() {
                    @Override
                    public void OnDialogPositiveBtn() {*/


                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);


                    /*    new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                NotificationManager nMgr = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                nMgr.cancelAll();
                            }
                        }, 5000);*/

                    }

                    /*@Override
                    public void OnDialogNegativeBtn() {

                    }
                });


            }*/
        });
    }
}
