package com.app.mylineup.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.app.mylineup.R;
import com.app.mylineup.adapter.SpinnerItemAdapter;
import com.app.mylineup.custom_view.MyEditText;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.databinding.ActivityCreateLittleOneBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.fragment.WishlistItemsFragment;
import com.app.mylineup.fragment.WishlistListFragment;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.myProfile.MyProfile;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import fisk.chipcloud.Chip;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import fisk.chipcloud.ChipListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.mylineup.activity.LittleOneProfileActivity.isDeleted;

public class CreateLittleOneActivity extends ImageActivity implements BaseActivity.onHeaderAction {
    private ActivityCreateLittleOneBinding binding;

    private ArrayAdapter<String> adapter;
    private ChipCloudConfig drawableWithCloseConfig;

    private MyProfile userProfile;

    private List<List<String>> clothing_footwear_list, like_interest_list;
    private ChipCloud storeChip, brandChip;

    private String userBirthDate = null;
    private String littleOneId = "";//if edit little one then assign id

    private CallbackLittleOne callbackLittleOne;
    //    protected Uri imageUri;
    private String imageFilePth = null;
    private boolean isSaveClicked = false;
    private String TAG = CreateLittleOneActivity.class.getSimpleName();
    private int previousHeight = 0;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_create_little_one);
        isSaveClicked = false;
        init();
    }

    private void init() {
        if (getIntent().getStringExtra(Constant.FROM) == null) {
            setHeader(getString(R.string.lbl_create_little_one), null, null);
        } else {
            setHeader(getString(R.string.lbl_edit_little_one), null, R.drawable.ic_dots);
            setInterFace(this::onClick);
            littleOneId = getIntent().getStringExtra(Constant.LITTLE_ONE_ID);
        }

        callbackLittleOne = LittleOneSingleton.getCallBack();

        binding.tvDate.setOnClickListener(v ->
        {
            String str = binding.tvDate.getText().toString().trim();
            Global.openDatePicker(activity, str, false, false, false, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    userBirthDate = sdf.format(myCalendar.getTime());

                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    //int position = p.indexOf(',');
                    //String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    binding.tvDate.setText(p);
                }
            });
        });

        binding.tvDone.setOnClickListener(view ->
        {

            if (binding.tvDone.isEnabled() && binding.tvDone.isClickable()) {

                isSaveClicked = true;
            /*isSaveClicked = true;
            binding.tvDone.setClickable(false);*/
                Log.w("isClickableEnable", "isClickable => " + binding.tvDone.isClickable() + " isEnable => " + binding.tvDone.isEnabled());
                binding.tvDone.setEnabled(false);
                binding.tvDone.setClickable(false);
                validateData();
            }
        });

        binding.tvEditPhoto.setOnClickListener(v ->
        {
            Log.w("profilePicUrl", "" + userProfile.getUser().getProfilePicture() + " ==> " + (userProfile.getUser().getProfilePicture().contains("default_user")));
            boolean isRemoveOptionDisplay = true;
            if (userProfile.getUser().getProfilePicture().contains("default_user")) {
                isRemoveOptionDisplay = false;
            } else {

                isRemoveOptionDisplay = true;
            }
            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) ->
            {
                if (imageFilePath != null) {
                    this.imageUri = imageUri;
                    binding.ivProfile.setImageURI(imageUri);
                    this.imageFilePth = imageFilePath;
                    userProfile.getUser().setProfilePicture(imageFilePath);
                } else {
                    this.imageUri = null;
                    //binding.ivProfile.setImageResource(R.drawable.app_icon);
                    Picasso.get().load("https://www.mylineuphub.com/dashboard/assets/images/default_user.png").into(binding.ivProfile);

                    this.imageFilePth = null;
                    if (!userProfile.getUser().getProfilePicture().contains("default_user.png")) {
                        if (!userProfile.getUser().getProfilePicture().contains("storage")) {
                            String status = NetworkUtil.getConnectivityStatusString(activity);
                            if (status.equalsIgnoreCase("Not connected to Internet"))
                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                            else {

                                callRemoveProfilePicApi();
                            }
                        }
                    }
                }
            }, false, false, true, isRemoveOptionDisplay, CreateLittleOneActivity.this);
        });

        binding.tvAddBrand.setOnClickListener(v -> {

            boolean isSameFound = false;
            String str = binding.autoBrandName.getText().toString().trim();
            if (!str.equalsIgnoreCase("")) {
                for (int i = 0; i < brandChip.getAllChips().size(); i++) {
                    Log.w("brandChip", "" + brandChip.getAllChips().get(i).getLabel());
                    if (brandChip.getAllChips().get(i).getLabel().trim().toLowerCase().equalsIgnoreCase(str.toLowerCase())) {
                        isSameFound = true;
                        break;
                    }
                }

                if (!isSameFound) {
                    brandChip.addChip(str, null);
                }
                binding.autoBrandName.setText("");

            }

         /*   for(int i=0;i<brandChip.getAllChips().size();i++){
                Log.w("brandChip",""+brandChip.getAllChips().get(i).getLabel());
            }
            if (!binding.autoBrandName.getText().toString().trim().equalsIgnoreCase("")) {
                brandChip.addChip(binding.autoBrandName.getText().toString(), null);
                binding.autoBrandName.setText("");
            }*/
        });

        binding.tvAddStore.setOnClickListener(v -> {
            boolean isSameFound = false;
            String str = binding.autoStoreName.getText().toString().trim();
            if (!str.equalsIgnoreCase("")) {

                for (int i = 0; i < storeChip.getAllChips().size(); i++) {
                    Log.w("brandChip", "" + storeChip.getAllChips().get(i).getLabel());
                    if (storeChip.getAllChips().get(i).getLabel().trim().toLowerCase().equalsIgnoreCase(str.toLowerCase())) {
                        isSameFound = true;
                        break;
                    }
                }

                if (!isSameFound) {
                    storeChip.addChip(str, null);
                    binding.autoStoreName.setText("");
                }
                binding.autoStoreName.setText("");
            }

          /*  if (!binding.autoStoreName.getText().toString().trim().equalsIgnoreCase("")) {
                storeChip.addChip(binding.autoStoreName.getText().toString(), null);
                binding.autoStoreName.setText("");
            }*/
        });


        binding.flexBoxBrand.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    int[] location = new int[2];
                    binding.flexBoxBrand.getLocationOnScreen(location);
                    int y = location[1];
                    Log.e(TAG, "onItemClick: flexBoxBrand y : " + y);
                    binding.nsvContainer.scrollTo(0, y);
                }
            }
        });

        binding.flexBoxStore.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    int[] location = new int[2];
                    binding.flexBoxStore.getLocationOnScreen(location);
                    int y = location[1];
                    Log.e(TAG, "onItemClick: flexBoxStore y : " + y);
                    binding.nsvContainer.scrollTo(0, y);
                }
            }
        });

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getLittleOneField();
    }

    private void validateData() {
        Log.w("binding.tvDone", "" + binding.tvDone.isEnabled());
        if (!Global.isValidString(binding.etFirstName.getText().toString())) {
            binding.tvDone.setEnabled(true);
            binding.tvDone.setClickable(true);
            isSaveClicked = false;
            dialogCommon.showDialog(getString(R.string.please_enter_first_name));
        } else if (!Global.isValidString(binding.etLastName.getText().toString())) {
            isSaveClicked = false;
            binding.tvDone.setEnabled(true);
            binding.tvDone.setClickable(true);
            dialogCommon.showDialog(getString(R.string.please_enter_last_name));
        } else if (!Global.isValidString(binding.tvDate.getText().toString())) {
            binding.tvDone.setEnabled(true);
            binding.tvDone.setClickable(true);
            isSaveClicked = false;
            dialogCommon.showDialog(getString(R.string.please_select_date));
        } else {
            if (storeChip == null || brandChip == null) {
                binding.tvDone.setEnabled(true);
                binding.tvDone.setClickable(true);
                isSaveClicked = false;
                dialogCommon.showDialog(getString(R.string.please_try_after_time));
            }

            String topBrand = "", favouriteStore = "";
            for (int i = 0; i < storeChip.getAllChips().size(); i++) {
                if (i == (storeChip.getAllChips().size() - 1)) {
                    favouriteStore = favouriteStore + "" + storeChip.getAllChips().get(i).getLabel();
                } else {
                    favouriteStore = favouriteStore + "" + storeChip.getAllChips().get(i).getLabel() + ",";
                }
            }

            for (int i = 0; i < brandChip.getAllChips().size(); i++) {
                if (i == (brandChip.getAllChips().size() - 1)) {
                    topBrand = topBrand + "" + brandChip.getAllChips().get(i).getLabel();
                } else {
                    topBrand = topBrand + "" + brandChip.getAllChips().get(i).getLabel() + ",";
                }
            }

            String firstName = binding.etFirstName.getText().toString().trim();
            String lastName = binding.etLastName.getText().toString().trim();

            try {
                JSONObject mainObject = new JSONObject();

                //add clothing and footwear
                JSONArray clothingFootwearArray = new JSONArray();
                for (int i = 0; i < binding.llClothingFootwear.getChildCount(); i++) {
                    JSONObject object = new JSONObject();
                    String data = "";
                    if (binding.llClothingFootwear.getChildAt(i) instanceof LinearLayout) {
                        LinearLayout linearLayout = (LinearLayout) binding.llClothingFootwear.getChildAt(i);
                        if (linearLayout.getChildAt(1) instanceof Spinner) {
                            Spinner spinner = (Spinner) linearLayout.getChildAt(1);
                            data = spinner.getSelectedItemPosition() > 0 ? userProfile.getClothingFootware().get(i).getData().get(spinner.getSelectedItemPosition() - 1) : "";
                        } else if (linearLayout.getChildAt(1) instanceof EditText) {
                            EditText editText = (MyEditText) linearLayout.getChildAt(1);
                            data = editText.getText().toString().trim();
                        }
                    }
                    object.put("field_id", userProfile.getClothingFootware().get(i).getFieldId());
                    object.put("data", data);

                    clothingFootwearArray.put(object);
                }

                /*-----------------------------*/
                List<String> cloathingList = new ArrayList<>();
                for (int i = 0; i < clothingFootwearArray.length(); i++) {
                    JSONObject object = clothingFootwearArray.getJSONObject(i);
                    cloathingList.add(object.optString("data"));
                }
                /*-----------------------------*/

                //mainObject.put("clothing_footware",clothingFootwearArray);

                //add like and interest array
                JSONArray likeInterestArray = new JSONArray();
                for (int i = 0; i < binding.llLikeInterest.getChildCount(); i++) {
                    JSONObject object = new JSONObject();
                    String data = "";
                    if (binding.llLikeInterest.getChildAt(i) instanceof LinearLayout) {
                        LinearLayout linearLayout = (LinearLayout) binding.llLikeInterest.getChildAt(i);
                        if (linearLayout.getChildAt(1) instanceof Spinner) {
                            Spinner spinner = (Spinner) linearLayout.getChildAt(1);
                            data = spinner.getSelectedItemPosition() > 0 ? userProfile.getLikesInterest().get(i).getData().get(spinner.getSelectedItemPosition() - 1) : "";
                        } else if (linearLayout.getChildAt(1) instanceof EditText) {
                            EditText editText = (MyEditText) linearLayout.getChildAt(1);
                            data = editText.getText().toString().trim();
                        }
                    }
                    object.put("field_id", userProfile.getLikesInterest().get(i).getFieldId());
                    object.put("data", data);

                    likeInterestArray.put(object);
                }

                mainObject.put("likes_interest", likeInterestArray);

                //add top brand anf favourite store
                mainObject.put("favourite_store", favouriteStore);
                mainObject.put("top_brands", topBrand);

                Log.e("TAG", "object = " + mainObject.toString());

                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet")) {
                    //  binding.tvDone.setEnabled(true);
                    isSaveClicked = false;
                    binding.tvDone.setClickable(true);
                    binding.tvDone.setEnabled(true);
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                } else {
                    Log.w("isClickableEnable", "API Call isClickable => " + binding.tvDone.isClickable() + " isEnable => " + binding.tvDone.isEnabled());
                    //   if(!binding.tvDone.isEnabled()) {
                    callAddEditLittleOneAPi(firstName, lastName, userBirthDate, mainObject.toString(), cloathingList);
                    //  }
                }
            } catch (Exception e) {
                Log.e("TAG", "Error = " + e.getLocalizedMessage());
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

    }

    private void callAddEditLittleOneAPi(String firstName, String lastName, String userBirthDate,
                                         String user_info, List<String> cloathingList) {
        loaderDialog.show();
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), loginData.getUser().getId());
        RequestBody fName = RequestBody.create(MediaType.parse("text/plain"), firstName);
        RequestBody lName = RequestBody.create(MediaType.parse("text/plain"), lastName);
        RequestBody uBirthDate = RequestBody.create(MediaType.parse("text/plain"), userBirthDate);
        RequestBody lOneId = littleOneId.equalsIgnoreCase("") ? null :
                RequestBody.create(MediaType.parse("text/plain"), littleOneId);
        RequestBody top = RequestBody.create(MediaType.parse("text/plain"), cloathingList.get(0));
        RequestBody bottom = RequestBody.create(MediaType.parse("text/plain"), cloathingList.get(1));
        RequestBody shoes = RequestBody.create(MediaType.parse("text/plain"), cloathingList.get(2));
        RequestBody uInfo = RequestBody.create(MediaType.parse("text/plain"), user_info);
        MultipartBody.Part imgFile;
        if (imageFilePth != null) {
            imgFile = MultipartBody.Part.createFormData("profile_picture", imageFilePth, RequestBody.create(MediaType.parse("image/*"), new File(imageFilePth)));
        } else {
            imgFile = null;
        }

        Call<Object> call = application.getApis().addLittleOne(
                id, fName, lName, uBirthDate, lOneId, top, bottom, shoes, imgFile, uInfo);

//        Call<Object> call = application.getApis().addLittleOne(
//                loginData.getUser().getId(), firstName, lastName, userBirthDate,
//                littleOneId.equalsIgnoreCase("") ? null : littleOneId,
//                cloathingList.get(0), cloathingList.get(1), cloathingList.get(2), user_info);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                binding.tvDone.setEnabled(true);
                isSaveClicked = false;
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                Log.e(TAG, "OnDialogPositiveBtn: ");
                                if (AccountSettingActivity.activity != null) {
                                    AccountSettingActivity.isChange = true;
                                }

                                if (!littleOneId.equalsIgnoreCase("")) {
                                    if (callbackLittleOne != null) {
                                        callbackLittleOne.editLittleOne(firstName, lastName);
                                    }


                                    /*Intent intent = new Intent("LITTLE_ONE_CHANGE");
                                    intent.putExtra("edit", true);
                                    intent.putExtra("littleOneId", littleOneId);
                                    intent.putExtra("firstName", firstName);
                                    intent.putExtra("lastName", lastName);
                                    LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);*/
                                    Intent intent = new Intent();
                                    setResult(RESULT_OK, intent);
                                    onBackPressed();
                                    //activity.finish();
                                } else {
                                    try {
                                        if (jsonObject.has("little_one")) {
                                            JSONObject object = jsonObject.getJSONObject("little_one");
                                            LittleOne littleOne = new LittleOne();
                                            littleOne.setId(object.getInt("id") + "");
                                            littleOne.setFirstName(object.getString("first_name"));
                                            littleOne.setLastName(object.getString("last_name"));
                                            littleOne.setBirthdayDate(object.getString("birthday_date"));
                                            if (callbackLittleOne != null) {
                                                callbackLittleOne.addLittleOne(littleOne);
                                            }

                                            /*Intent intent = new Intent("LITTLE_ONE_CHANGE");
                                            intent.putExtra("edit", true);
                                            intent.putExtra("data", littleOne);
                                            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);*/
                                            //onBackPressed();
                                            activity.finish();
                                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                                        }
                                    } catch (Exception e) {
                                        Log.e("TAG", "Error = " + e.getLocalizedMessage());
                                    }
                                }
                            }

                            @Override
                            public void OnDialogNegativeBtn() {
                                Log.e(TAG, "OnDialogNegativeBtn: ");
                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void getLittleOneField() {
        loaderDialog.show();
        Call<Object> call = application.getApis().getLittleOneData(littleOneId);
        Log.e("TAG", "url = " + call.request().url());
        //Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        userProfile = gson.fromJson(x, MyProfile.class);
                        Log.w("userProfile", "" + userProfile.getUser().getProfilePicture());
                        setValue();
                        addClothingFootwearDynamically();
                        addLikeInterestDynamically();
                        setTopBrandFavouriteStore();
                    }
                } catch (Exception e) {
                    Log.e("TAG", "Error = " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setValue() {
        if (Global.isValidString(userProfile.getUser().getFirstName())) {
            binding.etFirstName.setText(userProfile.getUser().getFirstName());
        }
        if (Global.isValidString(userProfile.getUser().getLastName())) {
            binding.etLastName.setText(userProfile.getUser().getLastName());
        }
        if (Global.isValidString(userProfile.getUser().getBirthdayDate())) {
            userBirthDate = userProfile.getUser().getBirthdayDate();
            binding.tvDate.setText(Utility.getDateInFormat(userProfile.getUser().getBirthdayDate()));
        }

        if (Global.isValidString(userProfile.getUser().getProfilePicture())) {
            Picasso.get().load(userProfile.getUser().getProfilePicture()).into(binding.ivProfile);
        }
    }

    private void setTopBrandFavouriteStore() {
        setUpBrandAutoComplete(userProfile.getTopBrands(), userProfile.getUser().getTopBrands());
        setUpStoreAutoComplete(userProfile.getFavouriteStore(), userProfile.getUser().getFavouriteStore());
    }

    //create like interest field dynamically
    private void addLikeInterestDynamically() {
        like_interest_list = new ArrayList<>();

        List<String> stringList;
        for (int i = 0; i < userProfile.getLikesInterest().size(); i++) {
            stringList = new ArrayList<>();
            if (userProfile.getLikesInterest().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER)) {
                stringList.add("Select...");
                for (int j = 0; j < userProfile.getLikesInterest().get(i).getData().size(); j++) {
                    stringList.add(userProfile.getLikesInterest().get(i).getData().get(j));
                }
            }
            like_interest_list.add(stringList);
        }

        Spinner spinner;
        MyTextView textView;

        LinearLayout linearLayout;
        Typeface face = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_medium));
        Typeface typeface = Typeface.createFromAsset(getAssets(),
                "font/Ubuntu-Regular.ttf");

        for (int i = 0; i < userProfile.getLikesInterest().size(); i++) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen._15sdp));


            linearLayout = new LinearLayout(activity);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(params);

            textView = new MyTextView(activity);
            textView.setText(userProfile.getLikesInterest().get(i).getFieldName());
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_hint));
            textView.setMediumTextView(textView);
            textView.setAllCaps(true);
            textView.setTextColor(getResources().getColor(R.color.textColor));
            linearLayout.addView(textView);

            if (userProfile.getLikesInterest().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER)) {
                spinner = new Spinner(activity);
                spinner.setId(i);
                spinner.setTag(i);
                spinner.setOnItemSelectedListener(listener3);
                spinner.setBackground(getResources().getDrawable(R.drawable.ic_spinner));
                linearLayout.addView(spinner);

                /*ArrayAdapter<String>*/
                SpinnerItemAdapter aa4 = new /*ArrayAdapter*/SpinnerItemAdapter(activity, android.R.layout.simple_spinner_dropdown_item/* R.layout.item_spinner*/, clothing_footwear_list.get(i));
                //aa4.setDropDownViewResource(R.layout.item_spinner_dropdown);
                spinner.setAdapter(aa4);

                for (int y = 0; y < userProfile.getLikesInterest().get(i).getData().size(); y++) {
                    if (userProfile.getLikesInterest().get(i).getData().get(y).equalsIgnoreCase(userProfile.getLikesInterest().get(i).getUser_value())) {
                        spinner.setSelection(y + 1);
                        break;
                    }
                }
            } else if (userProfile.getLikesInterest().get(i).getFieldType().equalsIgnoreCase(Constant.TEXTVIEW)) {
                MyEditText myEditText;
                myEditText = new MyEditText(activity);
                myEditText.setBackground(getResources().getDrawable(R.drawable.edt_bg_selector));
                myEditText.setHint(userProfile.getLikesInterest().get(i).getFieldName());
                myEditText.setMaxLines(5);
                myEditText.setPadding(0, (int) getResources().getDimension(R.dimen._5sdp), 0, (int) getResources().getDimension(R.dimen._8sdp));
                //myEditText.setSingleLine();
                myEditText.setTextColor(getResources().getColor(R.color.colorBlack));
                myEditText.setHintTextColor(getResources().getColor(R.color.textColorHint));
                //myEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._13sdp));
                myEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._12sdp));
                myEditText.setText(userProfile.getLikesInterest().get(i).getUser_value());
                myEditText.setTypeface(typeface);

                linearLayout.addView(myEditText);

                myEditText.setOnTouchListener(new View.OnTouchListener() {

                    public boolean onTouch(View v, MotionEvent event) {
                        if (myEditText.hasFocus()) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                case MotionEvent.ACTION_SCROLL:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    return true;
                            }
                        }
                        return false;
                    }
                });
            }

            binding.llLikeInterest.addView(linearLayout);
        }
    }

    //create clothing footwear field dynamically
    private void addClothingFootwearDynamically() {
        clothing_footwear_list = new ArrayList<>();

        List<String> stringList;
        for (int i = 0; i < userProfile.getClothingFootware().size(); i++) {
            stringList = new ArrayList<>();
            if (userProfile.getClothingFootware().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER)) {
                stringList.add("Select...");
                for (int j = 0; j < userProfile.getClothingFootware().get(i).getData().size(); j++) {
                    stringList.add(userProfile.getClothingFootware().get(i).getData().get(j));
                }
            }
            clothing_footwear_list.add(stringList);
        }

        Spinner spinner;
        MyTextView textView;
        MyEditText myEditText;
        LinearLayout linearLayout;

        for (int i = 0; i < userProfile.getClothingFootware().size(); i++) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen._15sdp));

            linearLayout = new LinearLayout(activity);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(params);


            textView = new MyTextView(activity);
            textView.setText(userProfile.getClothingFootware().get(i).getFieldName());
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_hint));
            textView.setMediumTextView(textView);
            textView.setAllCaps(true);
            textView.setTextColor(getResources().getColor(R.color.textColor));
            linearLayout.addView(textView);

            if (userProfile.getClothingFootware().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER)) {
                spinner = new Spinner(activity);
                spinner.setId(i);
                spinner.setTag(i);
                spinner.setOnItemSelectedListener(listener2);
                spinner.setBackground(getResources().getDrawable(R.drawable.ic_spinner));
                linearLayout.addView(spinner);

                /*ArrayAdapter<String>*/
                SpinnerItemAdapter aa4 = new /*ArrayAdapter*/SpinnerItemAdapter(activity, android.R.layout.simple_spinner_dropdown_item/*R.layout.item_spinner*/, clothing_footwear_list.get(i));
                // aa4.setDropDownViewResource(R.layout.item_spinner_dropdown);
                spinner.setAdapter(aa4);

                for (int y = 0; y < userProfile.getClothingFootware().get(i).getData().size(); y++) {
                    if (userProfile.getClothingFootware().get(i).getData().get(y).equalsIgnoreCase(userProfile.getClothingFootware().get(i).getUser_value())) {
                        spinner.setSelection(y + 1);
                        break;
                    }
                }
            } else if (userProfile.getClothingFootware().get(i).getFieldType().equalsIgnoreCase(Constant.TEXTVIEW)) {
                myEditText = new MyEditText(activity);
                myEditText.setBackground(getResources().getDrawable(R.drawable.edt_bg_selector));
                myEditText.setHint(userProfile.getClothingFootware().get(i).getFieldName());
                if (userProfile.getClothingFootware().get(i).getFieldName().equalsIgnoreCase("Shoes")) {
                    myEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                }
                myEditText.setMaxLines(1);
                myEditText.setPadding(0, (int) getResources().getDimension(R.dimen._5sdp), 0, (int) getResources().getDimension(R.dimen._8sdp));
                myEditText.setSingleLine();
                myEditText.setTextColor(getResources().getColor(R.color.colorBlack));
                myEditText.setHintTextColor(getResources().getColor(R.color.textColorHint));
                myEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._13sdp));
                myEditText.setText(userProfile.getClothingFootware().get(i).getUser_value());

                Typeface typeface = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_regular));
                myEditText.setTypeface(typeface);

                linearLayout.addView(myEditText);
            }

            binding.llClothingFootwear.addView(linearLayout);
        }
    }

    private void setUpStoreAutoComplete(List<String> favouriteStore, List<String> userSelected) {
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, favouriteStore);
        drawableWithCloseConfig = new ChipCloudConfig().selectMode(ChipCloud.SelectMode.multi).uncheckedTextColor(getResources().getColor(R.color.colorBlack)).showClose(Color.parseColor("#a6a6a6"), 500);

        storeChip = new ChipCloud(this, binding.flexBoxStore, drawableWithCloseConfig);
        binding.autoStoreName.setAdapter(adapter);
        binding.autoStoreName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("TAG", "Click");
//                if (!binding.autoStoreName.getText().toString().trim().equalsIgnoreCase("")) {
//                    for (int i = 0; i < storeChip.getAllChips().size(); i++) {
//                        if (storeChip.getAllChips().get(i).getLabel().equalsIgnoreCase(binding.autoStoreName.getText().toString().trim())) {
//                            dialogCommon.showDialog(getString(R.string.msg_already_added));
//                        }
//                    }
//                    storeChip.addChip(binding.autoStoreName.getText().toString(), null);
//                    binding.autoStoreName.setText("");
//                }

                boolean isSameFound = false;
                String str = binding.autoStoreName.getText().toString().trim();
                if (!str.equalsIgnoreCase("")) {
                    for (int i = 0; i < storeChip.getAllChips().size(); i++) {
                        Log.w("brandChip", "" + storeChip.getAllChips().get(i).getLabel());
                        if (storeChip.getAllChips().get(i).getLabel().trim().toLowerCase().equalsIgnoreCase(str.toLowerCase())) {
                            dialogCommon.showDialog(getString(R.string.msg_already_added));
                            isSameFound = true;
                            break;
                        }
                    }

                    if (!isSameFound) {
//                        binding.autoStoreName.clearFocus();
//                        int[] location = new int[2];
//                        binding.flexBoxStore.getLocationOnScreen(location);
//                        int y = location[1];
//                        Log.e(TAG, "onItemClick: flexBoxStore y : " + y);
//                        int delta = binding.flexBoxStore.getHeight() - previousHeight;
//                        if (delta > 0) {
//                            previousHeight = binding.flexBoxStore.getHeight();
//                            binding.nsvContainer.scrollBy(0, delta);
//                        } else if (delta < 0) {
//                            previousHeight = binding.flexBoxStore.getHeight();
//                            binding.nsvContainer.scrollBy(0, -delta);
//                        }
//                        binding.nsvContainer.scrollBy(0, (int) (binding.autoStoreName.getY() - binding.flexBoxStore.getY()));
                        storeChip.addChip(str, null);
//                        binding.autoStoreName.requestFocus();
                    }
                    binding.autoStoreName.setText("");

                }
            }
        });
//        binding.autoStoreName.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    if (event.getRawX() >= (binding.autoStoreName.getRight() - binding.autoStoreName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//                        Log.e("TAG", "Click");
//                        if (!binding.autoStoreName.getText().toString().trim().equalsIgnoreCase("")) {
//                            for (int i = 0; i < storeChip.getAllChips().size(); i++) {
//                                if (storeChip.getAllChips().get(i).getLabel().equalsIgnoreCase(binding.autoStoreName.getText().toString().trim())) {
//                                    dialogCommon.showDialog(getString(R.string.msg_already_added));
//                                    return true;
//                                }
//                            }
//                            storeChip.addChip(binding.autoStoreName.getText().toString(), null);
//                            binding.autoStoreName.setText("");
//                        }
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

        ///set selected value
        for (int i = 0; i < userSelected.size(); i++) {
            storeChip.addChip(userSelected.get(i), null);
        }

        storeChip.setListener(new ChipListener() {
            @Override
            public void onChipCheckChanged(int index, String label, boolean checked, boolean userClick, Chip chip) {

            }

            @Override
            public void onChipDeleted(int index, String label, Chip chip) {
                storeChip.removeChip(index);
            }
        });
    }

    private void setUpBrandAutoComplete(List<String> topBrands, List<String> userSelected) {
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, topBrands);
        drawableWithCloseConfig = new ChipCloudConfig().selectMode(ChipCloud.SelectMode.multi).uncheckedTextColor(getResources().getColor(R.color.colorBlack)).showClose(Color.parseColor("#a6a6a6"), 500);

        brandChip = new ChipCloud(this, binding.flexBoxBrand, drawableWithCloseConfig);
        binding.autoBrandName.setAdapter(adapter);
        binding.autoBrandName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("TAG", "Click");
//                if (!binding.autoBrandName.getText().toString().trim().equalsIgnoreCase("")) {
//                    for (int i = 0; i < brandChip.getAllChips().size(); i++) {
//                        if (brandChip.getAllChips().get(i).getLabel().equalsIgnoreCase(binding.autoBrandName.getText().toString().trim())) {
//                            dialogCommon.showDialog(getString(R.string.msg_already_added));
//                        }
//                    }
//                    brandChip.addChip(binding.autoBrandName.getText().toString(), null);
//                    binding.autoBrandName.setText("");
//                }

                boolean isSameFound = false;
                String str = binding.autoBrandName.getText().toString().trim();
                if (!str.equalsIgnoreCase("")) {
                    for (int i = 0; i < brandChip.getAllChips().size(); i++) {
                        Log.w("brandChip", "" + brandChip.getAllChips().get(i).getLabel());
                        if (brandChip.getAllChips().get(i).getLabel().trim().toLowerCase().equalsIgnoreCase(str.toLowerCase())) {
                            dialogCommon.showDialog(getString(R.string.msg_already_added));
                            isSameFound = true;
                            break;
                        }
                    }

                    if (!isSameFound) {
                        binding.flexBoxBrand.requestFocus();
                        brandChip.addChip(str, null);
                    }
                    binding.autoBrandName.setText("");

                }
            }
        });
//        binding.autoBrandName.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    if (event.getRawX() >= (binding.autoBrandName.getRight() - binding.autoBrandName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        // your action here
//                        Log.e("TAG", "Click");
//                        if (!binding.autoBrandName.getText().toString().trim().equalsIgnoreCase("")) {
//                            for (int i = 0; i < brandChip.getAllChips().size(); i++) {
//                                if (brandChip.getAllChips().get(i).getLabel().equalsIgnoreCase(binding.autoBrandName.getText().toString().trim())) {
//                                    dialogCommon.showDialog(getString(R.string.msg_already_added));
//                                    return true;
//                                }
//                            }
//                            brandChip.addChip(binding.autoBrandName.getText().toString(), null);
//                            binding.autoBrandName.setText("");
//                        }
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });

        ///set selected value
        for (int i = 0; i < userSelected.size(); i++) {
            brandChip.addChip(userSelected.get(i), null);
        }

        brandChip.setListener(new ChipListener() {
            @Override
            public void onChipCheckChanged(int index, String label, boolean checked, boolean userClick, Chip chip) {

            }

            @Override
            public void onChipDeleted(int index, String label, Chip chip) {
                brandChip.removeChip(index);
            }
        });
    }

    //set listener for clothing and footwear
    private AdapterView.OnItemSelectedListener listener2 = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            for (int i = 0; i < 5; i++) {
                if (parent.getId() == i) {
                    if (clothing_footwear_list.get(i).get(position).equalsIgnoreCase("Select...")) {
                        ((MyTextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    }
                    Log.e("TAG", "11111111111111111");
                    break;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    //set listener for like and interest
    private AdapterView.OnItemSelectedListener listener3 = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            for (int i = 0; i < 5; i++) {
                if (parent.getId() == i) {
                    if (like_interest_list.get(i).get(position).equalsIgnoreCase("Select...")) {
                        ((MyTextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    }
                    Log.e("TAG", "11111111111111111");
                    break;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }

    @Override
    public void onClick(int id) {
        if (R.id.ivRight == id) {
            Log.e("TAG", "Share");
            ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
            aboutMeBeen.add(new OnlyString(getString(R.string.delete_little_one)));
            //  aboutMeBeen.add(new OnlyString(getString(R.string.hide_little_one)));

            listDialog.showDialog("", true, getString(R.string.label_cancel), aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                @Override
                public void OnItemSelected(CommonPojo commonPojo, int position) {
                    if (commonPojo instanceof OnlyString) {
                        OnlyString been = (OnlyString) commonPojo;
                        Log.e("TAG", "item = " + been.getTitle());
                        if (been.getTitle().equalsIgnoreCase(getString(R.string.delete_little_one))) {
                            dialogCommon.showDialog(
                                    getString(R.string.app_name),
                                    getString(R.string.sure_want_to_delete_littleone),
                                    getString(R.string.ok),
                                    getString(R.string.label_cancel),
                                    true,
                                    true,
                                    new AlertDialogCommon.CallBackClickListener() {
                                        @Override
                                        public void OnDialogPositiveBtn() {

                                            // Delete wishlist API call.
                                            String status = NetworkUtil.getConnectivityStatusString(activity);
                                            if (status.equalsIgnoreCase("Not connected to Internet"))
                                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                                            else
                                                callDeleteLittleOneApi();
                                        }

                                        @Override
                                        public void OnDialogNegativeBtn() {

                                        }
                                    }
                            );

                            /*String status = NetworkUtil.getConnectivityStatusString(activity);
                            if (status.equalsIgnoreCase("Not connected to Internet"))
                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                            else
                                callDeleteLittleOneApi();*/
                        }
                    }
                }
            });
        }
    }


    private void callDeleteLittleOneApi() {
        loaderDialog.show();
        Call<Object> call = application.getApis().deleteLittleOne(littleOneId);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {

                                if (AccountSettingActivity.activity != null) {
                                    AccountSettingActivity.isChange = true;
                                }

                                if (LittleOneProfileActivity.activity != null) {
                                    isDeleted = true;
                                }

                                if (callbackLittleOne != null) {
                                    callbackLittleOne.deleteLittleOne();
                                }

                                LittleOnesGlobalCallbackSingleton.notifyAllListeners();

//                                if (LittleOnesGlobalCallbackSingleton.getListener(UserProfileActivity.CALLBACK_TAG, false) != null)
//                                    LittleOnesGlobalCallbackSingleton.getListener(UserProfileActivity.CALLBACK_TAG, false).deleteLittleOne();
//
//                                if (LittleOnesGlobalCallbackSingleton.getListener(AccountSettingActivity.CALLBACK_TAG, false) != null)
//                                    LittleOnesGlobalCallbackSingleton.getListener(AccountSettingActivity.CALLBACK_TAG, false).deleteLittleOne();
//
//                                if (LittleOnesGlobalCallbackSingleton.getListener(WishlistListFragment.CALLBACK_TAG, false) != null)
//                                    LittleOnesGlobalCallbackSingleton.getListener(WishlistListFragment.CALLBACK_TAG, false).deleteLittleOne();

                                finish();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void callRemoveProfilePicApi() {
        loaderDialog.show();
        Call<Object> call = application.getApis().removeprofilePicLittleOne(littleOneId, "little_one");
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);//{"status":true,"message":"Account activation mail is sent on your email address"}
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        dialogCommon.showDialog(jsonObject.getString("message"));

                        userProfile.getUser().setProfilePicture(jsonObject.getString("default"));
                        setValue();
                        /*loginData.getUser().setProfilePicture(jsonObject.getString("default"));
                        application.getSharedPref().saveSession(Constant.LOGIN_DATA, gson.toJson(loginData));*/
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }
}
