package com.app.mylineup.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityLoginBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.loginData.LoginData;
import com.app.mylineup.web_services.PrettyPrinter;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends LocationActivity {
    private ActivityLoginBinding binding;
    private String email = "";
    private String password = "";
    private boolean isFromItemDetail = false;
    private boolean isFromExternalShare;
    private Intent shareIntent;
    private String TAG = LoginActivity.class.getSimpleName();
    private Uri someUri;
    private String imgPath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        init();
    }

    private void init() {
        //getLastKnownLocation();
        getToken();

        if (getIntent().hasExtra("isFromItemDetail")) {
            isFromItemDetail = getIntent().getBooleanExtra("isFromItemDetail", false);
        }
        Log.w("isFromItemDetail", "" + isFromItemDetail);

        if (isFromItemDetail) {
            showLoginFirstDialog();
        }

        binding.tvSignUp.setOnClickListener(v ->
        {
           /* Intent tempIntent = new Intent();
            Log.w("someUri", "" + someUri);
            tempIntent.putExtra(Intent.EXTRA_STREAM, someUri);
            tempIntent.setAction(Intent.ACTION_SEND);*/


            Intent intent = new Intent(activity, RegisterActivity.class);
            if (isFromExternalShare) {
                intent.putExtra("fromShare", true);
                intent.putExtra("shareIntent", shareIntent);
            }
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.tvSignIn.setOnClickListener(v ->
        {
            checkValidation();
        });

        binding.tvForgotPassword.setOnClickListener(v ->
        {
            Intent intent = new Intent(activity, ForgotPasswordActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.googleLogin.setOnClickListener(v ->
        {
            Intent intent = new Intent(activity, GoogleLogin.class);
            startActivityForResult(intent, 10);
        });
        binding.fbLogin.setOnClickListener(v ->
        {
            Intent intent = new Intent(activity, FaceBookLogin.class);
            startActivityForResult(intent, 11);
        });

        binding.etEmailAddress.addTextChangedListener(new onTextChangeListener());
        binding.etPassword.addTextChangedListener(new onTextChangeListener());

        getHashKey();
        if (getIntent().hasExtra("shareIntent")) {
            Intent intent = getIntent().getParcelableExtra("shareIntent");
            checkIntent(intent);
            setIntent(null);
        }
    }

    private void checkIntent(Intent intent) {

        String receivedAction = intent.getAction();

        Log.w("receivedDataShare", "Action => " + receivedAction);
        Log.w("receivedDataShare", "EXTRA_TEXT => " + intent.getStringExtra(Intent.EXTRA_TEXT));

        if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                String[] PERMISSIONS = {
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                };

                Log.e("requestPermissionIntent", ".........");
                ActivityCompat.requestPermissions(this, PERMISSIONS, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
                return;
            }
            Uri receiveUri = (Uri) intent
                    .getParcelableExtra(Intent.EXTRA_STREAM);
            Log.w("receiveUri", "" + receiveUri);

            if (receiveUri != null) {

                try {
                    InputStream inputStream = getContentResolver().openInputStream(receiveUri);

                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    saveImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }



            }


            dialogCommon.showDialog(getResources().getString(R.string.please_login_to_create_item));
            isFromExternalShare = true;
            shareIntent = intent;
        }
    }

    public void saveImageBitmap(Bitmap finalBitmap) {
        Log.w("finalBitmap", "" + finalBitmap);

        File file = null;
        try {
            file = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgPath = file.getAbsolutePath().toString();
        someUri = Uri.parse(file.getAbsolutePath().toString());

        Log.e("imgPath", "" + imgPath);
        Log.e("someUri", "" + someUri);

    }

    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = new File(Environment.getExternalStorageDirectory() +
                File.separator +
                getString(R.string.app_name)
        );

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = new File(storageDir + File.separator , imageFileName + ".jpg");
        if (!image.exists()) {
            image.mkdirs();
            image.createNewFile();
        }
       // File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        String imageFilePath = image.getAbsolutePath();
        Log.w("imageFilePath", "" + imageFilePath);

        return image;
    }

    private void checkValidation() {
        if (!Global.isEmailValidat(binding.etEmailAddress.getText().toString())) {
            dialogCommon.showDialog(getString(R.string.please_enter_valid_email));
        } else if (!Global.isValidString(binding.etPassword.getText().toString())) {
            dialogCommon.showDialog(getString(R.string.please_enter_password));
        } else {
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
                callLoginApi();
        }
    }

    private void callLoginApi() {
        loaderDialog.show();
        email = binding.etEmailAddress.getText().toString().trim();
        password = binding.etPassword.getText().toString().trim();
        Call<Object> call = application.getApis().loginUser(
                email,
                password,
                application.getSharedPref().getSession(Constant.NOTIFICATION_TOKEN),
                "1",
                /*Constant.newgpsLatitude, Constant.newgpsLongitude*/"0","0");
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        LoginData loginData = gson.fromJson(x, LoginData.class);

                        if (loginData != null && loginData.isActivationPending()) {
                            showResendVerificationDialog();
                        } else {
                            processSuccessfulLogin(x, loginData);
                        }
                    } else if (jsonObject.has("activation_pending")
                            && jsonObject.getBoolean("activation_pending")) {
                        showResendVerificationDialog();
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void showResendVerificationDialog() {
        dialogCommon.showDialog(
                getString(R.string.app_name),
                getString(R.string.email_verification_message),
                getString(R.string.ok),
                getString(R.string.label_cancel),
                true,
                true,
                new AlertDialogCommon.CallBackClickListener() {
                    @Override
                    public void OnDialogPositiveBtn() {
                        String status = NetworkUtil.getConnectivityStatusString(activity);
                        if (status.equalsIgnoreCase("Not connected to Internet"))
                            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                        else
                            callResendEmailForVerificationAPI();
                    }

                    @Override
                    public void OnDialogNegativeBtn() {

                    }
                });
    }

    private void processSuccessfulLogin(String x, LoginData loginData) {
        application.getSharedPref().saveSession(Constant.IS_LOGIN, "1");
        application.getSharedPref().saveSession(Constant.LOGIN_DATA, x);
        application.getSharedPref().saveSession(Constant.HEADER_KEY, "X-API-KEY");
        application.getSharedPref().saveSession(Constant.HEADER_VALUE, loginData.getUser().getApiKey());
        application.getSharedPref().saveSession(Constant.SILENT_NOTIF_MSG, "");

        if (!isFromExternalShare) {
            Intent intent = new Intent(activity, HomeActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else {
            Intent tempIntent = new Intent();
            Log.w("someUri", "" + someUri);
            tempIntent.putExtra(Intent.EXTRA_STREAM, someUri);
            tempIntent.setAction(Intent.ACTION_SEND);
            tempIntent.setType("image/jpeg");

            Intent intent = new Intent(activity, CreateItemActivity.class);
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra("shareIntent", tempIntent/*shareIntent*/);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }
        finish();
        if (IntroSliderActivity.activity != null) {
            IntroSliderActivity.activity.finish();
        }
    }

    private void callResendEmailForVerificationAPI() {
        Call<Object> call = application.getApis().resendEmailForVerification(email);

        call.enqueue(
                new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        String x = gson.toJson(response.body());
                        Log.e("TAG", "response = " + x);
                        try {
                            JSONObject jsonObject = new JSONObject(x);
                            if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                                dialogCommon.showDialog(jsonObject.getString("message"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        loaderDialog.dismiss();
                        t.printStackTrace();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        application.registerReceiver();
    }

    public class onTextChangeListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.e("TAG", "Text = " + s.toString());
            if (binding.etEmailAddress.getText().toString().equalsIgnoreCase("") ||
                    binding.etPassword.getText().toString().equalsIgnoreCase("")) {
                binding.tvSignIn.setSelected(false);
            } else {
                binding.tvSignIn.setSelected(true);
            }
        }
    }

    private void getToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.e("TAG", "getInstanceId " + token);
                        application.getSharedPref().saveSession(Constant.NOTIFICATION_TOKEN, token);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                Log.e("TAG", "Ok");
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    checkAlreadyExistOrNot(data.getStringExtra(Constant.SOCIAL_LOGIN_DATA));
            } else if (resultCode == RESULT_CANCELED) {
                Log.e("TAG", "Cancel");
            }
        } else if (requestCode == 11) {
            if (resultCode == RESULT_OK) {
                Log.e("TAG", "Ok");
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    checkAlreadyExistOrNot(data.getStringExtra(Constant.SOCIAL_LOGIN_DATA));
            } else if (resultCode == RESULT_CANCELED) {
                Log.e("TAG", "Cancel");
            }
        }
    }

    private void checkAlreadyExistOrNot(String userData) {

        String email = "";
        try {
            JSONObject object = new JSONObject(userData);
            email = object.getString("email");
        } catch (Exception e) {

        }
        loaderDialog.show();
        Call<Object> call = application.getApis().checkSocialLoginExist(email, application.getSharedPref().getSession(Constant.NOTIFICATION_TOKEN), "1");
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        LoginData loginData = gson.fromJson(x, LoginData.class);

                        processSuccessfulLogin(x, loginData);
                    } else {
                        if (jsonObject.has("status") && !jsonObject.getBoolean("status")
                                && (jsonObject.getDouble("is_deactivated") == 0.0)/*jsonObject.getBoolean("is_deactivated")*/) {
                            dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    Intent intent = new Intent(activity, RegisterActivity.class);
                                    if (isFromExternalShare) {
                                        Intent tempIntent = new Intent();
                                        Log.w("someUri", "" + someUri);
                                        tempIntent.putExtra(Intent.EXTRA_STREAM, someUri);
                                        tempIntent.setAction(Intent.ACTION_SEND);

                                        intent.putExtra("fromShare", true);
                                        intent.putExtra("shareIntent", tempIntent/*shareIntent*/);
                                    }
                                    intent.putExtra(Constant.SOCIAL_LOGIN_DATA, userData);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });
                        }


                    }
                } catch (Exception e) {
                    Log.e("TAG", "checkAlreadyExistOrNot Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("isFromItemDetail")) {
            isFromItemDetail = intent.getBooleanExtra("isFromItemDetail", false);
        }
        if (isFromItemDetail) {
            showLoginFirstDialog();
        }
        if (intent.hasExtra("shareIntent")) {
            checkIntent(getIntent().getParcelableExtra("shareIntent"));
        }
    }

    private void getHashKey() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.app.mylineup", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("TAG", "hash key = " + something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: ");
        isFromExternalShare = false;
        shareIntent = null;
    }

    private void showLoginFirstDialog() {
        //Please login before you proceed
        dialogCommon.showDialog(getString(R.string.str_login_to_proceed));
    }

}
