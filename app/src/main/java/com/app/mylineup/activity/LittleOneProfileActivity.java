package com.app.mylineup.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.TabAdapter;
import com.app.mylineup.databinding.ActivityLittleOneProfileBinding;
import com.app.mylineup.fragment.AboutMeFragment;
import com.app.mylineup.fragment.ActivityFragment;
import com.app.mylineup.fragment.UserWishlistFragment;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.CallbackWishList;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.interfaces.WishListSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.littleOnes.mylittleone.LittleOneProfile;
import com.app.mylineup.pojo.littleOnes.mylittleone.Profile;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.singleton.WishlistCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.net.URLDecoder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LittleOneProfileActivity extends BaseActivity implements BaseActivity.onHeaderAction, UserWishlistFragment.ChangeProfileCallback {

    static final String CALLBACK_TAG = LittleOneProfileActivity.class.getSimpleName();
    private ActivityLittleOneProfileBinding binding;

    private TabAdapter adapter;
    private boolean isMyAcc = false;
    private String littleOneId = "";
    private String parentId = "";
    /*private MyProfileChange myProfileChange;*/
    private CallbackLittleOne callbackLittleOne;
    private boolean isNameChange;
    private String strNewFirstName = "", strNewLastName = "";
    private int selectedTab = 0;
    private boolean isFromUpdate = false;
    private UserWishlistFragment userWishlistFragment;
    private Profile localLittleOneProfile;
    private boolean isProfileCalled = false;
    static boolean isDeleted = false;
    private boolean isChangeInWishlist = false;

    @Override
    protected String getCallbackTag() {
        return CALLBACK_TAG;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_little_one_profile);
        isProfileCalled = false;
        init();
    }

    private void init() {
        callbackLittleOne = LittleOneSingleton.getCallBack();
        if (getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_LITTLE_ONE)) {
            binding.llSelectImg.setVisibility(View.GONE);

            if (getIntent().hasExtra(Constant.USER_NAME) && !getIntent().getStringExtra(Constant.USER_NAME).equalsIgnoreCase("")) {
                String userName = getIntent().getStringExtra(Constant.USER_NAME) + "'s Little One";
                setHeader(userName, null, null);
            }

            //setInterFace(this::onClick);
            if (getIntent().hasExtra(Constant.LITTLE_ONE_ID)) {
                littleOneId = getIntent().getStringExtra(Constant.LITTLE_ONE_ID);
            }
            /*if(getIntent().hasExtra(Constant.PARENT_ID)){
                parentId = getIntent().getStringExtra(Constant.PARENT_ID);
            }*/
        }

        //registerReceiver1();

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else {
            Log.w("getUserProfile", "Line  92...");
            getUserProfile();
        }

        WishlistCallbackSingleton.addListener(CALLBACK_TAG, new CallbackWishList() {
            @Override
            public void editWishlist(String name, String madeForID) {

            }

            @Override
            public void createItem(String wishlistId) {

            }

            @Override
            public void deleteWishlist() {

            }

            @Override
            public void changeLittleOne() {
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else {
                    Log.w("getUserProfile", "Line  92...");
                    getUserProfile();
                }
            }
        });


    }

    private void getUserProfile() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getLittleOneProfile(littleOneId);
        Log.e("TAG", "getLittleOneProfile = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                try {
                    if (x != null) {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            LittleOneProfile littleOneProfile = gson.fromJson(x, LittleOneProfile.class);
                            if (littleOneProfile != null) {

                                setValue(littleOneProfile);
                            }
                        } else {
                            dialogCommon.showDialog(jsonObject.getString("message"));
                        }
                    }
                    //{"profile":{"profile_picture":"https://www.mylineuphub.com/assets/images/default.png","name":"SAURAVKUMAR THAKKAR"},"status":false,"message":"No data found"}
                } catch (Exception e) {
                } finally {
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });


//        WishListSingleton.get(this, new CallbackWishList() {
//            @Override
//            public void editWishlist(String name, String madeForID) {
//                try {
//                    Log.w("madeForID", "userProfileWishlist madeForID => " + madeForID + "  userId => " + userProfileWishlist.get(getAdapterPosition()).getUserId());
//                    if (madeForID.equalsIgnoreCase(userProfileWishlist.get(getAdapterPosition()).getUserId())) {
//                        tvWishlistName.setText(
//                                URLDecoder.decode(name, "UTF-8"));
//                    } else {
//                        userProfileWishlist.remove(getAdapterPosition());
//                        notifyItemRemoved(getAdapterPosition());
//                        listChangeCallback.onListChange(madeForID);
//                    }
//                    // + " On "
//                    //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    tvWishlistName.setText(
//                            name);// + " On "
//                    //+ DateUtil.toDateString(list.get(position).getEventDate(), DATE_FORMAT_Y_M_D, DATE_FORMAT_M_D_Y));
//                }
//            }
//
//            @Override
//            public void createItem(String wishlistId) {
//
//            }
//
//            @Override
//            public void deleteWishlist() {
//                userProfileWishlist.remove(getAdapterPosition());
//                notifyItemRemoved(getAdapterPosition());
//            }
//
//            @Override
//            public void changeLittleOne() {
//
//            }
//
//        });
    }

    @Override
    public void onClick(int id) {
        if (R.id.ivRight == id) {
            LittleOneSingleton.get(activity, new CallbackLittleOne() {
                @Override
                public void editLittleOne(String firstName, String lastName) {
                    isNameChange = true;
                    binding.tvUserName.setText(firstName + " " + lastName);
                    strNewFirstName = firstName;
                    strNewLastName = lastName;
                }

                @Override
                public void deleteLittleOne() {
                }

                @Override
                public void hideLittleOne() {
                }

                @Override
                public void addLittleOne(LittleOne littleOne) {

                }
            });

            Intent intent = new Intent(this, CreateLittleOneActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_EDIT_LITTLE_ONE);
            intent.putExtra(Constant.LITTLE_ONE_ID, littleOneId);
            startActivityForResult(intent, 25);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else if (R.id.ivBack == id) {
            if (callbackLittleOne != null) {
                callbackLittleOne.editLittleOne(strNewFirstName, strNewLastName);
            }
        }
//        if (R.id.ivRight2 == id)
//        {
//            Log.e("TAG", "Share");
//        } else if (R.id.ivRight == id)
//        {
//            if (isMyAcc)
//            {
//                Log.e("TAG", "Edit");
//
//                Intent intent = new Intent(activity, EditProfilerActivity.class);
//                startActivityForResult(intent, 10);
//                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//            } else
//            {
//                ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
//                aboutMeBeen.add(new OnlyString(getString(R.string.remove_connection)));
//
//                listDialog.showDialog("", true, getString(R.string.label_cancel), aboutMeBeen, new AlertListDialog.CallBackClickListener()
//                {
//                    @Override
//                    public void OnItemSelected(CommonPojo commonPojo, int position)
//                    {
//                        if (commonPojo instanceof OnlyString)
//                        {
//                            OnlyString been = (OnlyString) commonPojo;
//                            Log.e("TAG", "item = " + been.getTitle());
//                            if (been.getTitle().equalsIgnoreCase(getString(R.string.remove_connection)))
//                            {
//                                // remove connection
//                                dialogCommon.showDialog(
//                                        getString(R.string.app_name),
//                                        getString(R.string.disconnect_user_confirmation),
//                                        getString(R.string.ok),
//                                        getString(R.string.label_cancel),
//                                        true,
//                                        true,
//                                        new AlertDialogCommon.CallBackClickListener()
//                                        {
//                                            @Override
//                                            public void OnDialogPositiveBtn()
//                                            {
//                                                callRemoveConnectionAPI();
//                                            }
//
//                                            @Override
//                                            public void OnDialogNegativeBtn()
//                                            {
//
//                                            }
//                                        }
//                                );
//                            }
//                        }
//                    }
//                });
//            }
//        }
    }

    /*private void callRemoveConnectionAPI()
    {

        loaderDialog.show();
        Call<Object> call = application.getApis().removeConnection(loginData.getUser().getId(), littleOneId);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>()
        {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response)
            {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try
                {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener()
                        {
                            @Override
                            public void OnDialogPositiveBtn()
                            {
                                onBackPressed();
                            }

                            @Override
                            public void OnDialogNegativeBtn()
                            {
                            }
                        });
                    } else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t)
            {
                loaderDialog.dismiss();
            }
        });

    }
*/

    @Override
    public void onBackPressed() {
        if (isNameChange) {
            if (callbackLittleOne != null) {
                callbackLittleOne.editLittleOne(strNewFirstName, strNewLastName);
            }
        }

        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isDeleted) {
            finish();
            isDeleted = false;
        }
        application.setCurrentActivity(activity);
        if (localLittleOneProfile != null) {
            Log.w("onResumeisMyAcc", "isMyAcc => " + isMyAcc+"     littleOneId  =>  " + "    loginUserId => " + localLittleOneProfile.getUserId() + "      isProfileCalled => "+isProfileCalled + "   isChangeInWishlist => "+isChangeInWishlist);
            // if (localLittleOneProfile.getUserId().equalsIgnoreCase(loginData.getUser().getId()) || isMyAcc) {
            if(isChangeInWishlist){
                isChangeInWishlist = false;
                callProfile(littleOneId);
            }else {
                if (!isProfileCalled) {
                    setValue();
                }
            }
            // }
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }

    }


    private void setValue() {
        loginData = getLoginData();

        if (Global.isValidString(loginData.getUser().getFirstName())) {
            binding.tvUserName.setText(localLittleOneProfile.getFirstName() + " " + localLittleOneProfile.getLastName()/*loginData.getUser().getFirstName() + " " + loginData.getUser().getLastName()*/);
        }

        if (Global.isValidString(localLittleOneProfile.getProfilePicture()/*loginData.getUser().getProfilePicture()*/)) {
            Picasso.get().load(localLittleOneProfile.getProfilePicture()/*loginData.getUser().getProfilePicture()*/).into(binding.ivImage);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            /*TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
            tab.select();*/
                adapter.notifyDataSetChanged();
                userWishlistFragment.setCount();
            }
        }, 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("onActivityResult", "........." + requestCode);
        if (requestCode == 10 && resultCode == RESULT_OK) {
            Log.w("getUserProfile", "Line  345...");
            getUserProfile();
        } else if (requestCode == 25 && resultCode == RESULT_OK) {
            Log.w("getUserProfile", "Line  348...");
            isFromUpdate = true;
            getUserProfile();
        }
    }

    private void setValue(@NotNull LittleOneProfile littleOneProfile) {
        Profile profile = littleOneProfile.getProfile();
        Log.e("TAG", "JSONObject = " + profile);
        localLittleOneProfile = profile;
        try {
            if (profile != null) {
                Log.w("isMyAccAnswer", "" + littleOneProfile.getProfile().getUserId());
                Log.w("isMyAccAnswer", "" + loginData.getUser().getId());
                Log.w("isMyAccAnswer", "Profile fname => " + profile.getFirstName());
                Log.w("isMyAccAnswer", "profile lname => " + profile.getLastName());
                /*if(littleOneProfile.getProfile().getUserId().toString().equals(loginData.getUser().getId().toString())){
                    isMyAcc =true;
                    Log.w("isMyAccAnswer", "Same => " + isMyAcc);
                }else{
                    Log.w("isMyAccAnswer", "NOT Same => " + isMyAcc);
                }*/
               /* isMyAcc = (littleOneProfile.getProfile().getUserId().equalsIgnoreCase(loginData.getUser().getId()));
                Log.w("isMyAccAnswer", "" + isMyAcc);*/

                String firstName = profile.getFirstName();
                String lastName = profile.getLastName();
                String profilePicture = profile.getProfilePicture();

                String fullname = firstName + " " + lastName;


                if (!TextUtils.isEmpty(firstName)) {
                    fullname = firstName;
                    //binding.tvUserName.setText(fullname);
                }

                if (!TextUtils.isEmpty(lastName)) {
                    fullname = fullname + " " + lastName;
                    // binding.tvUserName.append(" "+lastName);
                }

                binding.tvUserName.setText(fullname);

                Log.w("isMyAccAnswer", "tvUserName => " + fullname);


                if (!TextUtils.isEmpty(profilePicture)) {
                    Glide.with(this).load(profilePicture).into(binding.ivImage);
                }

                Log.w("profile.getParentName()", "" + profile.getParentName());
                if (!TextUtils.isEmpty(profile.getParentName())) {
              /*  if (getIntent().hasExtra(Constant.USER_NAME) && getIntent().getStringExtra(Constant.USER_NAME).equalsIgnoreCase(""))
                {*/
                    String userName = profile.getParentName() + "'s Little One";
                    if (profile.getUserId().equalsIgnoreCase(loginData.getUser().getId())) {
                        setHeader(userName, null, R.drawable.ic_edit_icon);
                        setInterFace(this::onClick);
                    } else {
                        setHeader(userName, null, null);
                        setInterFace(this::onClick);
                    }


                    //  }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*isMyAcc = profile.getUserId().equalsIgnoreCase(loginData.getUser().getId());*/

        adapter = new TabAdapter(getSupportFragmentManager());

        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setAdapter(adapter);

        /*UserWishlistFragment */
        userWishlistFragment =
                new UserWishlistFragment(isMyAcc, binding.tabLayout, littleOneProfile, this);
        adapter.addFragment(userWishlistFragment, getString(R.string.tab_wishlists));
        adapter.addFragment(
                new ActivityFragment(isMyAcc, littleOneProfile),
                getString(R.string.tab_activity)
        );
        adapter.addFragment(
                new AboutMeFragment(littleOneProfile),
                getString(R.string.tab_about_me)
        );


        Log.w("isFromUpdate", "" + isFromUpdate);
        if (isFromUpdate) {
            adapter.notifyDataSetChanged();
            TabLayout.Tab tab = binding.tabLayout.getTabAt(selectedTab);
            tab.select();
        } else {
            adapter.notifyDataSetChanged();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            /*TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
            tab.select();*/
                adapter.notifyDataSetChanged();
                userWishlistFragment.setCount();
            }
        }, 2000);

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedTab = position;
                if (position == 0) {
                    if (littleOneProfile.getLittleOneWishlist() != null && littleOneProfile.getLittleOneWishlist().size() > 0) {
                        binding.tabLayout.getTabAt(0).setText("Wishlists (" + littleOneProfile.getLittleOneWishlist().size() + ")");
                    } else {
                        binding.tabLayout.getTabAt(0).setText("Wishlists (0)");
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    public void callProfile(String madeForID) {
        isProfileCalled = true;
        Log.w("littleOneId", "littleOneId => " + littleOneId + "     madeForID => " + madeForID);
        if (!madeForID.equalsIgnoreCase(littleOneId) && !madeForID.equalsIgnoreCase(loginData.getUser().getId())) {
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else {
                littleOneId = madeForID;
                Log.w("getUserProfile", "Line  498...");
                getUserProfile();
            }
        } else {
            onBackPressed();
        }
    }


/*
    private void registerReceiver1() {

        myProfileChange = new MyProfileChange();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.addChangeInWishList");
        registerReceiver(myProfileChange, filter);
    }*/
/*
    private class MyProfileChange extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            getUserProfile();
        }
    }*/

    /*@Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myProfileChange);
    }*/
}
