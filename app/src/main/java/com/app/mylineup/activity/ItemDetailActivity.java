package com.app.mylineup.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.app.mylineup.BuildConfig;
import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityItemDetailBinding;
import com.app.mylineup.databinding.DialogMarkAsPurchasedBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.CallbackWishListItem;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.interfaces.WishListItemSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.AboutMeBeen;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;
import com.app.mylineup.pojo.loginData.LoginData;
import com.app.mylineup.pojo.wishlistItems.Item;
import com.app.mylineup.pojo.wishlistItems.Wishlist;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemDetailActivity extends BaseActivity implements BaseActivity.onHeaderAction/*, VideoViewDialog.OnSkipClickListener*/ {

    private static final String TAG = "ItemDetailActivity";
    private ActivityItemDetailBinding binding;
    private BottomSheetDialog bottomSheerDialog;

    private DialogMarkAsPurchasedBinding parentView;

    private String itemId = "";
    private boolean isOnlyShare = false;
    private Item item = null;
    private String purchasDate = "";
    private boolean isPurchaseClicked = false;
    private static VideoViewDialog videoViewDialog;
    private CallbackWishListItem callbackWishListItem;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_item_detail);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isPurchaseClicked = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);

        if (isPurchaseClicked) {
            ((View) parentView.getRoot().getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheerDialog.show();
        } else {
            if (loginData == null) {
                Log.w("hereInDetail", "1111..");
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.putExtra("isFromItemDetail", true);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                finish();
            } else {
                Log.w("onResume...", "");
                FirebaseDynamicLinks.getInstance()
                        .getDynamicLink(getIntent())
                        .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                            @Override
                            public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                                // Get deep link from result (may be null if no link is found)
                                Uri deepLink = null;
                                if (pendingDynamicLinkData != null) {
                                    deepLink = pendingDynamicLinkData.getLink();
                                }

                                Uri urlData = getIntent().getData();
                                //urlData.getUserInfo();
                                if (urlData != null && urlData.getQuery() != null) {
                                    Log.w("pendingDynamicLinkData", "" + urlData.getQuery().substring(7));
                                    itemId = urlData.getQuery().substring(7);

                                }
                                init();
                                // Handle the deep link. For example, open the linked
                                // content, or apply promotional credit to the user's
                                // account.
                                // ...

                                // ...
                            }
                        })
                        .addOnFailureListener(this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "getDynamicLink:onFailure", e);
                            }
                        });
            }

        }


        // init();
    }

    private void init() {
        Log.w("initCalled", "....");
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.ITEM_ID) && !getIntent().getExtras().getString(Constant.ITEM_ID).equalsIgnoreCase("")) {
            itemId = getIntent().getExtras().getString(Constant.ITEM_ID);
        }
        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            callItemDetailApi(itemId);

        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.FROM) && getIntent().getExtras().getString(Constant.FROM).equalsIgnoreCase(Constant.FROM_USER_PROFILE)) {
            setHeader("Item Details", null, R.drawable.ic_share_icon);
            isOnlyShare = true;
        } else {
            setHeader("Item Details", R.drawable.ic_share_icon, R.drawable.ic_dots);
            isOnlyShare = false;
            binding.markAsPurchased.setVisibility(View.GONE);
        }

        callbackWishListItem = WishListItemSingleton.getCallBack();

        setInterFace(this);

        bottomSheerDialog = new BottomSheetDialog(activity);
        //View parentView = View.inflate(activity, R.layout.dialog_mark_as_purchased, null);
        parentView = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_mark_as_purchased, null, false);
        bottomSheerDialog.setContentView(parentView.getRoot());
        //((View) parentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        binding.tvMarkAsSubmit.setOnClickListener(v -> {
            isPurchaseClicked = true;
            if (!application.getSharedPref().getVideoData(Constant.REVIEW_SEEN)) {

                //if(!application.getSharedPref().getVideoData(Constant.REVIEW_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.REVIEW_SEEN, true);
                //  }
               /* ((View) parentView.getRoot().getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
                bottomSheerDialog.show();*/


                Intent intent = new Intent(this, CustomVideoDialog.class);
                intent.putExtra("videoType", 5);
                intent.putExtra("isSkipDisplay", false);
                // intent.putExtra("passIntent",intentI);
                //intent.putExtra("requestCode",10);
                //  intent.putExtra("onSkipListener", (Parcelable) this);
                startActivity(intent);


               /* videoViewDialog = new VideoViewDialog(5,  this, true);
                videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/

            } else {
                ((View) parentView.getRoot().getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
                bottomSheerDialog.show();
                /*Intent intent = new Intent(this, CreateWishListActivity.class);
                startActivity(intent);

                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/
            }

        });

        parentView.tvConfirm.setOnClickListener(v -> {
            if (purchasDate.equalsIgnoreCase("")) {
                dialogCommon.showDialog(getString(R.string.please_select_date_));
            } else {
                String nstatus = NetworkUtil.getConnectivityStatusString(activity);
                if (nstatus.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    callAPiMarkAsPurchased();
            }
        });

        parentView.tvDate.setOnClickListener(v -> {
            Global.openDatePicker(activity, "", false, true, true, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    purchasDate = sdf.format(myCalendar.getTime());

                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    //int position = p.indexOf(',');
                    //String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    parentView.tvDate.setText(p);
                }
            });
        });

        parentView.tvCancel.setOnClickListener(v -> {
            bottomSheerDialog.dismiss();
        });


        binding.llTags.setOnClickListener(v -> {
            if (item == null) return;
            Log.w("WishListNameId", "Id => " + item.getWishlist().get(0).getWishlistId() + "   Name => " + item.getWishlist().get(0).getName());
            Intent intent = new Intent(activity, WishlistActivity.class);
            if (isOnlyShare) {
                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
            }
            intent.putExtra(Constant.USER_ID, item.getUserId());
            intent.putExtra(Constant.WISHLIST_ID, item.getWishlist().get(0).getWishlistId());
            intent.putExtra(Constant.WISHLIST_NAME, item.getWishlist().get(0).getName());

            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });


    }

    @Override
    public void onBackPressed() {
        if (videoViewDialog != null && handleVideoDialog() != null) {
            return;
        } else {

            super.onBackPressed();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

        }
    }

    public static YouTubePlayer handleVideoDialog() {
        return videoViewDialog.hideFullScreen();
    }


    private void callAPiMarkAsPurchased() {

        loaderDialog.show();
        Call<Object> call = application.getApis().markAsPurchased(loginData.getUser().getId(), itemId, purchasDate);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        bottomSheerDialog.dismiss();
                        //binding.markAsPurchased.setVisibility(View.GONE);
                        binding.tvMarkAsSubmit.setBackgroundResource(R.drawable.bg_button_unselected);
                        binding.tvMarkAsSubmit.setOnClickListener(null);
                        binding.tvMarkAsSubmit.setAllCaps(false);
                        binding.tvMarkAsSubmit.setText("Purchased");

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {

                              /*  Date c = Calendar.getInstance().getTime();
                                System.out.println("Current time => " + c);

                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                String formattedDate = df.format(c);
                                Log.w("TodayDate", "" + formattedDate);
                                Log.w("purchasDate", "" + purchasDate);
                                boolean isPurchaseShow = true;
                                if (formattedDate.equals(purchasDate)) {
                                    isPurchaseShow = true;
                                } else {
                                    isPurchaseShow = false;
                                }*/
                                if (callbackWishListItem != null) {
                                    callbackWishListItem.purchaseWishListItem(true, itemId);
                                }

                                //  isToday(new Date(purchasDate));

                                purchasDate = "";


                                Intent intent = new Intent();
                                setResult(RESULT_OK, intent);
                                onBackPressed();
                                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

 /*   public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }
*/

    private void callItemDetailApi(String itemId) {

        String userId = "";
        if (item != null && item.getUserId() != null && !item.getUserId().isEmpty()) {
            userId = item.getUserId();
        } else {
            //if(loginData != null) {
            Log.w("loginData", "" + loginData);
            userId = loginData.getUser().getId();
            /*}else{
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.putExtra("isFromItemDetail",true);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                finish();
            }*/
        }
        if (!userId.equalsIgnoreCase("")) {
            loaderDialog.show();
        /*String userId;
        if (item != null && item.getUserId() != null && !item.getUserId().isEmpty()) {
            userId = item.getUserId();
        } else {
            userId = loginData.getUser().getId();
        }*/
            Call<Object> call = application.getApis().getItemDetail(itemId, userId);
            Log.e("TAG", "url = " + call.request().url());
            Log.e("TAG", "param = " + PrettyPrinter.print(call));

            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    String x = gson.toJson(response.body());
                    Log.e("TAG", "response = " + x);
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                    try {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            if (jsonObject.has("item")) {
                                item = gson.fromJson(jsonObject.getJSONObject("item").toString(), Item.class);
                                setValue(item);
                            }
                        } else {
                            dialogCommon.showDialog(jsonObject.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    loaderDialog.dismiss();
                }
            });
        }

    }

    private void setValue(Item item) {
        Log.e(TAG, "setValue: Called");
        if (item != null) {
            Log.e(TAG, "Item Details: " + item.toString());
            if (item.getImage() != null && !item.getImage().equalsIgnoreCase("") && !item.getImage().contains("default.png")) {

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//                params.gravity = Gravity.CENTER;
                binding.ivItemImg.setLayoutParams(params);

                Picasso.get().load(item.getImage()).fit()/*.placeholder(R.drawable.bg_color_square)*/.into(binding.ivItemImg);
            } else {
                binding.ivItemImg.setImageResource(R.drawable.app_icon);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(400, 400);
//                params.gravity = Gravity.CENTER;
                binding.ivItemImg.setLayoutParams(params);
            }

            Log.w("item.getPurchaseTag()", "" + item.getPurchaseTag());

            /*if (item.getStatus()!=null && !item.getStatus().isEmpty()) {
                if (!list.get(position).getPurchaseTag()) {
                    holder1.itemRowBinding.ivPurchased.setVisibility(View.GONE);
                }
                else {
                    holder1.itemRowBinding.ivPurchased.setVisibility(View.VISIBLE);
                }
            }*/


            if (item.getPurchaseTag() != null && item.getPurchaseTag() && item.getUserId().equalsIgnoreCase(loginData.getUser().getId())) {
                // binding.ivPurchased.setVisibility(View.VISIBLE);
                /*setHeader("Item Details", null, R.drawable.ic_share_icon);
                isOnlyShare = true;
                binding.markAsPurchased.setVisibility(View.GONE);*/

                setHeader("Item Details", R.drawable.ic_share_icon, R.drawable.ic_dots);
                isOnlyShare = false;
                binding.markAsPurchased.setVisibility(View.VISIBLE);
            } else {
                if (item.getPurchaseTag() != null && !item.getPurchaseTag() && item.getUserId().equalsIgnoreCase(loginData.getUser().getId())) {
                    setHeader("Item Details", R.drawable.ic_share_icon, R.drawable.ic_dots);
                    isOnlyShare = false;
                    binding.markAsPurchased.setVisibility(View.GONE);
                } else {
                    binding.markAsPurchased.setVisibility(View.VISIBLE);
                }
                binding.ivPurchased.setVisibility(View.GONE);
            }

            if (item.getName() != null && !item.getName().isEmpty()) {
                binding.tvItemName.setText(item.getName());
            }

            if (item.getBrand() != null && !item.getBrand().isEmpty()) {
                binding.tvBrandName.setText(item.getBrand());
            }

            if (item.getPrice() != null && !item.getPrice().isEmpty()) {
                String tempStr = item.getPrice().replace(",", "");
                //  String tempStrNew = tempStr.replace(".", "");
                binding.tvPrice.setText(getString(R.string.currency) + Utility.getFormattedAmount(Double.parseDouble(tempStr)));
            }

            Wishlist wl = item.getWishlist().get(0);
            if (wl != null && wl.getName() != null) {
                Log.e(TAG, "setValue: Wishlist Name:" + wl.getName());
                binding.tvWishlistName.setText(wl.getName());
            } else {
                Log.e(TAG, "setValue: Wishlist is null or name is empty");
            }

            Log.w(TAG, "" + item.getSize());
            if (item.getSize() != null && !item.getSize().isEmpty()) {
                binding.tvSize.setText(item.getSize());
                binding.llSize.setVisibility(View.VISIBLE);
            } else {
                binding.llSize.setVisibility(View.GONE);
            }

            Log.e(TAG, "where to buy: " + item.getWhereToBuy());
            if (item.getWhereToBuy() == null || item.getWhereToBuy().isEmpty()) {
                binding.llWhereToBuy.setVisibility(View.GONE);
                Log.e(TAG, "Hiding where to buy");
            } else {
                Log.e(TAG, "where to buy: " + item.getWhereToBuy());
                binding.tvWhereToBuy.setText(item.getWhereToBuy());
                binding.llWhereToBuy.setVisibility(View.VISIBLE);
            }

            Log.w("item.getDescription()", "" + item.getDescription());
            if (item.getDescription() != null && !item.getDescription().trim().isEmpty()) {
                binding.tvDes.setText(item.getDescription());
                binding.llDes.setVisibility(View.VISIBLE);
            } else {
                binding.llDes.setVisibility(View.INVISIBLE);
            }
            if (item.getUrl() == null || item.getUrl().isEmpty()) {
                binding.ivUrl.setVisibility(View.GONE);
            } else {
                binding.ivUrl.setVisibility(View.VISIBLE);
            }

            binding.ivUrl.setOnClickListener(v -> {
                try {
                    String urlOpen = "";
                    if (!item.getUrl().contains("http")) {
                        urlOpen = "http://" + item.getUrl();
                    } else {
                        urlOpen = item.getUrl();
                    }
                    Log.w("urlOpen", "" + urlOpen);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(urlOpen));
                    startActivity(i);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            Log.w("item.showPurchaseButton()", "" + item.showPurchaseButton());
            /*if (item.showPurchaseButton() != null && item.showPurchaseButton()) {
                //binding.tvMarkAsSubmit.setBackgroundResource(R.drawable.bg_button_unselected);
                //binding.tvMarkAsSubmit.setOnClickListener(null);
                binding.tvMarkAsSubmit.setText("Marked as Purchased");
            } else {
                binding.tvMarkAsSubmit.setBackgroundResource(R.drawable.bg_button_unselected);
                binding.tvMarkAsSubmit.setText("Purchased");
                //binding.markAsPurchased.setVisibility(View.GONE);
            }*/


            if (item.getStatus() != null && item.getStatus().equalsIgnoreCase("1")) {
                //binding.markAsPurchased.setVisibility(View.GONE);
                binding.tvMarkAsSubmit.setBackgroundResource(R.drawable.bg_button_unselected);
                binding.tvMarkAsSubmit.setOnClickListener(null);
                binding.tvMarkAsSubmit.setText("Purchased");
                binding.tvMarkAsSubmit.setAllCaps(false);
                //status : 1 - purchased, 0- not purchase
            } else {
                binding.tvMarkAsSubmit.setText("Mark as Purchased");
                binding.tvMarkAsSubmit.setAllCaps(true);
                //binding.markAsPurchased.setVisibility(View.VISIBLE);
            }
        } else {
            Log.e(TAG, "setValue: Item is null");
        }
    }


    public Bitmap getBitmapFromURL(String src) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


    @Override
    public void onClick(int id) {
        if (R.id.ivRight2 == id) {
            callShare();
        } else if (R.id.ivRight == id) {
            if (isOnlyShare) {
                callShare();
                return;
            }
            Log.e("TAG", "Edit");
            //dialogCommon.showDialog("Title","xyz","Ok","Cancel",true,true,null);
            ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
            if (!item.getStatus().equalsIgnoreCase("1") && item.getUserId().equalsIgnoreCase(loginData.getUser().getId())) {
                aboutMeBeen.add(new OnlyString(getString(R.string.edit_item)));
            }

            aboutMeBeen.add(new OnlyString(getString(R.string.delete_item)));

            listDialog.showDialog("", true, getString(R.string.label_cancel), aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                @Override
                public void OnItemSelected(CommonPojo commonPojo, int position) {
                    Log.e("TAG", commonPojo.toString() + " " + position);
                    if (commonPojo instanceof OnlyString) {
                        OnlyString been = (OnlyString) commonPojo;
                        Log.e("TAG", "item = " + been.getTitle());
                        if (been.getTitle().equalsIgnoreCase(getString(R.string.edit_item))) {

                            String status = NetworkUtil.getConnectivityStatusString(activity);
                            if (status.equalsIgnoreCase("Not connected to Internet"))
                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                            else {

                                Intent intent = new Intent(activity, CreateItemActivity.class);
                                intent.putExtra(Constant.ITEM_DETAIL, item);
                                startActivityForResult(intent, 10);
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        } else if (been.getTitle().equalsIgnoreCase(getString(R.string.delete_item))) {
                            String status = NetworkUtil.getConnectivityStatusString(activity);
                            if (status.equalsIgnoreCase("Not connected to Internet"))
                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                            else {
                                //delete
                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.sure_want_to_delete), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                                    @Override
                                    public void OnDialogPositiveBtn() {

                                        callDeleteItemApi();
                                    }

                                    @Override
                                    public void OnDialogNegativeBtn() {

                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    }

    private void shareFilter() {


        List<Intent> targets = new ArrayList<>();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (item.getUrl() != null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
            sendIntent.putExtra(Intent.EXTRA_TEXT, item.getUrl());
        } else {
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    item.getName() + "\n\n" + item.getImage());
        }
        sendIntent.setType("text/plain");

        List<ResolveInfo> candidates = getApplication().getPackageManager().
                queryIntentActivities(sendIntent, 0);

        Log.w("currentPackageName", "" + getApplication().getPackageName());
        int appPosition = -1;

        for (int i = 0; i < candidates.size(); i++) {
            ResolveInfo candidate = candidates.get(i);

            String packageName = candidate.activityInfo.packageName;
            Log.w("allPackageName", "" + packageName);
            if (!packageName.contains(getApplication().getPackageName())) {
                Intent target = new Intent();
                target.setAction(Intent.ACTION_SEND);
                if (item.getUrl() != null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
                    target.putExtra(Intent.EXTRA_TEXT, item.getUrl());
                } else {
                    target.putExtra(Intent.EXTRA_TEXT,
                            item.getName() + "\n\n" + item.getImage());
                }
                target.setType("text/plain");

                target.setPackage(packageName);
                targets.add(target);
            } else {
                appPosition = i;
            }
        }
        // remove facebook which has a broken share intent
        /*for (ResolveInfo candidate : candidates) {
            String packageName = candidate.activityInfo.packageName;
            Log.w("allPackageName",""+packageName);
            if (!packageName.contains(getApplication().getPackageName())) {
                Intent target = new Intent();
                target.setAction(Intent.ACTION_SEND);
                if (item.getUrl()!=null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
                    target.putExtra(Intent.EXTRA_TEXT, item.getUrl());
                }
                else {
                    target.putExtra(Intent.EXTRA_TEXT,
                            item.getName() + "\n\n" + item.getImage());
                }
                target.setType("text/plain");

                target.setPackage(packageName);
                targets.add(target);
            }else{

            }
        }*/
        Intent chooser = Intent.createChooser(sendIntent, "Share Via");

        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, targets.toArray(new Parcelable[targets.size()]));
        startActivity(chooser);

    }

    private void callShare() {

        Log.w("itemShareData", "Image => " + item.getImage());
        Log.w("itemShareData", "Url => " + item.getUrl());

        //shareFilter();


       /* Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (item.getUrl() != null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
            sendIntent.putExtra(Intent.EXTRA_TEXT, item.getUrl());
        } else {
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    item.getName() + "\n\n" + item.getImage());
        }
        sendIntent.setType("text/plain");
        startActivity(sendIntent);

        Intent openInChooser = Intent.createChooser(sendIntent, "Share");

        PackageManager pm = getPackageManager();
        List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();

        Log.w("allAppList",""+resInfo.size());

        Log.w(" getPackageName()",""+ getPackageName());*/

        shareExludingApp(this, getPackageName());

        /*Bitmap bitmap = null;
        if(item.getImage() != null){
            bitmap = getBitmapFromURL(item.getImage());
        }
        try {

            if(bitmap != null){
                Log.e("TAG","Share");
                File file = new File(this.getExternalCacheDir(),"logicchip.png");
                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                file.setReadable(true, false);

                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                intent.putExtra(android.content.Intent.EXTRA_TEXT, item.getName());
                intent.setType("image/png");
                startActivity(Intent.createChooser(intent, "Share image via"));
            }
        }
        catch (Exception e){}*/

    }

    public void shareExludingApp(Context ctx, String packageNameToExclude) {

        List<LabeledIntent> targetedShareIntents = new ArrayList<LabeledIntent>();
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        String itemUrl = "https://www.mylineuphub.com/dashboard/admin/item/view?itemid=" + item.getId();

        share.putExtra(Intent.EXTRA_TEXT, itemUrl);
        /*if (item.getUrl() != null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
            share.putExtra(Intent.EXTRA_TEXT, item.getUrl());
        } else {
            share.putExtra(Intent.EXTRA_TEXT,
                    item.getName() + "\n\n" + item.getImage());
        }*/
        PackageManager pm = getPackageManager();
        List<ResolveInfo> resInfo = ctx.getPackageManager().queryIntentActivities(createShareIntent(), 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = new Intent();//createShareIntent();
                if (item.getUrl() != null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
                    targetedShare.putExtra(Intent.EXTRA_TEXT, item.getUrl());
                } else {
                    targetedShare.putExtra(Intent.EXTRA_TEXT,
                            item.getName() + "\n\n" + item.getImage());
                }

                Log.w("info.activityInfo.packageName", "" + info.activityInfo.packageName);
                if (!info.activityInfo.packageName.equalsIgnoreCase(packageNameToExclude)) {
                    targetedShare.setPackage(info.activityInfo.packageName);
                    // targetedShareIntents.add(targetedShare);

                    targetedShareIntents.add(new LabeledIntent(targetedShare, info.activityInfo.packageName, info.loadLabel(pm), info.icon));
                }
            }

            Intent chooserIntent = Intent.createChooser(share,
                    "Select app to share");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                    targetedShareIntents.toArray(new LabeledIntent[targetedShareIntents.size()]));
            ctx.startActivity(chooserIntent);
        }

    }

    private Intent createShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        if (item.getUrl() != null && !item.getUrl().isEmpty() && Utility.isValidUrl(item.getUrl())) {
            sendIntent.putExtra(Intent.EXTRA_TEXT, item.getUrl());
        } else {
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    item.getName() + "\n\n" + item.getImage());
        }
        sendIntent.setType("text/plain");
        // startActivity(sendIntent);

       /* Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("image/*");
        if (text != null) {
            share.putExtra(Intent.EXTRA_TEXT, text);
        }
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));*/
        return sendIntent;
    }

    private void callDeleteItemApi() {

        loaderDialog.show();
        Call<Object> call = application.getApis().deleteItem(itemId);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {

                        Intent intent = new Intent("android.addChangeInData");
                        sendBroadcast(intent);

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                //onBackPressed();
                                Intent intent = new Intent(activity, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                                finish();

                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });

                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("onActivityResult", "......" + (requestCode == 10));
        if (requestCode == 10 && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra(Constant.ITEM_ID)) {
                callItemDetailApi(data.getStringExtra(Constant.ITEM_ID));
            }
        }
    }

    /*@Override
    public void onSkipClick(int videoType) {
        if(!application.getSharedPref().getVideoData(Constant.REVIEW_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.REVIEW_SEEN, true);
        }
        ((View) parentView.getRoot().getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheerDialog.show();
    }*/
}
