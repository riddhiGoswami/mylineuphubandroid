package com.app.mylineup.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityRegisterBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.DateUtil;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.loginData.LoginData;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {
    private ActivityRegisterBinding binding;
    private CustomTabsIntent customTabsIntent;

    private String socialData = "";
    private String socialId = "";
    private boolean isFromExternalShare;
    private Intent shareIntent;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        init();
    }

    private void init() {
        if (getIntent() != null) {
            if (getIntent().hasExtra(Constant.SOCIAL_LOGIN_DATA)) {
                socialData = getIntent().getStringExtra(Constant.SOCIAL_LOGIN_DATA);
                binding.llConfirmPassword.setVisibility(View.GONE);
                binding.llPassword.setVisibility(View.GONE);

                try {
                    JSONObject object = new JSONObject(socialData);
                    String name = object.getString("name");
                    String email = object.getString("email");
                    socialId = object.getString("id");

                    if (!name.equalsIgnoreCase("")) {
                        binding.etFullName.setText(name);
                        binding.etFullName.setEnabled(false);
                        binding.etFullName.setKeyListener(null);
                        binding.etFullName.setFocusable(false);
                    }
                    if (!email.equalsIgnoreCase("")) {
                        binding.etEmail.setText(email);
                        binding.etEmail.setEnabled(false);
                        binding.etEmail.setKeyListener(null);
                        binding.etEmail.setFocusable(false);
                    }

                } catch (Exception e) {
                    Log.e("TAG", "Error = " + e.getLocalizedMessage());
                }
            }

            if (getIntent().hasExtra("fromShare")) {
                isFromExternalShare = getIntent().getBooleanExtra("fromShare", false);
                if (getIntent().hasExtra("shareIntent")) {
                    shareIntent = getIntent().getParcelableExtra("shareIntent");
                }
            }

        }


        binding.tvSignIn.setOnClickListener(v -> {
            onBackPressed();
        });


        binding.tvBirthDate.setOnClickListener(view -> {
            String str = "";
            if (!binding.tvBirthDate.getText().toString().trim().equalsIgnoreCase("")) {
                str = binding.tvBirthDate.getText().toString().trim();
            }
            Global.openDatePicker(activity, "", false, false, false, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    //int position = p.indexOf(',');
                    //String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    binding.tvBirthDate.setText(p);
                }
            });
        });

        binding.tvCreateAcc.setOnClickListener(v -> {
            if (!socialData.equalsIgnoreCase("")) {
                checkSocialValidation();
            } else {
                checkValidation();
            }
        });

        binding.googleLogin.setOnClickListener(v -> {
            Intent intent = new Intent(activity, GoogleLogin.class);
            startActivityForResult(intent, 10);
        });

        binding.fbLogin.setOnClickListener(v -> {
            Intent intent = new Intent(activity, FaceBookLogin.class);
            startActivityForResult(intent, 11);
        });

        binding.etEmail.addTextChangedListener(new onTextChangeListener());
        binding.etPassword.addTextChangedListener(new onTextChangeListener());
        binding.etRePassword.addTextChangedListener(new onTextChangeListener());
        binding.etFullName.addTextChangedListener(new onTextChangeListener());
        binding.etMobile.addTextChangedListener(new onTextChangeListener());

        binding.etMobile.addTextChangedListener(new Utility.PhoneNumberChecker(binding.etMobile));

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(RegisterActivity.this, R.color.colorAccent));
        builder.addDefaultShareMenuItem();
        builder.setShowTitle(true);
        customTabsIntent = builder.build();

        binding.tvTermsOfUse.setOnClickListener(v -> {
            // customTabsIntent.launchUrl(RegisterActivity.this, Uri.parse(Constant.TERMS_OF_SERVICE_URL));
            String url = Constant.TERMS_OF_SERVICE_URL;
            Intent intent = new Intent(this, OpenUrlActivity.class/*Intent.ACTION_VIEW,Uri.parse(url)*/);
            intent.putExtra("showUrl",url);
            intent.putExtra("showTitle","Terms of service");
            intent.setDataAndType(Uri.parse(url), "application/pdf");

            startActivity(intent);
        });

        binding.tvPrivacyPolicy.setOnClickListener(v -> {
            //customTabsIntent.launchUrl(RegisterActivity.this, Uri.parse(Constant.PRIVACY_POLICY_URL));
            String url = Constant.PRIVACY_POLICY_URL;
            Intent intent = new Intent(this, OpenUrlActivity.class/*Intent.ACTION_VIEW,Uri.parse(url)*/);
            intent.putExtra("showUrl",url);
            intent.putExtra("showTitle","Privacy Policy");
            //intent.setDataAndType(Uri.parse(url), "application/pdf");
            startActivity(intent);
        });

        binding.tvAccept.setOnClickListener(v -> {
            binding.checkBoxPrivacy.setChecked(!binding.checkBoxPrivacy.isChecked());
        });

        binding.tvAmpersand.setOnClickListener(v -> {
            binding.checkBoxPrivacy.setChecked(!binding.checkBoxPrivacy.isChecked());
        });

//        setPrivacyPolicy();

    }

    private void checkSocialValidation() {
        if (!Global.isValidString(binding.etFullName.getText().toString())) {
            dialogCommon.showDialog(getString(R.string.please_enter_full_name));
        } else if (!Global.isEmailValidat(binding.etEmail.getText().toString())) {
            dialogCommon.showDialog(getString(R.string.please_enter_valid_email));
        } else if (!Global.isValidString(binding.etMobile.getText().toString())) {
            dialogCommon.showDialog(getString(R.string.please_enter_phone_number));
        } else if (!binding.checkBoxPrivacy.isChecked()) {
            dialogCommon.showDialog(getString(R.string.please_accept_terms_privacy_policy));
        } else {
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet")) {
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            } else {
                callSocialLogin();
            }
        }
    }

    private void callSocialLogin() {

        String id = "", socialType = "";
        try {
            JSONObject object = new JSONObject(socialData);
            id = object.getString("id");
            socialType = object.getString("socialType");
        } catch (Exception e) {

        }

        String birthday = "";
        if (binding.tvBirthDate.getText() != null && !binding.tvBirthDate.getText().toString().isEmpty()) {
            birthday = DateUtil.toDateString(
                    binding.tvBirthDate.getText().toString(),
                    DateUtil.DATE_FORMAT_M_D_Y,
                    DateUtil.DATE_FORMAT_Y_M_D);
        }

        String zip = "";
        if (binding.etZipCode.getText() != null && !binding.etZipCode.getText().toString().isEmpty()) {
            zip = binding.etZipCode.getText().toString();
        }

        loaderDialog.show();
        Call<Object> call = application.getApis().submitSocialLogin(binding.etEmail.getText().toString(),
                application.getSharedPref().getSession(Constant.NOTIFICATION_TOKEN), "1",
                binding.etFullName.getText().toString(), binding.etMobile.getText().toString(), id, socialType, zip, birthday);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        LoginData loginData = gson.fromJson(x, LoginData.class);

                        application.getSharedPref().saveSession(Constant.IS_LOGIN, "1");
                        application.getSharedPref().saveSession(Constant.LOGIN_DATA, x);
                        application.getSharedPref().saveSession(Constant.HEADER_KEY, "X-API-KEY");
                        application.getSharedPref().saveSession(Constant.HEADER_VALUE, loginData.getUser().getApiKey());
                        application.getSharedPref().saveSession(Constant.SILENT_NOTIF_MSG, "");

                        if (!isFromExternalShare) {
                            Intent intent = new Intent(activity, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        } else {
                            Intent intent = new Intent(activity, CreateItemActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("shareIntent", shareIntent);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                        finish();

//                        Intent intent = new Intent(activity, HomeActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//                        startActivity(intent);
//                        finish();
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setPrivacyPolicy() {
        SpannableString spanString = new SpannableString(getResources().getString(R.string.privacy_policy));
        Matcher matcher = Pattern.compile("Terms and Condition").matcher(spanString);
        Matcher matcher1 = Pattern.compile("Privacy Policy").matcher(spanString);

        while (matcher.find()) {
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            spanString.setSpan(foregroundSpan, matcher.start(), matcher.end(), 0); //to highlight word havgin '@'
            final String tag = matcher.group(0);

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Log.e("TAG", "acvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
                    Intent i = new Intent(activity, TermsConditionActivity.class);
                    i.putExtra("TITLE", "Terms and Condition");
                    startActivity(i);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorAccent));

                }
            };
            spanString.setSpan(clickableSpan, matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        while (matcher1.find()) {
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            spanString.setSpan(foregroundSpan, matcher1.start(), matcher1.end(), 0); //to highlight word havgin '@'
            final String tag = matcher1.group(0);

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    //fffffffffff
                    Log.e("TAG", "ddddddddddddddddddddddddd");
                    Intent i = new Intent(activity, TermsConditionActivity.class);
                    i.putExtra("TITLE", "Privacy Policy");
                    startActivity(i);
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorAccent));

                }
            };
            spanString.setSpan(clickableSpan, matcher1.start(), matcher1.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        binding.tvPrivacyPolicy.setText(spanString);
        binding.tvPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void checkValidation() {
        Log.w("isBtnSelected",""+binding.tvCreateAcc.isSelected());
        if(binding.tvCreateAcc.isSelected()) {
            Log.e("validateNumber", "" + binding.etMobile.getText().toString().length());
            if (!Global.isValidString(binding.etFullName.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_full_name));
            } else if (!Global.isEmailValidat(binding.etEmail.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_valid_email));
            }
//        else if(!Global.isValidString(binding.tvBirthDate.getText().toString()))
//        {dialogCommon.showDialog(getString(R.string.please_select_date));return;}
//        else if(!Global.isValidString(binding.etZipCode.getText().toString()))
//        {dialogCommon.showDialog(getString(R.string.please_enter_zip_code));return;}
            else if (!Global.isValidString(binding.etMobile.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_phone_number));
            } else if ((binding.etMobile.getText().toString().length() - 4) < 10) {
                dialogCommon.showDialog(getString(R.string.valid_phone_num));
            } else if (!Global.isValidString(binding.etPassword.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_password));
            } else if (binding.etPassword.getText().toString().length() < 6) {
                dialogCommon.showDialog(getString(R.string.password_validation));
            } else if (!Global.isValidString(binding.etRePassword.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_reenter_password));
            } else if (!binding.etPassword.getText().toString().equalsIgnoreCase(binding.etRePassword.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.password_re_password_not_match));
            } else if (!binding.checkBoxPrivacy.isChecked()) {
                dialogCommon.showDialog(getString(R.string.please_accept_terms_privacy_policy));
            } else {
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet")) {
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                } else {
                    callRegisterApi();
                }
            }
        }
    }

    private void callRegisterApi() {
        loaderDialog.show();
        String zip = "";
        if (binding.etZipCode.getText() != null && !binding.etZipCode.getText().toString().isEmpty()) {
            zip = binding.etZipCode.getText().toString();
        }

        String birthday = "";
        if (binding.tvBirthDate.getText() != null && !binding.tvBirthDate.getText().toString().isEmpty()) {
            birthday = DateUtil.toDateString(
                    binding.tvBirthDate.getText().toString(),
                    DateUtil.DATE_FORMAT_M_D_Y,
                    DateUtil.DATE_FORMAT_Y_M_D);
        }

        Call<Object> call = application.getApis().registerUser(binding.etFullName.getText().toString().trim(),
                binding.etMobile.getText().toString().trim(),
                binding.etEmail.getText().toString().trim(),
                binding.etPassword.getText().toString().trim(),
                application.getSharedPref().getSession(Constant.NOTIFICATION_TOKEN),
                "1",
                /*Constant.newgpsLatitude*/"0",
                /*Constant.newgpsLongitude*/"0",
                android.os.Build.MODEL,
                zip,
                birthday);

        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        binding.etRePassword.setText("");
                        binding.etPassword.setText("");
                        binding.etEmail.setText("");
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                onBackPressed();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }

    private class onTextChangeListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.e("TAG", "Text = " + s.toString());
            if (socialData.equalsIgnoreCase("")) {
                if (binding.etFullName.getText().toString().equalsIgnoreCase("") ||
                        binding.etMobile.getText().toString().equalsIgnoreCase("") ||
                        binding.etEmail.getText().toString().equalsIgnoreCase("") ||
                        binding.etPassword.getText().toString().equalsIgnoreCase("") ||
                        binding.etRePassword.getText().toString().equalsIgnoreCase("")) {
                    binding.tvCreateAcc.setSelected(false);
                    //binding.tvCreateAcc.setEnabled(false);
                } else {
                    binding.tvCreateAcc.setSelected(true);
                    //binding.tvCreateAcc.setEnabled(true);
                }
            } else {
                if (binding.etFullName.getText().toString().equalsIgnoreCase("") ||
                        binding.etMobile.getText().toString().equalsIgnoreCase("") ||
                        binding.etEmail.getText().toString().equalsIgnoreCase("")) {
                    binding.tvCreateAcc.setSelected(false);
                   // binding.tvCreateAcc.setEnabled(false);
                } else {
                    binding.tvCreateAcc.setSelected(true);
                   // binding.tvCreateAcc.setEnabled(true);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                Log.e("TAG", "Ok");
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    checkAlreadyExistOrNot(data.getStringExtra(Constant.SOCIAL_LOGIN_DATA));
            } else if (resultCode == RESULT_CANCELED) {
                Log.e("TAG", "Cancel");
            }
        } else if (requestCode == 11) {
            if (resultCode == RESULT_OK) {
                Log.e("TAG", "Ok");
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    checkAlreadyExistOrNot(data.getStringExtra(Constant.SOCIAL_LOGIN_DATA));
            } else if (resultCode == RESULT_CANCELED) {
                Log.e("TAG", "Cancel");
            }
        }
    }

    private void checkAlreadyExistOrNot(String userData) {

        String email = "";
        try {
            JSONObject object = new JSONObject(userData);
            email = object.getString("email");
        } catch (Exception e) {

        }
        loaderDialog.show();
        Call<Object> call = application.getApis().checkSocialLoginExist(email, application.getSharedPref().getSession(Constant.NOTIFICATION_TOKEN), "1");
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        LoginData loginData = gson.fromJson(x, LoginData.class);

                        application.getSharedPref().saveSession(Constant.IS_LOGIN, "1");
                        application.getSharedPref().saveSession(Constant.LOGIN_DATA, x);
                        application.getSharedPref().saveSession(Constant.HEADER_KEY, "X-API-KEY");
                        application.getSharedPref().saveSession(Constant.HEADER_VALUE, loginData.getUser().getApiKey());
                        application.getSharedPref().saveSession(Constant.SILENT_NOTIF_MSG, "");

                        if (!isFromExternalShare) {
                            Intent intent = new Intent(activity, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        } else {
                            Intent intent = new Intent(activity, CreateItemActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("shareIntent", shareIntent);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                        finish();

//                        Intent intent = new Intent(activity, HomeActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//                        startActivity(intent);
//                        finish();
                    } else {
                        finish();
                        overridePendingTransition(0, 0);
                        Intent intent = getIntent();
                        intent.putExtra(Constant.SOCIAL_LOGIN_DATA, userData);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }
}
