package com.app.mylineup.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityTemsConditionBinding;

public class TermsConditionActivity extends BaseActivity {

    private ActivityTemsConditionBinding binding;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tems_condition);
        init();
    }

    private void init(){
        setHeader(getIntent().getStringExtra("TITLE"),null,null);
    }
}
