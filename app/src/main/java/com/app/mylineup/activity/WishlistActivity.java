package com.app.mylineup.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.WishListAdapter;
import com.app.mylineup.databinding.ActivityWishlistBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.CallbackWishList;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.interfaces.WishListSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.wishlistItems.Item;
import com.app.mylineup.pojo.wishlistItems.WishlistItems;
import com.app.mylineup.pojo.wishlistItems.Wishlist_;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.youtube.player.YouTubePlayer;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistActivity extends ImageActivity implements BaseActivity.onHeaderAction/*, VideoViewDialog.OnSkipClickListener*/ {
    private ActivityWishlistBinding binding;
    private WishListAdapter wishListAdapter;
    private WishlistItems wishlistItems;
    private Wishlist_ currentWishlist;
    private List<Item> itemList = new ArrayList<>();

    private Uri imageUri;
    private String wishlistID = "", wishlistName = "";
    private String littleOneId;
    private String userId = "", userName="";
    private boolean isMyAcc = true;

    private boolean isFromShare = false;
    private boolean isWishlistExpired = false;
    public static boolean isFromEditWishlist = false;
    public static String afterEditName = "";
    private static VideoViewDialog videoViewDialog;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wishlist);
        binding.tvAddItem.setSelected(true);
//        init();
    }

    private void init() {
        Log.w("hereWishListScreen","init");
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.FROM) && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_USER_PROFILE)) {
            binding.tvAddItem.setVisibility(View.GONE);
            isMyAcc = false;
        }

        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.FROM) && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_SHARE)) {
            isFromShare = true;
        }
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.WISHLIST_ID) && getIntent().getStringExtra(Constant.WISHLIST_ID) != null) {
            Log.w("wishlistIDInDetail", "" + getIntent().getStringExtra(Constant.WISHLIST_ID));
            Log.w("wishlistID", "wishlistID => " + wishlistID);

            if(wishlistID.equalsIgnoreCase("") && !getIntent().getStringExtra(Constant.WISHLIST_ID).equalsIgnoreCase(wishlistID)){
                wishlistID = getIntent().getStringExtra(Constant.WISHLIST_ID);
            }
        }
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.USER_ID) && getIntent().getStringExtra(Constant.USER_ID) != null) {
            userId = getIntent().getStringExtra(Constant.USER_ID);
        } else {
            userId = loginData.getUser().getId();
        }
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.USER_NAME) && getIntent().getStringExtra(Constant.USER_NAME) != null) {
            userName = getIntent().getStringExtra(Constant.USER_NAME);
        } else {
            userName = loginData.getUser().getFirstName() + " "+loginData.getUser().getLastName();
        }
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.EXP_DATE)) {
            Log.w("EXP_DATE", "111...");
            isWishlistExpired = getIntent().getBooleanExtra(Constant.EXP_DATE, false);
        }
        Log.w("isMyAcc", "" + isMyAcc);

        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.WISHLIST_NAME) && getIntent().getStringExtra(Constant.WISHLIST_NAME) != null) {
            Log.w("wishlistNameInDetail", "" + getIntent().getStringExtra(Constant.WISHLIST_NAME));
            Log.w("wishlistName", "wishlistName => " + wishlistName);
            if(wishlistName.equalsIgnoreCase("") && !getIntent().getStringExtra(Constant.WISHLIST_NAME).equalsIgnoreCase(wishlistName)){
                wishlistName = getIntent().getStringExtra(Constant.WISHLIST_NAME);
                if (isMyAcc) {
//                setHeader(wishlistName,null,R.drawable.ic_edit_icon);
                    if (isFromEditWishlist && !afterEditName.equalsIgnoreCase("")) {
                        setHeader(afterEditName, null, R.drawable.ic_dots);
                        afterEditName = "";
                        isFromEditWishlist = false;
                        Log.w("wishlistName", "wishlistName => inside if" );
                    } else {
                        Log.w("wishlistName", "wishlistName => inside else" );
                        setHeader(wishlistName, null, R.drawable.ic_dots);
                    }
                } else {
                    Log.w("wishlistName", "wishlistName => main else" );
                    setHeader(wishlistName, null, null);
                }
            }

        } else {
            //setHeader("23rd Birthday",R.drawable.ic_share_icon,R.drawable.ic_edit_icon);
            setHeader("", null, null);
        }

        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.LITTLE_ONE_ID) && getIntent().getStringExtra(Constant.LITTLE_ONE_ID) != null) {
            littleOneId = getIntent().getStringExtra(Constant.LITTLE_ONE_ID);
        }

        setInterFace(this::onClick);

        wishListAdapter = new WishListAdapter(activity, itemList, Constant.FROM_WISHLIST_DETAIL, isMyAcc, null, null);
        binding.rvWishList.setLayoutManager(new GridLayoutManager(activity, 2));
        binding.rvWishList.setAdapter(wishListAdapter);


        binding.tvAddItem.setOnClickListener(v -> {

            Log.w("currentExpDate", "" + isWishlistExpired);
            //if(!isWishlistExpired){
            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                if (imageUri == null) {
                    Intent intent = new Intent(activity, CreateItemActivity.class);
                    intent.putExtra(Constant.WISHLIST_NAME, tvHeader.getText().toString().trim());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    this.imageUri = imageUri;
                    startCrop(imageUri);
                }

            }, false, true, false, true, WishlistActivity.this);

            //openCamera();
            /*}else{
                dialogCommon.showDialog(
                        getString(R.string.app_name),
                        getString(R.string.str_wishlist_expired),
                        getString(R.string.ok),
                        getString(R.string.label_cancel),
                        true,
                        false,
                        new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {

                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
            }*/


        });
        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getItemsApiCall();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.w("hereWishListScreen","onNewIntent");
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constant.FROM) && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_USER_PROFILE)) {
            binding.tvAddItem.setVisibility(View.GONE);
            isMyAcc = false;
        }
        Log.w("hereWishListScreen","onNewIntent"+isMyAcc);
        if (intent.getExtras() != null && intent.hasExtra(Constant.WISHLIST_ID) && intent.getStringExtra(Constant.WISHLIST_ID) != null) {
            Log.w("wishlistIDInDetail", "" + intent.getStringExtra(Constant.WISHLIST_ID));
            wishlistID = intent.getStringExtra(Constant.WISHLIST_ID);
        }
        if (intent.getExtras() != null && intent.hasExtra(Constant.WISHLIST_NAME) && intent.getStringExtra(Constant.WISHLIST_NAME) != null) {
            Log.w("wishlistNameInDetail", "" + intent.getStringExtra(Constant.WISHLIST_NAME));
            wishlistName = intent.getStringExtra(Constant.WISHLIST_NAME);
            if (isMyAcc) {
//                setHeader(wishlistName,null,R.drawable.ic_edit_icon);
                if (isFromEditWishlist && !afterEditName.equalsIgnoreCase("")) {
                    setHeader(afterEditName, null, R.drawable.ic_dots);
                    afterEditName = "";
                    isFromEditWishlist = false;
                } else {
                    setHeader(wishlistName, null, R.drawable.ic_dots);
                }
            } else {
                setHeader(wishlistName, null, null);
            }
        } else {
            //setHeader("23rd Birthday",R.drawable.ic_share_icon,R.drawable.ic_edit_icon);
            setHeader("", null, null);
        }

    }

    private void getItemsApiCall() {

        itemList.clear();
        loaderDialog.show();
        Call<Object> call = application.getApis().getItemList(userId, wishlistID, null);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    if (x == null) return;

                    JSONObject jsonObject = new JSONObject(x);
                    wishlistItems = gson.fromJson(x, WishlistItems.class);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {

                        currentWishlist = wishlistItems.getWishlist();
                        if (wishlistItems != null && wishlistItems.getItems() != null && wishlistItems.getItems().size() > 0) {
                            itemList.addAll(wishlistItems.getItems());
                            wishListAdapter.notifyDataSetChanged();
                            binding.llNoData.setVisibility(View.GONE);
                        } else {
                            binding.llNoData.setVisibility(View.VISIBLE);

                            if (getIntent().getExtras() != null && userId.equalsIgnoreCase(loginData.getUser().getId())) {
                              binding.tvNoData.setText(getString(R.string.click_below_wishlist));
                            }else{
                                binding.tvNoData.setText(userName+ " "+getString(R.string.no_item_added));
                            }

                        }

                        wishlistName = currentWishlist.getName();
                        tvHeader.setText(wishlistName);

                        /*if (isMyAcc) {
                            if (currentWishlist != null && currentWishlist.getEventExpired()) {
                                binding.tvAddItem.setEnabled(false);
                                binding.tvAddItem.setSelected(false);
                                dialogCommon.showDialog(getString(R.string.edit_wishlist_to_add_items));
//                            binding.tvNoData.setText(R.string.edit_wishlist_to_add_items);
                            } else {
                                binding.tvAddItem.setEnabled(true);
                                binding.tvAddItem.setSelected(true);
                            }
                        }*/
                    } else {
                        binding.ivNoData.setVisibility(View.VISIBLE);
                        if (getIntent().getExtras() != null && userId.equalsIgnoreCase(loginData.getUser().getId())) {
                            binding.tvNoData.setText(getString(R.string.click_below_wishlist));
                        }else{
                            binding.tvNoData.setText(userName+ " "+getString(R.string.no_item_added));
                        }
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(int id) {
        if (R.id.ivRight2 == id) {
            Log.e("TAG", "Share");
        } else if (R.id.ivRight == id) {

            ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
            aboutMeBeen.add(new OnlyString(getString(R.string.edit_wishlist)));
            aboutMeBeen.add(new OnlyString(getString(R.string.delete_wishlist)));

            listDialog.showDialog("", true, getString(R.string.label_cancel), aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                @Override
                public void OnItemSelected(CommonPojo commonPojo, int position) {
                    if (commonPojo instanceof OnlyString) {
                        OnlyString been = (OnlyString) commonPojo;
                        Log.e("TAG", "item = " + been.getTitle());
                        if (been.getTitle().equalsIgnoreCase(getString(R.string.edit_wishlist))) {

                            if (wishlistItems != null && wishlistItems.getWishlist() != null) {
                               /* Intent intent = new Intent(activity, CreateWishListActivity.class);
                                intent.putExtra(Constant.FROM,WishlistActivity.class.getName());
                                intent.putExtra(Constant.WISHLIST_DATA,wishlistItems.getWishlist());
                                startActivityForResult(intent,10);
                                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/

                                callDialog();

                              /*  Intent videoIntent = new Intent(activity, VideoViewDialog.class);
                                videoIntent.putExtra("videoType",1);
                                videoIntent.putExtra("intentToPass",intent);
                                videoIntent.putExtra("hasReqCode",10);
                                startActivity(videoIntent);*/


                            }
                        } else if (been.getTitle().equalsIgnoreCase(getString(R.string.delete_wishlist))) {

                            //delete
                            dialogCommon.showDialog(
                                    getString(R.string.app_name),
                                    getString(R.string.sure_want_to_delete_wishlist),
                                    getString(R.string.ok),
                                    getString(R.string.label_cancel),
                                    true,
                                    true,
                                    new AlertDialogCommon.CallBackClickListener() {
                                        @Override
                                        public void OnDialogPositiveBtn() {

                                            // Delete wishlist API call.
                                            String status = NetworkUtil.getConnectivityStatusString(activity);
                                            if (status.equalsIgnoreCase("Not connected to Internet"))
                                                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                                            else
                                                callDeleteWishlistApi();
                                        }

                                        @Override
                                        public void OnDialogNegativeBtn() {

                                        }
                                    }
                            );
                        }
                    }
                }
            });


            Log.e("TAG", "Edit");
        }
    }

    private void callDialog() {
        if (!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {

           // if (!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
          //  }

            Intent intentI = new Intent(activity, CreateWishListActivity.class);
            intentI.putExtra(Constant.FROM, WishlistActivity.class.getName());
            intentI.putExtra(Constant.WISHLIST_DATA, wishlistItems.getWishlist());
         /*   startActivityForResult(intent,10);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/

            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 1);
            intent.putExtra("isSkipDisplay", true);
            intent.putExtra("passIntent", intentI);
            intent.putExtra("requestCode", 10);
            //  intent.putExtra("onSkipListener", (Parcelable) this);
            startActivityForResult(intent, 10);

            /*videoViewDialog = new VideoViewDialog(1, this, true);
            videoViewDialog.show(getSupportFragmentManager(),  "Video Display Dialog");*/
        } else {
            Intent intent = new Intent(activity, CreateWishListActivity.class);
            intent.putExtra(Constant.FROM, WishlistActivity.class.getName());
            intent.putExtra(Constant.WISHLIST_DATA, wishlistItems.getWishlist());
            startActivityForResult(intent, 10);

            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        Log.w("isFromEditWishlist", "" + isFromEditWishlist);
        WishListSingleton.get(activity, new CallbackWishList() {

            @Override
            public void editWishlist(String name, String madeForID) {

            }

            @Override
            public void createItem(String wishlistId) {
                wishlistID = wishlistId;
            }

            @Override
            public void deleteWishlist() {

            }

            @Override
            public void changeLittleOne() {

            }
        });
        init();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*if(requestCode == Constant.REQUEST_ID_MULTIPLE_PERMISSIONS)
        {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                openCamera();
            }
        }*/
    }

    private void openCamera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, Constant.CAMERA_RESULT);
        } else {
            String[] PERMISSIONS = {
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            };

            ActivityCompat.requestPermissions(this, PERMISSIONS, Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            /*if (requestCode == Constant.CAMERA_RESULT)
            {
                if (imageUri != null) {
                    startCrop(imageUri);
                } else {
                    Toast.makeText(activity, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }*/
            if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
            if (requestCode == 10) {
                if (data != null && data.hasExtra("NAME")) {
                    wishlistName = data.getStringExtra("NAME");
                    tvHeader.setText(data.getStringExtra("NAME"));
                    wishlistItems.getWishlist().setName(data.getStringExtra("NAME"));
                    wishlistItems.getWishlist().setEventDate(data.getStringExtra("DATE"));
                    wishlistItems.getWishlist().setWishFor(data.getStringExtra("wish_for"));
                    wishlistItems.getWishlist().setLittleOneId(data.getStringExtra("little_one_id"));
                }
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Intent intent = new Intent(activity, CreateItemActivity.class);
            intent.putExtra(Constant.WISHLIST_NAME, wishlistName);
            intent.setData(resultUri);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            finish();
            //ResultActivity.startWithUri(activity, resultUri);

        } else {
            Toast.makeText(activity, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("TAG", "handleCropError: ", cropError);
            Toast.makeText(activity, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(activity, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void startCrop(@NonNull Uri uri) {
        String destinationFileName = "SampleCropImage";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(activity);

    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        //uCrop = uCrop.withAspectRatio(1, 1);//radio_square
        return uCrop;
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        options.setCompressionQuality(50);

        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }

    @Override
    public void onBackPressed() {
        Log.e("TAG", "onBackPressed");
        if (isFromShare) {
            Intent intent = new Intent(activity, HomeActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            finishAffinity();
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

            //overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            //finish();
            super.onBackPressed();
        } else {
            if (videoViewDialog != null && handleVideoDialog() != null) {
                return;
            } else {
                super.onBackPressed();
                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

            }
        }

    }

    public static YouTubePlayer handleVideoDialog() {
        return videoViewDialog.hideFullScreen();
    }

    private void callDeleteWishlistApi() {

        loaderDialog.show();
        Call<Object> call = application.getApis().deleteWishlist(wishlistID);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {

                        Intent intent = new Intent("android.addChangeInData");
                        sendBroadcast(intent);

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                UserProfileActivity.isFromWishlist = true;

                                if (loginData.getWishlist() != null) {
                                    for (int i = 0; i < loginData.getWishlist().size(); i++) {
                                        if (loginData.getWishlist().get(i).getId().equalsIgnoreCase(wishlistID)) {
                                            loginData.getWishlist().remove(i);
                                            break;
                                        }
                                    }
                                    application.getSharedPref().saveSession(Constant.LOGIN_DATA, gson.toJson(loginData));
                                }


                                /*onBackPressed();*/
                                Intent intent = new Intent(activity,HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });

                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });

    }

   /* @Override
    public void onSkipClick(int videoType) {

        if(!application.getSharedPref().getVideoData(Constant.WISHLIST_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.WISHLIST_SEEN, true);
        }

        Intent intent = new Intent(activity, CreateWishListActivity.class);
        intent.putExtra(Constant.FROM,WishlistActivity.class.getName());
        intent.putExtra(Constant.WISHLIST_DATA,wishlistItems.getWishlist());
        startActivityForResult(intent,10);
        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
    }*/
}
