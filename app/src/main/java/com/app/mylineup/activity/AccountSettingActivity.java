package com.app.mylineup.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.app.mylineup.R;
import com.app.mylineup.adapter.LittleOneAdapter;
import com.app.mylineup.databinding.ActivityAccountSettingBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.myProfile.MyProfile;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.youtube.player.YouTubePlayer;
import com.squareup.picasso.Picasso;

import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountSettingActivity extends BaseActivity /*implements VideoViewDialog.OnSkipClickListener*/// implements View.OnClickListener
{
    public static final String CALLBACK_TAG = AccountSettingActivity.class.getSimpleName();
    protected static AccountSettingActivity activity;
    protected static boolean isChange = false;
    private ActivityAccountSettingBinding binding;
    private LittleOneAdapter littleOneAdapter;
    private List<LittleOne> littleOne = new ArrayList<>();

    private CustomTabsIntent customTabsIntent;
    private static VideoViewDialog videoViewDialog;


    @Override
    protected String getCallbackTag() {
        return CALLBACK_TAG;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = AccountSettingActivity.this;
        //LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter("LITTLE_ONE_CHANGE"));
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_account_setting);
        init();
    }

    private void init() {
        setHeader(getString(R.string.account_setting), null, null);
        isChange = false;

        if (loginData != null) {
            if (!loginData.getUser().isResetPassword()) {
                binding.llChangePassword.setVisibility(View.GONE);
            }
        }

        //binding.tvAppVersion.setText(BuildConfig.VERSION_NAME);

        binding.llProfile.setOnClickListener(v -> {
            Intent intent = new Intent(activity, UserProfileActivity.class);
            intent.putExtra(Constant.FROM, Constant.FROM_MYPROFILE);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.ivEditProfile.setOnClickListener(v -> {

            if (!application.getSharedPref().getVideoData(Constant.PROFILE_SEEN)) {

                //  if (!application.getSharedPref().getVideoData(Constant.PROFILE_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.PROFILE_SEEN, true);
                // }

                Intent intentI = new Intent(activity, EditProfilerActivity.class);
                // startActivityForResult(intentI, 10);
                //overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                Intent intent = new Intent(this, CustomVideoDialog.class);
                intent.putExtra("videoType", 4);
                intent.putExtra("isSkipDisplay", true);
                intent.putExtra("passIntent", intentI);
                intent.putExtra("requestCode", 10);
                //  intent.putExtra("onSkipListener", (Parcelable) this);
                startActivityForResult(intent, 10);

                /*videoViewDialog = new VideoViewDialog(4, this, true);
                videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
            } else {
                Intent intent = new Intent(activity, EditProfilerActivity.class);
                startActivityForResult(intent, 10);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }


        });


        binding.llNotification.setOnClickListener(v -> {
            Intent intent = new Intent(activity, NotificationsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.llChangePassword.setOnClickListener(view -> {
            Intent intent = new Intent(activity, ChangePasswordActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.llFaq.setOnClickListener(v -> {
            /*Intent intent = new Intent(activity, FaqActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

            customTabsIntent.launchUrl(AccountSettingActivity.this, Uri.parse(Constant.FAQ_URL));
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        });

        binding.llAffiliate.setOnClickListener(v -> {
            /*Intent intent = new Intent(activity, FaqActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

            customTabsIntent.launchUrl(AccountSettingActivity.this, Uri.parse(Constant.AFFILIATE_DISCLOUSER_POLICY));
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        });

        binding.llCreateLittleOne.setOnClickListener(view -> {

            LittleOneSingleton.get(activity, new CallbackLittleOne() {
                @Override
                public void editLittleOne(String firstName, String lastName) {
                }

                @Override
                public void deleteLittleOne() {
                }

                @Override
                public void hideLittleOne() {
                }

                @Override
                public void addLittleOne(LittleOne littleOne) {
                    AccountSettingActivity.this.littleOne.add(littleOne);
                    littleOneAdapter.notifyDataSetChanged();
                }
            });

            if (!application.getSharedPref().getVideoData(Constant.LITTLEONE_SEEN)) {
                //if (!application.getSharedPref().getVideoData(Constant.LITTLEONE_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.LITTLEONE_SEEN, true);
                //   }

                Intent intentI = new Intent(activity, CreateLittleOneActivity.class);

                //overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                Intent intent = new Intent(this, CustomVideoDialog.class);
                intent.putExtra("videoType", 3);
                intent.putExtra("isSkipDisplay", true);
                intent.putExtra("passIntent", intentI);
                //  intent.putExtra("onSkipListener", (Parcelable) this);
                startActivity(intent);

                /*videoViewDialog = new VideoViewDialog(3, this, true);
                videoViewDialog.show(getSupportFragmentManager(), "Share Download Image Bottom Sheet");*/

            } else {
                Intent intent = new Intent(activity, CreateLittleOneActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }


            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        });

        binding.llReportIssue.setOnClickListener(v -> {
            sendEmail(Constant.SUPPORT_MAIL_URL);
        });

        binding.llSuggestion.setOnClickListener(v -> {
                    sendEmail(Constant.SUGGESTION_MAIL_URL);
                }
        );

        binding.tvSignOut.setOnClickListener(view -> {
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.msg_logout), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                @Override
                public void OnDialogPositiveBtn() {
                    String status = NetworkUtil.getConnectivityStatusString(activity);
                    if (status.equalsIgnoreCase("Not connected to Internet"))
                        dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                    else {
                        callLogoutApi();
                    }
                }

                @Override
                public void OnDialogNegativeBtn() {

                }
            });
        });

        binding.llPrivacyPolicy.setOnClickListener(view -> {

            String url = Constant.PRIVACY_POLICY_URL;
            Intent intent = new Intent(this, OpenUrlActivity.class/*Intent.ACTION_VIEW,Uri.parse(url)*/);
            intent.putExtra("showUrl",url);
            intent.putExtra("showTitle","Privacy Policy");
            //intent.setDataAndType(Uri.parse(url), "application/pdf");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

           /* String url = Constant.PRIVACY_POLICY_URL;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(url), "application/pdf");
            startActivity(intent);*/
        });

        binding.llTermsOfService.setOnClickListener(view -> {


            String url = Constant.TERMS_OF_SERVICE_URL;
            Intent intent = new Intent(this, OpenUrlActivity.class/*Intent.ACTION_VIEW,Uri.parse(url)*/);
            intent.putExtra("showUrl",url);
            intent.putExtra("showTitle","Terms of service");
            intent.setDataAndType(Uri.parse(url), "application/pdf");

            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

           /* String url = Constant.TERMS_OF_SERVICE_URL;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(url), "application/pdf");

            startActivity(intent);*/
        });

        binding.llAddWishlist.setOnClickListener(view -> {

            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 1);
            intent.putExtra("isSkipDisplay", false);
            //  intent.putExtra("onSkipListener", (Parcelable) this);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


            /*videoViewDialog = new VideoViewDialog(1, this,false);
            videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
        });
        binding.llAddConnection.setOnClickListener(view -> {
            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 2);
            intent.putExtra("isSkipDisplay", false);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            /*videoViewDialog = new VideoViewDialog(2, this,false);
            videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
        });
        binding.llAddLittleOne.setOnClickListener(view -> {
            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 3);
            intent.putExtra("isSkipDisplay", false);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            /*videoViewDialog = new VideoViewDialog(3, this, false);
            videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
        });
        binding.llProfileCompletion.setOnClickListener(view -> {
            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 4);
            intent.putExtra("isSkipDisplay", false);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            /*videoViewDialog = new VideoViewDialog(4, this, false);
            videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
        });
        binding.llReviewWishlist.setOnClickListener(view -> {
            Intent intent = new Intent(this, CustomVideoDialog.class);
            intent.putExtra("videoType", 5);
            intent.putExtra("isSkipDisplay", false);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            /*videoViewDialog = new VideoViewDialog(5, this, false);
            videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
        });

        setupRecyclerView();

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(AccountSettingActivity.this, R.color.colorAccent));
        builder.addDefaultShareMenuItem();
        builder.setShowTitle(true);
        customTabsIntent = builder.build();

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else {
            callLittleOneApi();
        }

        LittleOnesGlobalCallbackSingleton.addListener(CALLBACK_TAG, new CallbackLittleOne() {
            @Override
            public void editLittleOne(String firstName, String lastName) {

            }

            @Override
            public void deleteLittleOne() {
                Log.e(CALLBACK_TAG, "deleteLittleOne: ");
                callLittleOneApi();
            }

            @Override
            public void hideLittleOne() {

            }

            @Override
            public void addLittleOne(LittleOne littleOne) {

            }
        });
    }

    private void setupRecyclerView() {
        littleOneAdapter = new LittleOneAdapter(activity, littleOne, LittleOneAdapter.FROM_ACCOUNT_SETTING);
        binding.rvLittleOne.setAdapter(littleOneAdapter);
        binding.rvLittleOne.setLayoutManager(new LinearLayoutManager(activity));
        binding.rvLittleOne.setNestedScrollingEnabled(false);
    }

    @Override
    public void onBackPressed() {
        Log.e("TAG", "onBackPressed");
        if (videoViewDialog != null && handleVideoDialog() != null) {
            return;
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
    }

    public static YouTubePlayer handleVideoDialog() {
        return videoViewDialog.hideFullScreen();
    }


    private void sendEmail(String mailToUrl) {
        String to = mailToUrl;
        String mailTo = "mailto:" + to;
        Intent emailIntent = new Intent(Intent.ACTION_VIEW);
        emailIntent.setData(Uri.parse(mailTo));
        startActivity(emailIntent);
    }

//    @Override
//    public void onClick(View v) {
//        Intent intent = null;
//        switch (v.getId()) {
//            case R.id.llProfile:
//                intent = new Intent(activity, UserProfileActivity.class);
//                intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//                break;
//            case R.id.ivEditProfile:
//                intent = new Intent(activity, EditProfilerActivity.class);
//                startActivityForResult(intent,10);
//                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//                break;
//            case R.id.llNotification:
//                intent = new Intent(activity, NotificationsActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//                break;
//            case R.id.llChangePassword:
//                intent = new Intent(activity, ChangePasswordActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//                break;
//            case R.id.llFaq:
//                intent = new Intent(activity, FaqActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//                break;
//            case R.id.llCreateLittleOne:
//                LittleOneSingleton.get(activity, new CallbackLittleOne() {
//                    @Override
//                    public void editLittleOne(String firstName, String lastName) {
//                    }
//                    @Override
//                    public void deleteLittleOne() {
//                    }
//                    @Override
//                    public void hideLittleOne() {
//                    }
//                    @Override
//                    public void addLittleOne(LittleOne littleOne) {
//                        myProfile.getLittleOne().add(littleOne);
//                        littleOneAdapter.notifyDataSetChanged();
//                    }
//                });
//                intent = new Intent(activity, CreateLittleOneActivirt.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
//                break;
//            case R.id.tvSignOut:
//                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.msg_logout), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
//                    @Override
//                    public void OnDialogPositiveBtn() {
//                        callLogoutApi();
//                    }
//
//                    @Override
//                    public void OnDialogNegativeBtn() {
//
//                    }
//                });
//                break;
//        }
//    }

    private void callLittleOneApi() {
        loaderDialog.show();
        Call<Object> call = application.getApis().getLittleOne(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        MyProfile myProfile = gson.fromJson(x, MyProfile.class);
                        littleOne.clear();
                        littleOne.addAll(myProfile.getLittleOne());
                        littleOneAdapter.notifyDataSetChanged();
//                        setAdapter();
                    } else {
                        littleOne.clear();
                        littleOneAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

//    private void setAdapter() {
//        littleOneAdapter = new LittleOneAdapter(activity, littleOne, LittleOneAdapter.FROM_ACCOUNT_SETTING);
//        binding.rvLittleOne.setAdapter(littleOneAdapter);
//        binding.rvLittleOne.setLayoutManager(new LinearLayoutManager(activity));
//        binding.rvLittleOne.setNestedScrollingEnabled(false);
//    }

    private void callLogoutApi() {
        loaderDialog.show();
        Call<Object> call = application.getApis().logout(loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                Log.e("TAG", "code = " + response.code());
                Log.e("TAG", "msg = " + response.message());
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        application.getSharedPref().saveSession(Constant.IS_LOGIN, "");
                        application.getSharedPref().clearAll();
                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    protected void updateLittleOneList(String littleOneId, LittleOne littleOne, String firstName, String lastName) {
        if (littleOneId != null) {
            for (int i = 0; i < this.littleOne.size(); i++) {
                if (this.littleOne.get(i).getId().equalsIgnoreCase(littleOneId)) {
                    this.littleOne.get(i).setFirstName(firstName);
                    this.littleOne.get(i).setLastName(lastName);
                    littleOneAdapter.notifyDataSetChanged();
                    break;
                }
            }
        } else {
            this.littleOne.add(littleOne);
            littleOneAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        if (isChange) {
            isChange = !isChange;
            callLittleOneApi();
        }

        setValue();
    }

    private void setValue() {
        loginData = getLoginData();
        //if (Global.isValidString(loginData.getUser().getFirstName())) {
        binding.tvUserName.setText(loginData.getUser().getFirstName() + " " + loginData.getUser().getLastName());
        //}

        if (Global.isValidString(loginData.getUser().getProfilePicture())) {
            Picasso.get().load(loginData.getUser().getProfilePicture()).into(binding.ivUsrImg);
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isEdit = intent.getBooleanExtra("edit", false);
            Log.e("TAG", "AccountSettingActivity");
            if (isEdit) {
                for (int i = 0; i < littleOne.size(); i++) {
                    if (littleOne.get(i).getId().equalsIgnoreCase(intent.getStringExtra("littleOneId"))) {
                        littleOne.get(i).setFirstName(intent.getStringExtra("firstName"));
                        littleOne.get(i).setLastName(intent.getStringExtra("lastName"));
                        littleOneAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            } else {
                LittleOne littleOne = (LittleOne) intent.getSerializableExtra("data");
                AccountSettingActivity.this.littleOne.add(littleOne);
                littleOneAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

   /* @Override
    public void onSkipClick(int videoType) {
        if (videoType == 4) {

            if(!application.getSharedPref().getVideoData(Constant.PROFILE_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.PROFILE_SEEN, true);
            }

            Intent intent = new Intent(activity, EditProfilerActivity.class);
            startActivityForResult(intent, 10);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else if (videoType == 3) {

            if(!application.getSharedPref().getVideoData(Constant.LITTLEONE_SEEN)) {
                application.getSharedPref().saveVideoData(Constant.LITTLEONE_SEEN, true);
            }

            Intent intent = new Intent(activity, CreateLittleOneActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        }

    }*/


}

