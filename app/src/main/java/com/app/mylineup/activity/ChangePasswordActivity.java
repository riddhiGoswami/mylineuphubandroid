package com.app.mylineup.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityChangePasswordBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {
    private ActivityChangePasswordBinding binding;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        init();
    }

    private void init() {
        setHeader(getString(R.string.lbl_change_password), null, null);
        binding.etCurrentPass.addTextChangedListener(new onTextChangeListener());
        binding.etNewPass.addTextChangedListener(new onTextChangeListener());
        binding.etConfirmPass.addTextChangedListener(new onTextChangeListener());

        binding.tvChangePass.setOnClickListener(view -> {
            validateDetail();
        });
    }

    private void validateDetail() {
        if(binding.tvChangePass.isSelected()) {
            if (!Global.isValidString(binding.etCurrentPass.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_current_password));
            } else if (!Global.isValidString(binding.etNewPass.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_new_password));
            } else if (binding.etNewPass.getText().toString().length() < 6) {
                dialogCommon.showDialog(getString(R.string.password_validation));
            } else if (!Global.isValidString(binding.etConfirmPass.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_confirm_password));
            } else if (!binding.etNewPass.getText().toString().equalsIgnoreCase(binding.etConfirmPass.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.password_re_password_not_match));
            } else {
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                    callChangePasswordApi();
            }
        }
    }

    private void callChangePasswordApi() {
        loaderDialog.show();
        Call<Object> call = application.getApis().changePassword(binding.etCurrentPass.getText().toString().trim(), binding.etNewPass.getText().toString().trim(),
                loginData.getUser().getId());
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);//{"status":true,"message":"Account activation mail is sent on your email address"}
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                onBackPressed();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }

    public class onTextChangeListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.e("TAG", "Text = " + s.toString());
            if (binding.etCurrentPass.getText().toString().equalsIgnoreCase("") ||
                    binding.etNewPass.getText().toString().equalsIgnoreCase("") ||
                    binding.etConfirmPass.getText().toString().equalsIgnoreCase("")) {
                binding.tvChangePass.setSelected(false);
            } else {
                binding.tvChangePass.setSelected(true);
            }
        }
    }
}
