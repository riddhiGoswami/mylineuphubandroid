package com.app.mylineup.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityHomeBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.ImageChooserDialog;
import com.app.mylineup.fragment.ConnectionFragment;
import com.app.mylineup.fragment.GiftFeedFragment;
import com.app.mylineup.fragment.NotificationFragment;
import com.app.mylineup.fragment.WishlistFragment;
import com.app.mylineup.fragment.WishlistListFragment;
import com.app.mylineup.interfaces.CallbackClickListner;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.RealPathUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static android.provider.Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION;
import static com.app.mylineup.other.Constant.REQUEST_ID_MULTIPLE_PERMISSIONS;
import static com.app.mylineup.other.Constant.REQUEST_STORAGE_READ_ACCESS_PERMISSION;

public class HomeActivity extends ImageActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private ActivityHomeBinding binding;
    private WishlistFragment wishlistFragment;
    private GiftFeedFragment giftFeedFragment;
    private NotificationFragment notificationFragment;
    private ConnectionFragment connectionFragment;
    private FragmentTransaction transaction;
    private int selectedOption = -1;
    private boolean isGoToSettingImage = false;
    private boolean isDialogAlreadyDisplayImage = false;

    private Uri imageUri;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        init();
    }

    private void init() {
        application.getSharedPref().saveSession(Constant.SILENT_NOTIF_MSG, "");
        connectionFragment = new ConnectionFragment();
        notificationFragment = new NotificationFragment();
        giftFeedFragment = new GiftFeedFragment();
        wishlistFragment = new WishlistFragment();

        String from = application.getSharedPref().getSession("FROM");
        Log.e("TAG", "from= " + from);
        if (from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Notification")) {
            loadNotification();
            application.getSharedPref().saveSession("FROM", "");
            binding.navigation.getMenu().findItem(R.id.i_notification).setChecked(true);
        } else if (from != null && !from.equalsIgnoreCase("") && from.equalsIgnoreCase("Notification_Weekly")) {
            loadGiftFeed();
            application.getSharedPref().saveSession("FROM", "");
            binding.navigation.getMenu().findItem(R.id.i_gift).setChecked(true);
        } else {
            loadWishList();
        }

        binding.navigation.setOnNavigationItemSelectedListener(this);
        binding.ivHexagon.setOnClickListener(v -> {
            callMethod();
        });

//        Log.e("TAG", "email = " + loginData.getUser().getEmail());
    }

//    public void callMethod() {
//        openImageChooser(new CallbackImageListener() {
//            @Override
//            public void onImageSuccess(String imageFilePath, Uri imageUri, int cameraPhotoOrientation) {
//                Log.w("itemSelected", "111..." + imageUri);
////                if (imageUri == null) {
////                    Intent intent = new Intent(activity, CreateItemActivity.class);
////                    startActivity(intent);
////                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
////                } else {
////                    HomeActivity.this.imageUri = imageUri;
////                    Log.w("HomeActivityStartCrop",".......");
////                    HomeActivity.this.startCrop(imageUri);
////                }
//            }
//        }, false, true, false, true);
//    }


    public void callMethod() {

        new ImageChooserDialog(HomeActivity.this, new CallbackClickListner() {
            @Override
            public void onClickListener(View view, int position) {
                Log.w("selectedItem", "" + position);
                if (position == 0) {
                    selectedOption = 0;
                   /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!Environment.isExternalStorageManager()) {
                            Intent i = new Intent();
                            i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                            //  startActivity(i);
                            startActivityForResult(i, 20);
                        } else {
                            captureImage();
                        }
                    } else {*/
                    captureImage(HomeActivity.this);
                    //}

                } else if (position == 1) {
                    selectedOption = 1;
                   /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!Environment.isExternalStorageManager()) {
                            Intent i = new Intent();
                            i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                            startActivityForResult(i, 20);
                            // startActivity(i);
                        } else {
                            openGalleryPermission();
                        }
                    } else {*/
                    openGalleryPermission(HomeActivity.this);

                    //}

                } else if (position == 2) {
                    selectedOption = 2;
                    checkUri(null,
                            null,
                            -1);
                } else if (position == 3) {
                    String defaultImage = application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE);
                    //No Image option selected
                    checkUri(defaultImage,
                            null,
                            -1);
                }
            }
        }, false, true);
    }

    private void checkUri(String imageFilePath, Uri imageUri, int cameraPhotoOrientation) {
        if (imageUri == null) {
            Intent intent = new Intent(HomeActivity.this, CreateItemActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else {
            HomeActivity.this.imageUri = imageUri;
            Log.w("HomeActivityStartCrop", ".......");
            HomeActivity.this.startCrop(imageUri);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        List<Fragment> fragList = getSupportFragmentManager().getFragments();
        for (int i = 0; i < fragList.size(); i++) {
            if (fragList.get(i) instanceof WishlistFragment) {
                Log.w("wishlistFragmentBackpre", "111...");
                if (WishlistListFragment.handleVideoDialog() != null) {
                    return;
                } else if (ConnectionFragment.handleVideoDialog() != null) {
                    return;
                } else {
                    super.onBackPressed();
                }

            }/*else if(fragList.get(i) instanceof CreateWishlistFragOne){
                Log.w("wishlistFragmentBackpress","CreateWishlistFragOne...");
                if(CreateWishlistFragOne.handleVideoDialog() != null){
                    return;
                }else{
                    super.onBackPressed();
                }
            }*/ else {
                super.onBackPressed();
            }
        }
    }


    /*private void openCamera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            *//*ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, Constant.CAMERA_RESULT);*//*

            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                startCrop(imageUri);

            },true,false,false);
        }
         else {
            String[] PERMISSIONS = {
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            };

            ActivityCompat.requestPermissions(this,PERMISSIONS,Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }*/

    private void loadGiftFeed() {
        transaction = getSupportFragmentManager().beginTransaction();
        if (connectionFragment.isAdded()) {
            transaction.hide(connectionFragment);
        }
        if (notificationFragment.isAdded()) {
            transaction.hide(notificationFragment);
        }
        if (wishlistFragment.isAdded()) {
            transaction.hide(wishlistFragment);
        }
        if (giftFeedFragment.isAdded()) {
            transaction.show(giftFeedFragment);
        } else {
            transaction.add(R.id.frame, giftFeedFragment);
        }

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadNotification() {
        transaction = getSupportFragmentManager().beginTransaction();
        if (connectionFragment.isAdded()) {
            transaction.hide(connectionFragment);
        }
        if (giftFeedFragment.isAdded()) {
            transaction.hide(giftFeedFragment);
        }
        if (wishlistFragment.isAdded()) {
            transaction.hide(wishlistFragment);
        }
        if (notificationFragment.isAdded()) {
            transaction.show(notificationFragment);
        } else {
            transaction.add(R.id.frame, notificationFragment);
        }

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadConnection() {
        transaction = getSupportFragmentManager().beginTransaction();
        if (notificationFragment.isAdded()) {
            transaction.hide(notificationFragment);
        }
        if (giftFeedFragment.isAdded()) {
            transaction.hide(giftFeedFragment);
        }
        if (wishlistFragment.isAdded()) {
            transaction.hide(wishlistFragment);
        }
        if (connectionFragment.isAdded()) {
            transaction.show(connectionFragment);
        } else {
            transaction.add(R.id.frame, connectionFragment);
        }

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadWishList() {
        transaction = getSupportFragmentManager().beginTransaction();
        if (connectionFragment.isAdded()) {
            transaction.hide(connectionFragment);
        }
        if (notificationFragment.isAdded()) {
            transaction.hide(notificationFragment);
        }
        if (giftFeedFragment.isAdded()) {
            transaction.hide(giftFeedFragment);
        }
        if (wishlistFragment.isAdded()) {
            transaction.show(wishlistFragment);
        } else {
            transaction.add(R.id.frame, wishlistFragment);
        }

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.i_wish:
                loadWishList();
                break;

            case R.id.i_gift:
                loadGiftFeed();
                break;

            case R.id.i_notification:
                loadNotification();
                break;

            case R.id.i_conection:
                loadConnection();
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       // super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean isPermissionAllow = true;
        Log.e("isPermissionImageAct", ".....");
        //  if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {

        try {

            Log.w("permissions", "permissions => " + permissions.length);
            for (int i = 0; i < permissions.length; i++) {
                Log.e("isPermissionGranted", "permission => " + permissions[i] + "    Result => " + grantResults[i]);
            }
            for (int grantResult : grantResults) {
                Log.e("isPermissionGranted", "" + (grantResult != PackageManager.PERMISSION_GRANTED));
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    isPermissionAllow = false;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  }
        Log.e("isPermissionGranted", "....." + requestCode);
        switch (requestCode) {
            case Constant.REQUEST_STORAGE_WRITE_ACCESS_PERMISSION:

                if (isPermissionAllow) {
                    takePicture();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!isDialogAlreadyDisplayImage) {
                            isDialogAlreadyDisplayImage = true;
                            isGoToSettingImage = true;
                            dialogCommon.showDialog("", "You need to allow permission to add item.", "Go to setting", getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    goToSettings();
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });
                        }

                    }
                    Toast.makeText(this, "Permission Denied !", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                Log.e("READ_ACCESS_PERMISSION", "" + REQUEST_STORAGE_READ_ACCESS_PERMISSION);
                Log.e("isPermissionAllow", "" + isPermissionAllow);
                if (isPermissionAllow) {
                    Log.w("isPermissionAllow", "" + (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent));
                    if (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent) {
                        openGallery();
                    } else {
                        Log.w("openGalleyCalled","Line 413");
                        openGallery();
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!isDialogAlreadyDisplayImage) {
                            isDialogAlreadyDisplayImage = true;
                            isGoToSettingImage = true;
                            dialogCommon.showDialog("", "You need to allow permission to add item.", "Go to setting", getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    goToSettings();
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });

                        }
                    }
                    Toast.makeText(this, "Permission Denied !", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                Log.e("MULTIPLE_PERMISSIONS", "" + REQUEST_ID_MULTIPLE_PERMISSIONS + " ====   " + (!CreateItemActivity.activity.isShareIntent) + " isPermissionAllow => " + isPermissionAllow);
                if (isPermissionAllow) {
                    Log.w("isPermissionAllow", "" + (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent));
                    if (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent) {
                        openGallery();
                    }
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
        }

        //shouldShowRequestPermissionRationale(permission)
            /*if(requestCode == Constant.REQUEST_ID_MULTIPLE_PERMISSIONS)
            {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                {
                    openCamera();
                }
            }
            else if(requestCode == Constant.REQUEST_ID_STORAGE_READ_WRITE_PERMISSIONS){

            }*/
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            photo = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(this,
                getPackageName() + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, Constant.CAMERA_RESULT);

        /*Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, PHOTO_REQUEST);*/
    }

    private void goToSettings() {
       /* Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);*/
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constant.OPEN_GALLERY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        Log.w("hereOnResult", "1111");
        Log.e(TAG, "onActivityResult requestCode : "+requestCode);
        Log.e(TAG, "onActivityResult resultCode : "+resultCode);
        if (resultCode == RESULT_OK) {
            /*if (requestCode == Constant.CAMERA_RESULT)
            {
                *//*Bitmap photo = null;
                try {
                    photo = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                }
                catch (Exception e)
                {}
                final Uri selectedUri = storeImage(photo);*//*
                if (imageUri != null) {
                    startCrop(imageUri);
                } else {
                    Toast.makeText(activity, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }*/
            if ((requestCode == Constant.CAMERA_RESULT || requestCode == Constant.OPEN_GALLERY)) {
                Uri sourceUri = imageUri;
                if (requestCode == Constant.OPEN_GALLERY) {
                    imageUri = data.getData();
                    imageFilePath = RealPathUtil.getRealPath(this, imageUri);
                    if (imageFilePath == null) {
                        return;
                    }
                }

                compressImage(imageUri);
                launchMediaScanIntent();
                checkUri(imageFilePath, imageUri, getCameraPhotoOrientation(imageFilePath));
//                onImageSuccess(imageFilePath, imageUri, getCameraPhotoOrientation(imageFilePath));

            } else if (requestCode == 20) {
                if (!Environment.isExternalStorageManager()) {
                    Intent i = new Intent();
                    i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    startActivityForResult(i, 20);
                } else {
                    Log.e("selectedOption", "" + selectedOption);
                    if (selectedOption == 0) {
                        captureImage(HomeActivity.this);
                    } else if (selectedOption == 1) {
                        openGalleryPermission(HomeActivity.this);
                    }
                }

            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    public void compressImage(Uri uri) {
        Log.w("compressImage", "inside compress image...");
        Bitmap bitmap = null;
        try {
            bitmap = getThumbnail(uri);
            saveImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void saveImageBitmap(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        //File file = new File (myDir, fname);
        File file = null;
        try {
            file = createImageFile();
        } catch (Exception e) {
            Log.w("createImagefile", "" + e.getMessage());
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(this,
                getPackageName() + ".provider", file);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void launchMediaScanIntent() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private Uri storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.e("TAG", "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

            Uri uri = Uri.fromFile(pictureFile);
            return uri;
        } catch (FileNotFoundException e) {
            Log.e("TAG", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e("TAG", "Error accessing file: " + e.getMessage());
        }
        return null;
    }

    private File getOutputMediaFile() {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/Android/data/" + getApplicationContext().getPackageName() + "/Files");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        Log.w("handleCrop", "1111 => " + resultUri);
        if (resultUri != null) {
            Intent intent = new Intent(HomeActivity.this, CreateItemActivity.class);
            intent.setData(resultUri);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            //ResultActivity.startWithUri(activity, resultUri);

        } else {
            Toast.makeText(HomeActivity.this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("TAG", "handleCropError: ", cropError);
            Toast.makeText(HomeActivity.this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(HomeActivity.this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }


    private void startCrop(@NonNull Uri uri) {

        String destinationFileName = "SampleCropImage";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(HomeActivity.this);
        Log.e(TAG, "startCrop");
    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        //uCrop = uCrop.withAspectRatio(1, 1);//radio_square
        return uCrop;
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        options.setCompressionQuality(50);

        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);

        if (loginData != null) {
            /*Log.w("silentMsgNotif",""+application.getSharedPref().getSession(Constant.SILENT_NOTIF_MSG));
            BaseActivity.activity.logoutUser(application.getSharedPref().getSession(Constant.SILENT_NOTIF_MSG));
        }else {*/
            for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
                Fragment fg = getSupportFragmentManager().getFragments().get(i);
                if (fg instanceof WishlistFragment) {
                    ((WishlistFragment) fg).setValue();
                } else if (fg instanceof NotificationFragment) {
                    ((NotificationFragment) fg).setValue();
                } else if (fg instanceof GiftFeedFragment) {
                    ((GiftFeedFragment) fg).setValue();
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
