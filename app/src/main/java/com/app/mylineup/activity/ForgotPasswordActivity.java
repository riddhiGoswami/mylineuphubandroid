package com.app.mylineup.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityForgotPasswordBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {
    private ActivityForgotPasswordBinding binding;
    private String email = "";

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        init();
    }

    private void init() {
        setHeader(getString(R.string.lbl_fogotpassword), null, null);

        binding.tvSendInstruction.setOnClickListener(v -> {
            if (!Global.isEmailValidat(binding.etEmail.getText().toString())) {
                dialogCommon.showDialog(getString(R.string.please_enter_valid_email));
            } else {
                String status = NetworkUtil.getConnectivityStatusString(activity);
                if (status.equalsIgnoreCase("Not connected to Internet"))
                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                else
                callForgotPasswordApi();
            }
        });

        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equalsIgnoreCase("")) {
                    binding.tvSendInstruction.setSelected(false);
                } else {
                    binding.tvSendInstruction.setSelected(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void callForgotPasswordApi() {
        loaderDialog.show();
        email = binding.etEmail.getText().toString().trim();
        Call<Object> call = application.getApis().forgotPassword(email);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                        dialogCommon.showDialog(getString(R.string.app_name), jsonObject.getString("message"), getString(R.string.ok), "", true, false, new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                onBackPressed();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });

                    } else if (jsonObject.has("activation_pending")
                            && jsonObject.getBoolean("activation_pending")) {
                        showResendVerificationDialog();
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }

    private void showResendVerificationDialog() {
        dialogCommon.showDialog(
                getString(R.string.app_name),
                getString(R.string.email_verification_message),
                getString(R.string.ok),
                getString(R.string.label_cancel),
                true,
                true,
                new AlertDialogCommon.CallBackClickListener() {
                    @Override
                    public void OnDialogPositiveBtn() {
                        String status = NetworkUtil.getConnectivityStatusString(activity);
                        if (status.equalsIgnoreCase("Not connected to Internet"))
                            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                        else
                        callResendEmailForVerificationAPI();
                    }

                    @Override
                    public void OnDialogNegativeBtn() {

                    }
                });
    }

    private void callResendEmailForVerificationAPI() {
        Call<Object> call = application.getApis().resendEmailForVerification(email);

        call.enqueue(
                new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, Response<Object> response) {
                        String x = gson.toJson(response.body());
                        Log.e("TAG", "response = " + x);
                        try {
                            JSONObject jsonObject = new JSONObject(x);
                            if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                                dialogCommon.showDialog(jsonObject.getString("message"));
//                                onBackPressed();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        loaderDialog.dismiss();
                        t.printStackTrace();
                    }
                });
    }
}
