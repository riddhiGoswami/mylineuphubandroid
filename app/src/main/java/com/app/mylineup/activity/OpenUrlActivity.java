package com.app.mylineup.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.app.mylineup.R;

public class OpenUrlActivity extends BaseActivity {

    private WebView webview_url;
    String url = "", title = "";
    // private ProgressBar progress_url_pdf;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_url);

        webview_url = findViewById(R.id.webview_url);
        //   progress_url_pdf=  findViewById(R.id.progress_url_pdf);

        url = getIntent().getStringExtra("showUrl");
        title = getIntent().getStringExtra("showTitle");
        Log.w("urlToShow", "" + url);
        //webview_url.getSettings().setJavaScriptEnabled(true);
        // webview_url.loadUrl(url);

        // webview_url.getSettings().setJavaScriptEnabled(true);
        webview_url.clearCache(true);
        /*webview_url.getSettings().setAllowFileAccessFromFileURLs(true);
        webview_url.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webview_url.getSettings().setBuiltInZoomControls(true);*/
        webview_url.setWebChromeClient(new WebChromeClient());
        //webview_url.setDownloadListener(new MyDownLoadListener());

        webview_url.loadUrl(url);

        setHeader(title, null, null);


        webview_url.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                Log.w("getViewHeight", "" + view.getContentHeight());
                if (view.getContentHeight() == 0) {
                    view.reload();
                } else {
                    //progress_url_pdf.setVisibility(View.GONE);
                }
                Log.w("setWebViewClient", "onPageFinished..");
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                Log.w("setWebViewClient", "onReceivedError..");
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }

    public class MyDownLoadListener implements DownloadListener {
        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {

            if (url != null) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }

        }
    }
}
