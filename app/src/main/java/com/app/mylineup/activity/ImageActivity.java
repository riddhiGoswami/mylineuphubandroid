package com.app.mylineup.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mylineup.R;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.ImageChooserDialog;
import com.app.mylineup.interfaces.CallbackClickListner;
import com.app.mylineup.interfaces.CallbackImage;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.RealPathUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;

import static android.provider.Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION;
import static com.app.mylineup.other.Constant.REQUEST_ID_MULTIPLE_PERMISSIONS;
import static com.app.mylineup.other.Constant.REQUEST_STORAGE_READ_ACCESS_PERMISSION;


public class ImageActivity extends BaseActivity implements CallbackImage {
    private ImageView ctn_ivHome, iv_toolbarLogo;
    protected TextView tv_toolbarTitle, tv_toolbarRightTitle;
    private LinearLayout ll_drawer;
    protected String imageFilePath;
    protected Uri imageUri;
    private CallbackImageListener callbackImageListener;
    private int selectedOption = -1;
    private boolean isGoToSettingImage = false;
    private boolean isDialogAlreadyDisplayImage = false;
    protected boolean isInBackground = false;
    protected boolean isUpdated = false;
    private boolean isFromCrop = false;
    private Context mContext;

    public interface CallbackImageCompress {
        void onImageCompressSuccess(String imageFilePath, Uri imageUri, int cameraPhotoOrientation);

        void onImageCompressFailed(String errorMessage);
    }

    public interface CallbackImageListener {
        void onImageSuccess(String imageFilePath, Uri imageUri, int cameraPhotoOrientation);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isInBackground = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isInBackground = true;
    }

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isGoToSettingImage = false;
        isDialogAlreadyDisplayImage = false;
    }


    @Override
    public void openImageChooser(CallbackImageListener callbackImageListener, boolean isOpenCamera, boolean isRemoveImage, boolean isRemoveImgDisplay, boolean isRemoveOptionDisplay, Context mContext) {
        this.mContext = mContext;
        isDialogAlreadyDisplayImage = false;
        isGoToSettingImage = false;
        this.callbackImageListener = callbackImageListener;
        if (isRemoveImgDisplay) {
            isRemoveImage = loginData.getUser().getProfilePicture().contains("default.png");
        }
        Log.e("isRemoveImage", "" + isRemoveImage);
        if (isOpenCamera) {
            captureImage(mContext);
            return;
        }
        new ImageChooserDialog(mContext, new CallbackClickListner() {
            @Override
            public void onClickListener(View view, int position) {
                Log.w("selectedItem", "" + position);
                if (position == 0) {
                    selectedOption = 0;
                   /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!Environment.isExternalStorageManager()) {
                            Intent i = new Intent();
                            i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                            //  startActivity(i);
                            startActivityForResult(i, 20);
                        } else {
                            captureImage();
                        }
                    } else {*/
                    captureImage(mContext);
                    //}

                } else if (position == 1) {
                    selectedOption = 1;
                   /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!Environment.isExternalStorageManager()) {
                            Intent i = new Intent();
                            i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                            startActivityForResult(i, 20);
                            // startActivity(i);
                        } else {
                            openGalleryPermission();
                        }
                    } else {*/
                    openGalleryPermission(mContext);

                    //}

                } else if (position == 2) {
                    selectedOption = 2;
                    if (callbackImageListener != null) {
                        callbackImageListener.onImageSuccess(null,
                                null,
                                -1);
                    }
                } else if (position == 3) {
                    String defaultImage = application.getSharedPref().getDefaultImageData(Constant.DEFAULT_ITEM_IMAGE);
                    //No Image option selected
                    if (callbackImageListener != null) {
                        callbackImageListener.onImageSuccess(defaultImage,
                                null,
                                -1);
                    }
                }
            }
        }, !isRemoveImage, isRemoveOptionDisplay);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("imageActivityResSetting", "..." + isGoToSettingImage + " selectedOption => " + selectedOption + "    getIntent()  => " + getIntent());
        if (isGoToSettingImage) {

            isGoToSettingImage = false;
            if (selectedOption == 0) {
                captureImage(mContext);
            } else if (selectedOption == 1) {
                openGalleryPermission(mContext);
            }


        } else {

            /*else  if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.R) {
            isGoToSettingImage = false;
            if (selectedOption == 0) {
                captureImage();
            } else if (selectedOption == 1) {
                openGalleryPermission();
            }
        }*/
        }
    }

    public void captureImage(Context context) {
        Log.e("captureImage", "111...");
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            ActivityCompat.requestPermissions(this, new
                    String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.MANAGE_EXTERNAL_STORAGE}, Constant.REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
        } else {*/

        int permissionCamera = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        int permissionReadExternal = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWriteExternal = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Permission List
        List<String> listPermissionsNeeded = new ArrayList<>();

        // Camera Permission
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                Toast.makeText(activity, "Camera Permission is required for this app to run", Toast.LENGTH_SHORT)
                        .show();
            }
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        // Read/Write Permission
        if (permissionWriteExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        // Location Permission
        if (permissionReadExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    Constant.REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
        } else {
            takePicture();
        }

//        ActivityCompat.requestPermissions(this, new
//                String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Constant.REQUEST_STORAGE_WRITE_ACCESS_PERMISSION);
        //}
    }

    protected void openGalleryPermission(Context context) {
        Log.e("openGalleryPermission", "111...");
     /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            ActivityCompat.requestPermissions(this, new
                    String[]{*//*Manifest.permission.WRITE_EXTERNAL_STORAGE,*//* Manifest.permission.MANAGE_EXTERNAL_STORAGE}, Constant.REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {*/

        int permissionReadExternal = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionWriteExternal = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        // Permission List
        List<String> listPermissionsNeeded = new ArrayList<>();

        // Read/Write Permission
        if (permissionWriteExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        // Location Permission
        if (permissionReadExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        Log.w("listPermissionsNeeded", "" + listPermissionsNeeded);
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Log.w("openGalleyCalled", "Line 264");
            openGallery();
        }

//        ActivityCompat.requestPermissions(this, new
//                String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        //}
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPermissionAllow = true;
        Log.e("isPermissionImageAct", ".....");
        //  if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {

        try {

            Log.w("permissions", "permissions => " + permissions.length);
            for (int i = 0; i < permissions.length; i++) {
                Log.e("isPermissionGranted", "permission => " + permissions[i] + "    Result => " + grantResults[i]);
            }
            for (int grantResult : grantResults) {
                Log.e("isPermissionGranted", "" + (grantResult != PackageManager.PERMISSION_GRANTED));
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    isPermissionAllow = false;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  }
        Log.e("isPermissionGranted", "....." + requestCode);
        Log.e("isPermissionGranted", "....." + isPermissionAllow);
        switch (requestCode) {
            case Constant.REQUEST_STORAGE_WRITE_ACCESS_PERMISSION:

                if (isPermissionAllow) {
                    takePicture();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!isDialogAlreadyDisplayImage) {
                            isDialogAlreadyDisplayImage = true;
                            isGoToSettingImage = true;
                            dialogCommon.showDialog("", "You need to allow permission to add item.", "Go to setting", getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    goToSettings();
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });
                        }

                    }
                    Toast.makeText(this, "Permission Denied !", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                Log.e("READ_ACCESS_PERMISSION", "" + REQUEST_STORAGE_READ_ACCESS_PERMISSION);
                Log.e("isPermissionAllow", "" + isPermissionAllow);
                if (isPermissionAllow) {
                    Log.w("isPermissionAllow", "" + (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent));
                    if (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent) {
                        Log.w("openGalleyCalled", "Line 333");
                        openGallery();
                    } else {
                        Log.w("openGalleyCalled", "Line 336");
                        openGallery();
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (!isDialogAlreadyDisplayImage) {
                            isDialogAlreadyDisplayImage = true;
                            isGoToSettingImage = true;
                            dialogCommon.showDialog("", "You need to allow permission to add item.", "Go to setting", getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                                @Override
                                public void OnDialogPositiveBtn() {
                                    goToSettings();
                                }

                                @Override
                                public void OnDialogNegativeBtn() {

                                }
                            });

                        }
                    }
                    Toast.makeText(this, "Permission Denied !", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                Log.e("MULTIPLE_PERMISSIONS", "" + REQUEST_ID_MULTIPLE_PERMISSIONS + " ====   " + (!CreateItemActivity.activity.isShareIntent) + " isPermissionAllow => " + isPermissionAllow);
                if (isPermissionAllow) {
                    Log.w("isPermissionAllow", "" + (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent));
                    if (CreateItemActivity.activity != null && !CreateItemActivity.activity.isShareIntent) {
                        Log.w("openGalleyCalled", "Line 365");
                        openGallery();
                    }
                } else {
                    Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void goToSettings() {
       /* Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivityForResult(myAppSettings, REQUEST_APP_SETTINGS);*/
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }


    protected File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = new File(Environment.getExternalStorageDirectory() +
                File.separator +
                getString(R.string.app_name)
        );

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        imageFilePath = image.getAbsolutePath();
        Log.w("imageFilePath", "" + imageFilePath);

        return image;
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = null;
        try {
            photo = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(this,
                getPackageName() + ".provider", photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, Constant.CAMERA_RESULT);

        /*Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, PHOTO_REQUEST);*/
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constant.OPEN_GALLERY);
    }

    protected void launchMediaScanIntent() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        this.sendBroadcast(mediaScanIntent);
    }

    protected String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    protected int getCameraPhotoOrientation(String imageFilePath) {
        Log.w("InsideExifInterface", "getCameraPhotoOrientation....");
        int rotate = 0;
        try {

            ExifInterface exif;

            exif = new ExifInterface(imageFilePath);
            String exifOrientation = exif
                    .getAttribute(ExifInterface.TAG_ORIENTATION);
            Log.d("exifOrientation", exifOrientation);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rotate;
    }

    public void compressImage(Uri uri) {
        Log.w("compressImage", "inside compress image...");
        Bitmap bitmap = null;
        try {
            bitmap = getThumbnail(uri);
            saveImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Bitmap convertGrayScale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    protected void saveImageBitmap(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        //File file = new File (myDir, fname);
        File file = null;
        try {
            file = createImageFile();
        } catch (Exception e) {
            Log.w("createImagefile", "" + e.getMessage());
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(this,
                getPackageName() + ".provider", file);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            //File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            File file = /*new File(Environment.getExternalStorageDirectory().toString() + "/Gator Lotto")*/createImageFile();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            return FileProvider.getUriForFile(this,
                    getPackageName() + ".provider", file);
            //bmpUri = Uri.fromFile(file);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("hereOnResult", "ImageActivity   1111");
        if ((requestCode == Constant.CAMERA_RESULT || requestCode == Constant.OPEN_GALLERY) && resultCode == Activity.RESULT_OK) {
            Uri sourceUri = imageUri;
            if (requestCode == Constant.OPEN_GALLERY) {
                imageUri = data.getData();
                imageFilePath = RealPathUtil.getRealPath(this, imageUri);
                if (imageFilePath == null) {
                    return;
                }
            }

            compressImage(imageUri);
            launchMediaScanIntent();
            if (callbackImageListener != null) {
                callbackImageListener.onImageSuccess(imageFilePath, imageUri, getCameraPhotoOrientation(imageFilePath));
            }

        } else if (requestCode == 20) {
            if (!Environment.isExternalStorageManager()) {
                Intent i = new Intent();
                i.setAction(ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(i, 20);
            } else {
                Log.e("selectedOption", "" + selectedOption);
                if (selectedOption == 0) {
                    captureImage(mContext);
                } else if (selectedOption == 1) {
                    openGalleryPermission(mContext);
                }
            }

        }
    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);

    }

    protected Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {
        if (imageFilePath == null)
            imageFilePath = uri.toString();
        Bitmap bitmapImage = BitmapFactory.decodeFile(String.valueOf(imageFilePath));
        bitmapImage = rotatedImage(bitmapImage);
        bitmapImage = getResizedBitmap(bitmapImage, 800);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapImage.compress(Bitmap.CompressFormat.PNG, 10, stream);
        return bitmapImage;
    }

    protected Bitmap rotatedImage(Bitmap bitmap) {
        Log.w("InsideExifInterface", "rotatedImage....");
        ExifInterface ei = null;
        Bitmap rotatedBitmap = bitmap;
        try {
            ei = new ExifInterface(imageFilePath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            Log.w("orientation", "" + orientation);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    rotatedBitmap = bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }
}
