package com.app.mylineup.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.FaqAdapter;
import com.app.mylineup.databinding.ActivityFaqBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.faq.Faq;
import com.app.mylineup.pojo.faq.FaqData;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaqActivity extends BaseActivity {
    private ActivityFaqBinding binding;
    private FaqAdapter faqAdapter;
    private List<Faq> list = new ArrayList<>();

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_faq);
        init();
    }

    private void init() {
        setHeader(getString(R.string.lbl_faq), null, null);

        faqAdapter = new FaqAdapter(activity, list);
        binding.rvFaq.setLayoutManager(new LinearLayoutManager(activity));
        binding.rvFaq.setAdapter(faqAdapter);

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            callFaqApi();

    }

    private void callFaqApi() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getFaq();
        Log.e("TAG", "url = " + call.request().url());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                Log.e("TAG", "code = " + response.code());
                Log.e("TAG", "msg = " + response.message());
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    FaqData faqData = gson.fromJson(x, FaqData.class);
                    if (faqData != null && faqData.getFaq() != null && faqData.getFaq().size() > 0) {
                        list.addAll(faqData.getFaq());
                        faqAdapter.notifyDataSetChanged();
                        binding.llNoData.setVisibility(View.GONE);
                    } else {
                        binding.llNoData.setVisibility(View.VISIBLE);
//                        binding.ivNoData.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {

            }
        });
    }
}
