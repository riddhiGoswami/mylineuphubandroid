package com.app.mylineup.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alphabetik.Alphabetik;
import com.app.mylineup.BuildConfig;
import com.app.mylineup.R;
import com.app.mylineup.adapter.ContactListAdapter;
import com.app.mylineup.databinding.ActivityContactBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.ContactChild;
import com.app.mylineup.pojo.ContactHeader;
import com.app.mylineup.pojo.contactList.ContactList;
import com.app.mylineup.pojo.myProfile.MyProfile;
import com.app.mylineup.pojo.userProfile.CheckConnected;
import com.app.mylineup.web_services.PrettyPrinter;
import com.github.tamir7.contacts.Contact;
import com.github.tamir7.contacts.Contacts;
import com.github.tamir7.contacts.Query;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;
import jagerfield.mobilecontactslibrary.ImportContacts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactListActivity extends BaseActivity implements ContactListAdapter.OnActionListener
{
    private ActivityContactBinding binding;
    private List<com.app.mylineup.interfaces.Contact> contactList = new ArrayList<>();
    private List<com.app.mylineup.interfaces.Contact> tempList = new ArrayList<>();
    private ContactListAdapter contactListAdapter;
    private LinearLayoutManager layoutManager;

    private ImportContacts importContacts;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact);
        Contacts.initialize(this);
        importContacts = new ImportContacts(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);
    }

    private void init()
    {
        setHeader(getString(R.string.contact_list),null,null);

        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, Constant.REQUEST_READ_CONTACT_PERMISSION);

        String[] customAlphabet = getResources().getStringArray(R.array.my_string_abcd);
        binding.alphSectionIndex.setAlphabet(customAlphabet);
        binding.alphSectionIndex.onSectionIndexClickListener(new Alphabetik.SectionIndexClickListener() {
            @Override
            public void onItemClick(View view, int position, String character) {
                String info = " Position = " + position + " Char = " + character;
                Log.e("View: ", view + "," + info);
                if(layoutManager != null){
                    layoutManager.setSmoothScrollbarEnabled(true);
                    layoutManager.scrollToPositionWithOffset(getPositionFromData(character), 0);
                }
                //binding.rvContact.smoothScrollToPosition(getPositionFromData(character));
            }
        });

        binding.etSearchContact.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                findSearchResult(s.toString());
            }
        });
    }

    private void findSearchResult(String search)
    {
        if(contactListAdapter == null || contactList == null || contactList.size() == 0){
            return;
        }
        if(!search.equalsIgnoreCase(""))
        {
            tempList.clear();
            contactListAdapter.notifyDataSetChanged();

            for(int i = 0;i<contactList.size();i++)
            {
                if(contactList.get(i).isSection() && (contactList.get(i).getDisplayName().toLowerCase().contains(search.toLowerCase())))
                {
                    tempList.add(contactList.get(i));
                }
            }

            contactListAdapter.notifyDataSetChanged();
        }
        else
        {
            tempList.clear();
            tempList.addAll(contactList);
            contactListAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Constant.REQUEST_READ_CONTACT_PERMISSION ) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED){
                Query q = Contacts.getQuery();
                q.hasPhoneNumber();
                List<Contact> contacts = q.find();
                Log.e("TAG","contacts.size() = "+contacts.size());
                setList(contacts);
                //setList1(contacts);

                //ArrayList<jagerfield.mobilecontactslibrary.Contact.Contact> listItem = importContacts.getContacts();
                //setList(listItem);
            }
            else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS}, Constant.REQUEST_READ_CONTACT_PERMISSION);
            }
        }
        else {
            dialogCommon.showDialog(getString(R.string.please_allow_permissions));
        }
    }

    private void setList(ArrayList<jagerfield.mobilecontactslibrary.Contact.Contact> listItem) {

        PhoneNumberUtil phoneNumberUtil = null;
        phoneNumberUtil = PhoneNumberUtil.createInstance(this);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0;i<listItem.size();i++) {
            if(listItem.get(i).getNumbers().size()>0 && listItem.get(i).getNumbers().get(0).getNormalizedNumber() != null
                    && !listItem.get(i).getNumbers().get(0).getNormalizedNumber().equalsIgnoreCase(""))
            {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name",listItem.get(i).getDisplaydName());
                    //jsonObject.put("phone",contacts.get(i).getPhoneNumbers().size()>0 ? contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber() : "");
                    jsonObject.put("phone",getCountryIsoCode(listItem.get(i).getDisplaydName(),listItem.get(i).getNumbers().size()>0 ? listItem.get(i).getNumbers().get(0).getNormalizedNumber() : "",phoneNumberUtil));
                    jsonArray.put(jsonObject);
                }catch (Exception e){
                    Log.e("TAG","Json creator error = "+e.getLocalizedMessage());
                }
            }
        }
        Log.e("TAG","Json = "+jsonArray.toString());
        if(jsonArray.length() > 0){
            //callGiveApi(jsonArray.toString());
        }
    }

    private void setList1(List<Contact> contacts) {

        List<String> myArrayList = Arrays.asList(getResources().getStringArray(R.array.my_string_abcd));
        for(int i = 0;i<myArrayList.size();i++)
        {
            contactList.add(new ContactHeader(myArrayList.get(i)));
            for(int j = 0;j<contacts.size();j++)
            {
                if(contacts.get(j).getDisplayName().toLowerCase().startsWith(myArrayList.get(i).toLowerCase()))
                {
                    contactList.add(new ContactChild(contacts.get(j).getId()+"",
                            contacts.get(j).getDisplayName(),
                            contacts.get(j).getGivenName(),
                            contacts.get(j).getFamilyName(),
                            contacts.get(j).getPhoneNumbers().size()>0 ? contacts.get(j).getPhoneNumbers().get(0).getNormalizedNumber() : "",
                            contacts.get(j).getEmails().size()>0 ? contacts.get(j).getEmails().get(0).getAddress() : "",
                            contacts.get(j).getPhotoUri(),""));
                }
            }
        }

        tempList.addAll(contactList);
        Log.e("TAG","contacts.size() = "+ contactList.size());

        contactListAdapter = new ContactListAdapter(activity,tempList,this,dialogCommon);
        layoutManager = new LinearLayoutManager(activity);
        binding.rvContact.setLayoutManager(layoutManager);
        binding.rvContact.setAdapter(contactListAdapter);
    }

    private void setList(List<Contact> contacts)
    {
        PhoneNumberUtil phoneNumberUtil = null;
        phoneNumberUtil = PhoneNumberUtil.createInstance(this);

        JSONArray jsonArray = new JSONArray();
        for(int i = 0;i<contacts.size();i++) {
            /*if(contacts.get(i).getPhoneNumbers().size()>0 && contacts.get(i).getPhoneNumbers().get(0).getNumber() != null && !contacts.get(i).getPhoneNumbers().get(0).getNumber().equalsIgnoreCase(""))
            {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name",contacts.get(i).getDisplayName());
                    //jsonObject.put("phone",contacts.get(i).getPhoneNumbers().size()>0 ? contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber() : "");
                    //jsonObject.put("phone",getCountryIsoCode(contacts.get(i).getDisplayName(),contacts.get(i).getPhoneNumbers().size()>0 ? contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber() : "",phoneNumberUtil));//most phone working
                    //jsonObject.put("phone",contacts.get(i).getPhoneNumbers().get(0).getNumber());
                    //jsonObject.put("phone",contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber() == null ? contacts.get(i).getPhoneNumbers().get(0).getNumber() : getCountryIsoCode(contacts.get(i).getDisplayName(),contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber(),phoneNumberUtil));
                    jsonObject.put("phone",contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber() == null ? contacts.get(i).getPhoneNumbers().get(0).getNumber() : contacts.get(i).getPhoneNumbers().get(0).getNormalizedNumber()); //work in kajal
                    jsonArray.put(jsonObject);
                }catch (Exception e){
                    Log.e("TAG","Json creator error = "+e.getLocalizedMessage());
                }
            }*/
            String myNumber="";
            if(loginData.getUser().getPhone() != null && !loginData.getUser().getPhone().equalsIgnoreCase("")){
                myNumber = loginData.getUser().getPhone().replaceAll("[() -]","");
            }
            if(contacts.get(i).getPhoneNumbers().size()>0 && contacts.get(i).getPhoneNumbers().get(0).getNumber() != null && !contacts.get(i).getPhoneNumbers().get(0).getNumber().equalsIgnoreCase(""))
            {
                try {
                    String number = contacts.get(i).getPhoneNumbers().get(0).getNumber();
                    if(number.contains("+1")){
                        number = number.replace("+1","");
                    }else if(number.contains("+91")){
                        number = number.replace("+91","");
                    }
                    number = number.replace("-","").replace(")","").replace("(","").replace(" ","");
                    if(!number.equalsIgnoreCase(myNumber)){
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("name",contacts.get(i).getDisplayName());
                        jsonObject.put("phone",number);
                        jsonArray.put(jsonObject);
                    }
                }catch (Exception e){
                    Log.e("TAG","Json creator error = "+e.getLocalizedMessage());
                }
            }
        }
        Log.e("TAG","Json = "+jsonArray.toString());
        if(jsonArray.length() > 0){
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            callGiveApi(jsonArray.toString());
        }
    }


    private int getPositionFromData(String character) {
        for(int i = 0;i<contactList.size();i++)
        {

            //if(contactList.get(i).isSection() && (contactList.get(i).getDisplayName().charAt(0)+"").equalsIgnoreCase("" + character))
            if(!contactList.get(i).isSection() && (contactList.get(i).getHeader().charAt(0)+"").equalsIgnoreCase("" + character))
            {
                return i;
            }
        }
        return 0;
    }

    private String getCountryIsoCode(String name,String number, PhoneNumberUtil phoneNumberUtil) {
        Log.e("TAG1","CODE1 = "+name);
    if (number.startsWith("+")) {
        number = number;
    }else {
        number = "+"+number;
    }

        Phonenumber.PhoneNumber phoneNumber = null;
    try {
        phoneNumber = phoneNumberUtil.parse(number, null);
    } catch (NumberParseException e) {
        Log.e("TAG1", "error during parsing a number");
    }
    if(phoneNumber == null) return number;

        return number.replace("+"+phoneNumber.getCountryCode(),"");
    //Log.e("TAG1","CODE = "+phoneNumberUtil.getRegionCodeForCountryCode(phoneNumber.getCountryCode()));
}

    private void callGiveApi(String contactJson)
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().findFriends(loginData.getUser().getId(),contactJson);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                Log.e("TAG","response code = "+response.code());
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        ContactList contactList = gson.fromJson(x,ContactList.class);
                        if(contactList.getContacts() != null && contactList.getContacts().size()>0){
                            setAdapter(contactList.getContacts());
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.e("TAG","Error = "+e.getLocalizedMessage());
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setAdapter(List<com.app.mylineup.pojo.contactList.Contact> contacts) {

        List<String> myArrayList = Arrays.asList(getResources().getStringArray(R.array.my_string_abcd));
        for(int i = 0;i<myArrayList.size();i++)
        {
            contactList.add(new ContactHeader(myArrayList.get(i)));
            for(int j = 0;j<contacts.size();j++)
            {
                if(contacts.get(j).getName().toLowerCase().startsWith(myArrayList.get(i).toLowerCase()))
                {
                    contactList.add(new ContactChild(contacts.get(j).getId(),
                            contacts.get(j).getName(),
                            "",
                            "",
                            contacts.get(j).getPhone(),
                            "",
                            contacts.get(j).getProfilePicture(),
                            contacts.get(j).getButtonType()));
                }
            }
        }


        /*for(int i = 0;i<myArrayList.size();i++)
        {
            contactList.add(new ContactHeader(myArrayList.get(i)));
            for(int j = 0;j<contacts.size();j++)
            {
                if(contacts.get(j).getDisplayName().toLowerCase().startsWith(myArrayList.get(i).toLowerCase()))
                {
                    contactList.add(new ContactChild(contacts.get(j).getDisplayName(),
                            contacts.get(j).getGivenName(),
                            contacts.get(j).getFamilyName(),
                            contacts.get(j).getPhoneNumbers().size()>0 ? contacts.get(j).getPhoneNumbers().get(0).getNormalizedNumber() : "",
                            contacts.get(j).getEmails().size()>0 ? contacts.get(j).getEmails().get(0).getAddress() : "",
                            contacts.get(j).getPhotoUri()));
                }
            }
        }*/

        tempList.addAll(contactList);
        Log.e("TAG","contacts.size() = "+ contactList.size());

        contactListAdapter = new ContactListAdapter(activity,tempList,this,dialogCommon);
        layoutManager = new LinearLayoutManager(activity);
        binding.rvContact.setLayoutManager(layoutManager);
        binding.rvContact.setAdapter(contactListAdapter);
    }

    @Override
    public void onPerformAction(int type, int position, com.app.mylineup.interfaces.Contact contact) {
        //0-Invite,1-Connect,2-Friend,3-Accept request,4-Reject request,5-Request sent

        if(type == 0){
            //invite

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey there, check out MyLineup Hub app at: \nGoogle Play Store: \nhttps://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID+" \n\n" +
                            "Apple App Store: \nitms-apps://itunes.apple.com/app/bars/id1495617032");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        else if(type == 1){
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            performRequest("0",position);
        }
        else if(type == 2){
            //open profile
            /*Intent intent = new Intent(activity, UserProfileActivity.class);
            intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
            intent.putExtra(Constant.USER_ID,tempList.get(position).getId());
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/
            getCheckConnectedApi(tempList.get(position).getId());
        }
        else if(type == 3){
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            performRequest("1",position);//accept
        }
        else if(type == 4){
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            performRequest("2",position);//reject
        }
        else if(type == 5){
            //open profile

          /*  Intent intent = new Intent(activity, UserProfileActivity.class);
            intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
            intent.putExtra(Constant.USER_ID,tempList.get(position).getId());
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);*/
            getCheckConnectedApi(tempList.get(position).getId());
        }
    }

    private void performRequest(String requestType,int pos) {

        loaderDialog.show();
        Call<Object> call = application.getApis().friendRequest(loginData.getUser().getId(),tempList.get(pos).getId(),requestType);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        if(requestType.equalsIgnoreCase("0")){
                            tempList.get(pos).setType("4");
                            contactList.get(pos).setType("4");
                        }
                        else if(requestType.equalsIgnoreCase("1")){
                            tempList.get(pos).setType("2");
                            contactList.get(pos).setType("2");
                        }else {
                            tempList.get(pos).setType("1");
                            contactList.get(pos).setType("1");
                        }

                        contactListAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    public void getCheckConnectedApi(String userId) {

        loaderDialog.show();

        Call<Object> call = application.getApis().checkConnected(userId,loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                loaderDialog.dismiss();
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                try {
                    if(x != null){
                        JSONObject jsonObject = new JSONObject(x);
                        if(jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            CheckConnected checkConnected = gson.fromJson(x, CheckConnected.class);
                            if(checkConnected.getStatus()){
                                Intent intent = new Intent(activity, UserProfileActivity.class);
                                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                                intent.putExtra(Constant.USER_ID,checkConnected.getProfile_user_id());
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                        }
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }
}
