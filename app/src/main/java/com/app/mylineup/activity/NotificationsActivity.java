package com.app.mylineup.activity;

import android.os.Bundle;
import android.util.Log;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityNotificationBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity
{
    private ActivityNotificationBinding binding;

    @Override
    protected String getCallbackTag() {
        return "";
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.notifications),null,null);

        if(loginData.getNotificationSetting().getNewConnections() != 0) {
            binding.switchNewConnection.setChecked(true);
        }
        if (loginData.getNotificationSetting().getWeeklyUpdates() != 0) {
            binding.switchWeeklyUpdate.setChecked(true);
        }
        if (loginData.getNotificationSetting().getItemPurchased() != 0) {
            binding.switchPurchasedFromWishlist.setChecked(true);
        }
        if (loginData.getNotificationSetting().getAppNotification() != 0) {
            binding.switchNotification.setChecked(true);
        }

        binding.llNewConnection.setOnClickListener(view -> {
            binding.switchNewConnection.setChecked(!binding.switchNewConnection.isChecked());
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            callUpdateNotificationApi("new_connections",binding.switchNewConnection.isChecked() ? 1 : 0 );
        });
        binding.llWeeklyUpdate.setOnClickListener(view -> {
            binding.switchWeeklyUpdate.setChecked(!binding.switchWeeklyUpdate.isChecked());
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            callUpdateNotificationApi("weekly_updates",binding.switchWeeklyUpdate.isChecked() ? 1 : 0);
        });
        binding.llPurchasedFromWishlist.setOnClickListener(view -> {
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            binding.switchPurchasedFromWishlist.setChecked(!binding.switchPurchasedFromWishlist.isChecked());
            callUpdateNotificationApi("item_purchased",binding.switchPurchasedFromWishlist.isChecked() ? 1 : 0);
        });
        binding.llNotification.setOnClickListener(view -> {
            binding.switchNotification.setChecked(!binding.switchNotification.isChecked());
            String status = NetworkUtil.getConnectivityStatusString(activity);
            if (status.equalsIgnoreCase("Not connected to Internet"))
                dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
            else
            callUpdateNotificationApi("app_notification",binding.switchNotification.isChecked() ? 1 : 0);
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.enter_from_left,R.anim.exit_to_right);

    }

    private void callUpdateNotificationApi(String key, int status)
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().updateNotificationSetting(loginData.getUser().getId(),key,status+"");
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try
                {
                    if(key.equalsIgnoreCase("app_notification")){
                        binding.switchNewConnection.setChecked((status == 0)?false:true);
                        binding.switchWeeklyUpdate.setChecked((status == 0)?false:true);
                        binding.switchPurchasedFromWishlist.setChecked((status == 0)?false:true);
                        binding.switchNotification.setChecked((status == 0)?false:true);
                    }

                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        if(jsonObject.has("notification_setting")){
                            JSONObject object = jsonObject.getJSONObject("notification_setting");

                            if(object.has("new_connections"))
                            {loginData.getNotificationSetting().setNewConnections(object.getInt("new_connections"));}
                            if(object.has("weekly_updates"))
                            {loginData.getNotificationSetting().setWeeklyUpdates(object.getInt("weekly_updates"));}
                            if(object.has("item_purchased"))
                            {loginData.getNotificationSetting().setItemPurchased(object.getInt("item_purchased"));}
                            if(object.has("app_notification"))
                            {loginData.getNotificationSetting().setAppNotification(object.getInt("app_notification"));}

                            Log.e("TAG","login data = "+gson.toJson(loginData));
                            application.getSharedPref().saveSession(Constant.LOGIN_DATA,gson.toJson(loginData));
                        }
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                        if(key.equalsIgnoreCase("new_connections"))
                        {binding.switchNewConnection.setChecked(!binding.switchNewConnection.isChecked());}
                        else if(key.equalsIgnoreCase("weekly_updates"))
                        {binding.switchWeeklyUpdate.setChecked(!binding.switchWeeklyUpdate.isChecked());}
                        else if(key.equalsIgnoreCase("item_purchased"))
                        {binding.switchPurchasedFromWishlist.setChecked(!binding.switchPurchasedFromWishlist.isChecked());}
                        else if(key.equalsIgnoreCase("app_notification"))
                        {binding.switchNotification.setChecked(!binding.switchNotification.isChecked());}
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
