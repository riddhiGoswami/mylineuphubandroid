package com.app.mylineup.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.TabAdapter;
import com.app.mylineup.databinding.ActivityUserProfileBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.CustomVideoDialog;
import com.app.mylineup.dialog.VideoViewDialog;
import com.app.mylineup.fragment.AboutMeFragment;
import com.app.mylineup.fragment.ActivityFragment;
import com.app.mylineup.fragment.CreateWishlistFragOne;
import com.app.mylineup.fragment.LittleOnesFragment;
import com.app.mylineup.fragment.UserWishlistFragment;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.NetworkUtil;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.userProfile.MyLittleOne;
import com.app.mylineup.pojo.userProfile.UserProfile;
import com.app.mylineup.singleton.LittleOnesGlobalCallbackSingleton;
import com.app.mylineup.web_services.PrettyPrinter;
import com.google.android.youtube.player.YouTubePlayer;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends ImageActivity implements BaseActivity.onHeaderAction/*, VideoViewDialog.OnSkipClickListener*/, UserWishlistFragment.ChangeProfileCallback {

    static final String CALLBACK_TAG = UserProfileActivity.class.getSimpleName();
    private ActivityUserProfileBinding binding;

    private TabAdapter adapter;
    private boolean isMyAcc = true;
    private String userId = "";

    private MyProfileChange myProfileChange;
    public static boolean isFromWishlist = false;
    public static boolean isFromUserProfile = false;
    private static VideoViewDialog videoViewDialog;
    private boolean isNewAdd;
    private String TAG = UserProfileActivity.class.getSimpleName();
    private boolean isUpdated = false;
    private boolean isInBackground = false;

    @Override
    protected String getCallbackTag() {
        return CALLBACK_TAG;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_user_profile);
        isFromUserProfile = true;
        init();
    }

    private void init() {
        if (getIntent().getStringExtra(Constant.FROM) != null) {
            if (getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_USER_PROFILE)) {
                binding.llSelectImg.setVisibility(View.GONE);
                isMyAcc = false;

                setHeader("User Profile", null, R.drawable.ic_dots);
                setInterFace(this::onClick);
                if (getIntent().hasExtra(Constant.USER_ID)) {
                    userId = getIntent().getStringExtra(Constant.USER_ID);
                }
            } else if (getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_LITTLE_ONE)) {
                binding.llSelectImg.setVisibility(View.GONE);
                isMyAcc = false;

                if (getIntent().hasExtra(Constant.USER_NAME)) {
                    String userName = getIntent().getStringExtra(Constant.USER_NAME) + "'s Little One";
                    setHeader(userName, null, R.drawable.ic_home);
                }

                setInterFace(this::onClick);
                if (getIntent().hasExtra(Constant.LITTLE_ONE_ID)) {
                    userId = getIntent().getStringExtra(Constant.LITTLE_ONE_ID);
                }
            } else {
                setHeader(getString(R.string.my_profile), null, R.drawable.ic_edit_icon);
                setInterFace(this::onClick);
                userId = loginData.getUser().getId();
            }
        } else {
            setHeader(getString(R.string.my_profile), null, R.drawable.ic_edit_icon);
            setInterFace(this::onClick);
            userId = loginData.getUser().getId();
        }


        registerReceiver1();

        binding.llSelectImg.setOnClickListener(v ->
        {
            /*callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                binding.ivImage.setImageURI(imageUri);
                //this.imageFilePth = imageFilePath;
            },false);*/
        });

        String status = NetworkUtil.getConnectivityStatusString(activity);
        if (status.equalsIgnoreCase("Not connected to Internet"))
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
        else
            getUserProfile();
    }

    public void getUserProfile() {

        loaderDialog.show();
        Call<Object> call = application.getApis().getUserProfile(userId);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                try {
                    if (x != null) {
                        JSONObject jsonObject = new JSONObject(x);
                        if (jsonObject.has("status") && jsonObject.getBoolean("status")) {
                            UserProfile userProfile = gson.fromJson(x, UserProfile.class);
                            setValue(userProfile);
                        } else {
                            dialogCommon.showDialog(jsonObject.getString("message"));
                        }
                    }
                    //{"profile":{"profile_picture":"https://www.mylineuphub.com/assets/images/default.png","name":"SAURAVKUMAR THAKKAR"},"status":false,"message":"No data found"}
                } catch (Exception e) {
                } finally {
                    loaderDialog.dismiss();
                    PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setValue(UserProfile userProfile) {

        if (Global.isValidString(userProfile.getProfile().getName())) {
            binding.tvUserName.setText(userProfile.getProfile().getName());
        }

        if (Global.isValidString(userProfile.getProfile().getProfilePicture())) {
            Picasso.get().load(userProfile.getProfile().getProfilePicture()).into(binding.ivImage);
        }


        adapter = new TabAdapter(getSupportFragmentManager());

        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setAdapter(adapter);

        UserWishlistFragment userWishlistFragment = new UserWishlistFragment(isMyAcc, binding.tabLayout, userProfile, this);
        adapter.addFragment(userWishlistFragment, getString(R.string.tab_wishlists));
        adapter.addFragment(new ActivityFragment(isMyAcc, userProfile), getString(R.string.tab_activity));
        adapter.addFragment(new AboutMeFragment(userProfile), getString(R.string.tab_about_me));

        List<MyLittleOne> myLittleOnes = userProfile.getMyLittleOnes();

        //if (isMyAcc && myLittleOnes != null && !myLittleOnes.isEmpty()) {
        //if it's current logged in user's account show this tab else dont
        LittleOnesGlobalCallbackSingleton.addListener(CALLBACK_TAG, new CallbackLittleOne() {
            @Override
            public void editLittleOne(String firstName, String lastName) {

            }

            @Override
            public void deleteLittleOne() {
                Log.e(TAG, "deleteLittleOne");
                getUserProfile();
            }

            @Override
            public void hideLittleOne() {

            }

            @Override
            public void addLittleOne(LittleOne littleOne) {

            }
        });
        adapter.addFragment(new LittleOnesFragment(userProfile), getString(R.string.tab_little_ones));
        //}

        adapter.notifyDataSetChanged();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*TabLayout.Tab tab = binding.tabLayout.getTabAt(0);
                tab.select();*/
                adapter.notifyDataSetChanged();
                userWishlistFragment.setCount();
            }
        }, 2000);

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    if (userProfile.getWishlist() != null && userProfile.getWishlist().size() > 0) {
                        binding.tabLayout.getTabAt(0).setText("Wishlists (" + userProfile.getWishlist().size() + ")");
                    } else {
                        binding.tabLayout.getTabAt(0).setText("Wishlists (0)");
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (userProfile.getWishlist() != null && userProfile.getWishlist().size() > 0) {
            binding.tabLayout.getTabAt(0).setText("Wishlists (" + userProfile.getWishlist().size() + ")");
        } else {
            binding.tabLayout.getTabAt(0).setText("Wishlists (0)");
        }
    }

    @Override
    public void onClick(int id) {
        if (R.id.ivRight2 == id) {
            Log.e("TAG", "Share");
        } else if (R.id.ivRight == id) {
            if (isMyAcc) {
                Log.e("TAG", "Edit");

                LittleOneSingleton.get(activity, new CallbackLittleOne() {
                    @Override
                    public void editLittleOne(String firstName, String lastName) {

                    }

                    @Override
                    public void deleteLittleOne() {
                    }

                    @Override
                    public void hideLittleOne() {
                    }

                    @Override
                    public void addLittleOne(LittleOne littleOne) {
                        isNewAdd = true;
                        Log.e("onAddLittleOne", "111");
                        String status = NetworkUtil.getConnectivityStatusString(activity);
                        if (status.equalsIgnoreCase("Not connected to Internet"))
                            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                        else
                            getUserProfile();
                    }
                });


                if (!application.getSharedPref().getVideoData(Constant.PROFILE_SEEN)) {

                    // if(!application.getSharedPref().getVideoData(Constant.PROFILE_SEEN)) {
                    application.getSharedPref().saveVideoData(Constant.PROFILE_SEEN, true);
                    // }

                    Intent intentI = new Intent(activity, EditProfilerActivity.class);
                    /*startActivityForResult(intentI, 10);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);*/

                    Intent intent = new Intent(this, CustomVideoDialog.class);
                    intent.putExtra("videoType", 4);
                    intent.putExtra("isSkipDisplay", true);
                    intent.putExtra("passIntent", intentI);
                    intent.putExtra("requestCode", 10);
                    //  intent.putExtra("onSkipListener", (Parcelable) this);
                    startActivity(intent);


                   /* videoViewDialog = new VideoViewDialog(4, this, true);
                    videoViewDialog.show(getSupportFragmentManager(), "Video Display Dialog");*/
                } else {
                    Intent intent = new Intent(activity, EditProfilerActivity.class);
                    startActivityForResult(intent, 10);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }

            } else {
                ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
                aboutMeBeen.add(new OnlyString(getString(R.string.remove_connection)));

                listDialog.showDialog("", true, getString(R.string.label_cancel), aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                    @Override
                    public void OnItemSelected(CommonPojo commonPojo, int position) {
                        if (commonPojo instanceof OnlyString) {
                            OnlyString been = (OnlyString) commonPojo;
                            Log.e("TAG", "item = " + been.getTitle());
                            if (been.getTitle().equalsIgnoreCase(getString(R.string.remove_connection))) {
                                // remove connection
                                dialogCommon.showDialog(
                                        getString(R.string.app_name),
                                        getString(R.string.disconnect_user_confirmation),
                                        getString(R.string.ok),
                                        getString(R.string.label_cancel),
                                        true,
                                        true,
                                        new AlertDialogCommon.CallBackClickListener() {
                                            @Override
                                            public void OnDialogPositiveBtn() {
                                                String status = NetworkUtil.getConnectivityStatusString(activity);
                                                if (status.equalsIgnoreCase("Not connected to Internet"))
                                                    dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.no_internet), getString(R.string.ok), "", true, false, null);
                                                else
                                                    callRemoveConnectionAPI();
                                            }

                                            @Override
                                            public void OnDialogNegativeBtn() {

                                            }
                                        }
                                );
                            }
                        }
                    }
                });
            }
        }
    }


    private void callRemoveConnectionAPI() {

        loaderDialog.show();
        Call<Object> call = application.getApis().removeConnection(loginData.getUser().getId(), userId);
        Log.e("TAG", "url = " + call.request().url());
        Log.e("TAG", "param = " + PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG", "response = " + x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(), dialogCommon, activity, application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if (jsonObject.has("status") && jsonObject.getBoolean("status")) {

                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                onBackPressed();
                                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                            }

                            @Override
                            public void OnDialogNegativeBtn() {
                            }
                        });
                    } else {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        application.setCurrentActivity(activity);
        if (isMyAcc) {
            if (isFromWishlist) {
                isFromWishlist = false;
                getUserProfile();
            } else
                setValue();
        } else {
            Log.w("MyConnectUserProfile", ".....");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.w("hereIsItemDetailResult", "111...");

        if (requestCode == 10 && resultCode == RESULT_OK) {
            getUserProfile();
        }

    }

    private void setValue() {
        loginData = getLoginData();
        if (Global.isValidString(loginData.getUser().getFirstName())) {
            binding.tvUserName.setText(loginData.getUser().getFirstName() + " " + loginData.getUser().getLastName());
        }

        if (Global.isValidString(loginData.getUser().getProfilePicture())) {
            Picasso.get().load(loginData.getUser().getProfilePicture()).into(binding.ivImage);
        }
    }

    private void registerReceiver1() {

        myProfileChange = new MyProfileChange();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.addChangeInWishList");
        registerReceiver(myProfileChange, filter);
    }

    @Override
    public void callProfile(String userId) {

    }

    /*@Override
    public void onSkipClick(int videoType) {

        if(!application.getSharedPref().getVideoData(Constant.PROFILE_SEEN)) {
            application.getSharedPref().saveVideoData(Constant.PROFILE_SEEN, true);
        }

        Intent intent = new Intent(activity, EditProfilerActivity.class);
        startActivityForResult(intent, 10);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
    }*/

    private class MyProfileChange extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            getUserProfile();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: ");
        isFromUserProfile = false;
        isFromWishlist = false;
        unregisterReceiver(myProfileChange);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        List<Fragment> fragList = getSupportFragmentManager().getFragments();
        for (int i = 0; i < fragList.size(); i++) {
            if (fragList.get(i) instanceof UserWishlistFragment) {
                Log.w("wishlistFragment", "CreateWishlistFragOne...");
                if (UserWishlistFragment.handleVideoDialog() != null) {
                    return;
                } else {
                    super.onBackPressed();
                    overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

                }
            } else {
                super.onBackPressed();
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

            }
        }

        if (videoViewDialog != null && handleVideoDialog() != null) {
            return;
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);

        }
    }

    public static YouTubePlayer handleVideoDialog() {
        return videoViewDialog.hideFullScreen();
    }
}
