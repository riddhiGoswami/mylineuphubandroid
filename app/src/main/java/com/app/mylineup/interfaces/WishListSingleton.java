package com.app.mylineup.interfaces;

import android.content.Context;

public class WishListSingleton {

    private static WishListSingleton instance;
    private static CallbackWishList callbackWishList;
    private Context context;

    public WishListSingleton(Context context, CallbackWishList callbackWishList)
    {
        this.context = context;
        WishListSingleton.callbackWishList = callbackWishList;
    }

    public static WishListSingleton get(Context context, CallbackWishList callbackWishList)
    {
        instance = getSync(context, callbackWishList);

        instance.context = context;
        return instance;
    }

    private static synchronized WishListSingleton getSync(Context context, CallbackWishList callbackWishList) {
        instance = new WishListSingleton(context, callbackWishList);
        return instance;
    }

    public static CallbackWishList getCallBack()
    {
        return callbackWishList;
    }
}
