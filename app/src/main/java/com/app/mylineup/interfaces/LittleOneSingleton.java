package com.app.mylineup.interfaces;

import android.content.Context;

import com.app.mylineup.adapter.ConnectionAdapter;

public class LittleOneSingleton
{
    private static LittleOneSingleton instance;
    private static CallbackLittleOne callbackLittleOne;
    private Context context;

    public LittleOneSingleton(Context context, CallbackLittleOne callbackLittleOne)
    {
        this.context = context;
        LittleOneSingleton.callbackLittleOne = callbackLittleOne;
    }

    public static LittleOneSingleton get(Context context,CallbackLittleOne callbackLittleOne)
    {
        instance = getSync(context, callbackLittleOne);

        instance.context = context;
        return instance;
    }

    private static synchronized LittleOneSingleton getSync(Context context, CallbackLittleOne callbackLittleOne) {
        instance = new LittleOneSingleton(context, callbackLittleOne);
        return instance;
    }

    public static CallbackLittleOne getCallBack()
    {
        return callbackLittleOne;
    }
}
