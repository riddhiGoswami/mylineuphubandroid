package com.app.mylineup.interfaces;

import android.content.Context;

public class WishListItemSingleton
{
    private static WishListItemSingleton instance;
    private static CallbackWishListItem callbackWishListItem;
    private Context context;

    public WishListItemSingleton(Context context, CallbackWishListItem callbackLittleOne)
    {
        this.context = context;
        WishListItemSingleton.callbackWishListItem = callbackLittleOne;
    }

    public static WishListItemSingleton get(Context context, CallbackWishListItem callbackLittleOne)
    {
        instance = getSync(context, callbackLittleOne);

        instance.context = context;
        return instance;
    }

    private static synchronized WishListItemSingleton getSync(Context context, CallbackWishListItem callbackLittleOne) {
        instance = new WishListItemSingleton(context, callbackLittleOne);
        return instance;
    }

    public static CallbackWishListItem getCallBack()
    {
        return callbackWishListItem;
    }
}
