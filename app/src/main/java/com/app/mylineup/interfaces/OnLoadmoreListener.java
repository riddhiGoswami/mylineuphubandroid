package com.app.mylineup.interfaces;

public interface OnLoadmoreListener
{
    public void onLoadMore();
}
