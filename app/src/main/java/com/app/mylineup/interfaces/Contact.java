package com.app.mylineup.interfaces;

public interface Contact {

    boolean isSection();

    ////header

    String getHeader();

    void setHeader(String header);



    //section
    String getDisplayName();

    void setDisplayName(String displayName);

    String getFirstName();

    void setFirstName(String firstName) ;

    String getLastName() ;

    void setLastName(String lastName);

    String getMobileNo();

    void setMobileNo(String mobileNo) ;

    String getEmail();

    void setEmail(String email);

    String getImage();

    void setImage(String image);

    String getType();

    void setType(String type);

    String getId();

    void setId(String id);
}
