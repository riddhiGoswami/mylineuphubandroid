package com.app.mylineup.interfaces;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.app.mylineup.activity.ImageActivity;

public interface CallbackImage {
    void openImageChooser(ImageActivity.CallbackImageListener callbackImageListener, boolean isCameraOpen, boolean isRemoveImage, boolean isRemoveImgDisplay, boolean isRemoveOptionDisplay, Context mContext);
    Uri getLocalBitmapUri(Bitmap bmp);
}
