package com.app.mylineup.interfaces;

public interface OnItemClickGiftListener {
        void onClick(String profileId);
}
