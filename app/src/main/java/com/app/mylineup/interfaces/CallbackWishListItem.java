package com.app.mylineup.interfaces;

import com.app.mylineup.pojo.myProfile.LittleOne;

public interface CallbackWishListItem
{
    //void purchaseWishListItem( boolean isPurchased);
    void purchaseWishListItem( boolean isPurchased, String itemId);

}
