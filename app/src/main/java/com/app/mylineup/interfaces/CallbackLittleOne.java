package com.app.mylineup.interfaces;

import android.os.Parcelable;

import com.app.mylineup.pojo.myProfile.LittleOne;

import java.io.Serializable;

public interface CallbackLittleOne extends Serializable
{
    void editLittleOne(String firstName,String lastName);
    void deleteLittleOne();
    void hideLittleOne();
    void addLittleOne(LittleOne littleOne);
}
