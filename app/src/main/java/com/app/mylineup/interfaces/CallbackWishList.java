package com.app.mylineup.interfaces;

import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.pojo.myProfile.LittleOne;

public interface CallbackWishList {

    void editWishlist(String name, String madeForID);
    void createItem(String wishlistId);
    void deleteWishlist();
    void changeLittleOne();
    //void addWishlist(WishlistBeen littleOne);
}
