package com.app.mylineup.pojo.wishlistItems;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WishlistItems {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("wishlist")
    @Expose
    private Wishlist_ wishlist;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Wishlist_ getWishlist() {
        return wishlist;
    }

    public void setWishlist(Wishlist_ wishlist) {
        this.wishlist = wishlist;
    }

}
