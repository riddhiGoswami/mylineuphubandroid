package com.app.mylineup.pojo.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wishlist {

    @SerializedName("wishlist_id")
    @Expose
    private String wishlistId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("header")
    @Expose
    private String header;
    @SerializedName("wishlist_name")
    @Expose
    private String wishlistName;
    @SerializedName("total_items")
    @Expose
    private Double totalItems;
    @SerializedName("event_expired")
    @Expose
    private Boolean eventExpired;
    @SerializedName("item_id_last")
    @Expose
    private String itemIdLast;

    public String getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(String wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getWishlistName() {
        return wishlistName;
    }

    public void setWishlistName(String wishlistName) {
        this.wishlistName = wishlistName;
    }

    public String getTotalItems() {
        if (totalItems != null)
            return String.valueOf(totalItems.intValue());
        else return "0";
    }

    public void setTotalItems(Double totalItems) {
        this.totalItems = totalItems;
    }

    public Boolean getEventExpired() {
        return eventExpired;
    }

    public void setEventExpired(Boolean eventExpired) {
        this.eventExpired = eventExpired;
    }

    public String getItemIdLast() {
        return itemIdLast;
    }

    public void setItemIdLast(String itemIdLast) {
        this.itemIdLast = itemIdLast;
    }

    public Wishlist(String wishlistId, String userId, String profilePicture, String header, String wishlistName, Double totalItems, Boolean eventExpired, String itemIdLast) {
        this.wishlistId = wishlistId;
        this.userId = userId;
        this.profilePicture = profilePicture;
        this.header = header;
        this.wishlistName = wishlistName;
        this.totalItems = totalItems;
        this.eventExpired = eventExpired;
        this.itemIdLast = itemIdLast;
    }

    public Wishlist() {
    }
}
