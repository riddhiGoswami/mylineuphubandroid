package com.app.mylineup.pojo.giftFeeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpcomingOccasion {

    @SerializedName("sort_date")
    @Expose
    private String sortDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("wishlist_id")
    @Expose
    private String wishlistId;
    @SerializedName("little_one_id")
    @Expose
    private String littleOneId;



    public String getSortDate() {
        return sortDate;
    }

    public void setSortDate(String sortDate) {
        this.sortDate = sortDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(String wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getLittleOneId() {
        return littleOneId;
    }

    public void setLittleOneId(String littleOneId) {
        this.littleOneId = littleOneId;
    }

    public UpcomingOccasion(String sortDate, String type, String image, String name, String date, String userId, String wishlistId, String littleOneId) {
        this.sortDate = sortDate;
        this.type = type;
        this.image = image;
        this.name = name;
        this.date = date;
        this.userId = userId;
        this.wishlistId = wishlistId;
        this.littleOneId = littleOneId;
    }

    public UpcomingOccasion() {
    }

    @Override
    public String toString() {
        return "UpcomingOccasion{" +
                "sortDate='" + sortDate + '\'' +
                ", type='" + type + '\'' +
                ", image='" + image + '\'' +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", userId='" + userId + '\'' +
                ", wishlistId='" + wishlistId + '\''
                +", LittleOneId='" + littleOneId + '\'' +
                '}';
    }
}
