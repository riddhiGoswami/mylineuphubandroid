
package com.app.mylineup.pojo.littleOnes.mylittleone;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LittleOneWishlist
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("wish_for")
    @Expose
    private String wishFor;
    @SerializedName("little_one_id")
    @Expose
    private String littleOneId;
    @SerializedName("total_items")
    @Expose
    private Integer totalItems;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getWishFor() {
        return wishFor;
    }

    public void setWishFor(String wishFor) {
        this.wishFor = wishFor;
    }

    public String getLittleOneId() {
        return littleOneId;
    }

    public void setLittleOneId(String littleOneId) {
        this.littleOneId = littleOneId;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
