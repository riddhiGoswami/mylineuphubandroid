package com.app.mylineup.pojo.loginData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

@SerializedName("id")
@Expose
private String id;
@SerializedName("email")
@Expose
private String email;
@SerializedName("password")
@Expose
private String password;
@SerializedName("first_name")
@Expose
private String firstName;
@SerializedName("last_name")
@Expose
private String lastName;
@SerializedName("profile_picture")
@Expose
private String profilePicture;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("birthday_date")
@Expose
private String birthdayDate;
@SerializedName("anniversary_date")
@Expose
private String anniversaryDate;
@SerializedName("zip_code")
@Expose
private String zipCode;
@SerializedName("latitude")
@Expose
private String latitude;
@SerializedName("longitude")
@Expose
private String longitude;
@SerializedName("google_social_id")
@Expose
private String googleSocialId;
@SerializedName("facebook_social_id")
@Expose
private String facebookSocialId;
@SerializedName("device_type")
@Expose
private String deviceType;
@SerializedName("device_token")
@Expose
private String deviceToken;
@SerializedName("status")
@Expose
private String status;
@SerializedName("remember_token")
@Expose
private String rememberToken;
@SerializedName("activation_token")
@Expose
private String activationToken;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("updated_at")
@Expose
private String updatedAt;
@SerializedName("api_key")
@Expose
private String apiKey;

@SerializedName("reset_password")
@Expose
private boolean resetPassword;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getProfilePicture() {
return profilePicture;
}

public void setProfilePicture(String profilePicture) {
this.profilePicture = profilePicture;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getBirthdayDate() {
return birthdayDate;
}

public void setBirthdayDate(String birthdayDate) {
this.birthdayDate = birthdayDate;
}

public String getAnniversaryDate() {
return anniversaryDate;
}

public void setAnniversaryDate(String anniversaryDate) {
this.anniversaryDate = anniversaryDate;
}

public String getZipCode() {
return zipCode;
}

public void setZipCode(String zipCode) {
this.zipCode = zipCode;
}

public String getLatitude() {
return latitude;
}

public void setLatitude(String latitude) {
this.latitude = latitude;
}

public String getLongitude() {
return longitude;
}

public void setLongitude(String longitude) {
this.longitude = longitude;
}

public String getGoogleSocialId() {
return googleSocialId;
}

public void setGoogleSocialId(String googleSocialId) {
this.googleSocialId = googleSocialId;
}

public String getFacebookSocialId() {
return facebookSocialId;
}

public void setFacebookSocialId(String facebookSocialId) {
this.facebookSocialId = facebookSocialId;
}

public String getDeviceType() {
return deviceType;
}

public void setDeviceType(String deviceType) {
this.deviceType = deviceType;
}

public String getDeviceToken() {
return deviceToken;
}

public void setDeviceToken(String deviceToken) {
this.deviceToken = deviceToken;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getRememberToken() {
return rememberToken;
}

public void setRememberToken(String rememberToken) {
this.rememberToken = rememberToken;
}

public String getActivationToken() {
return activationToken;
}

public void setActivationToken(String activationToken) {
this.activationToken = activationToken;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public String getApiKey() {
return apiKey;
}

public void setApiKey(String apiKey) {
this.apiKey = apiKey;
}

    public boolean isResetPassword() {
        return resetPassword;
    }

    public void setResetPassword(boolean resetPassword) {
        this.resetPassword = resetPassword;
    }
}
