package com.app.mylineup.pojo.wishlistItems;


import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("where_to_buy")
    @Expose
    private String whereToBuy;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("purchase_user_id")
    @Expose
    private String purchaseUserId;
    @SerializedName("purchase_date")
    @Expose
    private String purchaseDate;
    @SerializedName("purchase_notified")
    @Expose
    private String purchaseNotified;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("show_purchase_btn")
    @Expose
    private Boolean showPurchaseButton = false;
    @SerializedName("purchase_tag")
    @Expose
    private Boolean purchaseTag = false;
    @SerializedName("wishlist")
    @Expose
    private List<Wishlist> wishlist = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWhereToBuy() {
        return whereToBuy;
    }

    public void setWhereToBuy(String whereToBuy) {
        this.whereToBuy = whereToBuy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPurchaseUserId() {
        return purchaseUserId;
    }

    public void setPurchaseUserId(String purchaseUserId) {
        this.purchaseUserId = purchaseUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<Wishlist> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<Wishlist> wishlist) {
        this.wishlist = wishlist;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseNotified() {
        return purchaseNotified;
    }

    public void setPurchaseNotified(String purchaseNotified) {
        this.purchaseNotified = purchaseNotified;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Boolean getPurchaseTag() {
        return purchaseTag;
    }

    public void setPurchaseTag(Boolean purchaseTag) {
        this.purchaseTag = purchaseTag;
    }

    public Boolean showPurchaseButton() {
        return showPurchaseButton;
    }

    public void setShowPurchaseButton(Boolean showPurchaseButton) {
        this.showPurchaseButton = showPurchaseButton;
    }

    public Item() {
    }

    public Item(String id, String userId, String name, String brand, String price, String url, String size, String whereToBuy, String description, String image, String status, String purchaseUserId, String purchaseDate, String purchaseNotified, String createdAt, String updatedAt, String deletedAt, Boolean showPurchaseButton, Boolean purchaseTag, List<Wishlist> wishlist) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.url = url;
        this.size = size;
        this.whereToBuy = whereToBuy;
        this.description = description;
        this.image = image;
        this.status = status;
        this.purchaseUserId = purchaseUserId;
        this.purchaseDate = purchaseDate;
        this.purchaseNotified = purchaseNotified;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.showPurchaseButton = showPurchaseButton;
        this.purchaseTag = purchaseTag;
        this.wishlist = wishlist;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", price='" + price + '\'' +
                ", url='" + url + '\'' +
                ", size='" + size + '\'' +
                ", whereToBuy='" + whereToBuy + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", purchaseUserId='" + purchaseUserId + '\'' +
                ", purchaseDate='" + purchaseDate + '\'' +
                ", purchaseNotified='" + purchaseNotified + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", deletedAt='" + deletedAt + '\'' +
                ", showPurchaseButton=" + showPurchaseButton +
                ", purchaseTag=" + purchaseTag +
                ", wishlist=" + wishlist +
                '}';
    }
}

