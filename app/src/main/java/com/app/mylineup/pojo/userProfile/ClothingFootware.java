package com.app.mylineup.pojo.userProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClothingFootware {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("variant_item_value")
    @Expose
    private String variantItemValue;
    @SerializedName("group_id")
    @Expose
    private String groupId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVariantItemValue() {
        return variantItemValue;
    }

    public void setVariantItemValue(String variantItemValue) {
        this.variantItemValue = variantItemValue;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
