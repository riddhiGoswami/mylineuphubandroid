package com.app.mylineup.pojo.myProfile;


import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyProfile {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("top_brands")
    @Expose
    private List<String> topBrands = null;
    @SerializedName("favourite_store")
    @Expose
    private List<String> favouriteStore = null;
    @SerializedName("clothing_footware")
    @Expose
    private List<ClothingFootware> clothingFootware = null;
    @SerializedName("likes_interest")
    @Expose
    private List<LikesInterest> likesInterest = null;
    @SerializedName("little_one")
    @Expose
    private List<LittleOne> littleOne = null;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getTopBrands() {
        return topBrands;
    }

    public void setTopBrands(List<String> topBrands) {
        this.topBrands = topBrands;
    }

    public List<String> getFavouriteStore() {
        return favouriteStore;
    }

    public void setFavouriteStore(List<String> favouriteStore) {
        this.favouriteStore = favouriteStore;
    }

    public List<ClothingFootware> getClothingFootware() {
        return clothingFootware;
    }

    public void setClothingFootware(List<ClothingFootware> clothingFootware) {
        this.clothingFootware = clothingFootware;
    }

    public List<LikesInterest> getLikesInterest() {
        return likesInterest;
    }

    public void setLikesInterest(List<LikesInterest> likesInterest) {
        this.likesInterest = likesInterest;
    }

    public List<LittleOne> getLittleOne() {
        return littleOne;
    }

    public void setLittleOne(List<LittleOne> littleOne) {
        this.littleOne = littleOne;
    }

}


