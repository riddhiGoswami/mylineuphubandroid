package com.app.mylineup.pojo.myProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LikesInterest
{
    @SerializedName("field_id")
    @Expose
    private String fieldId;
    @SerializedName("field_name")
    @Expose
    private String fieldName;
    @SerializedName("field_type")
    @Expose
    private String fieldType;
    @SerializedName("data")
    @Expose
    private List<String> data = null;
    @SerializedName("user_value")
    @Expose
    private String user_value;

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public String getUser_value() {
        return user_value;
    }

    public void setUser_value(String user_value) {
        this.user_value = user_value;
    }
}
