package com.app.mylineup.pojo.userProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

@SerializedName("item_id")
@Expose
private String itemId;
@SerializedName("wishlist_id")
@Expose
private String wishlistId;
@SerializedName("image")
@Expose
private String image;

public String getItemId() {
return itemId;
}

public void setItemId(String itemId) {
this.itemId = itemId;
}

public String getWishlistId() {
return wishlistId;
}

public void setWishlistId(String wishlistId) {
this.wishlistId = wishlistId;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

}
