package com.app.mylineup.pojo.wishlistItems;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Wishlist implements Serializable {

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("wishlist_id")
    @Expose
    private String wishlistId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("event_date")
    @Expose
    private String eventDate;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(String wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public Wishlist(String itemId, String wishlistId, String name, String eventDate) {
        this.itemId = itemId;
        this.wishlistId = wishlistId;
        this.name = name;
        this.eventDate = eventDate;
    }

    public Wishlist() {
    }

    @Override
    public String toString() {
        return "Wishlist{" +
                "itemId='" + itemId + '\'' +
                ", wishlistId='" + wishlistId + '\'' +
                ", name='" + name + '\'' +
                ", eventDate='" + eventDate + '\'' +
                '}';
    }
}
