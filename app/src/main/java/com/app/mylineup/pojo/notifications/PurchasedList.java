package com.app.mylineup.pojo.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurchasedList {

@SerializedName("profile_picture")
@Expose
private String profilePicture;
@SerializedName("purchase_user_id")
@Expose
private String purchaseUserId;
@SerializedName("message")
@Expose
private String message;

    @SerializedName("item_id")
    @Expose
    private String itemId;

public String getProfilePicture() {
return profilePicture;
}

public void setProfilePicture(String profilePicture) {
this.profilePicture = profilePicture;
}

public String getPurchaseUserId() {
return purchaseUserId;
}

public void setPurchaseUserId(String purchaseUserId) {
this.purchaseUserId = purchaseUserId;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}


    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
