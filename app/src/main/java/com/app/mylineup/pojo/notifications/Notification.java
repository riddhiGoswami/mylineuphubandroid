package com.app.mylineup.pojo.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("friend_id")
    @Expose
    private String friendId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("event_expired")
    @Expose
    private boolean eventExpired = false;
    @SerializedName("username")
    @Expose
    private String username;

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isEventExpired() {
        return eventExpired;
    }

    public void setEventExpired(boolean eventExpired) {
        this.eventExpired = eventExpired;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Notification(String profilePicture, String message, String createdAt, String friendId, String type, boolean eventExpired, String username) {
        this.profilePicture = profilePicture;
        this.message = message;
        this.createdAt = createdAt;
        this.friendId = friendId;
        this.type = type;
        this.eventExpired = eventExpired;
        this.username = username;
    }

    public Notification() {
    }
}
