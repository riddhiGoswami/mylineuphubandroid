package com.app.mylineup.pojo.wishlists;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("wishlist_id")
    @Expose
    private String wishlistId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("purchase_date")
    @Expose
    private String purchaseDate;
    @SerializedName("purchase_tag")
    @Expose
    private Boolean purchaseTag = false;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(String wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Boolean getPurchaseTag() {
        return purchaseTag;
    }

    public void setPurchaseTag(Boolean purchaseTag) {
        this.purchaseTag = purchaseTag;
    }

    public Item(String itemId, String wishlistId, String image, String status, String createdAt, String purchaseDate, Boolean purchaseTag) {
        this.itemId = itemId;
        this.wishlistId = wishlistId;
        this.image = image;
        this.status = status;
        this.createdAt = createdAt;
        this.purchaseDate = purchaseDate;
        this.purchaseTag = purchaseTag;
    }

    public Item() {
    }
}


