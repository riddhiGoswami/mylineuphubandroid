package com.app.mylineup.pojo.wishlists;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Wishlist {

    private int type = 1;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("wish_for")
    @Expose
    private String wishFor;
    @SerializedName("little_one_id")
    @Expose
    private String littleOneId;
    @SerializedName("total_items")
    @Expose
    private Double totalItems;
    @SerializedName("little_one_name")
    @Expose
    private String littleOneName;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("event_expired")
    @Expose
    private Boolean eventExpired;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Double totalItems) {
        this.totalItems = totalItems;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public Boolean getEventExpired() {
        return eventExpired;
    }

    public void setEventExpired(Boolean eventExpired) {
        this.eventExpired = eventExpired;
    }

    public String getWishFor() {
        return wishFor;
    }

    public void setWishFor(String wishFor) {
        this.wishFor = wishFor;
    }

    public String getLittleOneId() {
        return littleOneId;
    }

    public void setLittleOneId(String littleOneId) {
        this.littleOneId = littleOneId;
    }

    public String getLittleOneName() {
        return littleOneName;
    }

    public void setLittleOneName(String littleOneName) {
        this.littleOneName = littleOneName;
    }

    public Wishlist(int type, String id, String userId, String name, String eventDate, String wishFor, String littleOneId, Double totalItems, String littleOneName, List<Item> items, Boolean eventExpired) {
        this.type = type;
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.eventDate = eventDate;
        this.wishFor = wishFor;
        this.littleOneId = littleOneId;
        this.totalItems = totalItems;
        this.littleOneName = littleOneName;
        this.items = items;
        this.eventExpired = eventExpired;
    }

    public Wishlist() {
    }

    @Override
    public String toString() {
        return "Wishlist{" +
                "type=" + type +
                ", id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", eventDate='" + eventDate + '\'' +
                ", wishFor='" + wishFor + '\'' +
                ", littleOneId='" + littleOneId + '\'' +
                ", totalItems=" + totalItems +
                ", littleOneName='" + littleOneName + '\'' +
                ", items=" + items +
                ", eventExpired=" + eventExpired +
                '}';
    }
}
