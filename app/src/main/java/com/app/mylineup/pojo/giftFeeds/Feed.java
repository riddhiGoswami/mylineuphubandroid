package com.app.mylineup.pojo.giftFeeds;


import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feed {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("wishlist_id")
    @Expose
    private String wishListId;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("wishlist_name")
    @Expose
    private String wishListName;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("posted_at")
    @Expose
    private String postedAt;
    @SerializedName("feed_text")
    @Expose
    private String feedText;
    @SerializedName("little_one_id")
    @Expose
    private String littleOneId;
    @SerializedName("little_one_name")
    @Expose
    private String littleOneName;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(String postedAt) {
        this.postedAt = postedAt;
    }

    public String getFeedText() {
        return feedText;
    }

    public void setFeedText(String feedText) {
        this.feedText = feedText;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWishListId() {
        return wishListId;
    }

    public void setWishListId(String wishListId) {
        this.wishListId = wishListId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getWishListName() {
        return wishListName;
    }

    public void setWishListName(String wishListName) {
        this.wishListName = wishListName;
    }

    public String getLittleOneId() {
        return littleOneId;
    }

    public void setLittleOneId(String littleOneId) {
        this.littleOneId = littleOneId;
    }

    public String getLittleOneName() {
        return littleOneName;
    }

    public void setLittleOneName(String littleOneName) {
        this.littleOneName = littleOneName;
    }

    public Feed(String userId, String wishListId, String eventDate, String name, String wishListName, String profilePicture, String postedAt, String feedText, List<Item> items) {
        this.userId = userId;
        this.wishListId = wishListId;
        this.eventDate = eventDate;
        this.name = name;
        this.wishListName = wishListName;
        this.profilePicture = profilePicture;
        this.postedAt = postedAt;
        this.feedText = feedText;
        this.items = items;
    }

    public Feed() {
    }
}

