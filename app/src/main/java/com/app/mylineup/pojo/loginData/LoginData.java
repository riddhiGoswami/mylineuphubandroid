package com.app.mylineup.pojo.loginData;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginData {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("notification_setting")
    @Expose
    private NotificationSetting notificationSetting;
    @SerializedName("wishlist")
    @Expose
    private List<Wishlist> wishlist = null;
    @SerializedName("activation_pending")
    @Expose
    private Boolean isActivationPending = false;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public NotificationSetting getNotificationSetting() {
        return notificationSetting;
    }

    public void setNotificationSetting(NotificationSetting notificationSetting) {
        this.notificationSetting = notificationSetting;
    }

    public List<Wishlist> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<Wishlist> wishlist) {
        this.wishlist = wishlist;
    }

    public Boolean isActivationPending() {
        return isActivationPending;
    }

    public void setActivationPending(Boolean activationPending) {
        isActivationPending = activationPending;
    }

    public LoginData(User user, Boolean status, NotificationSetting notificationSetting, List<Wishlist> wishlist, Boolean isActivationPending) {
        this.user = user;
        this.status = status;
        this.notificationSetting = notificationSetting;
        this.wishlist = wishlist;
        this.isActivationPending = isActivationPending;
    }

    public LoginData() {
    }
}


