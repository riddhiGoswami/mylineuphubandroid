package com.app.mylineup.pojo.faq;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FaqData {

@SerializedName("faq")
@Expose
private List<Faq> faq = null;
@SerializedName("status")
@Expose
private Boolean status;

public List<Faq> getFaq() {
return faq;
}

public void setFaq(List<Faq> faq) {
this.faq = faq;
}

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

}
