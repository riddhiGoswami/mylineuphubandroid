package com.app.mylineup.pojo.myProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("variant_item_id")
    @Expose
    private String variantItemId;
    @SerializedName("value")
    @Expose
    private String value;

    @SerializedName("selected")
    @Expose
    private boolean selected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVariantItemId() {
        return variantItemId;
    }

    public void setVariantItemId(String variantItemId) {
        this.variantItemId = variantItemId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
