package com.app.mylineup.pojo;

public class VideoBeen extends CommonPojo {

    public boolean isWishlistSeen, isConnectionSeen, isLittleOneSeen, isProfileSeen, isReviewSeen;

    public VideoBeen(boolean isWishlistSeen, boolean isConnectionSeen, boolean isLittleOneSeen, boolean isProfileSeen, boolean isReviewSeen) {
        this.isWishlistSeen = isWishlistSeen;
        this.isConnectionSeen = isConnectionSeen;
        this.isLittleOneSeen = isLittleOneSeen;
        this.isProfileSeen = isProfileSeen;
        this.isReviewSeen = isReviewSeen;
    }

    public boolean isWishlistSeen() {
        return isWishlistSeen;
    }

    public void setWishlistSeen(boolean wishlistSeen) {
        isWishlistSeen = wishlistSeen;
    }

    public boolean isConnectionSeen() {
        return isConnectionSeen;
    }

    public void setConnectionSeen(boolean connectionSeen) {
        isConnectionSeen = connectionSeen;
    }

    public boolean isLittleOneSeen() {
        return isLittleOneSeen;
    }

    public void setLittleOneSeen(boolean littleOneSeen) {
        isLittleOneSeen = littleOneSeen;
    }

    public boolean isProfileSeen() {
        return isProfileSeen;
    }

    public void setProfileSeen(boolean profileSeen) {
        isProfileSeen = profileSeen;
    }

    public boolean isReviewSeen() {
        return isReviewSeen;
    }

    public void setReviewSeen(boolean reviewSeen) {
        isReviewSeen = reviewSeen;
    }
}
