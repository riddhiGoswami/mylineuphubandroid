package com.app.mylineup.pojo.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Notifications {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("friend_requests")
@Expose
private List<FriendRequest> friendRequests = null;
@SerializedName("friend_requests_see_all")
@Expose
private Boolean friendRequestsSeeAll;
@SerializedName("wishlist")
@Expose
private List<Wishlist> wishlist = null;
@SerializedName("wishlist_see_all")
@Expose
private Boolean wishlistSeeAll;
@SerializedName("notifications")
@Expose
private List<Notification> notifications = null;
@SerializedName("notifications_see_all")
@Expose
private Boolean notificationsSeeAll;
@SerializedName("purchased_list")
@Expose
private List<PurchasedList> purchasedList = null;
@SerializedName("purchased_list_see_all")
@Expose
private Boolean purchasedListSeeAll;
@SerializedName("message")
@Expose
private String message;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<FriendRequest> getFriendRequests() {
return friendRequests;
}

public void setFriendRequests(List<FriendRequest> friendRequests) {
this.friendRequests = friendRequests;
}

public Boolean getFriendRequestsSeeAll() {
return friendRequestsSeeAll;
}

public void setFriendRequestsSeeAll(Boolean friendRequestsSeeAll) {
this.friendRequestsSeeAll = friendRequestsSeeAll;
}

public List<Wishlist> getWishlist() {
return wishlist;
}

public void setWishlist(List<Wishlist> wishlist) {
this.wishlist = wishlist;
}

public Boolean getWishlistSeeAll() {
return wishlistSeeAll;
}

public void setWishlistSeeAll(Boolean wishlistSeeAll) {
this.wishlistSeeAll = wishlistSeeAll;
}

public List<Notification> getNotifications() {
return notifications;
}

public void setNotifications(List<Notification> notifications) {
this.notifications = notifications;
}

public Boolean getNotificationsSeeAll() {
return notificationsSeeAll;
}

public void setNotificationsSeeAll(Boolean notificationsSeeAll) {
this.notificationsSeeAll = notificationsSeeAll;
}

public List<PurchasedList> getPurchasedList() {
return purchasedList;
}

public void setPurchasedList(List<PurchasedList> purchasedList) {
this.purchasedList = purchasedList;
}

public Boolean getPurchasedListSeeAll() {
return purchasedListSeeAll;
}

public void setPurchasedListSeeAll(Boolean purchasedListSeeAll) {
this.purchasedListSeeAll = purchasedListSeeAll;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}
