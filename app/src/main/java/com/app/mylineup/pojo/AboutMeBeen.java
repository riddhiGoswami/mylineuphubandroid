package com.app.mylineup.pojo;

import java.io.Serializable;

public class AboutMeBeen extends CommonPojo {

    public String id, name, size;
    public int image;

    public AboutMeBeen(String id, String name, String size, int image)
    {
        this.id = id;
        this.name = name;
        this.size = size;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return name;
    }
}
