package com.app.mylineup.pojo.userProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckConnected {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("profile_user_id")
    @Expose
    private String profile_user_id;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getProfile_user_id() {
        return profile_user_id;
    }

    public void setProfile_user_id(String profile_user_id) {
        this.profile_user_id = profile_user_id;
    }
}
