package com.app.mylineup.pojo.userProfile;


import java.util.List;

import com.app.mylineup.pojo.wishlists.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Activity {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("wishlist_id")
    @Expose
    private String wishlistId;
    @SerializedName("wishlist_name")
    @Expose
    private String wishlistName;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("posted_at")
    @Expose
    private String postedAt;
    @SerializedName("feed_text")
    @Expose
    private String feedText;
    @SerializedName("items")
    @Expose
    private List<com.app.mylineup.pojo.wishlists.Item> items = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(String postedAt) {
        this.postedAt = postedAt;
    }

    public String getFeedText() {
        return feedText;
    }

    public void setFeedText(String feedText) {
        this.feedText = feedText;
    }

    public List<com.app.mylineup.pojo.wishlists.Item> getItems() {
        return items;
    }

    public void setItems(List<com.app.mylineup.pojo.wishlists.Item> items) {
        this.items = items;
    }

    public String getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(String wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getWishlistName() {
        return wishlistName;
    }

    public void setWishlistName(String wishlistName) {
        this.wishlistName = wishlistName;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public Activity(String userId, String wishlistId, String wishlistName, String eventDate, String name, String profilePicture, String postedAt, String feedText, List<Item> items) {
        this.userId = userId;
        this.wishlistId = wishlistId;
        this.wishlistName = wishlistName;
        this.eventDate = eventDate;
        this.name = name;
        this.profilePicture = profilePicture;
        this.postedAt = postedAt;
        this.feedText = feedText;
        this.items = items;
    }

    public Activity() {
    }
}


