package com.app.mylineup.pojo;

import com.app.mylineup.interfaces.Contact;

public class ContactChild implements Contact {

    String Id,displayName,firstName,lastName,mobileNo,email,image,type;

    public ContactChild(String Id,String displayName, String firstName, String lastName, String mobileNo, String email, String image,String type) {
        this.Id = Id;
        this.displayName = displayName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNo = mobileNo;
        this.email = email;
        this.image = image;
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean isSection() {
        return true;
    }

    @Override
    public String getHeader() {
        return null;
    }

    @Override
    public void setHeader(String header) {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
