package com.app.mylineup.pojo.contactList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactList {

@SerializedName("contacts")
@Expose
private List<Contact> contacts = null;
@SerializedName("status")
@Expose
private Boolean status;

public List<Contact> getContacts() {
return contacts;
}

public void setContacts(List<Contact> contacts) {
this.contacts = contacts;
}

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

}
