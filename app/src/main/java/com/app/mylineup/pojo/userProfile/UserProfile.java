package com.app.mylineup.pojo.userProfile;

import com.app.mylineup.pojo.wishlists.Wishlist;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfile {

    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("wishlist")
    @Expose
    private List<Wishlist> wishlist = null;
    @SerializedName("activity")
    @Expose
    private List<Activity> activity = null;
    @SerializedName("clothing_footware")
    @Expose
    private List<ClothingFootware> clothingFootware = null;
    @SerializedName("favourite_store")
    @Expose
    private List<String> favouriteStore = null;
    @SerializedName("top_brands")
    @Expose
    private List<String> topBrands = null;
    @SerializedName("likes_interest")
    @Expose
    private List<LikesInterest> likesInterest = null;
    @SerializedName("little_one")
    @Expose
    private List<MyLittleOne> myLittleOnes = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Wishlist> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<Wishlist> wishlist) {
        this.wishlist = wishlist;
    }

    public List<Activity> getActivity() {
        return activity;
    }

    public void setActivity(List<Activity> activity) {
        this.activity = activity;
    }

    public List<ClothingFootware> getClothingFootware() {
        return clothingFootware;
    }

    public void setClothingFootware(List<ClothingFootware> clothingFootware) {
        this.clothingFootware = clothingFootware;
    }

    public List<String> getFavouriteStore() {
        return favouriteStore;
    }

    public void setFavouriteStore(List<String> favouriteStore) {
        this.favouriteStore = favouriteStore;
    }

    public List<String> getTopBrands() {
        return topBrands;
    }

    public void setTopBrands(List<String> topBrands) {
        this.topBrands = topBrands;
    }

    public List<LikesInterest> getLikesInterest() {
        return likesInterest;
    }

    public void setLikesInterest(List<LikesInterest> likesInterest) {
        this.likesInterest = likesInterest;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<MyLittleOne> getMyLittleOnes() {
        return myLittleOnes;
    }

    public void setMyLittleOnes(List<MyLittleOne> myLittleOnes) {
        this.myLittleOnes = myLittleOnes;
    }
}
