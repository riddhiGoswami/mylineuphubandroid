package com.app.mylineup.pojo.giftFeeds;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GiftFeeds {

@SerializedName("status")
@Expose
private Boolean status;
@SerializedName("upcoming_occasions")
@Expose
private List<UpcomingOccasion> upcomingOccasions = null;
@SerializedName("feed")
@Expose
private List<Feed> feed = null;

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

public List<UpcomingOccasion> getUpcomingOccasions() {
return upcomingOccasions;
}

public void setUpcomingOccasions(List<UpcomingOccasion> upcomingOccasions) {
this.upcomingOccasions = upcomingOccasions;
}

public List<Feed> getFeed() {
return feed;
}

public void setFeed(List<Feed> feed) {
this.feed = feed;
}

}
