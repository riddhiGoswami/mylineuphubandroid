package com.app.mylineup.pojo.wishlistItems;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Wishlist_ implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("wish_for")
    @Expose
    private String wishFor;
    @SerializedName("little_one_id")
    @Expose
    private String littleOneId;
    @SerializedName("event_date")
    @Expose
    private String eventDate;
    @SerializedName("event_expired")
    @Expose
    private Boolean eventExpired;
    @SerializedName("item_id_last")
    @Expose
    private String itemIdLast;
    @SerializedName("total_items")
    @Expose
    private Double totalItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWishFor() {
        return wishFor;
    }

    public void setWishFor(String wishFor) {
        this.wishFor = wishFor;
    }

    public String getLittleOneId() {
        return littleOneId;
    }

    public void setLittleOneId(String littleOneId) {
        this.littleOneId = littleOneId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getTotalItems() {
        if (totalItems != null)
            return String.valueOf(totalItems.intValue());
        else return "0";
    }

    public void setTotalItems(Double totalItems) {
        this.totalItems = totalItems;
    }

    public Boolean getEventExpired() {
        return eventExpired;
    }

    public void setEventExpired(Boolean eventExpired) {
        this.eventExpired = eventExpired;
    }

    public String getItemIdLast() {
        return itemIdLast;
    }

    public void setItemIdLast(String itemIdLast) {
        this.itemIdLast = itemIdLast;
    }

    public Wishlist_(String id, String userId, String name, String wishFor, String littleOneId, String eventDate, Boolean eventExpired, String itemIdLast, Double totalItems) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.wishFor = wishFor;
        this.littleOneId = littleOneId;
        this.eventDate = eventDate;
        this.eventExpired = eventExpired;
        this.itemIdLast = itemIdLast;
        this.totalItems = totalItems;
    }

    public Wishlist_() {
    }
}
