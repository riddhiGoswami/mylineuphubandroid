package com.app.mylineup.pojo.notifications;



import android.content.Intent;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FriendRequest {

@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("username")
@Expose
private String username;
@SerializedName("profile_picture")
@Expose
private String profilePicture;
@SerializedName("matual_connection")
@Expose
private Double matualConnection;
@SerializedName("friends")
@Expose
private List<String> friends = null;
@SerializedName("requested_at")
@Expose
private String requestedAt;

private String actionType="0"; //0 - show accept and reject,1-accepted,2-rejected

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getUsername() {
return username;
}

public void setUsername(String username) {
this.username = username;
}

public String getProfilePicture() {
return profilePicture;
}

public void setProfilePicture(String profilePicture) {
this.profilePicture = profilePicture;
}

public String getMatualConnection() {
    if(matualConnection != null)
    return String.valueOf(matualConnection.intValue());
    else return "0";
}

public void setMatualConnection(Double matualConnection) {
this.matualConnection = matualConnection;
}

public List<String> getFriends() {
return friends;
}

public void setFriends(List<String> friends) {
this.friends = friends;
}

public String getRequestedAt() {
return requestedAt;
}

public void setRequestedAt(String requestedAt) {
this.requestedAt = requestedAt;
}


    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}

