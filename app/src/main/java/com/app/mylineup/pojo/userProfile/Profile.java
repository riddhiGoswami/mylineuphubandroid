package com.app.mylineup.pojo.userProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("birthday_date")
    @Expose
    private String birthdayDate;
    @SerializedName("anniversary_date")
    @Expose
    private String anniversaryDate;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("last_active_at")
    @Expose
    private String lastActiveAt;
    @SerializedName("social_type")
    @Expose
    private String socialType;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("activation_token")
    @Expose
    private String activationToken;
    @SerializedName("is_synchronize")
    @Expose
    private String isSynchronize;
    @SerializedName("zoho_id")
    @Expose
    private String zohoId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("anniversary_img")
    @Expose
    private String anniversaryImg;
    @SerializedName("birthday_img")
    @Expose
    private String birthdayImg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(String birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public String getAnniversaryDate() {
        return anniversaryDate;
    }

    public void setAnniversaryDate(String anniversaryDate) {
        this.anniversaryDate = anniversaryDate;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastActiveAt() {
        return lastActiveAt;
    }

    public void setLastActiveAt(String lastActiveAt) {
        this.lastActiveAt = lastActiveAt;
    }

    public String getSocialType() {
        return socialType;
    }

    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    public String getIsSynchronize() {
        return isSynchronize;
    }

    public void setIsSynchronize(String isSynchronize) {
        this.isSynchronize = isSynchronize;
    }

    public String getZohoId() {
        return zohoId;
    }

    public void setZohoId(String zohoId) {
        this.zohoId = zohoId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnniversaryImg() {
        return anniversaryImg;
    }

    public void setAnniversaryImg(String anniversaryImg) {
        this.anniversaryImg = anniversaryImg;
    }

    public String getBirthdayImg() {
        return birthdayImg;
    }

    public void setBirthdayImg(String birthdayImg) {
        this.birthdayImg = birthdayImg;
    }

    public Profile(String id, String email, String password, String firstName, String lastName, String profilePicture, String phone, String birthdayDate, String anniversaryDate, String zipCode, String latitude, String longitude, String deviceType, String deviceToken, String status, String lastActiveAt, String socialType, String socialId, String rememberToken, String activationToken, String isSynchronize, String zohoId, String createdAt, String updatedAt, String name, String anniversaryImg, String birthdayImg) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilePicture = profilePicture;
        this.phone = phone;
        this.birthdayDate = birthdayDate;
        this.anniversaryDate = anniversaryDate;
        this.zipCode = zipCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.deviceType = deviceType;
        this.deviceToken = deviceToken;
        this.status = status;
        this.lastActiveAt = lastActiveAt;
        this.socialType = socialType;
        this.socialId = socialId;
        this.rememberToken = rememberToken;
        this.activationToken = activationToken;
        this.isSynchronize = isSynchronize;
        this.zohoId = zohoId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.name = name;
        this.anniversaryImg = anniversaryImg;
        this.birthdayImg = birthdayImg;
    }

    public Profile() {
    }
}
