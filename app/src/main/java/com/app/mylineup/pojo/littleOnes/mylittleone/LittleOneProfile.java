
package com.app.mylineup.pojo.littleOnes.mylittleone;

import java.util.List;

import com.app.mylineup.pojo.userProfile.MyLittleOne;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LittleOneProfile {

    @SerializedName("profile")
    @Expose
    private Profile profile;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("clothing_footware")
    @Expose
    private List<ClothingFootware> clothingFootware = null;
    @SerializedName("favourite_store")
    @Expose
    private List<String> favouriteStore = null;
    @SerializedName("top_brands")
    @Expose
    private List<String> topBrands = null;
    @SerializedName("likes_interest")
    @Expose
    private List<LikesInterest> likesInterest = null;

    @SerializedName("wishlist")
    @Expose
    private List<LittleOneWishlist> littleOneWishlist = null;
    @SerializedName("activity")
    @Expose
    private List<Activity> activity = null;

    @SerializedName("little_one")
    @Expose
    private List<MyLittleOne> littleoneList = null;



    public List<LittleOneWishlist> getLittleOneWishlist() {
        return littleOneWishlist;
    }

    public void setLittleOneWishlist(List<LittleOneWishlist> littleOneWishlist) {
        this.littleOneWishlist = littleOneWishlist;
    }

    public List<Activity> getActivity() {
        return activity;
    }

    public void setActivity(List<Activity> activity) {
        this.activity = activity;
    }
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<ClothingFootware> getClothingFootware() {
        return clothingFootware;
    }

    public void setClothingFootware(List<ClothingFootware> clothingFootware) {
        this.clothingFootware = clothingFootware;
    }
    public List<String> getFavouriteStore() {
        return favouriteStore;
    }

    public void setFavouriteStore(List<String> favouriteStore) {
        this.favouriteStore = favouriteStore;
    }

    public List<String> getTopBrands() {
        return topBrands;
    }

    public void setTopBrands(List<String> topBrands) {
        this.topBrands = topBrands;
    }

    public List<LikesInterest> getLikesInterest() {
        return likesInterest;
    }

    public void setLikesInterest(List<LikesInterest> likesInterest) {
        this.likesInterest = likesInterest;
    }

    public List<MyLittleOne> getLittleoneList() {
        return littleoneList;
    }

    public void setLittleoneList(List<MyLittleOne> littleoneList) {
        this.littleoneList = littleoneList;
    }
}
